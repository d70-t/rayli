#include "KDA.hpp"
#include <fstream>
#include <io/kdtree.hpp>
#include <iostream>

std::array<int, 3> ofs[] = {
  {-1, 0, 0},   {1, 0, 0},   {0, -1, 0},  {0, 1, 0},  {0, 0, -1},  {0, 0, 1},
  {-2, 0, 0},   {2, 0, 0},   {0, -2, 0},  {0, 2, 0},  {0, 0, -2},  {0, 0, 2},
  {-1, -1, 0},  {-1, 1, 0},  {1, -1, 0},  {1, 1, 0},  {-1, 0, -1}, {-1, 0, 1},
  {1, 0, -1},   {1, 0, 1},   {0, -1, -1}, {0, -1, 1}, {0, 1, -1},  {0, 1, 1},
  {-1, -1, -1}, {-1, -1, 1}, {-1, 1, -1}, {-1, 1, 1}, {1, -1, -1}, {1, -1, 1},
  {1, 1, -1},   {1, 1, 1}};

template <typename Value>
void deactivate_internal(KDA<Value>& a) {
    auto s = a.shape();
    for (auto[i, j, k] : a.active_indices()) {
        bool active = false;
        auto v = a.at({i, j, k});
        for (auto[oi, oj, ok] : ofs) {
            if ((i == 0 && oi < 0) || (j == 0 && oj < 0) ||
                (k == 0 && ok < 0)) {
                continue;
            }
            if (i + oi >= s[0] || j + oj >= s[1] || k + ok >= s[2]) {
                continue;
            }
            if (a.at({i + oi, j + oj, k + ok}) != v) {
                active = true;
                a.set_active({i + oi, j + oj, k + ok}, true);
            }
        }
        if (!active) {
            a.set_active({i, j, k}, false);
        }
    }
}

void fill_kda(KDA<size_t>& a) {
    auto shape = a.shape();
    // for (auto[i, j, k] : a.active_indices()) {
    for (size_t i = 0; i < shape[0]; ++i) {
        for (size_t j = 0; j < shape[1]; ++j) {
            for (size_t k = 0; k < shape[2]; ++k) {
                double x = -1. + 2 * (i + .5) / shape[0];
                double y = -1. + 2 * (j + .5) / shape[1];
                double z = -1. + 2 * (k + .5) / shape[2];
                double r = x * x + y * y + z * z;
                size_t val = 0;
                if (r < 1) {
                    val = 1;
                }
                if (a.at({i, j, k}) != val) {
                    a[{i, j, k}] = val;
                }
                /*
                if (a.at({i, j, k}) != val) {
                    std::cout << "readback error: " << i << " " << j << " " << k
                << " "
                              << r << " " << a.at({i, j, k}) << "\n";
                }
                */
            }
        }
    }
}

int main() {
    auto a = KDA<size_t>{0, {2, 2, 2}};
    fill_kda(a);
    //std::cout << "active: " << a.active_indices().size() << "\n";
    // deactivate_internal(a);
    //std::cout << "active: " << a.active_indices().size() << "\n";
    // for(int d: {3/*, 4, 5, 6, 7*/}) {
    for (int d = 3; d <= 11; ++d) {
        std::cout << "depth: " << d << "\n";
        a.refine({d, d, d});
        //std::cout << "active: " << a.active_indices().size() << "\n";
        fill_kda(a);
        //deactivate_internal(a);
        a.compact();
        //std::cout << "active: " << a.active_indices().size() << "\n";
    }
    //a.compact();
    //std::cout << "active: " << a.active_indices().size() << "\n";

    auto kdt = a.to_kdt({{-1, -1, -1}, {1, 1, 1}});

    {
        std::ofstream outfile("/home/t/Tobias.Koelling/tmp/ball11.msgpack",
                              std::ios_base::out | std::ios_base::binary);
        msgpack::pack(outfile, kdt);
    }
}
