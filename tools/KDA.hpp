#pragma once

#include <array>
#include <variant>
#include <vector>

#include <vgrid/KDTree.hpp>

/*
template <class... Ts>
struct overloaded : Ts... {
    using Ts::operator()...;
};  // (1)
template <class... Ts>
overloaded(Ts...)->overloaded<Ts...>;  // (2)
*/

template <typename Value>
struct Leaf {
    Value value;
    bool active;
};

struct Split {
    size_t a;
    size_t b;
    int axis;
};

using Index = std::array<size_t, 3>;

template <typename Value>
using Node = std::variant<Leaf<Value>, Split>;

template <typename Value>
class KDA {
   public:
    KDA(Value background, std::array<int, 3> logshape) : logshape(logshape) {
        nodes.push_back(Leaf<Value>{background, true});
    }

    const Value& at(const Index& index) const {
        return get_leaf(index, logshape).value;
    }

    Value& operator[](const Index& index) {
        auto& leaf = get_set_leaf(index, logshape);
        leaf.active = true;
        return leaf.value;
    }

    std::array<size_t, 3> shape() const {
        return {size_t(1) << logshape[0], size_t(1) << logshape[1],
                size_t(1) << logshape[2]};
    }

    void set_active(const Index& index, bool active) {
        get_set_leaf(index, logshape).active = active;
    }

    std::vector<std::array<size_t, 3>> active_indices() const {
        struct StackT {
            size_t node;
            std::array<size_t, 3> index;
            std::array<int, 3> ls;
        };
        std::vector<std::array<size_t, 3>> indices;
        std::vector<StackT> stack;
        stack.push_back({0, {0, 0, 0}, logshape});
        while (!stack.empty()) {
            auto cur = stack.back();
            stack.pop_back();
            std::visit(
              overloaded{[&](const Leaf<Value>& leaf) {
                             if (!leaf.active) return;
                             auto start = cur.index;
                             auto end = cur.index;
                             for (int i = 0; i < 3; ++i) {
                                 start[i] <<= cur.ls[i];
                                 ++end[i];
                                 end[i] <<= cur.ls[i];
                             }
                             for (size_t i = start[0]; i < end[0]; ++i) {
                                 for (size_t j = start[1]; j < end[1]; ++j) {
                                     for (size_t k = start[2]; k < end[2];
                                          ++k) {
                                         indices.push_back({i, j, k});
                                     }
                                 }
                             }
                         },
                         [&](const Split& split) {
                             auto index = cur.index;
                             auto ls = cur.ls;
                             --ls[split.axis];
                             index[split.axis] <<= 1;
                             stack.push_back({split.a, index, ls});
                             ++index[split.axis];
                             stack.push_back({split.b, index, ls});
                         }},
              nodes[cur.node]);
        }
        return indices;
    }

    void refine(std::array<int, 3> new_logshape) { logshape = new_logshape; }

    void compact() { compact(0); }

    rayli::vgrid::KdTree<Value> to_kdt(const Bounds3d& bounds) const {
        struct InsertT {
            size_t node;
            Bounds3d bounds;
        };
        rayli::vgrid::KdTree<Value> kdt;
        kdt.bounds = bounds;
        std::vector<InsertT> insert_stack;
        std::vector<size_t> ref_stack;
        insert_stack.push_back({0, bounds});
        while (!insert_stack.empty()) {
            auto cur = insert_stack.back();
            insert_stack.pop_back();
            std::visit(
              overloaded{[&](const Leaf<Value>& leaf) {
                             kdt.add_leaf(leaf.value);
                             if (!ref_stack.empty()) {
                                 kdt.nodes[ref_stack.back()].set_second_child(
                                   kdt.nodes.size());
                                 ref_stack.pop_back();
                             }
                         },
                         [&](const Split& split) {
                             auto center = (cur.bounds.lower[split.axis] +
                                            cur.bounds.upper[split.axis]) /
                                           2.;
                             ref_stack.push_back(
                               kdt.add_split(split.axis, center));
                             auto ba = cur.bounds;
                             ba.upper[split.axis] = center;
                             auto bb = cur.bounds;
                             bb.lower[split.axis] = center;
                             insert_stack.push_back({split.b, bb});
                             insert_stack.push_back({split.a, ba});
                         }},
              nodes[cur.node]);
        }
        return kdt;
    }

   private:
    std::vector<Node<Value>> nodes;
    std::array<int, 3> logshape;

    const Leaf<Value>& get_leaf(Index index, std::array<int, 3> ls,
                                ssize_t node = 0) const {
        // std::cout << "get\n";
        return std::visit(
          overloaded{
            [](const Leaf<Value>& leaf) -> const Leaf<Value>& { return leaf; },
            [&](const Split& split) -> const Leaf<Value>& {
                ls[split.axis] -= 1;
                bool is_upper = index[split.axis] & 1 << ls[split.axis];
                if (is_upper) {
                    return get_leaf(index, ls, split.b);
                } else {
                    return get_leaf(index, ls, split.a);
                }
            }},
          nodes[node]);
    }

    int next_axis(int axis, std::array<int, 3> ls) {
        for (int ofs = 1; ofs <= 3; ++ofs) {
            int a = (axis + ofs) % 3;
            if (ls[a] != 0) {
                return a;
            }
        }
        return 0;
    }

    Leaf<Value>& get_set_leaf(Index index, std::array<int, 3> ls,
                              size_t node = 0, int axis = 0) {
        // std::cout << "get_set\n";
        if (ls[0] == 0 && ls[1] == 0 && ls[2] == 0) {
            return std::get<Leaf<Value>>(nodes[node]);
        }
        return std::visit(
          overloaded{
            [&](Leaf<Value> leaf) -> Leaf<Value>& {
                nodes.push_back(leaf);
                nodes.push_back(leaf);
                nodes[node] = Split{nodes.size() - 2, nodes.size() - 1, axis};
                return get_set_leaf(index, ls, node, axis);
            },
            [&](Split& split) -> Leaf<Value>& {
                ls[axis] -= 1;
                bool is_upper = index[split.axis] & 1 << ls[split.axis];
                if (is_upper) {
                    return get_set_leaf(index, ls, split.b,
                                        next_axis(axis, ls));
                } else {
                    return get_set_leaf(index, ls, split.a,
                                        next_axis(axis, ls));
                }
            }},
          nodes[node]);
    }

    void compact(size_t node) {
        std::visit(
          overloaded{
            [&](const Leaf<Value>& leaf) {},
            [&](const Split& split) {
                compact(split.a);
                compact(split.b);
                if (std::holds_alternative<Leaf<Value>>(nodes[split.a]) &&
                    std::holds_alternative<Leaf<Value>>(nodes[split.b])) {
                    auto a = std::get<Leaf<Value>>(nodes[split.a]);
                    auto b = std::get<Leaf<Value>>(nodes[split.b]);
                    if (a.value == b.value) {
                        nodes[node] =
                          Leaf<Value>{a.value, a.active || b.active};
                    }
                }
            }},
          nodes[node]);
    }
};
