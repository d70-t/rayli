#include <Eigen.hpp>
#include <GeometryUtils.hpp>
#include <benchmark/benchmark.h>
#include <spdlog/spdlog.h>
#include <vgrid/ConvexPolytopes.hpp>
#include <vgrid/EquidistantCartesian.hpp>

#include <random>

static void BM_rot_from_mu_via_acos(benchmark::State& state) {
    Eigen::Vector3d a = {0.7, 3.4, -2.1};
    a.normalize();
    Eigen::Vector3d b = {0.3, 1.7, 2.2};
    b.normalize();

    volatile double mu = 0.4;
    for (auto _ : state) {
        auto r = Eigen::AngleAxis(std::acos(mu), a) * b;
        benchmark::DoNotOptimize(r);
    }
}
BENCHMARK(BM_rot_from_mu_via_acos);

void BM_rot_from_mu(benchmark::State& state) {
    Eigen::Vector3d a = {0.7, 3.4, -2.1};
    a.normalize();
    Eigen::Vector3d b = {0.3, 1.7, 2.2};
    b.normalize();

    volatile double mu = 0.4;
    for (auto _ : state) {
        auto r = rotation_from_cos_axis(mu, a) * b;
        benchmark::DoNotOptimize(r);
    }
}
BENCHMARK(BM_rot_from_mu);

static void BM_random_angle(benchmark::State& state) {
    std::random_device rd;
    std::mt19937_64 rng(rd());
    Eigen::Vector3d a = {0.7, 3.4, -2.1};
    a.normalize();

    for (auto _ : state) {
        std::uniform_real_distribution<double> dist2(0, 2 * M_PI);
        double phi = dist2(rng);
        auto p = Eigen::AngleAxis(phi, a) * any_perpendicular(a);
        benchmark::DoNotOptimize(p);
    }
}
BENCHMARK(BM_random_angle);

static void BM_random_direction(benchmark::State& state) {
    std::random_device rd;
    std::mt19937_64 rng(rd());
    Eigen::Vector3d a = {0.7, 3.4, -2.1};
    a.normalize();

    for (auto _ : state) {
        std::uniform_real_distribution<double> dist(-1., 1.);
        double x, y, r;
        do {
            x = dist(rng);
            y = dist(rng);
            r = x * x + y * y;
        } while (r > 1.);
        r = sqrt(r);
        auto p =
          rotation_from_cos_sin_axis(x / r, y / r, a) * any_perpendicular(a);
        benchmark::DoNotOptimize(p);
    }
}
BENCHMARK(BM_random_direction);

static void BM_polytope_intersection(benchmark::State& state) {
    using namespace rayli::vgrid;
    auto unit_box = polytopes_from_hexahedra({{0., 0., 0.},
                                              {1., 0., 0.},
                                              {1., 1., 0.},
                                              {0., 1., 0.},
                                              {0., 0., 1.},
                                              {1., 0., 1.},
                                              {1., 1., 1.},
                                              {0., 1., 1.}},
                                             {{0, 1, 2, 3, 4, 5, 6, 7}});
    auto box = unit_box.get_polytope(0);
    auto r = Ray{{0.1, 0.2, -1}, Eigen::Vector3d{0.1, -0.3, 1}};

    for (auto _ : state) {
        benchmark::DoNotOptimize(box.intersect(r));
    }
    state.counters["interaction_rate"] =
      benchmark::Counter(state.iterations() * 3, benchmark::Counter::kIsRate);
}
BENCHMARK(BM_polytope_intersection);

static void BM_polytope_intersection2(benchmark::State& state) {
    using namespace rayli::vgrid;
    auto unit_box = polytopes_from_hexahedra({{0., 0., 0.},
                                              {1., 0., 0.},
                                              {1., 1., 0.},
                                              {0., 1., 0.},
                                              {0., 0., 1.},
                                              {1., 0., 1.},
                                              {1., 1., 1.},
                                              {0., 1., 1.}},
                                             {{0, 1, 2, 3, 4, 5, 6, 7}});
    auto box = unit_box.get_polytope(0);
    auto r = Ray{{0.1, 0.2, -1}, Eigen::Vector3d{0.1, -0.3, 1}};

    for (auto _ : state) {
        benchmark::DoNotOptimize(box.intersect2(r));
    }
    state.counters["interaction_rate"] =
      benchmark::Counter(state.iterations() * 2, benchmark::Counter::kIsRate);
}
BENCHMARK(BM_polytope_intersection2);

static rayli::vgrid::ConvexPolytopes many_wedges(size_t nx, size_t ny,
                                                 size_t nz) {
    std::vector<Eigen::Vector3d> vertices;
    std::vector<std::array<size_t, 6>> wedges;
    for (size_t i = 0; i < nx + 1; ++i) {
        for (size_t j = 0; j < ny + 1; ++j) {
            for (size_t k = 0; k < nz + 1; ++k) {
                vertices.push_back({double(i), double(j), double(k)});
            }
        }
    }
    for (size_t i = 0; i < nx; ++i) {
        for (size_t j = 0; j < ny; ++j) {
            for (size_t k = 0; k < nz; ++k) {
                wedges.push_back(
                  {(i + 0) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 0),
                   (i + 1) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 0),
                   (i + 0) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) + (k + 0),
                   (i + 0) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 1),
                   (i + 1) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 1),
                   (i + 0) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) +
                     (k + 1)});
                wedges.push_back(
                  {(i + 1) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 0),
                   (i + 1) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) + (k + 0),
                   (i + 0) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) + (k + 0),
                   (i + 1) * (ny + 1) * (nz + 1) + (j + 0) * (nz + 1) + (k + 1),
                   (i + 1) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) + (k + 1),
                   (i + 0) * (ny + 1) * (nz + 1) + (j + 1) * (nz + 1) +
                     (k + 1)});
            }
        }
    }
    return rayli::vgrid::polytopes_from_wedges(vertices, wedges);
}

static void BM_polytope_trace_noinit(benchmark::State& state) {
    spdlog::set_level(spdlog::level::warn);
    auto grid = many_wedges(state.range(0), state.range(0), state.range(0));
    grid.is_convex = true;
    grid.build_accelerators();
    auto r = Ray{{0.1, -0.2, -0.1}, Eigen::Vector3d{0.1, 1.3, 1}};

    size_t num_interactions = 0;
    auto path = grid.walk_along(r, 0, std::numeric_limits<double>::infinity());
    auto begin = path.begin();
    auto end = path.end();
    for (auto _ : state) {
        for (auto stepit = begin; stepit != end; ++stepit) {
            benchmark::DoNotOptimize(*stepit);
            ++num_interactions;
        }
    }
    state.SetComplexityN(state.range(0));
    state.counters["interaction_rate"] =
      benchmark::Counter(num_interactions, benchmark::Counter::kIsRate);
    state.counters["grid_size"] = benchmark::Counter(
      grid.planes.size() * sizeof(grid.polytopes_of_planes[0]) +
      grid.polytopes_of_planes.size() * sizeof(grid.polytopes_of_planes[0]) +
      grid.planes_of_polytopes.size() * sizeof(grid.planes_of_polytopes[0]) +
      grid.polytope_plane_indices.size() +
      sizeof(grid.polytope_plane_indices[0]));
    state.counters["interactions"] =
      benchmark::Counter(num_interactions / state.iterations());
}
BENCHMARK(BM_polytope_trace_noinit)
  ->RangeMultiplier(2)
  ->Range(1, 128)
  ->Complexity();

static void BM_polytope_trace(benchmark::State& state) {
    spdlog::set_level(spdlog::level::warn);
    auto grid = many_wedges(state.range(0), state.range(0), state.range(0));
    grid.is_convex = true;
    grid.build_accelerators();

    // std::cout << "polys:\n";
    // for(size_t i = 0; i < grid.polytope_plane_indices.size() - 1; ++i) {
    //    std::cout << i << ":";
    //    for(auto j = grid.polytope_plane_indices[i]; j <
    //    grid.polytope_plane_indices[i+1]; ++j) {
    //        std::cout << " " << grid.planes_of_polytopes[i].v();
    //    }
    //    std::cout << "\n";
    //}

    // std::cout << "planes:\n";
    // for(size_t i = 0; i < grid.polytopes_of_planes.size(); ++i) {
    //    std::cout << i << ": " << grid.polytopes_of_planes[i][0] << " " <<
    //    grid.polytopes_of_planes[i][1] << "\n";
    //}
    auto r = Ray{{0.1, -0.2, -0.1}, Eigen::Vector3d{0.1, 1.3, 1}};

    size_t num_interactions = 0;
    for (auto _ : state) {
        for (auto step :
             grid.walk_along(r, 0, std::numeric_limits<double>::infinity())) {
            benchmark::DoNotOptimize(step);
            ++num_interactions;
        }
    }
    state.SetComplexityN(state.range(0));
    state.counters["interaction_rate"] =
      benchmark::Counter(num_interactions, benchmark::Counter::kIsRate);
    state.counters["grid_size"] = benchmark::Counter(
      grid.planes.size() * sizeof(grid.polytopes_of_planes[0]) +
      grid.polytopes_of_planes.size() * sizeof(grid.polytopes_of_planes[0]) +
      grid.planes_of_polytopes.size() * sizeof(grid.planes_of_polytopes[0]) +
      grid.polytope_plane_indices.size() +
      sizeof(grid.polytope_plane_indices[0]));
    state.counters["interactions"] =
      benchmark::Counter(num_interactions / state.iterations());
}
BENCHMARK(BM_polytope_trace)->RangeMultiplier(2)->Range(1, 128)->Complexity();

static void BM_cartesian_trace(benchmark::State& state) {
    spdlog::set_level(spdlog::level::warn);
    auto grid = rayli::vgrid::EquidistantCartesian(
      {size_t(state.range(0)), size_t(state.range(0)), size_t(state.range(0))});
    auto r = Ray{{0.1, -0.2, -0.1}, Eigen::Vector3d{0.1, 1.3, 1}};

    size_t num_interactions = 0;
    for (auto _ : state) {
        for (auto step :
             grid.walk_along(r, 0, std::numeric_limits<double>::infinity())) {
            benchmark::DoNotOptimize(step);
            ++num_interactions;
        }
    }
    state.SetComplexityN(state.range(0));
    state.counters["interaction_rate"] =
      benchmark::Counter(num_interactions, benchmark::Counter::kIsRate);
    state.counters["interactions"] =
      benchmark::Counter(num_interactions / state.iterations());
}
BENCHMARK(BM_cartesian_trace)->RangeMultiplier(2)->Range(1, 128)->Complexity();

BENCHMARK_MAIN();
