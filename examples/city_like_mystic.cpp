#include <chrono>
#include <iostream>
#include <netcdf>
#include <thread>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/AASurfVGrid.hpp>
#include <vgrid/AddVGrid.hpp>
#include <vgrid/OneDimensional.hpp>
#include <vgrid/TriSurfVGrid.hpp>

struct NullSurfaceIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t surface_a(size_t ia) const { return 0; }
    size_t surface_b(size_t ib) const { return 0; }
};

struct ShiftSurfaceIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t surface_a(size_t ia) const {
        return ia + 1;  // shift idx by 1 to make space for ground mesh
    }
    size_t surface_b(size_t ib) const {
        return 0;  // otherwise return 0 for the ground mesh
    }
};

struct NullVolumeIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t volume_a(size_t ia) const { return 0; }
    size_t volume_b(size_t ib) const { return 0; }
    size_t volume_ab(size_t ia, size_t ib) const { return 0; }
};

int main(int argc, char** argv) {
    if (argc < 5) {
        std::cerr
          << " * The city_like_mystic example takes a triangle mesh as input \n"
             "   in the same format as Mystic from libRadtran does \n "
             "   and creates a virtual picture from the scene. \n"
             "   The scene additionally adds a constant surface albedo \n"
             "   and a fog layer in the lower atmoshphere \n"
             "   with optical thickness tau given as a command line "
             "argument.\n\n"
             " - usage: \n   "
          << argv[0]
          << " <srfc_input_file.nc> <outfile> <nrays> <tau>\n\n"
             "   e.g. call with arguments:\n"
             "   examples/city_like_mystic_zurich.nc out.nc 100000000 1.0\n";
        return -1;
    }

    double sza = deg2rad(60.);
    size_t nrays = std::atol(argv[3]);
    double tau = std::atof(argv[4]);

    std::vector<Eigen::Vector3d> vertices;
    std::vector<std::array<size_t, 3>> faces;
    std::vector<double> material_albedo;
    std::vector<size_t> material_of_faces;

    {
        using namespace netCDF;

        NcFile file(argv[1], NcFile::FileMode::read);
        size_t Nverts = file.getDim("Nvert").getSize();
        size_t Nfaces = file.getDim("Ntriangles").getSize();
        size_t Nmaterials = file.getDim("Nsurface_props").getSize();
        vertices.resize(Nverts);
        faces.resize(Nfaces);
        material_albedo.resize(Nmaterials);
        material_of_faces.resize(Nfaces);
        std::cerr << "Nvert " << Nverts << " Nfaces " << Nfaces
                  << " Nmaterials " << Nmaterials << " \n";

        file.getVar("vertices").getVar(&(vertices[0][0]));
        file.getVar("triangles").getVar(&(faces[0][0]));
        file.getVar("surface_properties_list").getVar(material_albedo.data());
        file.getVar("surface_properties_index")
          .getVar(material_of_faces.data());
    }

    auto mesh = std::make_shared<rayli::surface::TriangularMesh>(
      rayli::surface::TriangularMesh::from_vertices_and_triangles(vertices,
                                                                  faces));
    mesh->build_accelerators();

    auto buildings = TriSurfVGrid{mesh};  // triangle buildings mesh
    auto ground = rayli::vgrid::AASurfVGrid{
      0., 2};  // axis aligned surface mesh at 0 height oriented on z-axis (2)

    double cld_thickness = 10;  // height of 1D cloud layer
    auto cloud_grid = rayli::vgrid::OneDimensional{{0.0, cld_thickness}};

    // merge grids
    auto lower_grids = rayli::vgrid::AddVGrid{
      ground, cloud_grid, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto grid = rayli::vgrid::AddVGrid{
      buildings, lower_grids, ShiftSurfaceIndexer{}, NullVolumeIndexer{}};

    // scattering with Henyey Greenstein Phase function
    auto scatterer = HGScat<std::mt19937_64>(0.0);

    auto k_scat = constant_texture<size_t>(tau / cld_thickness);
    auto k_ext = k_scat;
    auto scat = scatterer * k_scat;

    auto surf = function_texture<size_t>([&](size_t i) {
        if (i == 0) {  // ground reflections with constant albedo
            return LambertianMaterial{0.1};
        } else {  // or use albedo from materials definition
            auto material_idx = material_of_faces[i - 1];
            return LambertianMaterial{material_albedo[material_idx]};
        }
    });

    auto sink = FarSink<std::mt19937_64>{
      Eigen::Vector3d{0., +sin(sza), cos(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    auto progress = ProgressBar(nrays);

    Eigen::Vector3d campos = {-25, -25, 250};
    Eigen::Vector3d target = {150, 150, 0};
    auto source = SimpleCamera{campos,
                               lookat(target - campos, {0., 0., 1.}),
                               1.2,
                               1.2,
                               nrays,
                               {},
                               progress.reporter()};

    // auto vr = rayli::vr::MaxScatterVr(1);
    // auto tracer = PathTracer{scn,vr};
    auto tracer = PathTracer{scn};
    auto recorder = ImageRecorder{400, 400};
    auto start = std::chrono::system_clock::now();

    progress.display_while([&]() { tracer.solve_par(source, recorder); });
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "traced " << nrays << " rays in " << diff.count()
              << " seconds, that is " << (nrays / diff.count())
              << " rays per second.\n";

    {  // dump results to netcdf
        using namespace netCDF;

        NcFile file(argv[2], NcFile::FileMode::replace);
        auto xdim = file.addDim("x", recorder.sx);
        auto ydim = file.addDim("y", recorder.sy);
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
        auto radvar =
          file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
        auto stdvar =
          file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
        datavar.putVar(recorder.data.data());
        countvar.putVar(recorder.count.data());
        auto rad = recorder.radiance();
        radvar.putVar(rad.data());
        auto rad_std = recorder.radiance_std();
        stdvar.putVar(rad_std.data());
    }
}
