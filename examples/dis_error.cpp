#include <chrono>
#include <iostream>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/OneDimensional.hpp>

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "usage: " << argv[0] << " <miefile> <nrays>\n";
        return -1;
    }

    double sza = deg2rad(50.);
    double vza = deg2rad(30.);

    auto optprop_loader = MieLoader(argv[1]);
    // auto optprop_loader = HSLoader(argv[1]);
    size_t nrays = std::atol(argv[2]);
    auto [scatterer, ext] = optprop_loader.load(870., 10.);

    double disort_result = 1.812579334e-01;
    auto grid = rayli::vgrid::OneDimensional({0, 500});

    // double disort_result = 8.375006169e-02;
    // auto grid = rayli::vgrid::OneDimensional({0, 50});

    auto scat_ref = RefAtm(scatterer);
    auto scat = constant_texture<size_t>(scat_ref);
    auto k_ext = constant_texture<size_t>(ext);
    auto surf = constant_texture<size_t>(LambertianMaterial{0});
    auto sink = FarSink<std::mt19937_64>{
      Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    auto source =
      ParallelCamera{{0., 0., 1000.},
                     lookat({sin(vza), 0., -cos(vza)}, {0., 1., 0.}),
                     1.,
                     1.,
                     nrays};

    auto gs = std::vector<double>{0.8,  0.825, 0.85, 0.875, 0.9,  0.92, 0.935,
                                  0.95, 0.96,  0.97, 0.98,  0.99, 0.995};
    auto p_diss = std::vector<double>{0.,  0.001, 0.003, 0.01, 0.03, 0.05, 0.07,
                   0.1, 0.15,  0.2,   0.25, 0.3,  0.35, 0.4};
    /*
    auto gs = std::vector<double>{0.98};
    auto p_diss = std::vector<double>{0.1};
    */
    // auto vr = rayli::vr::PBSVR{10, 100, HGScat<std::mt19937_64>{0.9}} +
    //          rayli::vr::RussianRouletteVR{{1e-4, .1}};
    std::cout << "#g p_dis T Tstd rps efficiency delta delta_of_stddev\n";
    for (auto g : gs) {
        // for(auto g: {0.9}) {
        for (auto p_dis : p_diss) {
            // auto vr =
            //  rayli::vr::DirectionalImportanceSamplingVR<std::mt19937_64>{p_dis,
            //                                                              g};

            auto vr = rayli::vr::NLRVR(
              22, 4, 11,
              rayli::vr::DirectionalImportanceSamplingVR<std::mt19937_64>{p_dis,
                                                                          g},
              rayli::vr::RussianRouletteVR{});
            // auto vr = rayli::vr::NoVarianceReduction{};<
            // auto vr = rayli::vr::RussianRouletteVR{};
            auto tracer = PathTracer{scn, vr};
            auto recorder = ImageRecorder{1, 1};
            auto start = std::chrono::system_clock::now();
            tracer.solve_par(source, recorder);
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> diff = end - start;
            // std::cout << "traced " << nrays << " rays in " << diff.count() <<
            // " seconds, that is " << (nrays/diff.count()) << " rays per
            // second.\n";
            auto rad = recorder.radiance();
            auto rad_std = recorder.radiance_std();
            // std::cout << "transmissivity: " << rad[0] << " +- " << rad_std[0]
            // << "\n";
            double efficiency = 1. / (rad_std[0] * diff.count());
            // std::cout << "g: " << g << " p_dis: " << p_dis << " T: " <<
            // rad[0]
            //          << " +- " << rad_std[0]
            //          << " rps: " << (nrays / diff.count())
            //          << " efficiency: " << efficiency << "\n";
            // std::cout << "    Delta: " << (rad[0] - disort_result) << ", "
            //          << ((rad[0] - disort_result) / rad_std[0])
            //          << " of stddev\n";
            std::cout << g << " " << p_dis << " " << rad[0] << " " << rad_std[0]
                      << " " << (nrays / diff.count()) << " " << efficiency
                      << " " << (rad[0] - disort_result) << " "
                      << ((rad[0] - disort_result) / rad_std[0]) << std::endl;
        }
    }
}