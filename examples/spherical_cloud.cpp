#include <iostream>
#include <algorithm>

#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <GeometryUtils.hpp>
#include <vgrid/BallVGrid.hpp>

int main(int argc, char** argv) {
    if (argc < 6) {
        std::cerr << "usage: " << argv[0] << " <miefile> <outfile> <lwp> <sza> <nrays>\n";
        return -1;
    }

    std::vector<double> wavelengths = {870., 2102.};
    double reff = 10.;

    std::string miefile = argv[1];
    std::string outfile = argv[2];
    double lwp = std::stod(argv[3]);
    double sza = deg2rad(std::stod(argv[4]));
    size_t nrays = std::atol(argv[5]);
    size_t nx = 100;
    double radius = 500.;

    auto lwc = constant_texture<size_t>(lwp/(2*radius));

    auto optprop_loader = MieLoader(miefile);
    Eigen::Vector3d center = {0., 0., 0.};
    auto ball = rayli::vgrid::BallVGrid{center, {radius}};

    Eigen::Vector3d sundir = Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized();
    Eigen::Vector3d zenith = {0., 0., 1.};
    auto sink = FarSink<std::mt19937_64>{sundir};
    Eigen::Vector3d campos = {0., 0., 5000.};
    Eigen::Vector3d outgoing_ray = (campos - center).normalized();

    std::vector<Eigen::Vector3d> surface_normals(nx);
    std::vector<double> xcoord(nx);
    std::vector<double> lwp_true(nx);
    for(size_t i = 0; i < nx; ++i) {
        double x = (i + 0.5) * 2. / nx - 1.;
        double y = 0.;
        double z = sqrt(std::max(0., 1. - x * x));
        surface_normals[i] = Eigen::Vector3d{x, y, z}.normalized();
        xcoord[i] = x;
        lwp_true[i] = z * lwp;
    }

    std::vector<double> data;
    std::vector<size_t> count;
    std::vector<double> transmission;
    std::vector<double> transmission_std;
    for(double wavelength: wavelengths) {
        std::cout << "computing wavelength: " << wavelength << " nm\n";
        auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
        auto scatterer_ref = RefAtm(scatterer);
        auto scat = scatterer_ref * lwc;
        auto k_ext = ext * lwc;
        auto surf = constant_texture<size_t>(NoMaterial{});
        auto scn = Scn{ball, k_ext, scat, surf, sink};

        auto progress = ProgressBar(nrays);
        auto source = ParallelCamera{campos,
                                     lookat(center - campos, {0., 1., 0.}),
                                     2*radius,
                                     0.,
                                     nrays,
                                     {},
                                     progress.reporter()};

        auto recorder = ImageRecorder{nx, 1};
        auto tracer = PathTracer{scn};
        progress.display_while([&]() { tracer.solve_par(source, recorder); });
        std::cout << "\n";
        std::copy(recorder.data.begin(), recorder.data.end(), std::back_inserter(data));
        std::copy(recorder.count.begin(), recorder.count.end(), std::back_inserter(count));
        auto rad = recorder.radiance();
        auto rad_std = recorder.radiance_std();
        std::copy(rad.begin(), rad.end(), std::back_inserter(transmission));
        std::copy(rad_std.begin(), rad_std.end(), std::back_inserter(transmission_std));
    }

    {
        using namespace netCDF;

        NcFile file(outfile, NcFile::FileMode::replace);
        auto xdim = file.addDim("x", nx);
        auto wvldim = file.addDim("wavelength", wavelengths.size());
        file.addVar("x", NcType::nc_DOUBLE, {xdim}).putVar(xcoord.data());
        file.addVar("wavelength", NcType::nc_DOUBLE, {wvldim}).putVar(wavelengths.data());
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {wvldim, xdim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {wvldim, xdim});
        auto radvar =
          file.addVar("transmission", NcType::nc_DOUBLE, {wvldim, xdim});
        auto stdvar =
          file.addVar("transmission_std", NcType::nc_DOUBLE, {wvldim, xdim});
        datavar.putVar(data.data());
        countvar.putVar(count.data());
        radvar.putVar(transmission.data());
        stdvar.putVar(transmission_std.data());

        auto v_dim = file.addDim("v", 3);
        Eigen::Vector3d viewdir = -outgoing_ray;
        file.addVar("outgoing_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&outgoing_ray[0]);
        file.addVar("viewdir", NcType::nc_DOUBLE, {v_dim}).putVar(&viewdir[0]);
        file.addVar("sun_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&sundir[0]);
        file.addVar("zenith", NcType::nc_DOUBLE, {v_dim}).putVar(&zenith[0]);
        file.addVar("surface_normals", NcType::nc_DOUBLE, {xdim, v_dim}).putVar(&surface_normals[0][0]);
        int shadow = 0;
        file.addVar("shadow_flag", NcType::nc_INT).putVar(&shadow);
        file.addVar("lwp_ref", NcType::nc_DOUBLE).putVar(&lwp);
        file.addVar("lwp_true", NcType::nc_DOUBLE, {xdim}).putVar(lwp_true.data());
    }
}
