#include <iostream>
#include <chrono>

#include <FarSink.hpp>
#include <material.hpp>
#include <recorder.hpp>
#include <atm.hpp>
#include <io/optprop.hpp>
#include <vgrid/OneDimensional.hpp>
#include <texture/texture.hpp>
#include <camera.hpp>
#include <PathTracer.hpp>

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "usage: " << argv[0] << " <miefile> <nrays>\n";
        return -1;
    }

    double sza = deg2rad(50.);
    double vza = deg2rad(30.);

    auto optprop_loader = MieLoader(argv[1]);
    // auto optprop_loader = HSLoader(argv[1]);
    size_t nrays = std::atol(argv[2]);
    auto [scatterer, ext] = optprop_loader.load(870., 10.);


    auto grid = rayli::vgrid::OneDimensional({0, 500});
    auto scat_ref = RefAtm(scatterer);
    auto scat = constant_texture<size_t>(scat_ref);
    auto k_ext = constant_texture<size_t>(ext);
    auto surf = constant_texture<size_t>(LambertianMaterial{0});
    auto sink =
      FarSink<std::mt19937_64>{Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    auto source = ParallelCamera{
        {0., 0., 1000.},
        lookat({sin(vza), 0., -cos(vza)}, {0., 1., 0.}),
        1.,
        1.,
        nrays};

    //auto vr = rayli::vr::PBSVR{10, 100, HGScat<std::mt19937_64>{0.9}} +
    //          rayli::vr::RussianRouletteVR{{1e-4, .1}};
    // auto vr = rayli::vr::DirectionalImportanceSamplingVR<std::mt19937_64>{0.01, 0.99};
    auto vr = rayli::vr::NLRVR(22, 4, 11,
                    rayli::vr::DirectionalImportanceSamplingVR<std::mt19937_64>{0.1, 0.9},
                    rayli::vr::RussianRouletteVR{});



    auto tracer = PathTracer{scn, vr};
    auto recorder = ImageRecorder{1, 1};
    auto start = std::chrono::system_clock::now();
    tracer.solve_par(source, recorder);
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end-start;
    std::cout << "traced " << nrays << " rays in " << diff.count() << " seconds, that is " << (nrays/diff.count()) << " rays per second.\n";

    auto rad = recorder.radiance();
    auto rad_std = recorder.radiance_std();
    std::cout << "transmissivity: " << rad[0] << " +- " << rad_std[0] << "\n";
}