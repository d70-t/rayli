#include <chrono>
#include <iostream>
#include <thread>

#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/ConvexPolytopes.hpp>

template <typename Scalar>
struct ErrorValue {
    Scalar value;
    Scalar error;
};

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "usage: " << argv[0] << " <miefile> <nrays> <cloud aspect>\n";
        return -1;
    }

    std::string miefile = argv[1];
    size_t nrays = std::atol(argv[2]);
    double cloud_aspect = 5.;

    if (argc >= 3) {
        cloud_aspect = std::stod(argv[3]);
    }

    double cloud_height = 1000.;

    double camera_distance = 10000.;

    auto make_cloud = [](double height, double aspect, double tilt, Eigen::Vector3d center = {0., 0., 0.}) {
        Eigen::Vector3d cloud_normal = {sin(tilt), 0., cos(tilt)};
        Eigen::Vector3d cloud_y = {0., 1., 0.};
        Eigen::Vector3d cloud_x = cloud_y.cross(cloud_normal);

        Eigen::Vector3d ex = cloud_x * height / 2. * aspect;
        Eigen::Vector3d ey = cloud_y * height / 2. * aspect;
        Eigen::Vector3d ez = cloud_normal * height / 2.;
        std::vector<Eigen::Vector3d> corners = {
            center - ex - ey - ez,
            center + ex - ey - ez,
            center + ex + ey - ez,
            center - ex + ey - ez,
            center - ex - ey + ez,
            center + ex - ey + ez,
            center + ex + ey + ez,
            center - ex + ey + ez
        };

        // std::cout << "corners:\n";
        // for(auto corner: corners) {
        //     std::cout << corner.transpose() << "\n";
        // }
        std::vector<std::array<size_t, 8>> corner_indices = {{0, 1, 2, 3, 4, 5, 6, 7}};

        auto grid = rayli::vgrid::polytopes_from_hexahedra(corners, corner_indices);
        grid.is_convex = true;
        // grid.build_accelerators();
        
        return grid;
    };
    
    auto optprop_loader = MieLoader(miefile);


    auto get_L_norm = [&optprop_loader, &make_cloud, cloud_height, cloud_aspect, camera_distance, nrays](double vza, double sza, double tilt, double lwp, double wavelength) {
        double reff = 10.;
        Eigen::Vector3d center = {0., 0., 0.};
        auto grid = make_cloud(cloud_height, cloud_aspect, tilt, center);
        auto lwc = constant_texture<size_t>(lwp / cloud_height);
        auto sink = FarSink<std::mt19937_64>{
          Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized()};
        Eigen::Vector3d campos = center + Eigen::Vector3d{sin(vza), 0., cos(vza)} * camera_distance;

        auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
        auto scatterer_ref = RefAtm(scatterer);
        auto scat = scatterer_ref * lwc;
        auto k_ext = ext * lwc;
        auto surf = constant_texture<size_t>(NoMaterial{});
        auto scn = Scn{grid, k_ext, scat, surf, sink};

        auto source = PointCamera{campos,
                                  (center - campos).normalized(),
                                  nrays};

        auto tracer = PathTracer{scn};
        auto recorder = PointRecorder{};
        tracer.solve_par(source, recorder);
        return ErrorValue<double>{recorder.e(), recorder.sigma()};
    };

    std::cout << "#vza sza tilt lwp wavelength L_norm L_norm_std cloud_aspect\n";
    for(double vza: {0., 15., 30., 45., 60.}) {
    //for(double vza: {0., 30., 60.}) {
        for(double sza: {-60., -45., -30., -15., 0., 15., 30., 45., 60.}) {
        //for(double sza: {-60., -30., 0., 30., 60.}) {
            for(double tilt: {0., 15., 30., 45., 60.}) {
            //for(double tilt: {0., 30., 60.}) {
                for(double lwp: {20., 40., 200., 1000., 3333.}) {
                    for(double wavelength: {870., 2102.}) {
                        auto res = get_L_norm(deg2rad(vza), deg2rad(sza), deg2rad(tilt), lwp, wavelength);
                        std::cout << vza << " " << sza << " " << tilt << " " << lwp << " " << wavelength << " " << res.value << " " << res.error << " " << cloud_aspect << std::endl;
                    }
                }
            }
        }
    }

    /*
    Eigen::Vector3d image_campos = campos - Eigen::Vector3d{0., cloud_aspect * cloud_height, 0.};
    double b = 0.98;
    double g1 = 0.89;
    double g2 = -0.66;
    auto scatterer =
      b * HGScat<std::mt19937_64>(g1) + (1. - b) * HGScat<std::mt19937_64>(g2);

    auto k_scat = lwc;
    auto k_ext = k_scat;
    auto scat = scatterer * k_scat;

    auto scn = Scn{grid, k_ext, scat, surf, sink};

    auto progress = ProgressBar(nrays);
    auto source = SimpleCamera{image_campos,
                               lookat(center - image_campos, {0., 1., 0.}),
                               1.,
                               1.,
                               nrays,
                               {},
                               progress.reporter()};

    auto tracer = PathTracer{scn};
    auto recorder = ImageRecorder{100, 100};
    auto start = std::chrono::system_clock::now();

    progress.display_while([&]() { tracer.solve_par(source, recorder); });
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "traced " << nrays << " rays in " << diff.count()
              << " seconds, that is " << (nrays / diff.count())
              << " rays per second.\n";

    {
        using namespace netCDF;

        NcFile file(outfile, NcFile::FileMode::replace);
        auto xdim = file.addDim("x", recorder.sx);
        auto ydim = file.addDim("y", recorder.sy);
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
        auto radvar =
          file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
        auto stdvar =
          file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
        datavar.putVar(recorder.data.data());
        countvar.putVar(recorder.count.data());
        auto rad = recorder.radiance();
        radvar.putVar(rad.data());
        auto rad_std = recorder.radiance_std();
        stdvar.putVar(rad_std.data());
    }
    */
}
