#include <chrono>
#include <iostream>
#include <thread>

#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/BallVGrid.hpp>
#include <vgrid/EquidistantCartesian.hpp>
#include <vgrid/AASurfVGrid.hpp>
#include <vgrid/AddVGrid.hpp>

struct NullSurfaceIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t surface_a(size_t ia) const {
        return 0;
    }
    size_t surface_b(size_t ib) const {
        return 0;
    }
};

struct NullVolumeIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t volume_a(size_t ia) const {
        return 0;
    }
    size_t volume_b(size_t ib) const {
        return 0;
    }
    size_t volume_ab(size_t ia, size_t ib) const {
        return 0;
    }
};

struct OffsetVolumeIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t a_count;
    size_t b_count;

    size_t volume_a(size_t ia) const {
        return volume_ab(ia, b_count);
    }
    size_t volume_b(size_t ib) const {
        return volume_ab(a_count, ib);
    }
    size_t volume_ab(size_t ia, size_t ib) const {
        return ib + ia * (b_count + 1);
    }
};

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "usage: " << argv[0] << " <cloudfile> <outfile> <thickness> <nrays>\n";
        return -1;
    }

    double sza = deg2rad(45.);

    size_t nrays = std::atol(argv[4]);
    double thickness = std::stod(argv[3]);
    std::string outfile = argv[2];
    std::string cloudfile = argv[1];

    std::vector<double> lwc;
    size_t cloud_nx, cloud_ny, cloud_nz;
    {
        using namespace netCDF;

        NcFile file(cloudfile, NcFile::FileMode::read);
        auto lwcvar = file.getVar("lwc");
        cloud_nx = lwcvar.getDim(0).getSize();
        cloud_ny = lwcvar.getDim(1).getSize();
        cloud_nz = lwcvar.getDim(2).getSize();
        lwc.resize(cloud_nx * cloud_ny * cloud_nz);
        lwcvar.getVar(lwc.data());
    }
    const size_t lwc_count = lwc.size();
    std::cerr << "loaded cloud with " << cloud_nx << "x" << cloud_ny << "x" << cloud_nz << " voxels (total: " << lwc_count << ")\n";

    double hscale = 0.05;
    double vscale = 0.13;
    auto lwccloud = rayli::vgrid::EquidistantCartesian{{cloud_nx, cloud_ny, cloud_nz},
                                                       {hscale, hscale, vscale},
                                                       {-hscale * cloud_nx / 2., 9., -1.}};

    auto ball = rayli::vgrid::BallVGrid{{0., 2., 0.}, {1.}};

    Eigen::Vector3d c = {0., 5.5, 0.};
    auto ball2 = rayli::vgrid::BallVGrid{c + Eigen::Vector3d{0., 0., 0.}, {1.}};
    auto ball3 = rayli::vgrid::BallVGrid{c + Eigen::Vector3d{0., 1., 0.}, {1.}};
    auto ball4 = rayli::vgrid::BallVGrid{c + Eigen::Vector3d{sqrt(1 - 0.25), 0.5, 0.}, {1.}};
    auto ball5 = rayli::vgrid::BallVGrid{c + Eigen::Vector3d{0.5 * sqrt(1 - 0.25), 0.5, sqrt(1. - 0.125)}, {1.}};

    auto multiball1 = rayli::vgrid::AddVGrid{ball2, ball3, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto multiball2 = rayli::vgrid::AddVGrid{ball4, ball5, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto multiball = rayli::vgrid::AddVGrid{multiball1, multiball2, NullSurfaceIndexer{}, NullVolumeIndexer{}};

    auto box = rayli::vgrid::EquidistantCartesian{{1,1,1}, {2., 2., 2.}, {-1., -3., -1.}};
    auto box_1d = rayli::vgrid::EquidistantCartesian{{1,1,1}, {2000., 1000., 2.}, {-1000., -1005., -1.}};

    auto ground = rayli::vgrid::AASurfVGrid{-3., 2};

    auto cloudgrid1 = rayli::vgrid::AddVGrid{box_1d, box, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto cloudgrid2 = rayli::vgrid::AddVGrid{ball, multiball, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto cloudgrid = rayli::vgrid::AddVGrid{cloudgrid1, cloudgrid2, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto grid_ = rayli::vgrid::AddVGrid{cloudgrid, ground, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto grid = rayli::vgrid::AddVGrid{grid_, lwccloud, NullSurfaceIndexer{}, OffsetVolumeIndexer{1, lwc.size()}};

    double b = 0.98;
    double g1 = 0.89;
    double g2 = -0.66;
    auto scatterer =
      b * HGScat<std::mt19937_64>(g1) + (1. - b) * HGScat<std::mt19937_64>(g2);

    auto k_scat =
      function_texture<size_t>([&](size_t i) {
                size_t lwc_index = i % (lwc_count + 1);
                if(lwc_index == lwc_count) {
                    return thickness;
                } else {
                    return lwc[lwc_index] * thickness * 2.5;
                }
            });
    auto k_ext = k_scat;
    auto scat = scatterer * k_scat;

    auto surf = constant_texture<size_t>(LambertianMaterial{0.15});

    auto sink = FarSink<std::mt19937_64>{
      Eigen::Vector3d{sin(sza), 0.3, cos(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    Eigen::Vector3d center = {0., 3., 0.};
    Eigen::Vector3d campos = {50., 3., 30.};
    auto progress = ProgressBar(nrays);
    auto source = SimpleCamera{campos,
                                 lookat(center - campos, {0., 0., 1.}),
                                 0.5,
                                 0.15,
                                 nrays,
                                 {},
                                 progress.reporter()};

    auto tracer = PathTracer{scn};
    //auto recorder = ImageRecorder{2000, 600};
    auto recorder = ImageRecorder{600, 180};
    //auto recorder = ImageRecorder{200, 60};
    auto start = std::chrono::system_clock::now();

    progress.display_while([&]() { tracer.solve_par(source, recorder); });
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "traced " << nrays << " rays in " << diff.count()
              << " seconds, that is " << (nrays / diff.count())
              << " rays per second.\n";

    {
        using namespace netCDF;

        NcFile file(outfile, NcFile::FileMode::replace);
        auto xdim = file.addDim("x", recorder.sx);
        auto ydim = file.addDim("y", recorder.sy);
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
        auto radvar =
          file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
        auto stdvar =
          file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
        datavar.putVar(recorder.data.data());
        countvar.putVar(recorder.count.data());
        auto rad = recorder.radiance();
        radvar.putVar(rad.data());
        auto rad_std = recorder.radiance_std();
        stdvar.putVar(rad_std.data());
    }
}
