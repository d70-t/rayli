#include <chrono>
#include <iostream>
#include <thread>

#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/BallVGrid.hpp>
#include <vgrid/EquidistantCartesian.hpp>
#include <vgrid/AASurfVGrid.hpp>
#include <vgrid/AddVGrid.hpp>

struct NullSurfaceIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t surface_a(size_t ia) const {
        return 0;
    }
    size_t surface_b(size_t ib) const {
        return 0;
    }
};

struct NullVolumeIndexer {
    using a_type = size_t;
    using b_type = size_t;
    using value_type = size_t;

    size_t volume_a(size_t ia) const {
        return 0;
    }
    size_t volume_b(size_t ib) const {
        return 0;
    }
    size_t volume_ab(size_t ia, size_t ib) const {
        return 0;
    }
};

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "usage: " << argv[0] << " <outfile> <thickness> <nrays>\n";
        return -1;
    }

    double sza = deg2rad(45.);

    size_t nrays = std::atol(argv[3]);
    double thickness = std::stod(argv[2]);

    auto ball = rayli::vgrid::BallVGrid{{0., 2., 0.}, {1.}};
    auto box = rayli::vgrid::EquidistantCartesian{{1,1,1}, {2., 2., 2.}, {-1., -3., -1.}};
    auto ground = rayli::vgrid::AASurfVGrid{-3., 2};
    auto cloudgrid = rayli::vgrid::AddVGrid{ball, box, NullSurfaceIndexer{}, NullVolumeIndexer{}};
    auto grid = rayli::vgrid::AddVGrid{cloudgrid, ground, NullSurfaceIndexer{}, NullVolumeIndexer{}};

    double b = 0.98;
    double g1 = 0.89;
    double g2 = -0.66;
    auto scatterer =
      b * HGScat<std::mt19937_64>(g1) + (1. - b) * HGScat<std::mt19937_64>(g2);

    auto k_scat =
      function_texture<size_t>([thickness](size_t i) { return thickness; });
    auto k_ext = k_scat;
    auto scat = scatterer * k_scat;

    auto surf = constant_texture<size_t>(LambertianMaterial{0.05});

    auto sink = FarSink<std::mt19937_64>{
      Eigen::Vector3d{sin(sza), 0.3, cos(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    Eigen::Vector3d center = {0., 0., 0.};
    Eigen::Vector3d campos = {15., 0., 10.};
    auto progress = ProgressBar(nrays);
    auto source = SimpleCamera{campos,
                                 lookat(center - campos, {0., 0., 1.}),
                                 0.5,
                                 0.3,
                                 nrays,
                                 {},
                                 progress.reporter()};

    auto tracer = PathTracer{scn};
    auto recorder = ImageRecorder{1000, 600};
    auto start = std::chrono::system_clock::now();

    progress.display_while([&]() { tracer.solve_par(source, recorder); });
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "traced " << nrays << " rays in " << diff.count()
              << " seconds, that is " << (nrays / diff.count())
              << " rays per second.\n";

    {
        using namespace netCDF;

        NcFile file(argv[1], NcFile::FileMode::replace);
        auto xdim = file.addDim("x", recorder.sx);
        auto ydim = file.addDim("y", recorder.sy);
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
        auto radvar =
          file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
        auto stdvar =
          file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
        datavar.putVar(recorder.data.data());
        countvar.putVar(recorder.count.data());
        auto rad = recorder.radiance();
        radvar.putVar(rad.data());
        auto rad_std = recorder.radiance_std();
        stdvar.putVar(rad_std.data());
    }
}