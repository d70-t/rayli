#include <GeometryUtils.hpp>
#include <vgrid/ConvexPolytopes.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <recorder.hpp>
#include <camera.hpp>
#include <texture/texture.hpp>
#include <atm.hpp>
#include <FarSink.hpp>
#include <PathTracer.hpp>

#include <netcdf>

#include <iostream>
#include <stdexcept>

template <typename F>
double bisect(F f, double xmin, double xmax, double xtol) {
    double xcenter = (xmax + xmin) / 2.;
    if (xmax - xmin < xtol) return xcenter;
    double ymin = f(xmin);
    double ycenter = f(xcenter);
    double ymax = f(xmax);
    if(ymin * ycenter < 0) {
        return bisect(f, xmin, xcenter, xtol);
    } else if(ymax * ycenter < 0) {
        return bisect(f, xcenter, xmax, xtol);
    } else {
        throw std::domain_error("xmin and xmax do not lead to opposing signs!");
    }
}

class WaveCloud {
    public:
        WaveCloud(double angle, double wave_height, double sx);
        std::vector<Eigen::Vector2d> linearize_top(size_t initial_points = 10000000, double max_error = 0.00001) const;

        double f(double x) const;
        double dfdx(double x) const;
        /** x value of the half height along the left upper slope
         * 
         *
         *      /\
         *     x  \
         * ---/|   \    /---
         *     |    \  /
         *     |     \/
         *     ^ this x value
         */
        double x_half_left() const;
    private:
        double wave_height, sx;
        double ta;
        double e = exp(1.);
};

WaveCloud::WaveCloud(double angle, double wave_height, double sx) :
    wave_height(wave_height), sx(sx) {
    ta = tan(deg2rad(angle));
}

std::vector<Eigen::Vector2d> WaveCloud::linearize_top(size_t initial_points, double max_error) const {
    double max_error_abs = wave_height * max_error;
    double xmin = -sx / 2.;
    double dx = sx / initial_points;
    std::vector<double> x(initial_points);
    std::vector<double> y(initial_points);
    for(size_t i = 0; i < initial_points; ++i) {
        x[i] = xmin + i * dx;
        y[i] = f(x[i]);
    }

    std::vector<Eigen::Vector2d> out;
    out.push_back({x.front(), y.front()});
    const auto run = [&](size_t i1, size_t i2, const auto& run) {
        if(i2 <= i1) return;
        double w = x[i2] - x[i1];
        double maxerr = 0;
        size_t imax = 0;
        for(size_t i = i1 + 1; i < i2; ++i) {
            double xrel = (x[i] - x[i1]) / w;
            double y_interp = xrel * y[i2] + (1. - xrel) * y[i1];
            double err = abs(y[i] - y_interp);
            if(err > maxerr) {
                maxerr = err;
                imax = i;
            }
        }
        if(maxerr > max_error_abs) {
            run(i1, imax, run);
            out.push_back({x[imax], y[imax]});
            run(imax, i2, run);
        }
    };
    run(0, x.size() - 1, run);
    out.push_back({x.back(), y.back()});
    return out;
}

double WaveCloud::x_half_left() const {
    double xmin = -100*ta*wave_height;
    double xmax = -2*ta*wave_height;
    double xtol = wave_height * 1e-9;
    return bisect([&](double x){ return this->f(x) - this->wave_height*0.5; }, xmin, xmax, xtol);
}

double WaveCloud::f(double x) const {
    return -(x/ta) * exp(-(x*x)/(2*e*wave_height*wave_height*ta*ta));
}

double WaveCloud::dfdx(double x) const {
    auto w = wave_height;
    return (x*x - e * ta*ta * w*w) / (ta*ta*ta * w*w) * exp(-x*x / (2. * e * ta*ta * w*w) - 1.);
}

/** \brief creates a ConvexPolytopes grid from linear pieces describing cloud top
 *
 * \param top (x,z) pairs of cloud top, must be strictly monotonic in x
 * \param zmin z value for lower boundary of the grid
 * \param ymin lower y boundary of the grid
 * \param ymax upper y boundary of the grid
 */
auto linear_top2d_to_hexahedral_grid(const std::vector<Eigen::Vector2d>& top,
                                     double zmin,
                                     double ymin,
                                     double ymax) {
    size_t nx = top.size();
    std::vector<Eigen::Vector3d> vertices(4 * nx);
    for(size_t i = 0; i < nx; ++i) {
        vertices[i + 0 * nx] = {top[i][0], ymin, zmin};
        vertices[i + 1 * nx] = {top[i][0], ymax, zmin};
        vertices[i + 2 * nx] = {top[i][0], ymin, top[i][1]};
        vertices[i + 3 * nx] = {top[i][0], ymax, top[i][1]};
    }
    std::vector<std::array<size_t, 8>> vertices_of_hexahedra(nx - 1);
    for(size_t i = 0; i < nx - 1; ++i) {
        vertices_of_hexahedra[i] = {i+0*nx, i+0*nx+1, i+1*nx+1, i+1*nx,
                                    i+2*nx, i+2*nx+1, i+3*nx+1, i+3*nx};
    }
    auto grid = rayli::vgrid::polytopes_from_hexahedra(std::move(vertices), std::move(vertices_of_hexahedra));
    grid.build_accelerators();
    return grid;
}

void show_usage(std::string_view progname) {
    std::cerr << "usage: " << progname << " picture <miefile> <nrays> <outfile>\n";
    std::cerr << "usage: " << progname << " center_scale <miefile> <sza> <vza> <nrays> <outfile>\n";
    std::cerr << "usage: " << progname << " left_scale <miefile> <sza> <vza> <nrays> <outfile>\n";
}

enum class RunMode {
    PICTURE,
    CENTER_SCALE,
    LEFT_SCALE
};

int main(int argc, char** argv) {
    if (argc < 2) {
        show_usage(argv[0]);
        return -1;
    }
    std::string mode = argv[1];

    std::string miefile;
    size_t nrays;
    std::string outfile;
    double viewdir_y_offset = 0.;

    double lwp = 100.;
    double cloud_height = 100.;
    double camera_distance = .5e4;
    double sza = deg2rad(15.);
    double vza = deg2rad(-15.);
    std::vector<double> wavelengths = {870., 2102.};
    double reff = 10.;

    RunMode run_mode;

    if(mode == "picture") {
        if (argc < 5) {
            show_usage(argv[0]);
            return -1;
        }
        miefile = argv[2];
        nrays = std::atol(argv[3]);
        outfile = argv[4];
        viewdir_y_offset = 1.;
        run_mode = RunMode::PICTURE;
    } else if (mode == "center_scale") {
        if (argc < 8) {
            show_usage(argv[0]);
            return -1;
        }
        miefile = argv[2];
        sza = deg2rad(std::stod(argv[3]));
        vza = deg2rad(std::stod(argv[4]));
        lwp = std::stod(argv[5]);
        nrays = std::atol(argv[6]);
        outfile = argv[7];
        run_mode = RunMode::CENTER_SCALE;
    } else if (mode == "left_scale") {
        if (argc < 8) {
            show_usage(argv[0]);
            return -1;
        }
        miefile = argv[2];
        sza = deg2rad(std::stod(argv[3]));
        vza = deg2rad(std::stod(argv[4]));
        lwp = std::stod(argv[5]);
        nrays = std::atol(argv[6]);
        outfile = argv[7];
        run_mode = RunMode::LEFT_SCALE;
    } else {
        show_usage(argv[0]);
        return -1;
    }
    
    Eigen::Vector3d sundir = Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized();
    Eigen::Vector3d viewdir = -Eigen::Vector3d{sin(vza), viewdir_y_offset, cos(vza)}.normalized();

    auto lwc = constant_texture<size_t>(lwp / cloud_height);
    auto sink = FarSink<std::mt19937_64>{sundir};

    auto optprop_loader = MieLoader(miefile);

    auto vacuum_ext = constant_texture<size_t>(0.);
    auto vacuum_scat = constant_texture<size_t>(VacuumScat<std::mt19937_64>{});

    auto surf = constant_texture<size_t>(NoMaterial{});
    auto black_surf = constant_texture<size_t>(LambertianMaterial{0.});

    auto build_grid = [&](double wave_fraction=0.5, double cloud_size=1e3) {
        auto cloud = WaveCloud(45., cloud_height * wave_fraction, cloud_size);
        auto grid = linear_top2d_to_hexahedral_grid(cloud.linearize_top(),
                                                    -cloud_height, -cloud_size/2., cloud_size/2.);
        return grid;
    };


    switch(run_mode) {
        case RunMode::PICTURE:
            {
                Eigen::Vector3d center = {0., 0., 0.};
                Eigen::Vector3d campos = center - viewdir * camera_distance;
                double wavelength = wavelengths.front();

                auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
                auto scatterer_ref = RefAtm(scatterer);
                auto scat = scatterer_ref * lwc;
                auto k_ext = ext * lwc;

                auto progress = ProgressBar(nrays);
                auto source = SimpleCamera{campos,
                                          lookat(center - campos, {0., 0., 1.}),
                                          0.3, 0.3, nrays,
                                          {}, progress.reporter()};

                auto recorder = ImageRecorder{500, 500};
                auto grid = build_grid();
                auto scn = Scn{grid, k_ext, scat, surf, sink};
                auto tracer = PathTracer{scn};
                progress.display_while([&]() { tracer.solve_par(source, recorder); });

                using namespace netCDF;

                NcFile file(outfile, NcFile::FileMode::replace);
                auto xdim = file.addDim("x", recorder.sx);
                auto ydim = file.addDim("y", recorder.sy);
                auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
                auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
                auto radvar =
                  file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
                auto stdvar =
                  file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
                datavar.putVar(recorder.data.data());
                countvar.putVar(recorder.count.data());
                auto rad = recorder.radiance();
                radvar.putVar(rad.data());
                auto rad_std = recorder.radiance_std();
                stdvar.putVar(rad_std.data());
            }
            break;
        case RunMode::CENTER_SCALE:
            {
                Eigen::Vector3d center = {0., 0., 0.};
                Eigen::Vector3d campos = center - viewdir * camera_distance;
                std::vector<double> wave_fractions = {1e-3, 1e-2, 0.5e-1, 1e-1, 0.2, 0.3, 0.5, 0.75, 1.};
                std::vector<Eigen::Vector3d> surface_normals;
                std::vector<Eigen::Vector3d> hitpoints;
                std::vector<double> transmissivity;
                std::vector<double> transmissivity_std;
                std::vector<double> data;
                std::vector<size_t> count;

                for(auto wave_fraction: wave_fractions) {
                    std::cout << "fraction: " << wave_fraction << "\n";
                    auto grid = build_grid(wave_fraction, 1e5);

                    auto black_source = PointCamera{campos,
                                                    (center - campos).normalized(),
                                                    1};
                    auto black_recorder = FirstSurfacePropertyRecorder<size_t>{};
                    auto black_scn = Scn{grid, vacuum_ext, vacuum_scat, black_surf};
                    auto black_tracer = PathTracer{black_scn};
                    black_tracer.solve(black_source, black_recorder);
                    surface_normals.push_back(black_recorder.surface_properties.front().normal);
                    hitpoints.push_back(black_recorder.surface_properties.front().hitpoint);


                    for(double wavelength: wavelengths) {
                        auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
                        auto scatterer_ref = RefAtm(scatterer);
                        auto scat = scatterer_ref * lwc;
                        auto k_ext = ext * lwc;
                        auto source = PointCamera{campos,
                                                  (center - campos).normalized(),
                                                  nrays};
                        auto recorder = PointRecorder{};
                        auto scn = Scn{grid, k_ext, scat, surf, sink};
                        auto tracer = PathTracer{scn};
                        tracer.solve_par(source, recorder);
                        transmissivity.push_back(recorder.e());
                        transmissivity_std.push_back(recorder.sigma());
                        data.push_back(recorder.data);
                        count.push_back(recorder.count);
                    }
                }

                Eigen::Vector3d zenith = {0., 0., 1.};

                using namespace netCDF;

                NcFile file(outfile, NcFile::FileMode::replace);
                auto height_dim = file.addDim("wave_height_rel", wave_fractions.size());
                auto wavelength_dim = file.addDim("wavelength", wavelengths.size());
                auto v_dim = file.addDim("v", 3);
                file.addVar("wave_height_rel", NcType::nc_DOUBLE, {height_dim}).putVar(wave_fractions.data());
                file.addVar("wavelength", NcType::nc_DOUBLE, {wavelength_dim}).putVar(wavelengths.data());
                file.addVar("transmission", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(transmissivity.data());
                file.addVar("transmission_std", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(transmissivity_std.data());
                file.addVar("data", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(data.data());
                file.addVar("count", NcType::nc_UINT64, {height_dim, wavelength_dim}).putVar(count.data());
                file.addVar("surface_normals", NcType::nc_DOUBLE, {height_dim, v_dim}).putVar(&surface_normals[0][0]);
                file.addVar("hitpoints", NcType::nc_DOUBLE, {height_dim, v_dim}).putVar(&hitpoints[0][0]);
                file.addVar("zenith", NcType::nc_DOUBLE, {v_dim}).putVar(&zenith[0]);
                file.addVar("viewdir", NcType::nc_DOUBLE, {v_dim}).putVar(&viewdir[0]);
                Eigen::Vector3d outgoing_rays = -viewdir;
                file.addVar("outgoing_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&outgoing_rays[0]);
                int shadow = 0;
                file.addVar("shadow_flag", NcType::nc_INT).putVar(&shadow);
                file.addVar("sun_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&sundir[0]);

                file.addVar("lwp_ref", NcType::nc_DOUBLE).putVar(&lwp);
                file.addVar("lwp_true", NcType::nc_DOUBLE).putVar(&lwp);
            }
            break;
        case RunMode::LEFT_SCALE:
            {
                std::vector<double> wave_fractions = {1e-3, 1e-2, 0.5e-1, 1e-1, 0.2, 0.3, 0.5, 0.75, 1.};
                std::vector<Eigen::Vector3d> surface_normals;
                std::vector<Eigen::Vector3d> hitpoints;
                std::vector<double> transmissivity;
                std::vector<double> transmissivity_std;
                std::vector<double> data;
                std::vector<size_t> count;

                for(auto wave_fraction: wave_fractions) {
                    std::cout << "fraction: " << wave_fraction << "\n";

                    double cloud_size = 1e5;
                    auto cloud = WaveCloud(45.,
                                           cloud_height * wave_fraction,
                                           cloud_size);
                    auto grid = linear_top2d_to_hexahedral_grid(cloud.linearize_top(),
                                                                -cloud_height,
                                                                -cloud_size/2.,
                                                                cloud_size/2.);

                    double xobs = cloud.x_half_left();
                    double zobs = cloud.f(xobs);
                    Eigen::Vector3d center = {xobs, 0., zobs};
                    Eigen::Vector3d campos = center - viewdir * camera_distance;
                    std::cout << "observing at: " << center.transpose() << "\n";

                    auto black_source = PointCamera{campos,
                                                    (center - campos).normalized(),
                                                    1};
                    auto black_recorder = FirstSurfacePropertyRecorder<size_t>{};
                    auto black_scn = Scn{grid, vacuum_ext, vacuum_scat, black_surf};
                    auto black_tracer = PathTracer{black_scn};
                    black_tracer.solve(black_source, black_recorder);
                    surface_normals.push_back(black_recorder.surface_properties.front().normal);
                    hitpoints.push_back(black_recorder.surface_properties.front().hitpoint);


                    for(double wavelength: wavelengths) {
                        auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
                        auto scatterer_ref = RefAtm(scatterer);
                        auto scat = scatterer_ref * lwc;
                        auto k_ext = ext * lwc;
                        auto source = PointCamera{campos,
                                                  (center - campos).normalized(),
                                                  nrays};
                        auto recorder = PointRecorder{};
                        auto scn = Scn{grid, k_ext, scat, surf, sink};
                        auto tracer = PathTracer{scn};
                        tracer.solve_par(source, recorder);
                        transmissivity.push_back(recorder.e());
                        transmissivity_std.push_back(recorder.sigma());
                        data.push_back(recorder.data);
                        count.push_back(recorder.count);
                    }
                }

                Eigen::Vector3d zenith = {0., 0., 1.};

                using namespace netCDF;

                NcFile file(outfile, NcFile::FileMode::replace);
                auto height_dim = file.addDim("wave_height_rel", wave_fractions.size());
                auto wavelength_dim = file.addDim("wavelength", wavelengths.size());
                auto v_dim = file.addDim("v", 3);
                file.addVar("wave_height_rel", NcType::nc_DOUBLE, {height_dim}).putVar(wave_fractions.data());
                file.addVar("wavelength", NcType::nc_DOUBLE, {wavelength_dim}).putVar(wavelengths.data());
                file.addVar("transmission", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(transmissivity.data());
                file.addVar("transmission_std", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(transmissivity_std.data());
                file.addVar("data", NcType::nc_DOUBLE, {height_dim, wavelength_dim}).putVar(data.data());
                file.addVar("count", NcType::nc_UINT64, {height_dim, wavelength_dim}).putVar(count.data());
                file.addVar("surface_normals", NcType::nc_DOUBLE, {height_dim, v_dim}).putVar(&surface_normals[0][0]);
                file.addVar("hitpoints", NcType::nc_DOUBLE, {height_dim, v_dim}).putVar(&hitpoints[0][0]);
                file.addVar("zenith", NcType::nc_DOUBLE, {v_dim}).putVar(&zenith[0]);
                file.addVar("viewdir", NcType::nc_DOUBLE, {v_dim}).putVar(&viewdir[0]);
                Eigen::Vector3d outgoing_rays = -viewdir;
                file.addVar("outgoing_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&outgoing_rays[0]);
                int shadow = 0;
                file.addVar("shadow_flag", NcType::nc_INT).putVar(&shadow);
                file.addVar("sun_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&sundir[0]);

                file.addVar("lwp_ref", NcType::nc_DOUBLE).putVar(&lwp);
                file.addVar("lwp_true", NcType::nc_DOUBLE).putVar(&lwp);
            }
            break;
    }
}
