#include <chrono>
#include <iostream>
#include <thread>

#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/EquidistantCartesian.hpp>
#include <vgrid/MysticCloud.hpp>
#include <vgrid/OneDimensional.hpp>

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "usage: " << argv[0] << " <outfile> <boundary_size> <nrays>\n";
        return -1;
    }

    double sza = deg2rad(90.);

    size_t nrays = std::atol(argv[3]);

    size_t n_boundary = 100;
    double boundary_size = std::stod(argv[2]);
    double sigma_max = 10.;

    // for grid simplicity:
    // actual -> mental
    // z -> x
    // x -> y
    // y -> z
    // auto grid = rayli::vgrid::MysticCloud{1., 1., 1, 1,
    // {0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.,10.}};
    std::vector<double> levels(n_boundary + 2);
    for (size_t i = 0; i < n_boundary + 1; ++i) {
        levels[i] = i * boundary_size / n_boundary;
    }
    levels[n_boundary + 1] = 1e6;

    auto grid = rayli::vgrid::MysticCloud{1., 1., 1, 1, std::move(levels)};

    double b = 0.98;
    double g1 = 0.89;
    double g2 = -0.66;
    auto scatterer =
      b * HGScat<std::mt19937_64>(g1) + (1. - b) * HGScat<std::mt19937_64>(g2);

    double scale = 1. / (n_boundary + 1) * sigma_max;
    auto k_scat =
      function_texture<size_t>([scale](size_t i) { return (i + 1) * scale; });
    auto k_ext = k_scat;
    auto scat = scatterer * k_scat;

    double x_ofs = 0;
    {
        double tau = 0;
        for (auto s : grid.walk_along({{0., 0., 0.}, {0., 0., 1.}}, 0., 1.)) {
            if (auto v = std::get_if<VolumeSlice>(&s)) {
                double tau_next = k_ext(v->idx) * (v->tfar - v->tnear);
                if(tau + tau_next >= 1.) {
                    double f = (1. - tau) / tau_next;
                    x_ofs = f * v->tfar + (1. - f) * v->tnear;
                    break;
                } else {
                    tau += tau_next;
                }
            }
        }
    }
    std::cout << "x_ofs: " << x_ofs << "\n";

    auto surf = constant_texture<size_t>(NoMaterial{});

    auto sink = FarSink<std::mt19937_64>{
      Eigen::Vector3d{0., cos(sza), -sin(sza)}.normalized()};
    auto scn = Scn{grid, k_ext, scat, surf, sink};
    Eigen::Vector3d center = {0.5, 0.5, x_ofs};
    Eigen::Vector3d campos = {10., 0.5, x_ofs};
    auto progress = ProgressBar(nrays);
    auto source = ParallelCamera{campos,
                                 lookat(center - campos, {0., 1., 0.}),
                                 1.5,
                                 1.5,
                                 nrays,
                                 {},
                                 progress.reporter()};

    auto tracer = PathTracer{scn};
    auto recorder = ImageRecorder{100, 100};
    auto start = std::chrono::system_clock::now();

    progress.display_while([&]() { tracer.solve_par(source, recorder); });
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "traced " << nrays << " rays in " << diff.count()
              << " seconds, that is " << (nrays / diff.count())
              << " rays per second.\n";

    {
        using namespace netCDF;

        NcFile file(argv[1], NcFile::FileMode::replace);
        auto xdim = file.addDim("x", recorder.sx);
        auto ydim = file.addDim("y", recorder.sy);
        auto datavar = file.addVar("data", NcType::nc_DOUBLE, {xdim, ydim});
        auto countvar = file.addVar("count", NcType::nc_UINT64, {xdim, ydim});
        auto radvar =
          file.addVar("transmissivity", NcType::nc_DOUBLE, {xdim, ydim});
        auto stdvar =
          file.addVar("transmissivity_std", NcType::nc_DOUBLE, {xdim, ydim});
        datavar.putVar(recorder.data.data());
        countvar.putVar(recorder.count.data());
        auto rad = recorder.radiance();
        radvar.putVar(rad.data());
        auto rad_std = recorder.radiance_std();
        stdvar.putVar(rad_std.data());
    }
}