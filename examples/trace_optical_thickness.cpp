#include <vgrid/MysticCloud.hpp>

#include <iostream>

int main(int argc, char** argv) {
    std::cout << "trace optical thickness\n";

    auto grid = rayli::vgrid::MysticCloud(1., 1., 1, 1, {0., 1., 2.7});

    for(auto slice: grid.walk_along(Ray{{0.5, 0.5, 0.5}, {0., 0., 1.}}, 0., std::numeric_limits<double>::infinity())) {
        if(auto pvol = std::get_if<VolumeSlice>(&slice)) {
            std::cout << "passing volume " << pvol->idx << " from " << pvol->tnear << " to " << pvol->tfar << "\n";
        }
    }
}
