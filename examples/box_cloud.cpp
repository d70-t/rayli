#include <iostream>
#include <netcdf>

#include <FarSink.hpp>
#include <PathTracer.hpp>
#include <atm.hpp>
#include <camera.hpp>
#include <io/optprop.hpp>
#include <material.hpp>
#include <progress.hpp>
#include <recorder.hpp>
#include <texture/texture.hpp>
#include <vgrid/EquidistantCartesian.hpp>

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "usage: " << argv[0] << " <miefile> <outfile> <nrays>\n";
        return -1;
    }

    std::string miefile = argv[1];
    std::string outfile = argv[2];
    size_t nrays = std::atol(argv[3]);

    double camera_distance = 1e4;
    double vza = deg2rad(15.);
    double sza = deg2rad(0.);
    double reff = 10.;

    std::vector<double> wavelengths = {870., 2102.};
    std::vector<double> lwps = {20., 40., 200., 1000., 3333.};
    std::vector<double> wh_aspects = {0.1, 0.2, 0.5, 1., 1.5, 2., 3., 4., 6., 10.};
    //std::vector<double> lwps = {40.};
    //std::vector<double> wh_aspects = {0.5, 1., 1.5};

    Eigen::Vector3d center = {0., 0., 0.};

    auto optprop_loader = MieLoader(miefile);

    Eigen::Vector3d sundir = Eigen::Vector3d{sin(sza), 0., cos(sza)}.normalized();
    Eigen::Vector3d outgoing_ray = Eigen::Vector3d{sin(vza), 0., cos(vza)};
    Eigen::Vector3d zenith = {0., 0., 1.};

    auto sink = FarSink<std::mt19937_64>{sundir};
    Eigen::Vector3d campos = center + outgoing_ray * camera_distance;

    auto surf = constant_texture<size_t>(NoMaterial{});

    std::vector<double> transmissivity;
    std::vector<double> transmissivity_std;

    for(double lwp: lwps) {
        for(double wh_aspect: wh_aspects) {
            for(double wavelength: wavelengths) {
                auto [scatterer, ext] = optprop_loader.load(wavelength, reff);
                auto scatterer_ref = RefAtm(scatterer);

                Eigen::Vector3d cloudsize = {wh_aspect * 500., wh_aspect * 500., 500.};
                auto grid = rayli::vgrid::EquidistantCartesian(
                        {1, 1, 1}, cloudsize,
                        {-cloudsize(0)/2., -cloudsize(1)/2., -cloudsize(2)});
                auto lwc = constant_texture<size_t>(lwp / cloudsize(2));
                auto scat = scatterer_ref * lwc;
                auto k_ext = ext * lwc;
                auto scn = Scn{grid, k_ext, scat, surf, sink};

                auto source = PointCamera{campos,
                                          (center - campos).normalized(),
                                          nrays};

                auto tracer = PathTracer{scn};
                auto recorder = PointRecorder{};
                tracer.solve_par(source, recorder);
                transmissivity.push_back(recorder.e());
                transmissivity_std.push_back(recorder.sigma());
                std::cout << wavelength << " " << lwp << " " << wh_aspect << " " << recorder.e() << "+-" << recorder.sigma() << "\n";
            }
        }
    }

    {
        using namespace netCDF;

        NcFile file(outfile, NcFile::FileMode::replace);
        auto wavelength_dim = file.addDim("wavelength", wavelengths.size());
        auto lwp_true_dim = file.addDim("lwp_true", lwps.size());
        auto wh_aspect_dim = file.addDim("wh_aspect", wh_aspects.size());
        auto wavelength_var = file.addVar("wavelength", NcType::nc_DOUBLE, {wavelength_dim});
        auto lwp_true_var = file.addVar("lwp_true", NcType::nc_DOUBLE, {lwp_true_dim});
        auto wh_aspect_var = file.addVar("wh_aspect", NcType::nc_DOUBLE, {wh_aspect_dim});
        
        wavelength_var.putVar(wavelengths.data());
        lwp_true_var.putVar(lwps.data());
        wh_aspect_var.putVar(wh_aspects.data());

        auto radvar =
          file.addVar("transmission", NcType::nc_DOUBLE, {lwp_true_dim, wh_aspect_dim, wavelength_dim});
        auto stdvar =
          file.addVar("transmission_std", NcType::nc_DOUBLE, {lwp_true_dim, wh_aspect_dim, wavelength_dim});
        radvar.putVar(transmissivity.data());
        stdvar.putVar(transmissivity_std.data());

        auto v_dim = file.addDim("v", 3);
        Eigen::Vector3d viewdir = -outgoing_ray;
        file.addVar("outgoing_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&outgoing_ray[0]);
        file.addVar("viewdir", NcType::nc_DOUBLE, {v_dim}).putVar(&viewdir[0]);
        file.addVar("sun_rays", NcType::nc_DOUBLE, {v_dim}).putVar(&sundir[0]);
        file.addVar("zenith", NcType::nc_DOUBLE, {v_dim}).putVar(&zenith[0]);
        file.addVar("surface_normals", NcType::nc_DOUBLE, {v_dim}).putVar(&zenith[0]);

        int shadow = 0;
        file.addVar("shadow_flag", NcType::nc_INT).putVar(&shadow);
    }
}
