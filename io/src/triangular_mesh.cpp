#include "io/triangular_mesh.hpp"
#include <netcdf>

namespace rayli::io {
rayli::surface::TriangularMesh
load_triangular_mesh(const std::filesystem::path& filename) {
    using namespace netCDF;
    NcFile file(filename, NcFile::FileMode::read);
    auto vertices_var = file.getVar("vertices");
    auto triangles_var = file.getVar("triangles");

    rayli::surface::TriangularMesh mesh;
    mesh.vertices.resize(vertices_var.getDim(0).getSize());
    vertices_var.getVar({0, 0}, {mesh.vertices.size(), 3}, {1, 1}, {3, 1},
                        &mesh.vertices[0](0));
    mesh.faces.resize(triangles_var.getDim(0).getSize());
    std::vector<int32_t> triangles_data(3 * mesh.faces.size());
    triangles_var.getVar({0, 0}, {mesh.faces.size(), 3}, {1, 1}, {3, 1},
                         triangles_data.data());
    for (size_t i = 0; i < mesh.faces.size(); ++i) {
        mesh.faces[i] = {{size_t(triangles_data[3 * i + 0]),
                          size_t(triangles_data[3 * i + 1]),
                          size_t(triangles_data[3 * i + 2])}};
    }

    mesh.repair();

    return mesh;
}
}  // namespace rayli::io
