#include "io/wedgegrid.hpp"
#include <netcdf>

#include <iostream>

namespace rayli::io {
void to_netcdf(std::filesystem::path filename,
               const rayli::vgrid::WedgeGrid& grid) {
    WedgeGridWriter(filename, grid);
}

WedgeGridWriter::WedgeGridWriter(std::filesystem::path filename,
                                 const rayli::vgrid::WedgeGrid& grid)
    : file(filename, netCDF::NcFile::FileMode::replace) {
    using namespace netCDF;
    vertexdim = file.addDim("vertex", grid.vertices.size());
    celldim = file.addDim("cell", grid.cells.size());
    facedim = file.addDim("face", grid.faces.size());
    vdim = file.addDim("v", 3);
    cdim = file.addDim("c", 6);
    fdim = file.addDim("f", 3);

    auto vertices_var =
      file.addVar("vertices", NcType::nc_DOUBLE, {vertexdim, vdim});
    auto cells_var = file.addVar("cells", NcType::nc_UINT64, {celldim, cdim});
    auto faces_var = file.addVar("faces", NcType::nc_UINT64, {facedim, fdim});

    vertices_var.putVar(&(grid.vertices[0](0)));
    cells_var.putVar(&grid.cells[0][0]);
    faces_var.putVar(&grid.faces[0][0]);
}
}  // namespace rayli::io
