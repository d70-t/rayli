#include "io/parsers.hpp"

#include <iostream>
#include <map>
#include <regex>
#include <sstream>

#include "GeometryUtils.hpp"

namespace rayli::io::parsers {
Eigen::Quaterniond parse_rotation(const std::string& rotation_string) {
    static const std::regex rot_re(
      "^R([xyz])\\((-?[0-9]+(?:\\.[0-9]*)?)((?:deg|rad)?)\\)$");
    static const std::map<std::string, Eigen::Vector3d> axes{
      {"x", Eigen::Vector3d::UnitX()},
      {"y", Eigen::Vector3d::UnitY()},
      {"z", Eigen::Vector3d::UnitZ()}};

    auto rot = Eigen::Quaterniond::Identity();
    std::istringstream rot_ss(rotation_string);
    std::string item;
    std::smatch m;
    while (std::getline(rot_ss, item, '*')) {
        if (std::regex_match(item, m, rot_re)) {
            auto axis = m[1];
            double angle = std::stod(m[2]);
            auto unit = m[3];
            if (unit == "deg") {
                angle = deg2rad(angle);
            } else if (unit != "rad") {
                std::cerr << "could not interpret angular unit " << unit
                          << " in rotation primitive: " << item << std::endl;
            }
            rot = Eigen::AngleAxisd(angle, axes.at(axis)) * rot;
        } else {
            std::cerr << "cout not parse rotation primitive: " << item
                      << std::endl;
        }
    }
    return rot;
}
}  // namespace rayli::io::parsers
