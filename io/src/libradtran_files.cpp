#include "io/libradtran_files.hpp"
#include "microphysics.hpp"
#include "optprop/optprop.hpp"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <regex>

// TODO: remove
#include <iostream>

namespace rayli::io::libradtran {

Cloud3DBase::Cloud3DBase(size_t Nx, size_t Ny, size_t Nz, double dx, double dy,
                         Cloud3DBase::ZGrid&& z)
    : _Nx(Nx), _Ny(Ny), _Nz(Nz), _dx(dx), _dy(dy), _z(std::move(z)) {}

template <typename Member>
std::unique_ptr<Cloud3D<Member>>
_read_cloud3d(std::istream& istream, size_t Nx, size_t Ny, size_t Nz, double dx,
              double dy, std::vector<double>&& z) {
    rayli::grid::RegularGrid<3, Member> data;
    data.resize(Nx, Ny, Nz);

    while (istream.good()) {
        size_t ix, iy, iz;
        Member m;
        istream >> ix >> iy >> iz >> m;
        if (!istream.good()) {
            break;
        }
        ix -= 1;
        iy -= 1;
        iz -= 1;
        assert(ix < Nx);
        assert(iy < Ny);
        assert(iz < Nz);
        data(ix, iy, iz) = m;
    }

    return std::make_unique<Cloud3D<Member>>(dx, dy, std::move(z),
                                             std::move(data));
}

std::unique_ptr<Cloud3DBase> read_cloud_3d(std::istream& istream) {
    size_t Nx, Ny, Nz;
    int flag;
    double dx, dy;
    std::vector<double> z;
    istream >> Nx >> Ny >> Nz >> flag;
    istream >> dx >> dy;
    z.reserve(Nz + 1);
    std::copy_n(std::istream_iterator<double>(istream), Nz + 1,
                std::back_inserter(z));
    dx *= 1000.;  // distances in file is in km
    dy *= 1000.;  // distances in file is in km
    std::transform(z.begin(), z.end(), z.begin(),
                   [](auto zi) { return zi * 1000.; });

    switch (flag) {
    case 1:
        return _read_cloud3d<rayli::optprop::HG>(istream, Nx, Ny, Nz, dx, dy,
                                                 std::move(z));
        break;
    case 2:
        return _read_cloud3d<rayli::microphysics::ext_reff>(
          istream, Nx, Ny, Nz, dx, dy, std::move(z));
        break;
    case 3:
        return _read_cloud3d<rayli::microphysics::LWC_reff>(
          istream, Nx, Ny, Nz, dx, dy, std::move(z));
        break;
    }
    return nullptr;
}
}  // namespace rayli::io::libradtran
