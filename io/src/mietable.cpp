#include <GeometryUtils.hpp>
#include <io/mietable.hpp>

#include <netcdf>

#include <algorithm>

namespace rayli::io {
Mietable::Mietable(const std::filesystem::path& filename) {
    netCDF::NcFile file(filename, netCDF::NcFile::FileMode::read);
    size_t nwvlns = file.getVar("wavelen").getDim(0).getSize();
    wvlns.resize(nwvlns);
    file.getVar("wavelen").getVar(wvlns.data());
    std::transform(begin(wvlns), end(wvlns), begin(wvlns),
                   [](auto v) { return v * 1000.; });
    size_t nreffs = file.getVar("reff").getDim(0).getSize();
    reffs.resize(nreffs);
    file.getVar("reff").getVar(reffs.data());

    phasetables.resize(nwvlns * nreffs);
    auto phasevar = file.getVar("phase");
    auto thetavar = file.getVar("theta");
    auto extvar = file.getVar("ext");
    auto ssavar = file.getVar("ssa");
    auto nthetavar = file.getVar("ntheta");
    for (size_t i = 0; i < nwvlns; ++i) {
        for (size_t j = 0; j < nreffs; ++j) {
            int32_t ntheta;
            nthetavar.getVar({i, j, 0}, {1, 1, 1}, &ntheta);
            auto& table = phasetable(i, j);
            table.theta.resize(ntheta);
            table.value.resize(ntheta);
            thetavar.getVar({i, j, 0, 0}, {1, 1, 1, size_t(ntheta)},
                            table.theta.data());
            std::transform(begin(table.theta), end(table.theta),
                           begin(table.theta), deg2rad<double>);
            phasevar.getVar({i, j, 0, 0}, {1, 1, 1, size_t(ntheta)},
                            table.value.data());
            std::transform(begin(table.value), end(table.value),
                           begin(table.value), [](auto v) { return v / 2.; });

            extvar.getVar({i, j}, {1, 1}, &table.ext);
            table.ext /= 1000.;
            ssavar.getVar({i, j}, {1, 1}, &table.ssa);
        }
    }
}

Mietable::Phasetable& Mietable::phasetable(size_t iwvln, size_t ireff) {
    return phasetables[iwvln * reffs.size() + ireff];
}

const Mietable::Phasetable& Mietable::phasetable(size_t iwvln,
                                                 size_t ireff) const {
    return phasetables[iwvln * reffs.size() + ireff];
}
}  // namespace rayli::io
