#include "io/icon.hpp"
#include "logging.hpp"
#include "microphysics.hpp"
#include <fstream>
#include <iterator>
#include <netcdf>

namespace rayli::io::icon {
std::unique_ptr<rayli::grid::ICONGrid>
load_netcdf(const std::filesystem::path& filename) {
    using namespace netCDF;
    auto logger = rayli::logging::get_logger("ICON Grid Loader");
    logger->info("loading ICON grid from {}", filename);
    NcFile file(filename, NcFile::FileMode::read);
    auto grid = std::make_unique<rayli::grid::ICONGrid>();
    auto vertex_dim = file.getDim("vertex");
    auto edge_dim = file.getDim("edge");
    auto cell_dim = file.getDim("cell");
    size_t edge_count = edge_dim.getSize();
    size_t cell_count = cell_dim.getSize();

    logger->info("grid has {} cells and {} edges", cell_count, edge_count);

    auto vertex_x = file.getVar("cartesian_x_vertices");
    auto vertex_y = file.getVar("cartesian_y_vertices");
    auto vertex_z = file.getVar("cartesian_z_vertices");
    grid->vertices.resize(vertex_dim.getSize());
    if (static_cast<void*>(grid->vertices.data()) ==
        static_cast<void*>(grid->vertices[0].data())) {
        // data is stored consecutively, we can do a direct import
        vertex_x.getVar({0}, {grid->vertices.size()}, {1}, {3},
                        grid->vertices[0].data() + 0);
        vertex_y.getVar({0}, {grid->vertices.size()}, {1}, {3},
                        grid->vertices[0].data() + 1);
        vertex_z.getVar({0}, {grid->vertices.size()}, {1}, {3},
                        grid->vertices[0].data() + 2);
    } else {
        logger->error("currently only efficient reading is implemented but the "
                      "memory order does not fit");
        return nullptr;
    }

    auto edge_nx = file.getVar("edge_primal_normal_cartesian_x");
    auto edge_ny = file.getVar("edge_primal_normal_cartesian_y");
    auto edge_nz = file.getVar("edge_primal_normal_cartesian_z");
    grid->edge_normals.resize(edge_count);
    if (static_cast<void*>(grid->edge_normals.data()) ==
        static_cast<void*>(grid->edge_normals[0].data())) {
        // data is stored consecutively, we can do a direct import
        edge_nx.getVar({0}, {edge_count}, {1}, {3},
                       grid->edge_normals[0].data() + 0);
        edge_ny.getVar({0}, {edge_count}, {1}, {3},
                       grid->edge_normals[0].data() + 1);
        edge_nz.getVar({0}, {edge_count}, {1}, {3},
                       grid->edge_normals[0].data() + 2);
    } else {
        logger->error("currently only efficient reading is implemented but the "
                      "memory order does not fit");
        return nullptr;
    }

    {
        auto edge_vertices = file.getVar("edge_vertices");
        std::vector<unsigned long long> temp(2 * edge_count);
        edge_vertices.getVar({0, 0}, {2, edge_count}, {1, 1}, {1, 2},
                             temp.data());
        grid->edge_vertices.resize(edge_count);
        for (size_t i = 0; i < edge_count; ++i) {
            grid->edge_vertices[i] = {temp[2 * i] - 1, temp[2 * i + 1] - 1};
        }
    }

    {
        auto cell_vertices = file.getVar("vertex_of_cell");
        auto cell_edges = file.getVar("edge_of_cell");
        std::vector<unsigned long long> temp_vertices(3 * cell_count);
        std::vector<unsigned long long> temp_edges(3 * cell_count);
        cell_vertices.getVar({0, 0}, {3, cell_count}, {1, 1}, {1, 3},
                             temp_vertices.data());
        cell_edges.getVar({0, 0}, {3, cell_count}, {1, 1}, {1, 3},
                          temp_edges.data());
        grid->cells.resize(cell_count);
        for (size_t i = 0; i < cell_count; ++i) {
            grid->cells[i].vertices = {{temp_vertices[3 * i + 0] - 1,
                                        temp_vertices[3 * i + 1] - 1,
                                        temp_vertices[3 * i + 2] - 1}};
            grid->cells[i].edges = {{temp_edges[3 * i + 0] - 1,
                                     temp_edges[3 * i + 1] - 1,
                                     temp_edges[3 * i + 2] - 1}};
        }
    }

    // grid->repair_edge_normals();
    grid->repair_edge_orientations();
    grid->repair_edge_cells();

    file.getAtt("sphere_radius").getValues(&(grid->rmin));
    grid->rmax = grid->rmin + 1e3;
    return grid;
}

std::vector<rayli::microphysics::LWC_reff>
load_LWC_reff_from_model(const std::filesystem::path& filename,
                         size_t timestep) {

    auto logger = rayli::logging::get_logger("ICON Model Loader");
    logger->info("loading model output from {}", filename);

    using namespace netCDF;
    NcFile file(filename, NcFile::FileMode::read);

    auto cells_dim = file.getDim("ncells");
    auto layer_dim = file.getDim("height");
    size_t ncells = cells_dim.getSize();
    size_t nlayers = layer_dim.getSize();
    size_t nvolume = ncells * nlayers;
    logger->info("model data has {} cells at {} height levels, totalling in {} "
                 "microphysic cells",
                 ncells, nlayers, nvolume);

    auto clw_var = file.getVar("clw");                // units: kg/kg
    auto droplet_count_var = file.getVar("qnc");      // units: 1/kg
    auto air_pressure_var = file.getVar("pres");      // units: Pa
    auto specific_humidity_var = file.getVar("hus");  // units: kg/kg
    auto temperature_var = file.getVar("ta");         // units: K

    logger->info("loading data layer by layer");

    std::vector<rayli::microphysics::LWC_reff> out(nvolume);
    std::vector<float> clw(ncells);
    std::vector<float> dc(ncells);
    std::vector<float> p(ncells);
    std::vector<float> hum(ncells);
    std::vector<float> temp(ncells);

    // dims in netcdf are (time, layer, cell)
    for (size_t i = 0; i < nlayers; ++i) {
        logger->info("loading layer {}.", i);
        std::vector<size_t> start{timestep, i, 0};
        std::vector<size_t> count{1, 1, ncells};
        clw_var.getVar(start, count, clw.data());
        droplet_count_var.getVar(start, count, dc.data());
        air_pressure_var.getVar(start, count, p.data());
        specific_humidity_var.getVar(start, count, hum.data());
        temperature_var.getVar(start, count, temp.data());
        logger->info("computing LWC and reff for layer {}.", i);
        for (size_t j = 0; j < ncells; ++j) {
            // note that ICON data is stored TOA -> Ground,
            // but rayli expects data to be Ground -> TOA
            size_t k = (nlayers - i - 1) + j * nlayers;
            double rho_air = rayli::microphysics::density_air_specific(
              p[j], hum[j], temp[j]);       // [kg/m³]
            double lwc = clw[j] * rho_air;  // [kg/m³]
            out[k].LWC = lwc * 1e3;
            out[k].reff =
              rayli::microphysics::reff_from_lwc_n(lwc, dc[j] * rho_air);
        }
    }
    return out;
}

std::vector<double> load_levels(const std::filesystem::path& filename,
                                double sphere_radius) {

    auto logger = rayli::logging::get_logger("ICON vertical grid Loader");
    logger->info("loading vertical grid from {}", filename);

    std::ifstream infile(filename);
    std::vector<double> out;
    std::copy(std::istream_iterator<double>(infile),
              std::istream_iterator<double>(), std::back_inserter(out));
    for (double& z : out) {
        z += sphere_radius;
    }
    logger->info("vertical grid has {} layers and {} levels.", out.size() - 1,
                 out.size());
    return out;
}
}  // namespace rayli::io::icon
