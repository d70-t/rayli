#include "io/recorder/recorder.hpp"

using namespace rayli::io::recorder;

ImageRecorder::~ImageRecorder() {}

Collector::Collector(size_t nx, size_t ny) : _nx(nx), _ny(ny) {
    _L = Eigen::ArrayXXd::Zero(nx, ny);
    _count = Eigen::ArrayXXd::Zero(nx, ny);
}

Collector& Collector::operator+=(const Collector& other) {
    _L += other.L();
    _count += other.count();
    return *this;
}
