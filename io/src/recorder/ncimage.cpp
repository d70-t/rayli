#include "io/recorder/ncimage.hpp"
#include <netcdf>

using namespace rayli::io::recorder;

NcImageRecorder::NcImageRecorder(const std::filesystem::path& filename,
                                 size_t nx, size_t ny)
    : ImageRecorder(nx, ny), collector(nx, ny), filename(filename) {}

size_t NcImageRecorder::nx() const { return collector.nx(); }

size_t NcImageRecorder::ny() const { return collector.ny(); }

Collector NcImageRecorder::get_collector() { return Collector(nx(), ny()); }

void NcImageRecorder::return_collector(Collector&& subcollector) {
    std::lock_guard _(collect_mutex);
    collector += subcollector;
}

void NcImageRecorder::finish() {
    Eigen::ArrayXXd radiance = collector.L() / collector.count();

    using namespace netCDF;
    NcFile file(filename, NcFile::FileMode::replace);
    auto xdim = file.addDim("x", nx());
    auto ydim = file.addDim("y", ny());
    auto rad_var = file.addVar("radiance", NcType::nc_DOUBLE, {ydim, xdim});
    rad_var.putVar(radiance.data());
    auto n_var = file.addVar("n", NcType::nc_UINT64, {ydim, xdim});
    n_var.putVar(collector.count().data());
}
