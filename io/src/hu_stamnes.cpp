#include "io/hu_stamnes.hpp"

#include <iostream>

#include <cassert>
#include <fstream>
#include <regex>
#include <string>

using namespace rayli::optprop::hu_stamnes;

namespace rayli::io::hu_stamnes {

struct FileLine {
    double wavelength;
    double a, b, c;
};

struct FileBlock {
    double reff_min;
    double reff_max;
    std::vector<FileLine> lines;
};

static const std::string re_float = "([0-9]+(?:\\.?[0-9]+)?)";
static const std::string re_efloat =
  "([+-]?[0-9]+(?:\\.[0-9]+(?:[eE][+-][0-9]+)?)?)";
static const std::string re_start = "^";
static const std::string re_end = "$";

std::vector<FileBlock> read_hs_file(std::istream& input) {
    static std::regex reff_range_regex(re_start + "\\s*" + re_float + "\\s+" +
                                       re_float + "\\s*" + re_end);
    static std::regex line_regex(re_start + "\\s*" + re_float + "\\s+" +
                                 re_efloat + "\\s*" + re_efloat + "\\s*" +
                                 re_efloat + "\\s*" + re_end);

    std::vector<FileBlock> out;
    bool valid = false;
    FileBlock current_block;
    std::string line;
    std::smatch m;

    auto finish_block = [&](FileBlock& block) {
        std::sort(block.lines.begin(), block.lines.end(),
                  [](const auto& a, const auto& b) {
                      return a.wavelength < b.wavelength;
                  });
        out.push_back(std::move(block));
        block.lines.clear();
    };

    while (input.good()) {
        std::getline(input, line);
        if (std::regex_match(line, m, reff_range_regex)) {
            if (valid) {
                finish_block(current_block);
            }
            current_block.reff_min = stod(m.str(1));
            current_block.reff_max = stod(m.str(2));
            valid = true;
        } else if (std::regex_match(line, m, line_regex)) {
            current_block.lines.push_back(
              {stod(m.str(1)), stod(m.str(2)), stod(m.str(3)), stod(m.str(4))});
        }
    }
    if (valid) {
        finish_block(current_block);
    }
    return out;
}

Table load_table(const std::filesystem::path& datapath) {
    auto abspath = std::filesystem::canonical(datapath);
    if (!std::filesystem::is_directory(abspath)) {
        std::cout << "directory for hu and stamnes tables not found: "
                  << abspath << std::endl;
    }
    std::ifstream asy_file(datapath / "wc.asy");
    std::ifstream ext_file(datapath / "wc.ext");
    std::ifstream ssa_file(datapath / "wc.ssa");

    auto asy_table = read_hs_file(asy_file);
    auto ext_table = read_hs_file(ext_file);
    auto ssa_table = read_hs_file(ssa_file);

    assert(asy_table.size() > 0);
    assert(asy_table.size() == ext_table.size());
    assert(asy_table.size() == ssa_table.size());

    Table out;

    for (const auto& line : asy_table[0].lines) {
        out.wavelength.push_back(line.wavelength);
    }

#ifdef DEBUG
    for (const auto& table : {asy_table, ext_table, ssa_table}) {
        for (const auto& block : table) {
            assert(block.lines.size() == out.wavelength.size());
            for (size_t i = 0; i < out.wavelength.size(); ++i) {
                assert(block.lines[i].wavelength == out.wavelength[i]);
            }
        }
    }
#endif

    for (const auto& block : asy_table) {
        out.reff_ranges.push_back({block.reff_min, block.reff_max, {}});
    }

#ifdef DEBUG
    for (const auto& table : {ext_table, ssa_table}) {
        for (size_t i = 0; i < out.reff_ranges.size(); ++i) {
            assert(out.reff_ranges[i].reff_min == table[i].reff_min);
            assert(out.reff_ranges[i].reff_max == table[i].reff_max);
        }
    }
#endif

    for (size_t i = 0; i < out.reff_ranges.size(); ++i) {
        auto& data = out.reff_ranges[i].data;
        data.resize(out.wavelength.size());
        for (size_t j = 0; j < out.wavelength.size(); ++j) {
            data[j].ext.a = ext_table[i].lines[j].a / 1000.;
            data[j].ext.b = ext_table[i].lines[j].b;
            data[j].ext.c = ext_table[i].lines[j].c / 1000.;

            data[j].coalbedo.a = ssa_table[i].lines[j].a;
            data[j].coalbedo.b = ssa_table[i].lines[j].b;
            data[j].coalbedo.c = ssa_table[i].lines[j].c;

            data[j].g.a = asy_table[i].lines[j].a;
            data[j].g.b = asy_table[i].lines[j].b;
            data[j].g.c = asy_table[i].lines[j].c;
        }
    }

    return out;
}
}  // namespace rayli::io::hu_stamnes
