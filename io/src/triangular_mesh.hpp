#include "io/triangular_mesh.hpp"

using namespace rayli::io;

rayli::surface::TriangularMesh
load_triangular_mesh(const std::filesystem::path& filename) {
    using namespace netCDF;
    NcFile file(filename, NcFile::FileMode::read);
    auto vertices_var = file.getVar("vertices");

    rayli::surface::TriangularMesh mesh;
    mesh.vertices.resize(vertices_var.getDim(0).getSize());
    vertices_var.getVar({0, 0}, {mesh.vertices.size(), 3}, {1, 1}, {3, 1},
                        &mesh.vertices[0](0));
    return mesh;
}
