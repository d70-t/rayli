cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)
project (rayli_io)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set (PROJECT_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/include")
set (PROJECT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src")

include_directories(${PROJECT_INCLUDE_DIR})

list(
    APPEND RAYLI_IO_SRC
    ${PROJECT_SOURCE_DIR}/libradtran_files.cpp
    ${PROJECT_SOURCE_DIR}/hu_stamnes.cpp
    ${PROJECT_SOURCE_DIR}/recorder/recorder.cpp
    ${PROJECT_SOURCE_DIR}/parsers.cpp
)

if(HAVE_NETCDF)
    list(
        APPEND RAYLI_IO_SRC
        ${PROJECT_SOURCE_DIR}/mietable.cpp
        ${PROJECT_SOURCE_DIR}/icon.cpp
        ${PROJECT_SOURCE_DIR}/recorder/ncimage.cpp
        ${PROJECT_SOURCE_DIR}/triangular_mesh.cpp
        ${PROJECT_SOURCE_DIR}/wedgegrid.cpp
    )
endif(HAVE_NETCDF)

add_library(rayli_io ${RAYLI_IO_SRC})

target_link_libraries(
    rayli_io
    rayli_common
    ${EIGEN_LIBS}
    ${NETCDF_LIBS}
    ${MSGPACK_LIBS}
)

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
target_link_libraries(
    rayli_io
    stdc++fs
)
endif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
target_include_directories(rayli_io PUBLIC ${PROJECT_INCLUDE_DIR})
target_compile_options(rayli_io PRIVATE ${COVERAGE_FLAGS})
