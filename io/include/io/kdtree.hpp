#include <Eigen.hpp>
#include <io/msgpackutils.hpp>
#include <vgrid/KDTree.hpp>

#include <msgpack.hpp>

#include <iostream>
#include <iterator>

template <typename Value>
void dump_kdtree(std::ostream& stream,
                 const rayli::vgrid::KdTree<Value>& tree) {
    stream << "BOUNDS: " << tree.bounds << "\n";
    for (const auto& node : tree.nodes) {
        if (node.is_leaf()) {
            stream << "LEAF: " << node.u.leaf.value << "\n";
        } else {
            stream << "SPLIT: " << int(node.axis()) << " "
                   << node.u.internal.split << " "
                   << node.u.internal.second_child << "\n";
        }
    }
}

template <typename Value>
rayli::vgrid::KdTree<Value> read_kdtree_msgpack(std::istream& stream) {
    std::vector<char> data;
    std::copy(std::istreambuf_iterator<char>(stream),
              std::istreambuf_iterator<char>(), std::back_inserter(data));
    msgpack::object_handle oh = msgpack::unpack(data.data(), data.size());
    msgpack::object const& obj = oh.get();
    rayli::vgrid::KdTree<Value> kdt;
    obj >> kdt;
    return kdt;
}

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
    namespace adaptor {

    template <typename N, int D>
    struct convert<Eigen::Matrix<N, D, 1>> {
        msgpack::object const& operator()(msgpack::object const& o,
                                          Eigen::Matrix<N, D, 1>& v) const {
            if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
            if (o.via.array.size != D) throw msgpack::type_error();
            for (int i = 0; i < D; ++i) {
                o.via.array.ptr[i] >> v(i);
            }
            return o;
        }
    };

    template <typename N, int D>
    struct pack<Eigen::Matrix<N, D, 1>> {
        template <typename Stream>
        msgpack::packer<Stream>&
        operator()(msgpack::packer<Stream>& o,
                   Eigen::Matrix<N, D, 1> const& v) const {
            o.pack_array(D);
            for (int i = 0; i < D; ++i) {
                o.pack(v(i));
            }
            return o;
        }
    };

    template <typename N, int D>
    struct object_with_zone<Eigen::Matrix<N, D, 1>> {
        void operator()(msgpack::object::with_zone& o,
                        Eigen::Matrix<N, D, 1> const& v) const {
            o.type = type::ARRAY;
            o.via.array.size = D;
            o.via.array.ptr = static_cast<msgpack::object*>(
              o.zone.allocate_align(sizeof(msgpack::object) * o.via.array.size,
                                    MSGPACK_ZONE_ALIGNOF(msgpack::object)));
            for (int i = 0; i < D; ++i) {
                o.via.array.ptr[0] = msgpack::object(v(i), o.zone);
            }
        }
    };

    template <typename N>
    struct convert<Bounds3<N>> {
        msgpack::object const& operator()(msgpack::object const& o,
                                          Bounds3d& b) const {
            if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
            if (o.via.array.size != 2) throw msgpack::type_error();
            o.via.array.ptr[0] >> b.lower;
            o.via.array.ptr[1] >> b.upper;
            return o;
        }
    };
    template <typename N>
    struct pack<Bounds3<N>> {
        template <typename Stream>
        msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& o,
                                            Bounds3<N> const& b) const {
            o.pack_array(2);
            o.pack(b.lower);
            o.pack(b.upper);
            return o;
        }
    };
    template <typename N>
    struct object_with_zone<Bounds3<N>> {
        void operator()(msgpack::object::with_zone& o,
                        Bounds3<N> const& b) const {
            o.type = type::ARRAY;
            o.via.array.size = 2;
            o.via.array.ptr = static_cast<msgpack::object*>(
              o.zone.allocate_align(sizeof(msgpack::object) * o.via.array.size,
                                    MSGPACK_ZONE_ALIGNOF(msgpack::object)));
            o.via.array.ptr[0] = msgpack::object(b.lower, o.zone);
            o.via.array.ptr[1] = msgpack::object(b.upper, o.zone);
        }
    };

    template <typename Value>
    struct convert<rayli::vgrid::KdNode<Value>> {
        msgpack::object const&
        operator()(msgpack::object const& o,
                   rayli::vgrid::KdNode<Value>& n) const {
            if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
            if (o.via.array.size < 1) throw msgpack::type_error();
            o.via.array.ptr[0] >> n.kind_axis;
            if (n.is_leaf()) {
                if (o.via.array.size != 2) throw msgpack::type_error();
                o.via.array.ptr[1] >> n.u.leaf.value;
            } else {
                if (o.via.array.size != 3) throw msgpack::type_error();
                o.via.array.ptr[1] >> n.u.internal.split;
                o.via.array.ptr[2] >> n.u.internal.second_child;
            }
            return o;
        }
    };
    template <typename Value>
    struct pack<rayli::vgrid::KdNode<Value>> {
        template <typename Stream>
        msgpack::packer<Stream>&
        operator()(msgpack::packer<Stream>& o,
                   rayli::vgrid::KdNode<Value> const& n) const {
            if (n.is_leaf()) {
                o.pack_array(2);
                o.pack(n.kind_axis);
                o.pack(n.u.leaf.value);
            } else {
                o.pack_array(3);
                o.pack(n.kind_axis);
                o.pack(n.u.internal.split);
                o.pack(n.u.internal.second_child);
            }
            return o;
        }
    };
    template <typename Value>
    struct object_with_zone<rayli::vgrid::KdNode<Value>> {
        void operator()(msgpack::object::with_zone& o,
                        rayli::vgrid::KdNode<Value> const& n) const {
            o.type = type::ARRAY;
            if (n.is_leaf()) {
                o.via.array.size = 2;
                o.via.array.ptr =
                  static_cast<msgpack::object*>(o.zone.allocate_align(
                    sizeof(msgpack::object) * o.via.array.size,
                    MSGPACK_ZONE_ALIGNOF(msgpack::object)));
                o.via.array.ptr[0] = msgpack::object(n.kind_axis, o.zone);
                o.via.array.ptr[1] = msgpack::object(n.u.leaf.value, o.zone);
            } else {
                o.via.array.size = 3;
                o.via.array.ptr =
                  static_cast<msgpack::object*>(o.zone.allocate_align(
                    sizeof(msgpack::object) * o.via.array.size,
                    MSGPACK_ZONE_ALIGNOF(msgpack::object)));
                o.via.array.ptr[0] = msgpack::object(n.kind_axis, o.zone);
                o.via.array.ptr[1] =
                  msgpack::object(n.u.internal.split, o.zone);
                o.via.array.ptr[2] =
                  msgpack::object(n.u.internal.second_child, o.zone);
            }
        }
    };

    template <typename Value>
    struct convert<rayli::vgrid::KdTree<Value>> {
        msgpack::object const&
        operator()(msgpack::object const& o,
                   rayli::vgrid::KdTree<Value>& t) const {
            if (o.type != msgpack::type::MAP) throw msgpack::type_error();
            auto tidx = rayli::io::find_msgpack_key("type", o.via.map);
            if (tidx >= o.via.map.size) throw msgpack::type_error();
            auto& type = o.via.map.ptr[tidx].val;
            if (type.type != msgpack::type::STR ||
                std::string_view(type.via.str.ptr, type.via.str.size) !=
                  "kdtree")
                throw msgpack::type_error();
            auto bidx = rayli::io::find_msgpack_key("bounds", o.via.map);
            if (bidx >= o.via.map.size) throw msgpack::type_error();
            auto nidx = rayli::io::find_msgpack_key("nodes", o.via.map);
            if (nidx >= o.via.map.size) throw msgpack::type_error();
            o.via.map.ptr[bidx].val >> t.bounds;
            o.via.map.ptr[nidx].val >> t.nodes;
            return o;
        }
    };

    template <typename Value>
    struct pack<rayli::vgrid::KdTree<Value>> {
        template <typename Stream>
        msgpack::packer<Stream>&
        operator()(msgpack::packer<Stream>& o,
                   rayli::vgrid::KdTree<Value> const& t) const {
            o.pack_map(3);
            o.pack("type");
            o.pack("kdtree");
            o.pack("bounds");
            o.pack(t.bounds);
            o.pack("nodes");
            o.pack(t.nodes);
            return o;
        }
    };
    template <typename Value>
    struct object_with_zone<rayli::vgrid::KdTree<Value>> {
        void operator()(msgpack::object::with_zone& o,
                        rayli::vgrid::KdTree<Value> const& t) const {
            o.type = type::MAP;
            o.via.map.size = 3;
            o.via.map.ptr = static_cast<msgpack::object_kv*>(
              o.zone.allocate_align(sizeof(msgpack::object_kv) * o.via.map.size,
                                    MSGPACK_ZONE_ALIGNOF(msgpack::object_kv)));
            o.via.map.ptr[0].key = msgpack::object("type", o.zone);
            o.via.map.ptr[0].val = msgpack::object("kdtree", o.zone);
            o.via.map.ptr[1].key = msgpack::object("bounds", o.zone);
            o.via.map.ptr[1].val = msgpack::object(t.bounds, o.zone);
            o.via.map.ptr[2].key = msgpack::object("nodes", o.zone);
            o.via.map.ptr[2].val = msgpack::object(t.nodes, o.zone);
        }
    };
    }  // namespace adaptor
}
}  // namespace msgpack
