#pragma once

#include <msgpack.hpp>
#include <string_view>

namespace rayli::io {
inline uint32_t find_msgpack_key(std::string_view key,
                                 const msgpack::object_map& m) {
    auto ofs = std::find_if(m.ptr, m.ptr + m.size, [&](const auto& kv) {
        if (kv.key.type != msgpack::type::STR) return false;
        return std::string_view(kv.key.via.str.ptr, kv.key.via.str.size) == key;
    });
    return ofs - m.ptr;
}
}  // namespace rayli::io
