#pragma once

#include "io/parsers.hpp"
#include <Ray.hpp>

#include <yaml-cpp/yaml.h>

namespace YAML {

template <>
struct convert<Eigen::Vector2d> {
    static Node encode(const Eigen::Vector2d& v) {
        Node node;
        for (size_t i = 0; i < 2; ++i) {
            node.push_back(v(i));
        }
        return node;
    }
    static bool decode(const Node& node, Eigen::Vector2d& rhs) {
        if (!node.IsSequence()) return false;
        if (!(node.size() == 2)) return false;
        for (size_t i = 0; i < 2; ++i) {
            rhs(i) = node[i].as<double>();
        }
        return true;
    }
};

template <>
struct convert<Eigen::Vector3d> {
    static Node encode(const Eigen::Vector3d& v) {
        Node node;
        for (size_t i = 0; i < 3; ++i) {
            node.push_back(v(i));
        }
        return node;
    }
    static bool decode(const Node& node, Eigen::Vector3d& rhs) {
        if (!node.IsSequence()) return false;
        if (!(node.size() == 3)) return false;
        for (size_t i = 0; i < 3; ++i) {
            rhs(i) = node[i].as<double>();
        }
        return true;
    }
};

template <>
struct convert<Eigen::Quaterniond> {
    static Node encode(const Eigen::Vector3d& q) {
        Node node;
        for (size_t i = 0; i < 4; ++i) {
            node.push_back(q(i));
        }
        return node;
    }
    static bool decode(const Node& node, Eigen::Quaterniond& rhs) {
        if (!node.IsScalar()) return false;
        rhs = rayli::io::parsers::parse_rotation(node.as<std::string>());
        return true;
    }
};

template <>
struct convert<Ray> {
    static Node encode(const Ray& ray) {
        Node node;
        node["origin"] = ray.o;
        node["direction"] = ray.d;
        node["tmax"] = ray.tmax;
        node["taumax"] = ray.taumax;
        return node;
    }
    static bool decode(const Node& node, Ray& ray) {
        if (!node.IsMap()) return false;
        if (!node["origin"]) return false;
        if (!node["direction"]) return false;
        ray.o = node["origin"].as<Eigen::Vector3d>();
        ray.d = node["direction"].as<Eigen::Vector3d>();
        if (node["tmax"]) {
            ray.tmax = node["tmax"].as<double>();
        }
        if (node["taumax"]) {
            ray.taumax = node["taumax"].as<double>();
        }
        return true;
    }
};
}  // namespace YAML
