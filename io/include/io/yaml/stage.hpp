#pragma once

#include "Stage.hpp"
#include "filesystem.hpp"
#include <iostream>
#include <memory>

namespace rayli::io::yaml {

std::unique_ptr<Stage> load_stage(const std::filesystem::path& filename);
std::unique_ptr<Stage> load_stage(std::istream& input);
}  // namespace rayli::io::yaml
