#pragma once

#include <vector>
#include <io/filesystem.hpp>

namespace rayli::io {

struct Mietable {
    Mietable(const std::filesystem::path& filename);

    struct Phasetable {
        std::vector<double> theta;
        std::vector<double> value;
        double ext;  /// < extinction coefficient m^-1 / (g/m^3)
        double ssa;  /// < single scattering albedo
    };

    Phasetable& phasetable(size_t iwvln, size_t ireff);
    const Phasetable& phasetable(size_t iwvln, size_t ireff) const;
    std::vector<double> wvlns;
    std::vector<double> reffs;
    std::vector<Phasetable> phasetables;
};
}  // namespace rayli::io
