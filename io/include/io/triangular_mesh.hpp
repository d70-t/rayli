#pragma once

#include <surface/TriangularMesh.hpp>

#include "filesystem.hpp"

namespace rayli::io {
rayli::surface::TriangularMesh
load_triangular_mesh(const std::filesystem::path& filename);
}
