#pragma once

#include "filesystem.hpp"
#include "optprop/hu_stamnes.hpp"

namespace rayli::io::hu_stamnes {
rayli::optprop::hu_stamnes::Table
load_table(const std::filesystem::path& datapath);
}
