#pragma once

#include <istream>
#include <memory>
#include <vector>

#include <grid/regular_grid.hpp>

namespace rayli::io::libradtran {

class Cloud3DBase {
   public:
    // typedef rayli::grid::RegularGrid<1, double> ZGrid;
    typedef std::vector<double> ZGrid;

    virtual ~Cloud3DBase() {}

    Cloud3DBase(size_t Nx, size_t Ny, size_t Nz, double dx, double dy,
                ZGrid&& z);

    inline size_t Nx() const { return _Nx; }
    inline size_t Ny() const { return _Ny; }
    inline size_t Nz() const { return _Nz; }

    inline double dx() const { return _dx; }
    inline double dy() const { return _dy; }
    inline const ZGrid& z() const { return _z; }

   private:
    size_t _Nx, _Ny, _Nz;
    double _dx, _dy;
    ZGrid _z;
};

template <typename Member>
class Cloud3D : public Cloud3DBase {
   public:
    typedef rayli::grid::RegularGrid<3, Member> Grid;

    Cloud3D(double dx, double dy, ZGrid&& z, Grid&& data)
        : Cloud3DBase(data.shape(0), data.shape(1), data.shape(2), dx, dy,
                      std::move(z)) {
        _data = std::move(data);
    }

    const Grid& data() const { return _data; }

   private:
    Grid _data;
};

std::unique_ptr<Cloud3DBase> read_cloud_3d(std::istream& istream);
}  // namespace rayli::io::libradtran
