#pragma once

#include <atm.hpp>

#include <io/hu_stamnes.hpp>
#include <io/mietable.hpp>

#include <algorithm>
#include <string>
#include <cmath>

inline auto to_scatterer(const rayli::io::Mietable::Phasetable& t) {
    std::vector<double> mu;
    mu.resize(t.theta.size());
    std::transform(begin(t.theta), end(t.theta), begin(mu), [](double t){ return std::cos(t); });
    return TabulatedMuScat<std::mt19937_64>(std::move(mu), t.value) *
           (t.ssa * t.ext);
}

class MieLoader {
   public:
    inline MieLoader(const std::string& filename) : table(filename) {
        std::cerr << "wvlns: " << table.wvlns.front() << "nm ... "
                  << table.wvlns.back() << "nm\n";
        std::cerr << "reffs: " << table.reffs.front() << "µm ... "
                  << table.reffs.back() << "µm\n";
    }

    inline auto load(double wvln, double reff) const {
        auto i1 = std::lower_bound(begin(table.wvlns), end(table.wvlns), wvln);
        auto i2 = i1--;
        if (i2 == end(table.wvlns)) {
            --i2;
        }
        auto j1 = std::lower_bound(begin(table.reffs), end(table.reffs), reff);
        auto j2 = j1--;
        if (j2 == end(table.reffs)) {
            --j2;
        }
        auto fi = (wvln - *i1) / (*i2 - *i1);
        auto fj = (reff - *j1) / (*j2 - *j1);

        // std::cerr << "MIE wavelength weight: " << fi << "\n";
        // std::cerr << "MIE reff weight: " << fj << "\n";

        const auto& t11 =
          table.phasetable(i1 - begin(table.wvlns), j1 - begin(table.reffs));
        const auto& t12 =
          table.phasetable(i1 - begin(table.wvlns), j2 - begin(table.reffs));
        const auto& t21 =
          table.phasetable(i2 - begin(table.wvlns), j1 - begin(table.reffs));
        const auto& t22 =
          table.phasetable(i2 - begin(table.wvlns), j2 - begin(table.reffs));

        auto scatterer = (1 - fi) * (1 - fj) * to_scatterer(t11) +
                         fi * (1 - fj) * to_scatterer(t21) +
                         (1 - fi) * fj * to_scatterer(t12) +
                         fi * fj * to_scatterer(t22);
        auto ext = (1 - fi) * (1 - fj) * t11.ext + fi * (1 - fj) * t21.ext +
                   (1 - fi) * fj * t12.ext + fi * fj * t22.ext;
        return std::make_pair(scatterer, ext);
    }

   private:
    rayli::io::Mietable table;
};

class HSLoader {
   public:
    inline HSLoader(const std::string& foldername)
        : table(rayli::io::hu_stamnes::load_table(foldername)) {}
    inline auto load(double wvln, double reff) const {
        auto monohs_table = table.interpolate_to_wavelength(wvln * 1e-3);
        auto op =
          get_optprop(monohs_table, rayli::microphysics::LWC_reff{1., reff});
        std::cout << "optptop: g=" << op.g << " ssa=" << op.ssa
                  << " ext=" << op.ext << "\n";
        auto scatterer = HGScat<std::mt19937_64>(op.g);

        auto scat = scatterer * (op.ssa * op.ext);

        return std::make_pair(scat, op.ext);
    }

   private:
    rayli::optprop::hu_stamnes::Table table;
};
