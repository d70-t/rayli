#pragma once

#include "deduplicate.hpp"
#include "libradtran_files.hpp"
#include "microphysics_resolver.hpp"
#include "volume/PhysicalVolume.hpp"
#include "volume/RegularGridWithZ.hpp"

#include <memory>

namespace rayli::io::libradtran {

template <typename G, typename Member>
std::unique_ptr<RegularGridWithZ<G, double>> deduplicate_extract(
  const rayli::io::libradtran::Cloud3D<Member>& cloud,
  const rayli::microphysics::MicrophysicsResolver<double, G>& r,
  std::pmr::memory_resource* volume_memory) {
    std::vector<size_t> idx;
    idx.reserve(cloud.data().size());
    std::vector<Member> microphysics;

    deduplicate(cloud.data().begin(), cloud.data().end(),
                std::back_inserter(idx), std::back_inserter(microphysics));

    std::cout << "# of grid cells: " << idx.size()
              << " # of distinct cells: " << microphysics.size() << std::endl;

    std::pmr::polymorphic_allocator<PhysicalVolume<G>> phys_vol_allocator(
      volume_memory);
    auto phys_vols = phys_vol_allocator.allocate(microphysics.size());
    assert(phys_vols);

    for (size_t i = 0; i < microphysics.size(); ++i) {
        Scatterer<G>* scatterer = nullptr;
        if (!microphysics[i].is_empty()) {
            scatterer = r.resolve(microphysics[i], volume_memory);
        }
        phys_vol_allocator.construct(phys_vols + i, scatterer);
    }

    rayli::grid::RegularGrid<3, Volume<G>*> grid;
    grid.resize(cloud.data().shape(0), cloud.data().shape(1),
                cloud.data().shape(2));

    std::transform(idx.begin(), idx.end(), grid.begin(),
                   [&](size_t i) { return &phys_vols[i]; });
    return std::make_unique<RegularGridWithZ<G, double>>(
      grid, cloud.z(),
      Bounds3d{
        {0, 0, cloud.z().front()},
        {cloud.Nx() * cloud.dx(), cloud.Ny() * cloud.dy(), cloud.z().back()}});
}

template <typename G>
std::unique_ptr<RegularGridWithZ<G, double>>
cloud_2_volume(const rayli::io::libradtran::Cloud3DBase& cloud,
               const rayli::microphysics::MicrophysicsResolver<double, G>& r,
               std::pmr::memory_resource* volume_memory) {
    using namespace rayli::io::libradtran;
    if (auto cl = dynamic_cast<const Cloud3D<rayli::optprop::HG>*>(&cloud);
        cl) {
        return deduplicate_extract<G>(*cl, r, volume_memory);
    }
    if (auto cl =
          dynamic_cast<const Cloud3D<rayli::microphysics::ext_reff>*>(&cloud);
        cl) {
        return deduplicate_extract<G>(*cl, r, volume_memory);
    }
    if (auto cl =
          dynamic_cast<const Cloud3D<rayli::microphysics::LWC_reff>*>(&cloud);
        cl) {
        return deduplicate_extract<G>(*cl, r, volume_memory);
    }
    throw rayli::exceptions::InvalidArgument(
      "invalid cloud 3d instance supplied");
}
}  // namespace rayli::io::libradtran
