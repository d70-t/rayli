#pragma once
#include "filesystem.hpp"
#include <vgrid/WedgeGrid.hpp>

#include "io/netcdf_traits.hpp"
#include <netcdf>

#include "texture/texture.hpp"

namespace rayli::io {
void to_netcdf(std::filesystem::path filename,
               const rayli::vgrid::WedgeGrid& grid);
class WedgeGridWriter {
   public:
    WedgeGridWriter(std::filesystem::path filename,
                    const rayli::vgrid::WedgeGrid& grid);
    template <typename T>
    void add_cell_variable(const std::string& name,
                           const Texture<T, size_t>& tex) {
        std::vector<T> temp(celldim.getSize());
        std::generate(temp.begin(), temp.end(),
                      [i = 0, &tex]() mutable { return tex(i++); });
        add_cell_variable(name, temp);
    }
    template <typename T>
    void add_cell_variable(const std::string& name,
                           const std::vector<T>& data) {
        auto var = file.addVar(name, NCTypeInfo<T>::nctype, {celldim});
        var.putVar(data.data());
    }
    template <typename T>
    void add_face_variable(const std::string& name,
                           const std::vector<T>& data) {
        auto var = file.addVar(name, NCTypeInfo<T>::nctype, {facedim});
        var.putVar(data.data());
    }

   private:
    netCDF::NcFile file;
    netCDF::NcDim vertexdim, celldim, facedim, vdim, cdim, fdim;
};
}  // namespace rayli::io
