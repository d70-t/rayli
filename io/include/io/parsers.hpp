#pragma once

#include "Eigen.hpp"

#include <string>

namespace rayli::io::parsers {

Eigen::Quaterniond parse_rotation(const std::string& rotation_string);
}
