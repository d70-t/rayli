#pragma once
#include <netcdf>

namespace rayli::io {
template <typename T>
struct NCTypeInfo {
    static_assert("NCTypeInfo is not defined for this type");
};
template <>
struct NCTypeInfo<float> {
    static inline const netCDF::NcType nctype = netCDF::NcType::nc_FLOAT;
};
template <>
struct NCTypeInfo<double> {
    static inline const netCDF::NcType nctype = netCDF::NcType::nc_DOUBLE;
};
template <>
struct NCTypeInfo<size_t> {
    static inline const netCDF::NcType nctype = netCDF::NcType::nc_UINT64;
};
}  // namespace rayli::io
