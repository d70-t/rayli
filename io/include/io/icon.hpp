#pragma once

#include "filesystem.hpp"
#include "grid/ICONGrid.hpp"
#include "microphysics.hpp"

namespace rayli::io::icon {
std::unique_ptr<rayli::grid::ICONGrid>
load_netcdf(const std::filesystem::path& filename);

/** \brief reads microphysics from ICON Model output
 *
 * Conversion into \f$r_{\rm eff}\f$ and \f${\rm LWC}\f$ is done following
 * Bugliaro (2011):
 *
 * \f[
 * r_{\rm eff} = \left(0.75 \frac{\rm LWC}{\pi k N \rho}\right)^{1/3} 10^{-6}
 * \f]
 *
 * \sa https://www.atmos-chem-phys.net/11/5603/2011/acp-11-5603-2011.html
 */
std::vector<rayli::microphysics::LWC_reff>
load_LWC_reff_from_model(const std::filesystem::path& filename,
                         size_t timestep);

/** \brief load levels from file
 *
 * The ascii file should contain one z level per line (floating point), starting
 * with the lowest z level in the first line.
 */
std::vector<double> load_levels(const std::filesystem::path& filename,
                                double sphere_radius);
}  // namespace rayli::io::icon
