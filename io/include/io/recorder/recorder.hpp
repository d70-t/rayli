#pragma once

#include "Eigen.hpp"

namespace rayli::io::recorder {

class Collector {
   public:
    Collector(size_t nx, size_t ny);

    inline size_t nx() const { return _nx; }
    inline size_t ny() const { return _ny; }
    inline void collect(double L, double count, size_t i, size_t j) {
        _L(i, j) += L;
        _count(i, j) += count;
    }
    inline const Eigen::ArrayXXd& L() const { return _L; }
    inline const Eigen::ArrayXXd& count() const { return _count; }

    Collector& operator+=(const Collector& other);

   private:
    size_t _nx, _ny;
    Eigen::ArrayXXd _L;
    Eigen::ArrayXXd _count;
};

class ImageRecorder {
   public:
    ImageRecorder(size_t nx, size_t ny)
        : _xstart(0), _xend(nx), _ystart(0), _yend(ny) {}
    virtual ~ImageRecorder() = 0;
    virtual size_t nx() const = 0;
    virtual size_t ny() const = 0;
    virtual Collector get_collector() = 0;
    virtual void return_collector(Collector&& collector) = 0;
    virtual void finish() = 0;

    inline size_t xstart() const { return _xstart; }
    inline size_t xend() const { return _xend; }
    inline size_t ystart() const { return _ystart; }
    inline size_t yend() const { return _yend; }
    inline void set_xlims(size_t xstart, size_t xend) {
        _xstart = xstart;
        _xend = xend;
    };
    inline void set_ylims(size_t ystart, size_t yend) {
        _ystart = ystart;
        _yend = yend;
    };

   private:
    size_t _xstart, _xend, _ystart, _yend;
};
}  // namespace rayli::io::recorder
