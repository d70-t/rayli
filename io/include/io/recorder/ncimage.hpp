#pragma once

#include "io/filesystem.hpp"
#include "recorder.hpp"

#include <mutex>

namespace rayli::io::recorder {

class NcImageRecorder : public ImageRecorder {
   public:
    NcImageRecorder(const std::filesystem::path& filename, size_t nx,
                    size_t ny);

    size_t nx() const override;
    size_t ny() const override;
    Collector get_collector() override;
    void return_collector(Collector&& collector) override;
    void finish() override;

   private:
    Collector collector;
    std::mutex collect_mutex;
    std::filesystem::path filename;
};
}  // namespace rayli::io::recorder
