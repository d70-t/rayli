#!/bin/bash
if [! -d "build_gcov" ]; then
    mkdir build_gcov
fi

cd build_gcov
cmake .. -DBUILD_FOR_COVERAGE=ON || exit -1
make -j || exit -1
test/run_test
gcovr -r .. -e build_gcov -e test -e 3rdparty --html --html-details --exclude-unreachable-branches -o coverage.html
xdg-open coverage.html
