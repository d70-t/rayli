import os
import re
import numpy as np
import xarray as xr

import xml.etree.ElementTree as ET
from xmftools.utils import xmf_varinfo, XMFFile, gen_attribute, hdf_dataitem

BOUNDS_RE = re.compile(r"<Bounds3 \[([^,]+),([^,]+),([^,]+)\] \[([^,]+),([^,]+),([^,]+)\]>")
SPLIT_RE = re.compile(r"SPLIT: ([0-2]) ([^ ]+) ([0-9]+)")
LEAF_RE = re.compile(r"LEAF: (.+)")

class Node(object):
    pass

class Leaf(Node):
    def __init__(self, value):
        self.value = value

class Split(Node):
    def __init__(self, axis, split, a, b):
        self.axis = axis
        self.split = split
        self.a = a
        self.b = b

class Tree(object):
    def __init__(self, bounds, rootnode):
        self.bounds = bounds
        self.rootnode = rootnode
    def leaf_bounds_iter(self):
        node_stack = [(self.rootnode, self.bounds)]
        while node_stack:
            current_node, bounds = node_stack.pop()
            if isinstance(current_node, Leaf):
                yield bounds
            else:
                a_bounds = bounds.copy()
                b_bounds = bounds.copy()
                a_bounds[1, current_node.axis] = current_node.split
                b_bounds[0, current_node.axis] = current_node.split
                node_stack.append((current_node.b, b_bounds))
                node_stack.append((current_node.a, a_bounds))
    def leaf_value_iter(self):
        node_stack = [self.rootnode]
        while node_stack:
            current_node = node_stack.pop()
            if isinstance(current_node, Leaf):
                yield current_node.value
            else:
                node_stack.append(current_node.b)
                node_stack.append(current_node.a)

def load_node_from_tuples(node_id, nodes):
    node = nodes[node_id]
    if node[0] == "leaf":
        return Leaf(node[1])
    elif node[0] == "split":
        axis, split, second_child = node[1:]
        return Split(axis, split,
                     load_node_from_tuples(node_id + 1, nodes),
                     load_node_from_tuples(second_child, nodes))
    else:
        raise ValueError("unknown node type: \"{}\"".format(node[0]))

def parse_tree(lines):
    nodes = []
    for line in lines:
        m = BOUNDS_RE.search(line)
        if m:
            bounds = np.array(map(float, m.groups())).reshape(2,3)
            continue
        m = SPLIT_RE.search(line)
        if m:
            nodes.append(("split", int(m.group(1)), float(m.group(2)), int(m.group(3))))
            continue
        m = LEAF_RE.search(line)
        if m:
            nodes.append(("leaf", m.group(1)))
            continue
    return Tree(bounds, load_node_from_tuples(0, nodes))

def parse_msgpack_tree(msgdata):
    import msgpack
    data = msgpack.load(msgdata)
    assert(data["type"] == "kdtree")
    bounds = np.array(data["bounds"], dtype="float")
    nodes = data["nodes"]
    def load_nodes(node_id):
        node = nodes[node_id]
        if node[0] == 4:
            return Leaf(node[1])
        else:
            axis, split, second_child = node
            return Split(axis, split, load_nodes(node_id + 1), load_nodes(second_child))
    return Tree(bounds, load_nodes(0))

def selector(idx):
    sel = np.zeros((2,3))
    for i, j in enumerate(idx):
        sel[j,i] = 1
    return sel

BOUNDS2CORNERS = np.array(map(selector, [
    [0,0,0],
    [1,0,0],
    [1,1,0],
    [0,1,0],
    [0,0,1],
    [1,0,1],
    [1,1,1],
    [0,1,1],
]))

def bounds2corners(bounds):
    return np.einsum("...ij,kij->...kj", bounds, BOUNDS2CORNERS)

def create_xmf(ds_filename, xmf_filename):
    ds = xr.open_dataset(ds_filename)
    rel_filename = os.path.relpath(ds_filename, os.path.dirname(xmf_filename))
    xmf = XMFFile()
    grid = ET.SubElement(xmf.domain, "Grid", {"Name": "Mesh", "GridType": "Uniform"})
    topology = ET.SubElement(grid, "Topology",
        {
            "TopologyType": "Hexahedron",
            "NumberOfElements": str(len(ds.hexahedra)),
        })
    data = ET.SubElement(topology, "DataItem", xmf_varinfo(ds.hexahedra))
    data.text = "{}:/hexahedra".format(rel_filename)
    geometry = ET.SubElement(grid, "Geometry", {"GeometryType": "XYZ"})
    data = ET.SubElement(geometry, "DataItem", xmf_varinfo(ds.vertices))
    data.text = "{}:/vertices".format(rel_filename)

    attribute = gen_attribute("values", ds["values"])
    hdf_dataitem(attribute, rel_filename, ds, "values")
    grid.append(attribute)

    xmf.write(xmf_filename)

def _main():
    import argparse
    parser = argparse.ArgumentParser("kdtree2visit")
    parser.add_argument("infile")
    parser.add_argument("outfile")
    parser.add_argument("-m", "--msgpack", default=False, action="store_true", help="use msgpack kdtree data")

    args = parser.parse_args()

    with open(args.infile) as fin:
        if args.msgpack:
            tree = parse_msgpack_tree(fin)
        else:
            tree = parse_tree(fin)
    leaf_bounds = np.stack(list(tree.leaf_bounds_iter()), axis=0)
    leaf_corners = bounds2corners(leaf_bounds)
    vertices, indices = np.unique(leaf_corners.reshape(-1,3), return_inverse=True, axis=0)
    indices = indices.reshape(-1, 8)
    values = np.array(list(tree.leaf_value_iter()))

    ds = xr.Dataset({
        "vertices": xr.DataArray(vertices, dims=("vertex", "v")),
        "hexahedra": xr.DataArray(indices, dims=("hexahedron", "h")),
        "values": xr.DataArray(values, dims=("hexahedron")),
        })
    ds.to_netcdf(args.outfile)
    create_xmf(args.outfile, args.outfile + ".xmf")

if __name__ == '__main__':
    _main()
