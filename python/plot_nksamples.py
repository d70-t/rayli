import numpy as np
import matplotlib.pylab as plt

def _main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    data = np.loadtxt(args.filename, delimiter=",")
    wvl, reff, sza, saz, vza, vaz, t_e, t_s = data.T

    wvls, wvl_idx = np.unique(wvl, return_inverse=True)
    vzas, vza_idx = np.unique(vza, return_inverse=True)

    vzas = np.rad2deg(vzas)

    t = np.zeros((len(wvls), len(vzas))) * np.nan
    s = np.zeros((len(wvls), len(vzas))) * np.nan

    t[wvl_idx, vza_idx] = t_e
    s[wvl_idx, vza_idx] = t_s

    for(i, wvl) in enumerate(wvls):
        plt.fill_between(vzas, t[i]+s[i], t[i]-s[i], color="C{}".format(i), alpha=.3)

    for(i, wvl) in enumerate(wvls):
        plt.plot(vzas, t[i], color="C{}".format(i), label="{} nm".format(wvl))

    plt.show()

if __name__ == "__main__":
    _main()
