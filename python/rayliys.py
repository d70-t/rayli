import os
import yaml
import jinja2
import sh

SLURM_DEFAULTS = {
    "partition": "ws",
    "mem": "16G",
    "time": "04:00:00",
    "mail-type": "END",
    "ntasks": "1",
    "exclusive": None,
    "export": "NONE",
    "nice": "100",
    "requeue": None,
}

JOB_TEMPLATE = jinja2.Template("""#!/bin/bash -l
{% for key, value in slurmconfig.items() -%}
#SBATCH --{{ key }}{% if value: %}={{ value }}{% endif %}
{% endfor %}

. {{ envpath }}
{{ rayliy }} << EOFDXFGABPQ
{{ rayli_config }}

EOFDXFGABPQ
""")

PYTHON_JOB_TEMPLATE = jinja2.Template("""#!/bin/bash -l
{% for key, value in slurmconfig.items() -%}
#SBATCH --{{ key }}{% if value: %}={{ value }}{% endif %}
{% endfor %}

python {{ script }} {{ args }}
""")

BASEPATH = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BINPATH = os.path.join(BASEPATH, "build_cluster", "bin")
RAYLIY = os.path.join(BINPATH, "rayliy")
COMBINE = os.path.join(BASEPATH, "python", "combine_nc.py")
IMAGE2PNG = os.path.join(BASEPATH, "python", "image2png.py")
ENVPATH = os.path.join(BASEPATH, "build_cluster", "activate.sh")

def join_dict(a, b):
    return dict(list(a.items()) + list(b.items()))

def dir_for_file(filename, ext):
    d = os.path.splitext(filename)[0] + ext
    if not os.path.exists(d):
        os.makedirs(d)
    return d

def _main():
    dryrun = False

    import sys
    try:
        configfile = open(sys.argv[1])
    except IndexError:
        configfile = sys.stdin
    config = yaml.load(configfile)
    if "slurm" in config:
        slurmconfig = config["slurm"]
        del config["slurm"]
    else:
        slurmconfig = {}
    slurmextra = join_dict(SLURM_DEFAULTS, slurmconfig.get("extra", {}))
    jobcount = int(slurmconfig.get("jobcount", 1))

    final_slurm_config = slurmextra.copy()

    recorder_path = config["scene"]["camera"]["recorder"]["path"]

    if jobcount > 1:
        resultdir = dir_for_file(recorder_path, ".partial")
        recorder_path_patttern = os.path.join(resultdir, "{}" + os.path.splitext(recorder_path)[1])
        config["scene"]["camera"]["recorder"]["path"] = recorder_path_patttern.format("${SLURM_ARRAY_TASK_ID}")
        final_slurm_config["array"] = "0-{}".format(jobcount - 1)

    logdir = dir_for_file(recorder_path, ".log")
    final_slurm_config["output"] = os.path.join(logdir, "%A_%a.out")
    final_slurm_config["error"] = os.path.join(logdir, "%A_%a.err")
    
    sbatch_input = JOB_TEMPLATE.render(slurmconfig=final_slurm_config,
                                       envpath=ENVPATH,
                                       rayliy=RAYLIY,
                                       rayli_config=yaml.dump(config))

    print sbatch_input

    if dryrun:
        rayli_job_id = "RAYLI_JOB_ID"
    else:
        res = sh.sbatch("--parsable", _in=sbatch_input)
        rayli_job_id = int(res)

    def aux_job_config(parent_id):
        config = {
            "partition": final_slurm_config["partition"],
            "time": "01:00:00",
            "ntasks": 1,
            "mem": "16G",
            "export": "NONE",
            "mail-type": final_slurm_config["mail-type"],
            "dependency": "afterok:{}".format(parent_id),
        }
        if "mail-user" in final_slurm_config:
            config["mail-user"] = final_slurm_config["mail-user"]
        return config

    if jobcount > 1:
        combine_input = PYTHON_JOB_TEMPLATE.render(slurmconfig=aux_job_config(rayli_job_id),
                                                   script=COMBINE,
                                                   args=" ".join([recorder_path, recorder_path_patttern.format("*")]))

        print combine_input

        if dryrun:
            combine_job_id = "COMBINE_JOB_ID"
        else:
            res = sh.sbatch("--parsable", _in=combine_input)
            combine_job_id = int(res)
        output_job_id = combine_job_id
    else:
        output_job_id = rayli_job_id


    image_name = os.path.splitext(recorder_path)[0] + ".png"
    image_input = PYTHON_JOB_TEMPLATE.render(slurmconfig=aux_job_config(output_job_id),
                                             script=IMAGE2PNG,
                                             args=" ".join([recorder_path, image_name]))

    print image_input

    if dryrun:
        image_job_id = "IMAGE_JOB_ID"
    else:
        res = sh.sbatch("--parsable", _in=image_input)
        image_job_id = int(res)



    if not dryrun:
        print "running simulation in SLURM JOB {}".format(rayli_job_id)
        if jobcount > 1:
            print "collecting results in SLURM JOB {}".format(combine_job_id)
        print "creating output image in SLURM JOB {}".format(image_job_id)



if __name__ == '__main__':
    _main()
