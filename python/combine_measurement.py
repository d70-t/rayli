import xarray as xr

def _main():
    import sys

    tofile = sys.argv[1]
    fromfiles = sys.argv[2:]

    ds = xr.open_mfdataset(fromfiles, concat_dim="arbitrary", data_vars="different")

    print(ds)

    data = ds["data"].sum(dim="arbitrary")
    if "arbitrary" in ds["count"].dims:
        count = ds["count"].sum(dim="arbitrary")
    else:
        count = ds["count"] * len(fromfiles)

    combined = xr.Dataset({
        "wavelength": ds.wavelength,
        "transmission": data / count,
        "data": data,
        "count": count,
        })
    for var in ["outgoing_rays", "viewdir", "sun_rays", "zenith", "surface_normals", "shadow_flag", "lwp_ref", "lwp_true", "x"]:
        if var in ds:
            combined[var] = ds[var]
    combined.to_netcdf(tofile)

if __name__ == '__main__':
    _main()
