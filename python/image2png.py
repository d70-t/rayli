from PIL import Image
import xarray as xr
import numpy as np

def _main():
    import sys

    fromfile = sys.argv[1]
    tofile = sys.argv[2]
    ds = xr.open_dataset(fromfile)

    if len(sys.argv) > 3:
        f = float(sys.argv[3])
    else:
        f = np.percentile(ds.radiance.data, 99)

    img = Image.fromarray(np.minimum((ds.radiance.data / f)**.5 * 255, 255).astype("uint8")[::-1])
    img.save(tofile)

if __name__ == '__main__':
    _main()
