import numpy as np
import scipy
from scipy.optimize import minimize
import sympy

def f(x, sigma, m=np):
    return -(x/sigma**2)*m.exp(-(x**2/(2*sigma**2)))

class Leaf(object):
    def __init__(self, value):
        self.value = value

class Split(object):
    def __init__(self, axis, loc, a, b):
        self.axis = axis
        self.loc = loc
        self.a = a
        self.b = b

def serialize_tree(tree):
    EXPAND = 1
    REF = 2
    todo = [(EXPAND, tree)]
    out = []
    while len(todo) > 0:
        task, node = todo.pop()
        if task == EXPAND:
            if isinstance(node, Leaf):
                out.append([4, node.value])
            else:
                todo.append((EXPAND, node.b))
                todo.append((REF, len(out)))
                todo.append((EXPAND, node.a))
                out.append([node.axis, node.loc, None])
        elif task == REF:
            out[node][-1] = len(out)
    return out

class Gridlifyer(object):
    def __init__(self, f, xsym):
        asym, bsym = map(sympy.Symbol, "ab")
        h = (sympy.integrate(f, (xsym, asym, bsym))).simplify()
        cost = (f * (bsym - asym) - h)**2
        self.cost_int = sympy.integrate(cost, (xsym, asym, bsym)).simplify()
        self.cost_l = sympy.lambdify([asym, bsym], self.cost_int,
                                     modules=['numpy', {'erf': scipy.special.erf}])
        self.h_l = sympy.lambdify([asym, bsym], h / (bsym - asym),
                                  modules=['numpy', {'erf': scipy.special.erf}])

    def cost_l_2(self, a, b, c):
        cost = 0
        if a != c:
            cost += self.cost_l(a, c)
        if c != b:
            cost += self.cost_l(c, b)
        return cost

    def cost_step(self, a, b, c):
        return abs(self.h_l(a, c) - self.h_l(b, c))
    
    def cost(self, a, b, c):
        return self.cost_l_2(a, b, c) + self.cost_step(a, b, c)

    def find_points(self, a, b, maxcost=0.05):
        todo = [(a, b)]
        yield a
        while len(todo) > 0:
            ai, bi = todo.pop()
            ci = (ai + bi) / 2
            res = minimize(lambda c: self.cost(ai, bi, c[0]), [ci], bounds=[(ai, bi)])
            c = res.x[0]
            if c == ai or c == bi or np.isnan(res.fun):
                costval = self.cost(ai, bi, ci)
                c = ci
            else:
                costval = res.fun
            if costval >= maxcost:
                todo.append((ai, c))
                todo.append((c, bi))
            yield c
        yield b

    def hvals(self, points):
        for a, b in zip(points[:-1], points[1:]):
            yield self.h_l(a, b)

    def create_2d_grid(self, bounds, maxcost=1e-4):
        points = np.unique(list(self.find_points(bounds[0][0], bounds[1][0], maxcost)))
        hvals = np.array(list(self.hvals(points)))
        hnan, = np.where(np.isnan(hvals))
        hmax = np.max(hvals)
        hmin = np.min(hvals)
        print("len: ", len(hvals))
        assert(hmax <= bounds[1][1])
        assert(hmin >= bounds[0][1])
        return self._create_2d_grid(bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], hmin, hmax, points, hvals)

    def _create_2d_grid(self, x0, x1, y0, y1, hmin, hmax, points, hvals):
        if hmax < y1:
            return Split(1, hmax,
                         self._create_2d_grid(x0, x1, y0, hmax, hmin, hmax, points, hvals),
                         Leaf(1))
        if hmin > y0:
            return Split(1, hmin,
                         Leaf(0),
                         self._create_2d_grid(x0, x1, hmin, y1, hmin, hmax, points, hvals))
        if len(hvals) == 1:
            return Split(1, hvals[0], Leaf(0), Leaf(1))
        else:
            xsplit = int(len(hvals)/2)
            pa = points[:xsplit+1]
            pb = points[xsplit:]
            ha = hvals[:xsplit]
            hb = hvals[xsplit:]
            return Split(0, points[xsplit],
                         self._create_2d_grid(x0, points[xsplit], y0, y1, np.min(ha), np.max(ha), pa, ha),
                         self._create_2d_grid(points[xsplit], x1, y0, y1, np.min(hb), np.max(hb), pb, hb))

def _main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--outfile", default=None)
    parser.add_argument("-s", "--show", default=False, action="store_true")
    parser.add_argument("-q", "--show-quick", default=False, action="store_true")
    parser.add_argument("-S", "--sigma", type=float, default=1.)
    parser.add_argument("-w", "--width", type=float, default=1.)
    parser.add_argument("--sx", type=float, default=10.)
    parser.add_argument("--sy", type=float, default=5.)
    parser.add_argument("--sz", type=float, default=10.)
    parser.add_argument("-m", "--max-cost", type=float, default=1.)

    args = parser.parse_args()
    print args

    x = sympy.Symbol("x")
    def func(x, m=np):
        return f(x/args.width, args.sigma, m=m) * args.width

    if not args.show_quick:
        fsym = func(x, m=sympy)
        gridlifyer = Gridlifyer(fsym, x)
    bounds = [[-args.sx, -args.sy, -args.sz], [args.sx, args.sy, args.sz]]
    if args.show:
        import matplotlib.pyplot as plt
        points = np.unique(list(gridlifyer.find_points(bounds[0][0], bounds[1][0], args.max_cost)))
        hvals = np.array(list(gridlifyer.hvals(points)))
        plt.step(points, np.concatenate([hvals, [hvals[-1]]]), where="post")
        xvals = np.linspace(bounds[0][0], bounds[1][0], 10000)
        plt.plot(xvals, func(xvals))
        plt.show()
    elif args.show_quick:
        import matplotlib.pyplot as plt
        xvals = np.linspace(bounds[0][0], bounds[1][0], 10000)
        plt.plot(xvals, func(xvals))
        plt.axhline(bounds[0][1], color="orange")
        plt.axhline(bounds[1][1], color="orange")
        plt.show()
    else:
        import msgpack
        tree = gridlifyer.create_2d_grid(bounds, args.max_cost)
        kdt = {"type": "kdtree", "bounds": bounds, "nodes": serialize_tree(tree)}
        with open(args.outfile, "w") as of:
            msgpack.dump(kdt, of)

if __name__ == "__main__":
    _main()
