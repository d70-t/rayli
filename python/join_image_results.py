import xarray as xr

def _main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("outfile")
    parser.add_argument("infiles", nargs="+")
    args = parser.parse_args()

    ds = xr.open_mfdataset(args.infiles, concat_dim="runs", autoclose=True)
    data = ds["data"].sum(dim="runs")
    count = ds["count"].sum(dim="runs")
    transmissivity = data / count
    xr.Dataset({"data": data, "count": count, "transmissivity": transmissivity}).to_netcdf(args.outfile)

if __name__ == "__main__":
    _main()
