import xarray as xr
import matplotlib.pylab as plt

def _main():
    import sys
    ds = xr.open_dataset(sys.argv[1])
    ((ds.data / ds["count"])**.5).T.plot(cmap="gray")
    plt.show()

if __name__ == "__main__":
    _main()
