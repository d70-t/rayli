import os
import yaml
import visit
import sh

import xarray as xr
import matplotlib.pyplot as plt

RAYLIY = sh.Command(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "build_cluster", "bin", "rayliy")))

def render(gridfile, datafile, baseconfig):
    view3d = visit.GetView3D()
    config = yaml.load(open(baseconfig))
    recorder = config["scene"]["camera"]["recorder"]

    camera = {"type": "visit",
              "focus": list(view3d.focus),
              "view normal": list(view3d.viewNormal),
              "view up": list(view3d.viewUp),
              "view angle": view3d.viewAngle,
              "parallel scale": view3d.parallelScale,
              "image pan": list(view3d.imagePan),
              "image zoom": view3d.imageZoom,
              "perspective": view3d.perspective == 1,
              "recorder": recorder,
              }
    
    print(yaml.dump(camera))

    config["scene"]["camera"] = camera

    for line in RAYLIY(_in=yaml.dump(config), _iter=True):
        print(line)
    
    ds = xr.open_dataset(config["scene"]["camera"]["recorder"]["path"])
    plt.imshow(ds.radiance.data**0.5, origin="lower")
    plt.show()
    return ds

