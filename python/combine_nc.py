import xarray as xr

def _main():
    import sys

    tofile = sys.argv[1]
    fromfiles = sys.argv[2:]

    ds = xr.open_mfdataset(fromfiles, concat_dim="arbitrary").chunk({"x": 100, "y": 100})

    combined = xr.Dataset({"n": ds.n.sum(dim="arbitrary")})
    combined["radiance"] = (ds.radiance * ds.n).sum(dim="arbitrary") / combined.n

    combined.to_netcdf(tofile)

if __name__ == '__main__':
    _main()
