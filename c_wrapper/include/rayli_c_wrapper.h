#ifndef __RAYLI_C_WRAPPER_H__
#define __RAYLI_C_WRAPPER_H__

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/** \brief version of rayli */
const char* rayli_version();

/** \brief compute fluxes using forward ray tracing
 *
 * @param Nthreads number of threads to use, if Nthreads == 0, we let the environment decide
 * @param verts_of_face vertex indices of faces
 * @param wedges_of_face indices of wedges that lie on either side of a face,
 * dim Nfaces * 2
 * @param vert_coords cartesian coordinates of vertices dim Nverts*3
 * @param kabs absorption coefficient for each wedge, units [1/m], dim Nwedges
 * @param ksca scattering coefficient for each wedge, units [1/m], dim Nwedges
 * @param g asymerty parameter for each wedge, units [1/m, 1/m, 1/1], dim Nwedges
 * @param albedo on each face, use -1 to make it transparent, or values between [0,1] for lambertian reflectors
 * @param sundir cartesian vector towards sun, length of vec is [W/m2], dim 3
 * @param flx_through_faces  irradiance through each face, dim Nfaces
 * @param abso_in_cells  absorption in each cell, dim Nwedges, unit [W/cell_volume]
 */
int rfft_wedge(
        size_t Nthreads,
        size_t Nphotons, size_t Nwedges, size_t Nfaces, size_t Nverts, int cyclic,
        size_t* verts_of_face, size_t* wedges_of_face, double* vert_coords,
        float* kabs, float* ksca, float* g,
        float* albedo_on_faces,
        float* sundir,
        double* flx_through_faces_edir,
        double* flx_through_faces_ediff,
        double* abso_in_cells);

int rfft_wedge_thermal(
    size_t num_threads,
    const size_t Nphotons, const size_t Nwedges,
    const size_t Nfaces, const size_t Nverts, int cyclic,
    const size_t* verts_of_face, const size_t* faces_of_wedges,
    const double* vert_coords,
    const float* kabs, const float* ksca, const float* g,
    const float* albedo_on_faces,
    const float* B_on_faces,
    const float* B_on_surfaces,
    double* flx_through_faces_ediff,
    double* abso_in_cells);

int rpt_img_wedge(
        size_t Nthreads,
        size_t img_Nx,
        size_t img_Ny,
        size_t Nphotons,
        size_t Nwedges,
        size_t Nfaces,
        size_t Nverts,
        int    cyclic,
        const size_t* verts_of_face,
        const size_t* wedges_of_face,
        const double* vert_coords,
        const float* kabs, const float* ksca, const float* g,
        const float* albedo_on_faces,
        const float* sundir,
        const float* cam_location,
        const float* cam_viewing_dir,
        const float* cam_up_vec,
        float fov_width, float fov_height,
        float* img);

#ifdef __cplusplus
}
#endif

#endif
