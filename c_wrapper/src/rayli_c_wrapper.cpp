#include <Eigen/Dense>
#include <algorithm>
#include <array>
#include <atomic>
#include <exception>
#include <iomanip>
#include <iostream>
#include <memory>
#include <vector>

#include <fmt/format.h>

#include "CircularRaySource.hpp"
#include "FarSink.hpp"
#include "ForwardThermalFluxTracer.hpp"
#include "ForwardsFluxTracer.hpp"
#include "PathTracer.hpp"
#include "RectangularLambertianRaySource.hpp"
#include "RectangularRaySource.hpp"
#include "VarianceReduction.hpp"
#include "atm.hpp"
#include "camera.hpp"
#include "material.hpp"
#include "rayli.hpp"
#include "rayli_c_wrapper.h"
#include "recorder.hpp"
#include "surface/TriangularMesh.hpp"
#include "texture/texture.hpp"
#include "thread_support.hpp"
#include "vgrid/ConvexPolytopes.hpp"
#include "vgrid/Cyclic.hpp"
#include "vgrid/TriCell.hpp"

const char* rayli_version() { return rayli::version().numeric_version; }

struct IdxPlusPnt {
    size_t idx;
    Eigen::Vector3d pnt;
};

template <typename T>
void inc_atomic(std::atomic<T>& a, T b) {
    for (T c = a; !a.compare_exchange_strong(c, c + b);)
        ;
}

double triangle_area(const Eigen::Vector3d& v1, const Eigen::Vector3d& v2,
                     const Eigen::Vector3d& v3) {
    return (v2 - v1).cross((v3 - v1)).norm() / 2;
}

Eigen::Vector3d barycentric_interpolation_weights(const Eigen::Vector3d& pnt,
                                                  const Eigen::Vector3d& v0,
                                                  const Eigen::Vector3d& v1,
                                                  const Eigen::Vector3d& v2) {

    return Eigen::Vector3d{// triangle areas
                           triangle_area(pnt, v1, v2),
                           triangle_area(pnt, v2, v0),
                           triangle_area(pnt, v0, v1)};
}

/* Interpolate Planck emission in a wedge */
float B_interp_wedge(
  const Eigen::Vector3d& pnt, const size_t wedge_id,
  const std::vector<std::array<size_t, 5>>& faces_of_wedges,
  const std::vector<std::array<size_t, 4>>& vertices_of_faces,
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<float>& B_on_faces) {

    std::array<size_t, 2> face_idx;  // top and bot face
    {  // look for triangles, i.e. faces with 3 verts
        size_t i = 0;
        for (auto face_id : faces_of_wedges[wedge_id]) {
            for (auto iv : vertices_of_faces[face_id]) {
                if (iv == std::numeric_limits<size_t>::max()) {
                    face_idx[i++] = face_id;
                    break;
                }
            }
        }
    }

    std::array<double, 2> distances;
    std::array<double, 2> Bhorizontal;

    for (size_t i = 0; i < 2; ++i) {
        size_t face_id = face_idx[i];
        std::array<size_t, 3> vert_idx;  // considered vertex_ids of wedge
        for (size_t k = 0, l = 0; k < 4; ++k) {
            size_t iv = vertices_of_faces[face_id][k];
            if (iv != std::numeric_limits<size_t>::max()) {
                vert_idx[l++] = iv;
            }
        }

        auto v0 = vertices[vert_idx[0]];
        auto v1 = vertices[vert_idx[1]];
        auto v2 = vertices[vert_idx[2]];
        const Eigen::Vector3d n = (v1 - v0).cross(v2 - v0).normalized();
        const Eigen::Vector3d v = pnt - v0;
        distances[i] = std::abs(v.dot(n));
        Bhorizontal[i] = B_on_faces[face_id];
    }

    const double tot_distance = distances[0] + distances[1];
    const double B = distances[0] / tot_distance * Bhorizontal[1] +
                     distances[1] / tot_distance * Bhorizontal[0];
    return B;
}

class FaceFlxRecorder {
   public:
    FaceFlxRecorder(double* retvec_edir, double* retvec_ediff,
                    double* retvec_abso) {
        flxvec_edir = reinterpret_cast<std::atomic<double>*>(retvec_edir);
        flxvec_ediff = reinterpret_cast<std::atomic<double>*>(retvec_ediff);
        vec_abso = reinterpret_cast<std::atomic<double>*>(retvec_abso);
    }

    FaceFlxRecorder(std::atomic<double>* retvec_edir,
                    std::atomic<double>* retvec_ediff,
                    std::atomic<double>* retvec_abso) {
        flxvec_edir = retvec_edir;
        flxvec_ediff = retvec_ediff;
        vec_abso = retvec_abso;
    }

    void on_new_ray(const Ray& r) { is_direct = true; }

    void on_volabs(size_t volume_id, double absorption) {
        inc_atomic(vec_abso[volume_id], double(absorption));
    }

    void on_enter_surface(const Ray& r, const SurfaceSlice& s, double L) {
        has_scattered_on_surface = false;
        if (is_direct) {
            inc_atomic(flxvec_edir[s.idx], double(L));
        } else {
            auto geom = s.compute_geometry(r);
            if (r.d.dot(geom.n) > 0) {  // along the normal
                inc_atomic(flxvec_ediff[2 * s.idx], double(L));
            } else {
                inc_atomic(flxvec_ediff[2 * s.idx + 1], double(L));
            }
        }
    }
    void on_leave_surface(const Ray& r, const SurfaceSlice& s, double L) {
        if (has_scattered_on_surface) {
            auto geom = s.compute_geometry(r);
            if (r.d.dot(geom.n) > 0) {  // along the normal
                inc_atomic(flxvec_ediff[2 * s.idx], double(L));
            } else {
                inc_atomic(flxvec_ediff[2 * s.idx + 1], double(L));
            }
        }
    }
    void on_scattering(const Ray& new_ray) {
        is_direct = false;
        has_scattered_on_surface = true;
    }

    void on_ray_finish() {}

    FaceFlxRecorder clone() {
        return FaceFlxRecorder(flxvec_edir, flxvec_ediff, vec_abso);
    }
    void join(const FaceFlxRecorder&) {}

   private:
    std::atomic<double>* flxvec_edir;
    std::atomic<double>* flxvec_ediff;
    std::atomic<double>* vec_abso;
    bool is_direct;
    bool has_scattered_on_surface;
};

template <typename Grid>
class ThermalFaceFlxRecorder {
   public:
    ThermalFaceFlxRecorder(const Grid& grid, size_t Nediff,
                           double* retvec_ediff, double* retvec_abso)
        : grid(grid) {
        flxvec_ediff = reinterpret_cast<std::atomic<double>*>(retvec_ediff);
        vec_abso = reinterpret_cast<std::atomic<double>*>(retvec_abso);
        N_ediff.resize(Nediff, 0);
    }

    ThermalFaceFlxRecorder(const Grid& grid, size_t Nediff,
                           std::atomic<double>* retvec_ediff,
                           std::atomic<double>* retvec_abso)
        : grid(grid) {
        N_ediff.resize(Nediff, 0);
        flxvec_ediff = retvec_ediff;
        vec_abso = retvec_abso;
    }

    void on_new_ray(const Ray& r) {}

    void on_volabs(size_t volume_id, double absorption) {}

    void on_enter_surface(const Ray& r, const SurfaceSlice& s, double L) {
        has_scattered_on_surface = false;
        auto geom = s.compute_geometry(r);
        auto alongnormal = r.d.dot(geom.n) > 0;
        const size_t i = alongnormal ? 2 * s.idx : 2 * s.idx + 1;
        ++N_ediff[i];
        inc_atomic(flxvec_ediff[i], double(L));

        auto [next_poly, current_poly] = grid->polytopes_of_planes[s.idx];
        if (!alongnormal) std::swap(next_poly, current_poly);

        if (current_poly < grid->polytope_count()) {
            inc_atomic(vec_abso[current_poly], -double(L));
        }
        if (next_poly < grid->polytope_count()) {
            inc_atomic(vec_abso[next_poly], +double(L));
        }
    }
    void on_leave_surface(const Ray& r, const SurfaceSlice& s, double L) {
        if (has_scattered_on_surface) {
            auto geom = s.compute_geometry(r);
            auto alongnormal = r.d.dot(geom.n) > 0;
            const size_t i = alongnormal ? 2 * s.idx : 2 * s.idx + 1;
            ++N_ediff[i];
            inc_atomic(flxvec_ediff[i], double(L));

            auto [next_poly, current_poly] = grid->polytopes_of_planes[s.idx];
            if (!alongnormal) std::swap(next_poly, current_poly);

            if (current_poly < grid->polytope_count()) {
                inc_atomic(vec_abso[current_poly], -double(L));
            }
            if (next_poly < grid->polytope_count()) {
                inc_atomic(vec_abso[next_poly], +double(L));
            }
        }
    }
    void on_scattering(const Ray& new_ray) { has_scattered_on_surface = true; }

    void on_ray_finish() {}

    ThermalFaceFlxRecorder clone() {
        return ThermalFaceFlxRecorder(grid, N_ediff.size(), flxvec_ediff,
                                      vec_abso);
    }

    void join(const ThermalFaceFlxRecorder& thread_rec) {
        for (size_t i = 0; i < N_ediff.size(); i++) {
            N_ediff[i] += thread_rec.N_ediff[i];
        }
    }

    std::vector<size_t> N_ediff;

   private:
    const Grid& grid;
    std::atomic<double>* flxvec_ediff;
    std::atomic<double>* vec_abso;
    bool has_scattered_on_surface;
};

template <typename IT>
double convex_polygon_area(const std::vector<Eigen::Vector3d>& vert_coords,
                           IT verts_begin, IT verts_end) {
    const auto& first_vert = vert_coords[*verts_begin];
    auto it_1 = ++verts_begin;
    auto it_2 = ++verts_begin;
    double area = 0;
    for (; it_2 != verts_end; ++it_1, ++it_2) {
        // std::cout << "area" <<  area << " verts "
        //    << first_vert.transpose() << " "
        //    << vert_coords[*it_1].transpose() << " "
        //    << vert_coords[*it_2].transpose() << "\n";
        area +=
          triangle_area(first_vert, vert_coords[*it_1], vert_coords[*it_2]);
    }
    return area;
}

int rpt_img_wedge(size_t num_threads, size_t img_Nx, size_t img_Ny,
                  size_t Nphotons, size_t Nwedges, size_t Nfaces, size_t Nverts,
                  int cyclic, const size_t* verts_of_face,
                  const size_t* faces_of_wedges, const double* vert_coords,
                  const float* kabs, const float* ksca, const float* g,
                  const float* albedo_on_faces, const float* sundir,
                  const float* cam_location, const float* cam_viewing_dir,
                  const float* cam_up_vec, float fov_width, float fov_height,
                  float* img) {

    if (!num_threads) {
        num_threads = cluster_aware_concurrency();
    }

    std::cout << "Calling rpt_img_wedge with " << num_threads << " threads, "
              << Nwedges << " wedges, " << Nfaces << " faces and " << Nverts
              << " vertices."
              << " Running " << Nphotons << " photons."
              << " Img dims:" << img_Nx << " " << img_Ny << "\n";

    Eigen::Vector3d sun_direction =
      Eigen::Vector3d{sundir[0], sundir[1], sundir[2]};

    auto sink = FarSink<std::mt19937_64>(sun_direction.normalized());

    std::vector<Eigen::Vector3d> verts(Nverts);
    std::vector<std::array<size_t, 5>> fow(Nwedges);
    std::vector<std::array<size_t, 4>> vof(Nfaces);

    for (size_t i = 0; i < Nwedges; ++i) {
        for (size_t j = 0; j < 5; ++j) {
            fow[i][j] = faces_of_wedges[i * 5 + j];
        }
    }
    for (size_t i = 0; i < Nfaces; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            vof[i][j] = verts_of_face[i * 4 + j];
        }
    }
    for (size_t i = 0; i < Nverts; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            verts[i][j] = vert_coords[i * 3 + j];
        }
    }

    static std::unique_ptr<rayli::vgrid::ConvexPolytopes> polygrid;

    // save number of mesh pts.
    // if that changes we invalidate the grid structure
    static size_t saved_mesh_pts = 0;

    if (!polygrid || saved_mesh_pts != Nwedges + Nfaces + Nverts) {
        polygrid = std::make_unique<rayli::vgrid::ConvexPolytopes>(
          rayli::vgrid::polytopes_from_wedges_with_facedefinitions(verts, fow,
                                                                   vof));
        polygrid->build_accelerators();
        saved_mesh_pts = Nwedges + Nfaces + Nverts;
    }

    auto kabs_tex = ForeignVectorTexture(kabs);
    auto ksca_tex = ForeignVectorTexture(ksca);
    auto g_tex = ForeignVectorTexture(g);

    auto kext_tex = kabs_tex + ksca_tex;
    auto scat =
      MapTexture{[](double g) { return HGScat<std::mt19937_64>(g); }, g_tex} *
      ksca_tex;

    // auto surf = constant_texture<size_t>(NoMaterial{});
    auto surf = function_texture<size_t>(
      [albedo_on_faces](
        size_t idx) -> VariantMaterial<NoMaterial, LambertianMaterial> {
          if (albedo_on_faces[idx] < 0) {
              return NoMaterial{};
          } else {
              return LambertianMaterial{albedo_on_faces[idx]};
          }
      });

    Eigen::Vector3d cam_loc =
      Eigen::Vector3d{cam_location[0], cam_location[1], cam_location[2]};
    Eigen::Vector3d cam_to = Eigen::Vector3d{
      cam_viewing_dir[0], cam_viewing_dir[1], cam_viewing_dir[2]};
    Eigen::Vector3d cam_up =
      Eigen::Vector3d{cam_up_vec[0], cam_up_vec[1], cam_up_vec[2]};

    auto source = SimpleCamera{cam_loc, lookat(cam_to, cam_up), fov_width,
                               fov_height, Nphotons};
    auto recorder = ImageRecorder{img_Nx, img_Ny};

    if (cyclic) {
        auto bounds = polygrid->bounds;
        {
            Eigen::Vector3d center = (bounds.lower + bounds.upper) / 2;
            auto eps = std::sqrt(std::numeric_limits<double>::epsilon());
            bounds.lower -= (center - bounds.lower) * eps;
            bounds.upper -= (center - bounds.upper) * eps;
        }
        bounds.lower(2) = -std::numeric_limits<double>::infinity();
        bounds.upper(2) = std::numeric_limits<double>::infinity();
        auto cyclic_grid = rayli::vgrid::Cyclic<rayli::vgrid::ConvexPolytopes>(
          *polygrid, bounds);
        std::cout << "Using Cyclic BC's with bounds:" << bounds << "\n";
        auto scn = Scn{cyclic_grid, kext_tex, scat, surf, sink};
        auto tracker = ScatterPathTracer{scn};
        tracker.solve_par(source, recorder, num_threads);
    } else {
        auto scn = Scn{*polygrid, kext_tex, scat, surf, sink};
        auto tracker = ScatterPathTracer{scn};
        tracker.solve_par(source, recorder, num_threads);
        std::cout << "Using Non-Cyclic BC's! \n";
    }

    for (size_t i = 0; i < img_Nx; ++i) {
        for (size_t j = 0; j < img_Ny; ++j) {
            img[i + j * img_Nx] =
              recorder.data[i * img_Ny + j] /
              std::max<size_t>(1, recorder.count[i * img_Ny + j]);
        }
    }
    return 0;
}

int rfft_wedge(size_t num_threads, size_t Nphotons, size_t Nwedges,
               size_t Nfaces, size_t Nverts, int cyclic, size_t* verts_of_face,
               size_t* faces_of_wedges, double* vert_coords, float* kabs,
               float* ksca, float* g, float* albedo_on_faces, float* sundir,
               double* flx_through_faces_edir, double* flx_through_faces_ediff,
               double* abso_in_cells) {

    if (!num_threads) {
        num_threads = cluster_aware_concurrency();
    }
    size_t Nphotons_thread = std::max(size_t(1), Nphotons / num_threads);
    size_t Nphotons_global = Nphotons_thread * num_threads;

    Eigen::Vector3d sun_direction =
      Eigen::Vector3d{sundir[0], sundir[1], sundir[2]};

    std::cout << "Calling rfft_wedge with " << num_threads << " threads, "
              << Nwedges << " wedges, " << Nfaces << " faces and " << Nverts
              << " vertices."
              << " Sundir [" << sun_direction.transpose() << "] "
              << " Running a total of " << std::setprecision(2)
              << double(Nphotons_global) << " photons, i.e. "
              << std::setprecision(2) << double(Nphotons_thread)
              << " photons per thread"
              << "\n";

    std::vector<Eigen::Vector3d> verts(Nverts);
    std::vector<std::array<size_t, 5>> fow(Nwedges);
    std::vector<std::array<size_t, 4>> vof(Nfaces);

    for (size_t i = 0; i < Nwedges; ++i) {
        for (size_t j = 0; j < 5; ++j) {
            fow[i][j] = faces_of_wedges[i * 5 + j];
        }
    }
    for (size_t i = 0; i < Nfaces; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            vof[i][j] = verts_of_face[i * 4 + j];
        }
    }
    for (size_t i = 0; i < Nverts; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            verts[i][j] = vert_coords[i * 3 + j];
        }
    }

    auto vr = rayli::vr::NoVarianceReduction{};
    // auto vr = rayli::vr::RussianRouletteVR{};
    // auto vr = rayli::vr::MaxScatterVr{10};
    // auto vr = rayli::vr::RussianRouletteVR{} + rayli::vr::MaxScatterVr{10};

    auto kabs_tex = ForeignVectorTexture(kabs);
    auto ksca_tex = ForeignVectorTexture(ksca);
    auto g_tex = ForeignVectorTexture(g);

    auto kext_tex = kabs_tex + ksca_tex;
    auto scat =
      MapTexture{[](double g) { return HGScat<std::mt19937_64>(g); }, g_tex} *
      ksca_tex;

    auto surf = function_texture<size_t>(
      [albedo_on_faces](
        size_t idx) -> VariantMaterial<NoMaterial, LambertianMaterial> {
          if (albedo_on_faces[idx] < 0) {
              return NoMaterial{};
          } else {
              return LambertianMaterial{albedo_on_faces[idx]};
          }
      });

    for (size_t f = 0; f < Nfaces; ++f) {
        flx_through_faces_edir[f] = 0;
        flx_through_faces_ediff[2 * f] = 0;
        flx_through_faces_ediff[2 * f + 1] = 0;
    }

    for (size_t c = 0; c < Nwedges; c++) {
        abso_in_cells[c] = 0;
    }

    auto fluxrec = FaceFlxRecorder{flx_through_faces_edir,
                                   flx_through_faces_ediff, abso_in_cells};

    auto crit = [=](size_t i, size_t thread_id = 0) {
        if (thread_id == 0 && i % 128 == 0) {  // progress bar
            double progress = double(i) / Nphotons_thread;
            int barWidth = 70;
            std::cout << "[";
            int pos = barWidth * progress;
            for (int i = 0; i < barWidth; ++i) {
                if (i < pos)
                    std::cout << "=";
                else if (i == pos)
                    std::cout << ">";
                else
                    std::cout << " ";
            }
            std::cout << "] " << i << " / " << Nphotons_thread << " "
                      << int(progress * 100.0) << " %\r";
            std::cout.flush();
        }
        return i < Nphotons_thread;
    };

    static std::unique_ptr<rayli::vgrid::ConvexPolytopes> polygrid;

    // save number of mesh pts.
    // if that changes we invalidate the grid structure
    static size_t saved_mesh_pts = 0;

    if (!polygrid || saved_mesh_pts != Nwedges + Nfaces + Nverts) {
        polygrid = std::make_unique<rayli::vgrid::ConvexPolytopes>(
          rayli::vgrid::polytopes_from_wedges_with_facedefinitions(verts, fow,
                                                                   vof));
        polygrid->build_accelerators();
        saved_mesh_pts = Nwedges + Nfaces + Nverts;
    }

    double ray_source_area;
    if (cyclic) {
        auto bounds = polygrid->bounds;
        {
            Eigen::Vector3d center = (bounds.lower + bounds.upper) / 2;
            auto eps = std::sqrt(std::numeric_limits<double>::epsilon());
            bounds.lower -= (center - bounds.lower) * eps;
            bounds.upper -= (center - bounds.upper) * eps;
        }
        bounds.lower(2) = -std::numeric_limits<double>::infinity();
        bounds.upper(2) = std::numeric_limits<double>::infinity();
        auto cyclic_grid = rayli::vgrid::Cyclic<rayli::vgrid::ConvexPolytopes>(
          *polygrid, bounds);
        std::cout << "Using Cyclic BC's with bounds:" << bounds << "\n";

        auto x = verts.back() - verts.front();
        auto e1 = Eigen::Vector3d::UnitX() * x(0);
        auto e2 = Eigen::Vector3d::UnitY() * x(1);
        auto ray_source =
          RectangularRaySource(verts.front() + Eigen::Vector3d::UnitZ(),
                               -sun_direction.normalized(), e1, e2);
        ray_source_area = ray_source.effective_area();

        auto solver = ForwardsFluxTracer(cyclic_grid, vr);
        solver.solve(ray_source, crit, kext_tex, scat, surf, surf, fluxrec,
                     num_threads);

    } else {
        std::cout << "Using Non-Cyclic BC's! \n";

        auto ray_source = ray_source_from_sun_direction(
          verts.begin(), verts.end(), sun_direction.normalized());
        ray_source_area = ray_source.effective_area();

        auto solver = ForwardsFluxTracer(*polygrid, vr);
        solver.solve(ray_source, crit, kext_tex, scat, surf, surf, fluxrec,
                     num_threads);
    }

    double norm_factor =
      sun_direction.norm() * ray_source_area / Nphotons_global;
    for (size_t f = 0; f < Nfaces; f++) {

        auto end = std::find_if(
          vof[f].begin(),
          vof[f].end(),  // search for first occurence of illegal vert indices
          [](size_t ivert) { return ivert >= polygrid->vertices.size(); });

        auto area =
          convex_polygon_area(polygrid->vertices, vof[f].begin(), end);
        flx_through_faces_edir[f] *= norm_factor / area;
        flx_through_faces_ediff[2 * f] *= norm_factor / area;
        flx_through_faces_ediff[2 * f + 1] *= norm_factor / area;
        // std::cout << fmt::format("{:8d} edir {:12.2f} edn {:12.2f} eup
        // {:12.2f}\n",
        //        f, flx_through_faces_edir[f], flx_through_faces_ediff[2*f],
        //        flx_through_faces_ediff[2*f+1]);
    }

    for (size_t c = 0; c < Nwedges; c++) {
        abso_in_cells[c] *= norm_factor;
    }

    return 0;
}

int rfft_wedge_thermal(size_t num_threads, const size_t Nphotons,
                       const size_t Nwedges, const size_t Nfaces,
                       const size_t Nverts, int cyclic,
                       const size_t* verts_of_face,
                       const size_t* faces_of_wedges, const double* vert_coords,
                       const float* kabs, const float* ksca, const float* g,
                       const float* albedo_on_faces, const float* B_on_faces, const float* B_on_surfaces,
                       double* flx_through_faces_ediff, double* abso_in_cells) {

    if (!num_threads) {
        num_threads = cluster_aware_concurrency();
    }
    size_t Nphotons_thread = std::max(size_t(1), Nphotons / num_threads);
    size_t Nphotons_global = Nphotons_thread * num_threads;

    std::cout << "Calling rfft_wedge_thermal with " << num_threads
              << " threads, " << Nwedges << " wedges, " << Nfaces
              << " faces and " << Nverts << " vertices."
              << " Running a total of " << std::setprecision(2)
              << double(Nphotons_global) << " photons, i.e. "
              << std::setprecision(2) << double(Nphotons_thread)
              << " photons per thread"
              << "\n";

    std::vector<Eigen::Vector3d> verts(Nverts);
    std::vector<std::array<size_t, 5>> fow(Nwedges);
    std::vector<std::array<size_t, 4>> vof(Nfaces);

    for (size_t i = 0; i < Nwedges; ++i) {
        for (size_t j = 0; j < 5; ++j) {
            fow[i][j] = faces_of_wedges[i * 5 + j];
        }
    }
    for (size_t i = 0; i < Nfaces; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            vof[i][j] = verts_of_face[i * 4 + j];
        }
    }
    for (size_t i = 0; i < Nverts; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            verts[i][j] = vert_coords[i * 3 + j];
        }
    }
    const std::vector<float> Bof(B_on_faces, B_on_faces + Nfaces);
    const std::vector<float> Bosrfc(B_on_surfaces, B_on_surfaces + Nfaces);

    auto vr = rayli::vr::NoVarianceReduction{};

    auto kabs_tex = ForeignVectorTexture(kabs);
    auto ksca_tex = ForeignVectorTexture(ksca);
    auto g_tex = ForeignVectorTexture(g);

    auto kext_tex = kabs_tex + ksca_tex;
    auto scat =
      MapTexture{[](double g) { return HGScat<std::mt19937_64>(g); }, g_tex} *
      ksca_tex;

    auto emis_tex =
      function_texture<IdxPlusPnt>([&fow, &vof, &verts, &Bof](IdxPlusPnt ipp) {
          auto B = B_interp_wedge(ipp.pnt, ipp.idx, fow, vof, verts, Bof);
          return B;
      });

    auto surf = function_texture<IdxPlusPnt>(
      [&albedo_on_faces, &Bosrfc](IdxPlusPnt ipp)
        -> VariantMaterial<NoMaterial, ThermalLambertianMaterial> {
          const double Ag = albedo_on_faces[ipp.idx];
          if (Ag < 0) {
              return NoMaterial{};
          } else {
              return ThermalLambertianMaterial{Ag, Bosrfc[ipp.idx]};
          }
      });

    for (size_t f = 0; f < 2 * Nfaces; ++f) {
        flx_through_faces_ediff[f] = 0;
    }
    for (size_t c = 0; c < Nwedges; c++) {
        abso_in_cells[c] = 0;
    }

    auto crit = [=](size_t i, size_t thread_id = 0) {
        if (thread_id == 0 && i % 1024 == 0) {  // progress bar
            double progress = double(i) / Nphotons_thread;
            int barWidth = 70;
            std::cout << "[";
            int pos = barWidth * progress;
            for (int i = 0; i < barWidth; ++i) {
                if (i < pos)
                    std::cout << "=";
                else if (i == pos)
                    std::cout << ">";
                else
                    std::cout << " ";
            }
            std::cout << "] " << i << " / " << Nphotons_thread << " "
                      << int(progress * 100.0) << " %\r";
            std::cout.flush();
        }
        return i < Nphotons_thread;
    };

    static std::unique_ptr<rayli::vgrid::ConvexPolytopes> polygrid;

    // save number of mesh pts.
    // if that changes we invalidate the grid structure
    static size_t saved_mesh_pts = 0;

    if (!polygrid || saved_mesh_pts != Nwedges + Nfaces + Nverts) {
        polygrid = std::make_unique<rayli::vgrid::ConvexPolytopes>(
          rayli::vgrid::polytopes_from_wedges_with_facedefinitions(verts, fow,
                                                                   vof));
        polygrid->build_accelerators();
        saved_mesh_pts = Nwedges + Nfaces + Nverts;
    }

    auto fluxrec = ThermalFaceFlxRecorder{
      polygrid, Nfaces * 2, flx_through_faces_ediff, abso_in_cells};

    double ray_source_area;
    if (cyclic) {

        auto bounds = polygrid->bounds;
        {
            Eigen::Vector3d center = (bounds.lower + bounds.upper) / 2;
            auto eps = std::sqrt(std::numeric_limits<double>::epsilon());
            bounds.lower -= (center - bounds.lower) * eps;
            bounds.upper -= (center - bounds.upper) * eps;
        }
        bounds.lower(2) = -std::numeric_limits<double>::infinity();
        bounds.upper(2) = std::numeric_limits<double>::infinity();
        auto cyclic_grid = rayli::vgrid::Cyclic<rayli::vgrid::ConvexPolytopes>(
          *polygrid, bounds);
        std::cout << "Using Cyclic BC's with bounds:" << bounds << "\n";

        auto e1 =
          Eigen::Vector3d::UnitX() * (bounds.upper(0) - bounds.lower(0));
        auto e2 =
          Eigen::Vector3d::UnitY() * (bounds.upper(1) - bounds.lower(1));
        auto ray_source = RectangularLambertianRaySource(
          verts.front() + Eigen::Vector3d::UnitZ(), e1, e2);
        ray_source_area = ray_source.effective_area();

        auto solver = ForwardThermalFluxTracer(cyclic_grid, vr);
        solver.solve(ray_source, crit, emis_tex, kext_tex, scat, surf, fluxrec,
                     num_threads);

    } else {
        std::cout << "Using Non-Cyclic BC's! \n";
        std::cout << "Open Boundaries Not tested! ... exiting\n";
        return -1;
    }

    {  // create a histogram of the number of samples on faces
        const size_t Nbins = 5;
        std::array<size_t, Nbins> sampling_bins = {0};

        for (size_t f = 0; f < 2 * Nfaces; f++) {
            if (fluxrec.N_ediff[f] > 0) {
                sampling_bins[std::min(
                  Nbins, 1 + (size_t)std::log10(fluxrec.N_ediff[f]))]++;
            } else {
                sampling_bins[0]++;
            }
        }

        if (sampling_bins[1] > 0) {
            std::cout
              << "\033[1;31m"
              << "Warning: a certainly reacheable surface was sampled crudely "
              << "- please consider using more photons "
              << "or the results may potentially have a large bias."
              << "\033[0m"
              << "\n";
            for (size_t i = 0; i < Nbins; ++i) {
                std::cout << " Sampling bin [ " << std::setw(6)
                          << std::pow(10, i - 1) << ", " << std::setw(6)
                          << std::pow(10, i) << "] Nfaces: " << sampling_bins[i]
                          << "\n";
            }
        }
    }

    for (size_t f = 0; f < 2 * Nfaces; f++) {
        if (fluxrec.N_ediff[f] > 0) {
            flx_through_faces_ediff[f] *= M_PI / fluxrec.N_ediff[f];
        } else {
            flx_through_faces_ediff[f] = std::numeric_limits<double>::quiet_NaN();
        }
    }

    for (size_t c = 0; c < Nwedges; c++) {
        abso_in_cells[c] *= M_PI * ray_source_area / Nphotons_global;
    }

    return 0;
}
