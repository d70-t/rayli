cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)
project (rayli_c_wrapper)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set (PROJECT_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/include")
set (PROJECT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src")

include_directories(${PROJECT_INCLUDE_DIR})

add_library(rayli_c_wrapper
    ${PROJECT_SOURCE_DIR}/rayli_c_wrapper.cpp
)

target_link_libraries(rayli_c_wrapper rayli_common)
target_include_directories(rayli_c_wrapper PUBLIC ${PROJECT_INCLUDE_DIR})
target_compile_options(rayli_c_wrapper PRIVATE ${COVERAGE_FLAGS})

set_target_properties(rayli_c_wrapper PROPERTIES PUBLIC_HEADER
  "${PROJECT_INCLUDE_DIR}/rayli_c_wrapper.h")

INSTALL(TARGETS rayli_c_wrapper
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        PUBLIC_HEADER DESTINATION include
)
