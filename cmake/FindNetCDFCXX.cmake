find_program (
    NCCXX4_CONFIG
    NAMES ncxx4-config
)

if (NCCXX4_CONFIG)
    exec_program("${NCCXX4_CONFIG}" ARGS --libs OUTPUT_VARIABLE NetCDFCXX_LIBRARIES)
    exec_program("${NCCXX4_CONFIG}" ARGS --includedir OUTPUT_VARIABLE NetCDFCXX_INCLUDE_DIR)
    set(NetCDFCXX_FOUND ON)
endif (NCCXX4_CONFIG)

if (NetCDFCXX_FOUND)
    if (NOT TARGET NetCDF::NetCDFCXX)
        add_library(NetCDF::NetCDFCXX UNKNOWN IMPORTED)
        set_target_properties(
            NetCDF::NetCDFCXX PROPERTIES
            IMPORTED_LOCATION "${NetCDFCXX_LIBRARIES}"
            INTERFACE_INCLUDE_DIRECTORIES "${NetCDF_INCLUDE_DIR}"
        )
    endif ()
endif (NetCDFCXX_FOUND)
