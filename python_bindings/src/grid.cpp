#include <Ray.hpp>
#include <vgrid/MysticCloud.hpp>

#include <sstream>

#include "pybind11_common.h"

void init_grid(py::module& m) {
    py::class_<Ray>(m, "Ray")
      .def(py::init<Eigen::Vector3d, Eigen::Vector3d>())
      .def("__call__", &Ray::operator())
      .def("__repr__", [](const Ray& ray) {
          std::stringstream s;
          s << ray;
          return s.str();
      });

    using namespace rayli::vgrid;

    py::class_<MysticCloud>(m, "MysticCloud")
      .def(py::init<double, double, size_t, size_t, std::vector<double>>())
      .def("walk_along", &MysticCloud::walk_along, py::arg("ray"),
           py::arg("tnear") = 0,
           py::arg("tfar") = std::numeric_limits<double>::infinity(),
           py::arg("last_volume_id") = std::nullopt);

    py::class_<MysticCloud::RayPath>(m, "MysticCloudRayPath")
      .def("__iter__",
           [](const MysticCloud::RayPath& path) {
               return py::make_iterator(path.begin(), path.end());
           },
           py::keep_alive<0, 1>());

    py::class_<VolumeSlice>(m, "VolumeSlice")
//      .def(py::init<double, double, size_t>())
      .def_readwrite("tnear", &VolumeSlice::tnear)
      .def_readwrite("tfar", &VolumeSlice::tfar)
      .def_readwrite("idx", &VolumeSlice::idx);

    py::class_<SurfaceSlice>(m, "SurfaceSlice")
//      .def(py::init<double, size_t>())
      .def_readwrite("t", &SurfaceSlice::t)
      .def_readwrite("idx", &SurfaceSlice::idx);
}
