#include <pybind11/pybind11.h>

namespace py = pybind11;

void init_grid(py::module&);
void init_atm(py::module&);
void init_triangular_mesh(py::module&);
void init_material(py::module&);

PYBIND11_MODULE(pyrayli, m) {
    m.doc() = "rayli ray based light simulator";
    init_grid(m);
    init_atm(m);
    init_triangular_mesh(m);
    init_material(m);
}
