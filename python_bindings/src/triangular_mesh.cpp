#include <surface/TriangularMesh.hpp>

#include "pybind11_common.h"
void init_triangular_mesh(py::module& m) {
    using namespace rayli::surface;

    py::class_<TriangularMesh>(m, "TriangularMesh")
      .def("repair", &TriangularMesh::repair)
      .def("build_accelerators", &TriangularMesh::build_accelerators)
      .def("find_intersecting_face_idx", [](const TriangularMesh& self,
                                            const Ray& ray, double tnear,
                                            double tfar) {
          double t;
          size_t idx = self.find_intersecting_face_idx(ray, tnear, tfar, t);
          return std::make_pair(idx, t);
      });

    m.def("make_trimesh", [](std::vector<Eigen::Vector3d>& vertices,
                             std::vector<std::array<size_t, 3>> triangles) {
        return TriangularMesh::from_vertices_and_triangles(vertices, triangles);
    });
}
