#include <atm.hpp>
#include <random>

#include "pybind11_common.h"

using G = std::mt19937_64;

void init_atm(py::module& m) {
    py::class_<Scat<G>>(m, "Scat")
      .def_property_readonly("sigma_scat", &Scat<G>::sigma_scat)
      .def("f", &Scat<G>::f)
      .def("pdf", &Scat<G>::pdf)
      .def("sample_f", &Scat<G>::sample_f);

    py::class_<HGScat<G>, Scat<G>>(m, "HGScat").def(py::init<double>());

    py::class_<RayleighScat<G>, Scat<G>>(m, "RayleighScat").def(py::init<>());

    py::class_<Atm<G>, Scat<G>>(m, "Atm")
      .def(py::init<HGScat<G>>())
      .def(py::init<RayleighScat<G>>())
      .def("__add__", [](const Atm<G>& self,
                         const Atm<G>& other) { return Atm<G>{self + other}; })
      .def("__mul__",
           [](const Atm<G>& self, double f) { return Atm<G>{self * f}; })
      .def("__rmul__",
           [](const Atm<G>& self, double f) { return Atm<G>{f * self}; });

    py::implicitly_convertible<HGScat<G>, Atm<G>>();
    py::implicitly_convertible<RayleighScat<G>, Atm<G>>();

    m.def("make_rayleigh", []() { return Atm<G>{RayleighScat<G>{}}; });
    m.def("make_hg", [](double g) { return Atm<G>{HGScat<G>{g}}; });
    m.def("make_tabulated", [](std::vector<double> mu, std::vector<double> f) {
        return Atm<G>{TabulatedMuScat<G>{mu, f}};
    });

    py::class_<DirectedSample>(m, "DirectedSample")
      .def_readwrite("d", &DirectedSample::d)
      .def_readwrite("f", &DirectedSample::f)
      .def_readwrite("pdf", &DirectedSample::pdf);

    py::class_<G>(m, "rng");

    m.def("make_rng", []() {
        std::random_device rd;
        return G{rd()};
    });
}
