#include <brdf/worldbrdf.hpp>
#include <material.hpp>
#include <random>

#include "pybind11_common.h"

using G = std::mt19937_64;

void init_material(py::module& m) {
    py::class_<LambertianMaterial>(m, "LambertianMaterial")
      .def(py::init([](double r) { return LambertianMaterial{r}; }))
      .def("f", &LambertianMaterial::f)
      .def("pdf", &LambertianMaterial::pdf)
      .def("sample_f", [](const LambertianMaterial& self,
                          const Eigen::Vector3d in_direction_world, G& rng) {
          return self.sample_f(in_direction_world, rng);
      });

    py::class_<WorldBRDFTransform>(m, "WorldBRDFTransform")
      .def(py::init([](const Eigen::Vector3d& s, const Eigen::Vector3d& t,
                       const Eigen::Vector3d& u) {
          return WorldBRDFTransform{s, t, u};
      }))
      .def_static("from_n_and_dpdu", WorldBRDFTransform::from_n_and_dpdu)
      .def("world_to_local", &WorldBRDFTransform::world_to_local)
      .def("local_to_world", &WorldBRDFTransform::local_to_world)
      .def("f",
           [](const WorldBRDFTransform& self, const LambertianMaterial& brdf,
              const Eigen::Vector3d in_direction_world,
              const Eigen::Vector3d& out_direction_world) {
               return self.f(brdf, in_direction_world, out_direction_world);
           })
      .def("pdf",
           [](const WorldBRDFTransform& self, const LambertianMaterial& brdf,
              const Eigen::Vector3d in_direction_world,
              const Eigen::Vector3d& out_direction_world) {
               return self.pdf(brdf, in_direction_world, out_direction_world);
           })
      .def("sample_f",
           [](const WorldBRDFTransform& self, const LambertianMaterial& brdf,
              const Eigen::Vector3d in_direction_world,
              G& rng) { return self.sample_f(brdf, in_direction_world, rng); });
}
