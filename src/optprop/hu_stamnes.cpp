#include <cmath>

#include "optprop/hu_stamnes.hpp"

namespace rayli::optprop::hu_stamnes {

double ABC::eval(double reff) const { return a * std::pow(reff, b) + c; }

ABC operator+(const ABC& a, const ABC& b) {
    return {a.a + b.a, a.b + b.b, a.c + b.c};
}

ABC operator*(double a, const ABC& b) { return {a * b.a, a * b.b, a * b.c}; }

ABC operator*(const ABC& a, double b) { return b * a; }

ECG operator+(const ECG& a, const ECG& b) {
    return {a.ext + b.ext, a.coalbedo + b.coalbedo, a.g + b.g};
}

ECG operator*(double a, const ECG& b) {
    return {a * b.ext, a * b.coalbedo, a * b.g};
}

ECG operator*(const ECG& a, double b) { return b * a; }

MonochromaticTable
Table::interpolate_to_wavelength(double selected_wavelength) const {
    MonochromaticTable out;

    auto lb = std::lower_bound(wavelength.begin(), wavelength.end(),
                               selected_wavelength);
    size_t i1, i2;
    if (lb == wavelength.begin()) {
        // extrapolate downwards
        i1 = i2 = 0;
    } else if (lb == wavelength.end()) {
        // extrapolate upwards
        i1 = i2 = wavelength.size() - 1;
    } else {
        i2 = std::distance(wavelength.begin(), lb);
        i1 = i2 - 1;
    }
    double f = 1;
    if (i1 != i2) {
        f = (selected_wavelength - wavelength[i1]) /
            (wavelength[i2] - wavelength[i1]);
    }
    auto interp = [&](const auto& data) {
        return (1 - f) * data[i1] + f * data[i2];
    };
    for (const auto& rr : reff_ranges) {
        const auto& d = rr.data;
        out.reff_ranges.push_back({rr.reff_min, rr.reff_max, interp(d)});
    }
    return out;
}

rayli::optprop::HG get_optprop(const ECG& params,
                               const rayli::microphysics::LWC_reff& mp) {
    rayli::optprop::HG out;
    out.ext = mp.LWC * params.ext.eval(mp.reff);
    out.g = params.g.eval(mp.reff);
    out.ssa = 1 - params.coalbedo.eval(mp.reff);
    return out;
}

rayli::optprop::HG get_optprop(const ECG& params,
                               const rayli::microphysics::ext_reff& mp) {
    rayli::optprop::HG out;
    out.ext = mp.ext;
    out.g = params.g.eval(mp.reff);
    out.ssa = 1 - params.coalbedo.eval(mp.reff);
    return out;
}
}  // namespace rayli::optprop::hu_stamnes
