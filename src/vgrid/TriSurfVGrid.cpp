#include <vgrid/TriSurfVGrid.hpp>

TriSurfVGrid::TriSurfVGrid(std::shared_ptr<rayli::surface::TriangularMesh> mesh)
    : mesh(mesh) {}

void TriSurfVGrid::RayIterator::advance() {
    size_t next_idx =
      grid.mesh->find_intersecting_face_idx(ray, tnear, tfar, t);
    while (next_idx < grid.mesh->face_count() && next_idx == face_idx) {
        tnear = std::nextafter(t, tfar);
        next_idx = grid.mesh->find_intersecting_face_idx(ray, tnear, tfar, t);
    }

    face_idx = next_idx;
    if (face_idx >= grid.mesh->face_count()) {
        valid = false;
    }
}
