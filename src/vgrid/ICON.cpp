#include "vgrid/ICON.hpp"

#include <algorithm>

namespace rayli::vgrid {
size_t UniformVerticalGrid::find_layer(size_t cell_id, double z) {
    SPDLOG_TRACE(logger, "searching for layer of cell_id={}, z={}", cell_id, z);
    size_t d = std::distance(levels.begin(),
                             std::upper_bound(levels.begin(), levels.end(), z));
    if (d == 0) return 0;
    d -= 1;
    if (d >= layer_count()) return layer_count() - 1;
    return d;
}

std::pair<size_t, double>
UniformVerticalGrid::find_next_layer(size_t cell_id, size_t start_layer,
                                     const Ray& ray, double tnear,
                                     double tfar) {
    SPDLOG_TRACE(logger,
                 "searching for next layer layer of cell_id={}, start_layer={} "
                 "layer_count={}",
                 cell_id, start_layer, layer_count());
    assert(start_layer < layer_count());

    // note that tangential movement can never reach the lower ball shell,
    // so tangential movement is intentionally counted as up.
    bool moves_down = ray.d.dot(ray(tnear)) < 0;
    if (moves_down) {
        SPDLOG_TRACE(logger, "ray moves down");
        // only downward movement can hit the inner shell,
        // and if it hits the shell, the hit is allways closer.
        auto t = first_ray_ball_intersection(level_below(start_layer), ray);
        if (t) {
            return {start_layer - 1, std::max(tnear, t.value())};
        } else {
            SPDLOG_TRACE(logger,
                         "could not find an intersection with inner shell");
        }
    } else {
        SPDLOG_TRACE(logger, "ray moves up or tangential");
    }
    // if ray is inside the outer ball (which it should), it can only leave the
    // ball through the second intersection. If it is not inside the ball (which
    // can happen due to numerical instability, we also want the second
    // intersection to prevent an infinite loop.
    auto t = second_ray_ball_intersection(level_above(start_layer), ray);
    if (t) {
        return {start_layer + 1, std::max(tnear, t.value())};
    }
    return {std::numeric_limits<size_t>::max(),
            std::numeric_limits<double>::infinity()};
}
}  // namespace rayli::vgrid
