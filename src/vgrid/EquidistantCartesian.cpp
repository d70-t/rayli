#include <vgrid/EquidistantCartesian.hpp>

namespace rayli::vgrid {
std::array<size_t, 3> EquidistantCartesian::find_grid_coordinates(
  const Eigen::Vector3d& x, const Eigen::Vector3d& direction) const {
    size_t ix = (x(0) - o(0)) / d(0);
    size_t iy = (x(1) - o(1)) / d(1);
    size_t iz = (x(2) - o(2)) / d(2);
    // deal with rays on edges
    if (gx(ix) == x(0) && direction(0) < 0) ix -= 1;
    if (gy(iy) == x(1) && direction(1) < 0) iy -= 1;
    if (gz(iz) == x(2) && direction(2) < 0) iz -= 1;
    return {ix, iy, iz};
}
}  // namespace rayli::vgrid
