#include <vgrid/TestboxVGrid.hpp>

namespace rayli::vgrid {

TestboxVGrid::RayIterator::RayIterator(const Self& grid, Ray ray, double tnear,
                                       double tfar) {
    t = std::numeric_limits<double>::infinity();
    size_t iusel = 0;
    size_t ivsel = 1;
    double usel = 0;
    double vsel = 0;
    for (size_t i = 0; i < 3; ++i) {
        double ti = (grid.corner(i) - ray.o(i)) / ray.d(i);
        if (!(ti >= tnear && ti <= tfar)) continue;
        Eigen::Vector3d p = ray(ti);
        size_t iu = (i + 1) % 3;
        size_t iv = (i + 2) % 3;
        double u = (p(iu) - grid.corner(iu)) / grid.extent(iu);
        if (u < 0 || u >= 1) continue;
        double v = (p(iv) - grid.corner(iv)) / grid.extent(iv);
        if (v < 0 || v >= 1) continue;
        if (ti < t) {
            t = ti;
            axis = i;
            iusel = iu;
            ivsel = iv;
            usel = u;
            vsel = v;
        }
    }
    if (!std::isfinite(t)) return;
    size_t ix = usel * grid.box_count[iusel];
    size_t iy = vsel * grid.box_count[ivsel];
    surface_id = (ix + iy) % 2;
}

const TestboxVGrid::RayIterator::value_type
TestboxVGrid::RayIterator::operator*() const {
    return SurfaceSlice{
      t, surface_id, [this](const Ray& ray, const SurfaceSlice& slice) {
          Eigen::Vector3d p = ray(slice.t);
          Eigen::Vector3d dpdu = Eigen::Vector3d::Zero();
          dpdu((axis + 1) % 3) = 1.;
          Eigen::Vector3d dpdv = Eigen::Vector3d::Zero();
          dpdv((axis + 2) % 3) = 1.;
          Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
          Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
          return rayli::SurfaceGeometry{
            {p((axis + 1) % 3), p((axis + 2) % 3)}, dpdu, dpdv, dndu, dndv};
      }};
}
}  // namespace rayli::vgrid
