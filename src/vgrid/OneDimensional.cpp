#include <vgrid/OneDimensional.hpp>

namespace rayli::vgrid {
std::array<size_t, 3>
OneDimensional::find_grid_coordinates(const Eigen::Vector3d& x,
                                      const Eigen::Vector3d& direction) const {
    size_t iz_above = std::distance(
      levels.begin(), std::upper_bound(levels.begin(), levels.end(), x(2)));
    size_t iz = iz_above - 1;
    // deal with rays on edges
    if (gz(iz) == x(2) && direction(2) < 0) {
        iz -= 1;
    }
    return {0, 0, iz};
}
}  // namespace rayli::vgrid
