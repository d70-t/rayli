#include <GeometryUtils.hpp>
#include <vgrid/BallVGrid.hpp>

namespace rayli::vgrid {

BallVGrid::RayIterator::RayIterator(const Self& grid_, Ray ray_, double tnear_,
                                    double tfar_)
    : grid(&grid_), ray(ray_), tnear(tnear_), tfar(tfar_) {
    ray.o -= grid->center;  // transform in coordinates with center == 0
    double tclose = -ray.o.dot(ray.d);
    double dclose = ray(tclose).norm();
    iclose = std::distance(
      begin(grid->radii),
      std::upper_bound(begin(grid->radii), end(grid->radii), dclose));
    inbound = tclose > tnear;
    if (iclose >= grid->radii.size()) {
        // missing grid
        invalidate();
        return;
    }
    // icurrent is the index of the current volume (i.e. the enclosing radius)
    size_t icurrent = std::distance(
      begin(grid->radii), std::upper_bound(begin(grid->radii), end(grid->radii),
                                           ray(tnear).norm()));
    if (icurrent >= grid->radii.size()) {
        // outside of grid
        if (inbound) {
            // advance into grid
            iout = icurrent - 1;
            tnext = intersect_t_next(iout);
            advance();
        } else {
            // missing grid
            invalidate();
            return;
        }
    } else {
        // inside of grid
        if (iclose == icurrent) {
            inbound = false;
        }
        if (inbound) {
            iin = icurrent;
            iout = icurrent - 1;
        } else {
            iout = icurrent;
            if (iclose == icurrent) {
                iin = icurrent;
            } else {
                iin = icurrent - 1;
            }
        }
        tnext = std::min(tfar, intersect_t_next(iout));
    }
}

BallVGrid::RayIterator::reference BallVGrid::RayIterator::operator*() const {
    assert(is_valid());
    return VolumeSlice{tnear, tnext, inbound ? iin : iout};
}

void BallVGrid::RayIterator::advance() {
    iin = iout;
    if (inbound) {
        if (iin == iclose) {
            iout = iin;
            inbound = false;
        } else {
            iout = iin - 1;
        }
    } else {
        iout = iin + 1;
        if (iout >= grid->radii.size()) {
            invalidate();
            return;
        }
    }
    tnear = tnext;
    tnext = intersect_t_next(iout);
}

double BallVGrid::RayIterator::intersect_t_next(size_t i) {
    double t1, t2;
    if (!ray_ball_intersection(grid->radii[i], ray, &t1, &t2)) {
        return std::numeric_limits<double>::quiet_NaN();
    }
    if (inbound) {
        return t1;
    } else {
        return t2;
    }
}
}  // namespace rayli::vgrid
