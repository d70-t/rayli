#include <vgrid/AASurfVGrid.hpp>

namespace rayli::vgrid {

AASurfVGrid::RayIterator::RayIterator(const Self& grid, Ray ray, double tnear,
                                      double tfar) {
    axis = grid.axis;
    t = (grid.location - ray.o(axis)) / ray.d(axis);
    if (!(t >= tnear && t <= tfar)) {
        t = std::numeric_limits<double>::quiet_NaN();
    }
}

const AASurfVGrid::RayIterator::value_type
AASurfVGrid::RayIterator::operator*() const {
    auto axis = this->axis;
    return SurfaceSlice{
      t, 0, [axis](const Ray& ray, const SurfaceSlice& slice) {
          Eigen::Vector3d p = ray(slice.t);
          Eigen::Vector3d dpdu = Eigen::Vector3d::Zero();
          dpdu((axis + 1) % 3) = 1.;
          Eigen::Vector3d dpdv = Eigen::Vector3d::Zero();
          dpdv((axis + 2) % 3) = 1.;
          Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
          Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
          return rayli::SurfaceGeometry{
            {p((axis + 1) % 3), p((axis + 2) % 3)}, dpdu, dpdv, dndu, dndv};
      }};
}
}  // namespace rayli::vgrid
