#include <utils.hpp>
#include <vgrid/ConvexPolytopes.hpp>

#include <map>
#include <set>

namespace rayli::vgrid {

bool ConvexPolytopes::Polytope::includes(const Eigen::Vector3d& p) const {
    for (size_t i = 0; i < face_count(); ++i) {
        if (distance_to_face(i, p) < 0) {
            return false;
        }
    }
    return true;
}

ConvexPolytopes::PolytopeIntersectionResult
ConvexPolytopes::Polytope::intersect(const Ray& r) const {
    PolytopeIntersectionResult res;
    size_t begin = first_plane_idx();
    size_t end = last_plane_idx();
    for (size_t p = begin; p < end; ++p) {
        const auto& i = cp.planes_of_polytopes[p];
        const auto& plane = cp.planes[i.v()];

        double ofs = plane.n.dot(r.d);
        double d = plane.distance(r.o);
        if (!i.b()) {
            ofs = -ofs;
            d = -d;
        }
        if (ofs > 0) {
            if (d < res.t2 * ofs) {
                res.t2 = d / ofs;
                res.plane2 = i;
            }
        } else if (ofs < 0) {
            if (d < res.t1 * ofs) {
                res.t1 = d / ofs;
                res.plane1 = i;
            }
        } else if (d < 0) {
            // parallel to halfspace, could only intersect if origin is inside
            res.t1 = -std::numeric_limits<double>::infinity();
            res.t2 = std::numeric_limits<double>::infinity();
            return res;
        }
    }

    if (res.t1 > res.t2) {
        res.t1 = -std::numeric_limits<double>::infinity();
        res.t2 = std::numeric_limits<double>::infinity();
    }
    return res;
}

ConvexPolytopes::PolytopeIntersectionResultSingle
ConvexPolytopes::Polytope::intersect2(const Ray& r) const {
    PolytopeIntersectionResultSingle res;
    size_t begin = first_plane_idx();
    size_t end = last_plane_idx();
    for (size_t p = begin; p < end; ++p) {
        const auto& i = cp.planes_of_polytopes[p];
        const auto& plane = cp.planes[i.v()];

        bool dir = i.b();
        double ofs = plane.n.dot(r.d) * (dir ? 1 : -1);
        if (ofs < 0) {
            // inbound plane
            continue;
        }
        double d = plane.distance(r.o) * (dir ? 1 : -1);
        if (ofs > 0) {
            if (d < res.t * ofs) {
                res.t = d / ofs;
                res.plane = i;
            }
        } else if (d < 0) {
            // parallel to halfspace, could only intersect if origin is inside
            res.t = std::numeric_limits<double>::infinity();
            return res;
        }
    }
    return res;
}

Bounds3d ConvexPolytopes::Polytope::get_bounds() const {
    assert(vertex_count() > 0);
    Bounds3d bounds(get_vertex(0));
    for (size_t i = 1; i < vertex_count(); ++i) {
        bounds.extend_inplace(get_vertex(i));
    }
    return bounds;
}

ConvexPolytopes::RayIterator::RayIterator(const Self& grid_, Ray ray_,
                                          double tnear_, double tfar_,
                                          const RayIterator& last_iterator)
    : grid(&grid_), ray(ray_), tnear(tnear_), tfar(tfar_) {
    state = State::ENTER;
    initialize_from_other(last_iterator);
}

ConvexPolytopes::RayIterator::reference
ConvexPolytopes::RayIterator::operator*() const {
    switch (state) {
    case State::ENTER:
        throw std::runtime_error("invalid state");
    case State::ON_FACE: {
        auto compute_geometry = [this](const Ray& ray,
                                       const SurfaceSlice& slice) {
            // TODO: find a definition for u and v!
            // Eigen::Vector3d p = ray(slice.t);
            double u = 0;
            double v = 0;
            Eigen::Vector3d dpdu = this->grid->face_dpdu[slice.idx];
            Eigen::Vector3d dpdv = dpdu.cross(this->grid->planes[slice.idx].n);

            Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
            Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
            return rayli::SurfaceGeometry{{u, v}, dpdu, dpdv, dndu, dndv};
        };
        return SurfaceSlice{current_polytope.intersection.t1,
                            current_polytope.intersection.plane1.v(),
                            compute_geometry};
    }
    case State::IN_VOLUME:
        return VolumeSlice{current_polytope.intersection.t1,
                           current_polytope.intersection.t2,
                           current_polytope.polytope};
    case State::EXIT:
        throw std::runtime_error("invalid state");
    }
    throw std::runtime_error("invalid state");
}

void ConvexPolytopes::RayIterator::initialize() {
    if (state == State::ON_FACE) {
        current_polytope =
          grid->find_first_polytope(ray, tnear, tfar, last_polytope_id);
    } else {
        current_polytope = grid->find_first_polytope(ray, tnear, tfar);
    }
    if (current_polytope.polytope >= grid->polytope_count()) {
        invalidate();
        return;
    }
    if (current_polytope.intersection.t1 >= tnear) {
        state = State::ON_FACE;
    } else {
        state = State::IN_VOLUME;
        current_polytope.intersection.t1 = tnear;
        current_polytope.intersection.plane1 = {};
    }
}

void ConvexPolytopes::RayIterator::initialize_from_other(
  const RayIterator& other) {
    if (!other.is_valid()) {
        return initialize();
    }
    switch (other.state) {
    case State::ENTER:
        return initialize();
    case State::ON_FACE: {
        state = State::ON_FACE;
        current_polytope.intersection.t1 = tnear;
        current_polytope.intersection.plane1 =
          other.current_polytope.intersection.plane1;
        const auto& polys =
          grid->polytopes_of_planes[current_polytope.intersection.plane1.v()];
        if (ray.d.dot(
              grid->planes[current_polytope.intersection.plane1.v()].n) < 0) {
            current_polytope.polytope = polys[0];
        } else {
            current_polytope.polytope = polys[1];
        }
        auto res =
          grid->get_polytope(current_polytope.polytope).intersect2(ray);
        if (res.t < tnear) {
            return initialize();
        }
        current_polytope.intersection.t2 = res.t;
        current_polytope.intersection.plane2 = res.plane;
        advance();
        return;
    }
    case State::IN_VOLUME: {
        state = State::IN_VOLUME;
        current_polytope.intersection.t1 = tnear;
        current_polytope.intersection.plane1 = {};
        current_polytope.polytope = other.current_polytope.polytope;
        auto res =
          grid->get_polytope(current_polytope.polytope).intersect2(ray);
        if (res.t < tnear) {
            return initialize();
        }
        current_polytope.intersection.t2 = res.t;
        current_polytope.intersection.plane2 = res.plane;
        return;
    }
    case State::EXIT:
        return initialize();
    }
    throw std::runtime_error("invalid state");
}

void ConvexPolytopes::RayIterator::advance() {
    switch (state) {
    case State::ENTER:
        invalidate();
        break;
    case State::ON_FACE:
        if (current_polytope.polytope < grid->polytope_count()) {
            state = State::IN_VOLUME;
        } else {
            if (grid->is_convex) {
                invalidate();
            } else {
                tnear = current_polytope.intersection.t1;
                initialize();
            }
        }
        break;
    case State::IN_VOLUME: {
        const auto& polys =
          grid->polytopes_of_planes[current_polytope.intersection.plane2.v()];
        size_t next_polytope = polys[0];
        if (next_polytope == current_polytope.polytope) {
            next_polytope = polys[1];
        }
        last_polytope_id = current_polytope.polytope;
        current_polytope.polytope = next_polytope;
        current_polytope.intersection.t1 = current_polytope.intersection.t2;
        current_polytope.intersection.plane1 = {
          current_polytope.intersection.plane2.v(),
          !current_polytope.intersection.plane2.b()};
        if (next_polytope < grid->polytope_count()) {
            auto res = grid->get_polytope(next_polytope).intersect2(ray);
            current_polytope.intersection.t2 = res.t;
            current_polytope.intersection.plane2 = res.plane;
        }
        state = State::ON_FACE;
        break;
    }
    case State::EXIT:
        invalidate();
        break;
    }
    // in case we get stuck in an element we advance again
    // (e.g. happens in case of rounding errors where a camera looks at a
    // vertical wall an the sun direction is aligned with the wall)
    if (state == State::IN_VOLUME) {
        if (current_polytope.intersection.t1 >
            current_polytope.intersection.t2) {
            advance();
        }
    }
}

ConvexPolytopes::PolytopeSearchResult
ConvexPolytopes::find_first_polytope(const Ray& r, double tnear, double tfar,
                                     size_t exclude_polytope) const {
    PolytopeSearchResult res = {};
    if (polytope_finding_accel) {
        double t;
        polytope_finding_accel->find_intersecting_element(
          r, tnear, tfar, t,
          [&](size_t element_id, const Ray& r, double tnear, double tfar,
              double& tnext) {
              if (element_id == exclude_polytope) return false;
              auto intersection = get_polytope(element_id).intersect(r);
              if (std::isfinite(intersection.t1) && intersection.t1 >= tnear &&
                  (intersection.t1 < res.intersection.t1 ||
                   !std::isfinite(res.intersection.t1))) {
                  res.polytope = element_id;
                  res.intersection = intersection;
                  tnext = intersection.t1;
                  return true;
              } else if (std::isfinite(intersection.t2) &&
                         intersection.t2 > tnear &&
                         intersection.t2 < res.intersection.t2) {
                  res.polytope = element_id;
                  res.intersection = intersection;
                  tnext = tnear;
                  return true;
              }
              return false;
          });
    } else {
        for (size_t i = 0; i < polytope_count(); ++i) {
            if (i == exclude_polytope) continue;
            auto intersection = get_polytope(i).intersect(r);
            if (std::isfinite(intersection.t1) && intersection.t1 >= tnear &&
                (intersection.t1 < res.intersection.t1 ||
                 !std::isfinite(res.intersection.t1))) {
                res.polytope = i;
                res.intersection = intersection;
            } else if (std::isfinite(intersection.t2) &&
                       intersection.t2 > tnear &&
                       intersection.t2 < res.intersection.t2) {
                res.polytope = i;
                res.intersection = intersection;
            }
        }
    }
    return res;
}

void ConvexPolytopes::repair_polytopes_of_planes() {
    polytopes_of_planes.resize(planes.size());
    for (auto& el : polytopes_of_planes) {
        el = {std::numeric_limits<size_t>::max(),
              std::numeric_limits<size_t>::max()};
    }
    for (size_t i = 0; i < polytope_count(); ++i) {
        for (size_t j = polytope_plane_indices[i];
             j < polytope_plane_indices[i + 1]; ++j) {
            auto plane = planes_of_polytopes[j];
            assert(plane.v() < polytopes_of_planes.size());
            if (plane.b()) {
                polytopes_of_planes[plane.v()][0] = i;
            } else {
                polytopes_of_planes[plane.v()][1] = i;
            }
        }
    }
}

void ConvexPolytopes::repair_bounds() {
    bounds = Bounds3d(vertices.front());
    for (auto vert : vertices) {
        bounds.extend_inplace(vert);
    }
}

void ConvexPolytopes::build_accelerators() {
    std::vector<Bounds3d> polytope_bounds;
    polytope_bounds.reserve(polytope_count());
    for (size_t i = 0; i < polytope_count(); ++i) {
        polytope_bounds.push_back(get_polytope(i).get_bounds());
    }
    polytope_finding_accel = std::make_unique<KDAccelerator>(polytope_bounds);
}

ConvexPolytopes polytopes_from_hexahedra(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 8>>& vertices_of_hexahedra) {
    auto cp = ConvexPolytopes{};
    cp.planes.clear();

    std::map<std::array<size_t, 4>, size_t> verts2faces;
    auto insert_face =
      [&](std::array<size_t, 4> verts) -> IndexWithBool<size_t> {
        Eigen::Vector3d n1 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        std::sort(verts.begin(), verts.end());
        Eigen::Vector3d n2 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        bool orientation = n1.dot(n2) > 0;
        auto [it, success] = verts2faces.try_emplace(verts, cp.planes.size());
        if (success) {
            double d = n2.dot(vertices[verts[0]]);
            cp.planes.push_back({n2, d});
            Eigen::Vector3d dpdu =
              (vertices[verts[1]] - vertices[verts[0]]).normalized();
            cp.face_dpdu.push_back(dpdu);
            return {cp.planes.size() - 1, orientation};
        } else {
            return {it->second, orientation};
        }
    };

    cp.vertices = vertices;
    cp.vertices_of_polytopes.resize(8 * vertices_of_hexahedra.size());
    cp.planes_of_polytopes.resize(6 * vertices_of_hexahedra.size());
    cp.polytope_vertex_indices.resize(vertices_of_hexahedra.size() + 1);
    cp.polytope_vertex_indices[0] = 0;
    cp.polytope_plane_indices.resize(vertices_of_hexahedra.size() + 1);
    cp.polytope_plane_indices[0] = 0;
    for (size_t i = 0; i < vertices_of_hexahedra.size(); ++i) {
        const auto& verts = vertices_of_hexahedra[i];
        for (size_t j = 0; j < 8; ++j) {
            cp.vertices_of_polytopes[8 * i + j] = verts[j];
        }
        cp.polytope_vertex_indices[i + 1] = 8 * (i + 1);
        cp.planes_of_polytopes[6 * i + 0] =
          insert_face({verts[3], verts[2], verts[1], verts[0]});
        cp.planes_of_polytopes[6 * i + 1] =
          insert_face({verts[0], verts[1], verts[5], verts[4]});
        cp.planes_of_polytopes[6 * i + 2] =
          insert_face({verts[1], verts[2], verts[6], verts[5]});
        cp.planes_of_polytopes[6 * i + 3] =
          insert_face({verts[2], verts[3], verts[7], verts[6]});
        cp.planes_of_polytopes[6 * i + 4] =
          insert_face({verts[3], verts[0], verts[4], verts[7]});
        cp.planes_of_polytopes[6 * i + 5] =
          insert_face({verts[4], verts[5], verts[6], verts[7]});
        cp.polytope_plane_indices[i + 1] = 6 * (i + 1);
    }
    cp.repair_polytopes_of_planes();
    cp.repair_bounds();
    return cp;
}

ConvexPolytopes polytopes_from_wedges(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 6>> vertices_of_wedges) {

    auto cp = ConvexPolytopes{};
    cp.planes.clear();

    std::map<std::array<size_t, 3>, size_t> verts3_2faces;
    std::map<std::array<size_t, 4>, size_t> verts4_2faces;

    auto insert_face3 =
      [&](std::array<size_t, 3> verts) -> IndexWithBool<size_t> {
        Eigen::Vector3d n1 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        std::sort(verts.begin(), verts.end());
        Eigen::Vector3d n2 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        bool orientation = n1.dot(n2) > 0;
        auto [it, success] = verts3_2faces.try_emplace(verts, cp.planes.size());
        if (success) {
            double d = n2.dot(vertices[verts[0]]);
            cp.planes.push_back({n2, d});
            Eigen::Vector3d dpdu =
              (vertices[verts[1]] - vertices[verts[0]]).normalized();
            cp.face_dpdu.push_back(dpdu);
            return {cp.planes.size() - 1, orientation};
        } else {
            return {it->second, orientation};
        }
    };

    auto insert_face4 =
      [&](std::array<size_t, 4> verts) -> IndexWithBool<size_t> {
        Eigen::Vector3d n1 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        std::sort(verts.begin(), verts.end());
        Eigen::Vector3d n2 = (vertices[verts[1]] - vertices[verts[0]])
                               .cross(vertices[verts[2]] - vertices[verts[0]])
                               .normalized();
        bool orientation = n1.dot(n2) > 0;
        auto [it, success] = verts4_2faces.try_emplace(verts, cp.planes.size());
        if (success) {
            double d = n2.dot(vertices[verts[0]]);
            cp.planes.push_back({n2, d});
            Eigen::Vector3d dpdu =
              (vertices[verts[1]] - vertices[verts[0]]).normalized();
            cp.face_dpdu.push_back(dpdu);
            return {cp.planes.size() - 1, orientation};
        } else {
            return {it->second, orientation};
        }
    };

    cp.vertices = vertices;
    cp.vertices_of_polytopes.resize(6 * vertices_of_wedges.size());
    cp.planes_of_polytopes.resize(5 * vertices_of_wedges.size());
    cp.polytope_vertex_indices.resize(vertices_of_wedges.size() + 1);
    cp.polytope_vertex_indices[0] = 0;
    cp.polytope_plane_indices.resize(vertices_of_wedges.size() + 1);
    cp.polytope_plane_indices[0] = 0;
    for (size_t i = 0; i < vertices_of_wedges.size(); ++i) {
        const auto& verts = vertices_of_wedges[i];
        for (size_t j = 0; j < 6; ++j) {
            cp.vertices_of_polytopes[6 * i + j] = verts[j];
        }
        cp.polytope_vertex_indices[i + 1] = 6 * (i + 1);
        cp.planes_of_polytopes[5 * i + 0] =
          insert_face3({verts[2], verts[1], verts[0]});
        cp.planes_of_polytopes[5 * i + 1] =
          insert_face4({verts[0], verts[1], verts[4], verts[3]});
        cp.planes_of_polytopes[5 * i + 2] =
          insert_face4({verts[1], verts[2], verts[5], verts[4]});
        cp.planes_of_polytopes[5 * i + 3] =
          insert_face4({verts[2], verts[0], verts[3], verts[5]});
        cp.planes_of_polytopes[5 * i + 4] =
          insert_face3({verts[3], verts[4], verts[5]});
        cp.polytope_plane_indices[i + 1] = 5 * (i + 1);
    }
    cp.repair_polytopes_of_planes();
    cp.repair_bounds();
    return cp;
}

ConvexPolytopes polytopes_from_wedges_with_facedefinitions(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 5>>& faces_of_wedges,
  const std::vector<std::array<size_t, 4>>& vertices_of_faces) {

    auto cp = ConvexPolytopes{};
    cp.vertices = vertices;
    cp.vertices_of_polytopes.clear();
    cp.planes_of_polytopes.resize(5 * faces_of_wedges.size());
    cp.polytope_vertex_indices.resize(faces_of_wedges.size() + 1);
    cp.polytope_vertex_indices[0] = 0;
    cp.polytope_plane_indices.resize(faces_of_wedges.size() + 1);
    cp.polytope_plane_indices[0] = 0;

    std::set<size_t> vertices_of_polytope;

    cp.face_dpdu.resize(vertices_of_faces.size());
    cp.planes.resize(vertices_of_faces.size());

    for (size_t iface = 0; iface < vertices_of_faces.size(); ++iface) {
        const auto& verts = vertices_of_faces[iface];

        cp.face_dpdu[iface] =
          (vertices[verts[1]] - vertices[verts[0]]).normalized();

        Eigen::Vector3d n = cp.face_dpdu[iface]
                              .cross(vertices[verts[2]] - vertices[verts[0]])
                              .normalized();

        double d = n.dot(vertices[verts[0]]);
        cp.planes[iface] = {n, d};
    }

    for (size_t icell = 0; icell < faces_of_wedges.size(); ++icell) {
        const auto& faces = faces_of_wedges[icell];
        vertices_of_polytope.clear();
        for (size_t j = 0; j < 5; ++j) {
            for (auto vert : vertices_of_faces[faces[j]]) {
                if (vert != std::numeric_limits<size_t>::max()) {
                    vertices_of_polytope.insert(vert);
                }
            }
        }

        Eigen::Vector3d wedge_center = Eigen::Vector3d::Zero();
        for (auto ivert : vertices_of_polytope) {
            wedge_center += vertices[ivert];
        }
        wedge_center /= vertices_of_polytope.size();

        for (size_t j = 0; j < 5; ++j) {
            size_t iface = faces[j];
            const auto& plane = cp.planes[iface];
            bool orientation =
              plane.n.dot(wedge_center) <
              plane.d;  // true if face normal away from cell center
            cp.planes_of_polytopes[5 * icell + j] = {iface, orientation};
        }

        cp.polytope_plane_indices[icell + 1] = 5 * (icell + 1);

        std::copy(vertices_of_polytope.begin(), vertices_of_polytope.end(),
                  std::back_inserter(cp.vertices_of_polytopes));

        cp.polytope_vertex_indices[icell + 1] = cp.vertices_of_polytopes.size();
    }

    cp.repair_polytopes_of_planes();
    cp.repair_bounds();
    return cp;
}
}  // namespace rayli::vgrid
