#include "vgrid/WedgeGrid.hpp"

namespace rayli::vgrid {

WedgeGrid::WedgeGrid(std::vector<Eigen::Vector3d> vertices,
                     std::vector<std::array<size_t, 6>> cells,
                     std::vector<std::array<size_t, 3>> faces)
    : vertices(std::move(vertices)), cells(std::move(cells)),
      faces(std::move(faces)) {}
}  // namespace rayli::vgrid
