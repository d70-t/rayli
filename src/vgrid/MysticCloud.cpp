#include "vgrid/MysticCloud.hpp"
#include "utils.hpp"

namespace rayli::vgrid {

MysticCloud::MysticCloud(double dx, double dy, size_t Nx, size_t Ny,
                         std::vector<double> z_lev_)
    : CartesianBase<MysticCloud>(
        {{0, 0, z_lev_.front()}, {dx * Nx, dy * Ny, z_lev_.back()}},
        {Nx, Ny, z_lev_.size() - 1}),
      dx(dx), dy(dy) {
    z_lev = std::move(z_lev_);
}

std::array<size_t, 3>
MysticCloud::find_grid_coordinates(const Eigen::Vector3d& x,
                                   const Eigen::Vector3d& direction) const {
    size_t ix = x(0) / dx;
    size_t iy = x(1) / dy;
    size_t iz_above = std::distance(
      z_lev.begin(), std::upper_bound(z_lev.begin(), z_lev.end(), x(2)));
    size_t iz = iz_above - 1;
    // deal with rays on edges
    if (gx(ix) == x(0) && direction(0) < 0) ix -= 1;
    if (gy(iy) == x(1) && direction(1) < 0) iy -= 1;
    if (gz(iz) == x(2) && direction(2) < 0) iz -= 1;
    return {ix, iy, iz};
}
}  // namespace rayli::vgrid
