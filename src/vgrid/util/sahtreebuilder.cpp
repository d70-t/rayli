#include "vgrid/util/sahtreebuilder.hpp"
#include <numeric>

namespace rayli::vgrid::util {

class _RecursiveSAHTreeBuilder {
   public:
    _RecursiveSAHTreeBuilder(RecursiveSAHTreeBuilder& base_builder,
                             const Bounds3d& current_bounds,
                             size_t* active_elements,
                             size_t active_element_count,
                             size_t* spare_elements_buffer, int remaining_depth,
                             int bad_refines);

    size_t build();

   private:
    std::pair<double, size_t> find_best_split_along_axis(int axis);
    void initialize_edges_of_axis(int axis);
    std::pair<double, size_t> compute_lowest_cost_along_axis(int axis);
    size_t create_leaf();
    size_t create_split(int axis, size_t offset);

    RecursiveSAHTreeBuilder& base_builder;

    const Bounds3d& current_bounds;
    size_t* active_elements;
    size_t active_element_count;
    size_t* spare_elements_buffer;
    int remaining_depth;
    int bad_refines;

    double inv_total_surface_area;
    Eigen::Vector3d d;
    double old_cost;
};

RecursiveSAHTreeBuilder::RecursiveSAHTreeBuilder(
  Tree& tree, const std::vector<Bounds3d>& element_bounds,
  std::vector<std::vector<size_t>>& element_ids,
  size_t target_elements_per_node, double intersection_cost,
  double traversal_cost, double empty_bonus)
    : tree(tree), element_bounds(element_bounds), element_ids(element_ids),
      target_elements_per_node(target_elements_per_node),
      intersection_cost(intersection_cost), traversal_cost(traversal_cost),
      empty_bonus(empty_bonus),
      logger(rayli::logging::get_logger("SAH KD Builder")) {
    logger->info("creating builder, trg elems: {}, intersection cost: {}, "
                 "traversal cost: {}, empty bonus: {}",
                 target_elements_per_node, intersection_cost, traversal_cost,
                 empty_bonus);
}

size_t RecursiveSAHTreeBuilder::build(int max_depth) {
    assert(element_bounds.size() > 0);

    // reset to sensible value before extending
    tree.bounds = element_bounds[0];
    for (const auto& eb : element_bounds) {
        tree.bounds.extend_inplace(eb);
    }

    // max_depth heuristic from PBRT Book Page 289
    if (max_depth < 0) {
        max_depth = std::round(8 + 1.3 * std::log2(element_bounds.size()));
    }

    // preallocation of working memory
    for (auto& bev : bound_edges) {
        bev.reserve(2 * element_bounds.size());
    }

    std::vector<size_t> active_elements(element_bounds.size());
    std::iota(active_elements.begin(), active_elements.end(), 0);
    std::vector<size_t> spare_elements_buffer((max_depth + 1) *
                                              element_bounds.size());

    _RecursiveSAHTreeBuilder internal_builder(
      *this, tree.bounds, active_elements.data(), active_elements.size(),
      spare_elements_buffer.data(), max_depth, 0);
    return internal_builder.build();
}

size_t RecursiveSAHTreeBuilder::init_leaf(std::vector<size_t>&& elements) {
    size_t element_ofs = element_ids.size();
    element_ids.push_back(std::move(elements));
    return tree.add_leaf(element_ofs);
}

size_t RecursiveSAHTreeBuilder::init_leaf(size_t* elements,
                                          size_t element_count) {
    return init_leaf(std::vector<size_t>(elements, elements + element_count));
}

_RecursiveSAHTreeBuilder::_RecursiveSAHTreeBuilder(
  RecursiveSAHTreeBuilder& base_builder, const Bounds3d& current_bounds,
  size_t* active_elements, size_t active_element_count,
  size_t* spare_elements_buffer, int remaining_depth, int bad_refines)
    : base_builder(base_builder), current_bounds(current_bounds),
      active_elements(active_elements),
      active_element_count(active_element_count),
      spare_elements_buffer(spare_elements_buffer),
      remaining_depth(remaining_depth), bad_refines(bad_refines) {

    double total_surface_area = current_bounds.surface_area();
    assert(total_surface_area > 0);
    inv_total_surface_area = 1 / total_surface_area;
    d = current_bounds.upper - current_bounds.lower;
    old_cost = base_builder.intersection_cost * active_element_count;

    SPDLOG_DEBUG(base_builder.logger,
                 "created internal builder, "
                 "elems: {}, depth: {}, bad ref: {}, "
                 "bounds: {}, area: {}",
                 active_element_count, remaining_depth, bad_refines,
                 current_bounds, total_surface_area);
}

size_t _RecursiveSAHTreeBuilder::build() {
    if (active_element_count <= base_builder.target_elements_per_node ||
        remaining_depth == 0) {
        SPDLOG_DEBUG(base_builder.logger,
                     "creating leaf early, nelems: {}, remaining depth: {}",
                     active_element_count, remaining_depth);
        return create_leaf();
    }
    size_t best_offset;
    double best_cost;
    int axis = current_bounds.maximum_extent();
    for (int retries = 0; retries < 3; ++retries) {
        std::tie(best_cost, best_offset) = find_best_split_along_axis(axis);
        SPDLOG_DEBUG(base_builder.logger, "best cost along axis {} is: {} @ {}",
                     axis, best_cost, best_offset);
        if (std::isfinite(best_cost)) {
            break;
        }
        axis = (axis + 1) % 3;
    }
    if (best_cost > old_cost) ++bad_refines;
    if ((best_cost > 4 * old_cost && active_element_count < 16) ||
        !std::isfinite(best_cost) || bad_refines >= 3) {
        SPDLOG_DEBUG(base_builder.logger,
                     "creating leaf late, nelems: {}, "
                     "best cost: {}, bad refines: {}",
                     active_element_count, best_cost, bad_refines);
        return create_leaf();
    }
    return create_split(axis, best_offset);
}

std::pair<double, size_t>
_RecursiveSAHTreeBuilder::find_best_split_along_axis(int axis) {
    initialize_edges_of_axis(axis);
    return compute_lowest_cost_along_axis(axis);
}

void _RecursiveSAHTreeBuilder::initialize_edges_of_axis(int axis) {
    auto& current_edges = base_builder.bound_edges[axis];
    current_edges.clear();
    for (size_t i = 0; i < active_element_count; ++i) {
        size_t element_idx = active_elements[i];
        auto& bounds = base_builder.element_bounds[element_idx];
        current_edges.emplace_back(bounds.lower[axis], element_idx,
                                   EdgeType::start);
        current_edges.emplace_back(bounds.upper[axis], element_idx,
                                   EdgeType::end);
    }
    std::sort(current_edges.begin(), current_edges.end(),
              [](const auto& e0, const auto& e1) {
                  if (e0.t == e1.t) {
                      return int(e0.type) < int(e1.type);
                  } else {
                      return e0.t < e1.t;
                  }
              });
}

std::pair<double, size_t>
_RecursiveSAHTreeBuilder::compute_lowest_cost_along_axis(int axis) {
    auto& current_edges = base_builder.bound_edges[axis];

    double best_cost = std::numeric_limits<double>::infinity();
    size_t best_offset = std::numeric_limits<size_t>::max();

    // compute cost for every potential split
    size_t n_below = 0;
    size_t n_above = active_element_count;

    Bounds3d below_bounds = current_bounds;
    Bounds3d above_bounds = current_bounds;

    for (size_t i = 0; i < current_edges.size(); ++i) {
        const auto& edge = current_edges[i];
        if (edge.type == EdgeType::end) --n_above;
        if (edge.t > current_bounds.lower(axis) &&
            edge.t < current_bounds.upper(axis)) {
            // potential split is strictly inside outer node
            below_bounds.upper(axis) = edge.t;
            above_bounds.lower(axis) = edge.t;
            double p_below =
              below_bounds.surface_area() * inv_total_surface_area;
            double p_above =
              above_bounds.surface_area() * inv_total_surface_area;
            double eb =
              (n_above == 0 || n_below == 0) ? base_builder.empty_bonus : 0;
            double cost = base_builder.traversal_cost +
                          base_builder.intersection_cost * (1 - eb) *
                            (p_below * n_below + p_above * n_above);
            if (cost < best_cost) {
                best_cost = cost;
                best_offset = i;
            }
        }
        if (edge.type == EdgeType::start) ++n_below;
    }
    return {best_cost, best_offset};
}

size_t _RecursiveSAHTreeBuilder::create_leaf() {
    size_t node_idx =
      base_builder.init_leaf(active_elements, active_element_count);
    SPDLOG_DEBUG(base_builder.logger, "created leaf of {} elements, idx: {}",
                 active_element_count, node_idx);
    return node_idx;
}

size_t _RecursiveSAHTreeBuilder::create_split(int axis, size_t offset) {
    auto& current_edges = base_builder.bound_edges[axis];
    size_t n0 = 0, n1 = 0;
    for (size_t i = 0; i < offset; ++i) {
        if (current_edges[i].type == EdgeType::start) {
            active_elements[n0++] = current_edges[i].id;
        }
    }
    for (size_t i = offset + 1; i < current_edges.size(); ++i) {
        if (current_edges[i].type == EdgeType::end) {
            spare_elements_buffer[n1++] = current_edges[i].id;
        }
    }

    assert(n0 <= active_element_count);
    assert(n1 <= active_element_count);

    double tsplit = current_edges[offset].t;

    Bounds3d below_bounds = current_bounds;
    Bounds3d above_bounds = current_bounds;

    below_bounds.upper[axis] = tsplit;
    above_bounds.lower[axis] = tsplit;

    size_t node_idx = base_builder.tree.add_split(axis, tsplit);
    SPDLOG_DEBUG(base_builder.logger,
                 "creating split ax: {}, t: {}, n0: {}, n1: {}, idx: {}", axis,
                 tsplit, n0, n1, node_idx);
    {
        _RecursiveSAHTreeBuilder lower_builder(
          base_builder, below_bounds, active_elements, n0,
          spare_elements_buffer + n1, remaining_depth - 1, bad_refines);
        lower_builder.build();
    }
    {
        _RecursiveSAHTreeBuilder upper_builder(
          base_builder, above_bounds, spare_elements_buffer, n1,
          spare_elements_buffer + n1, remaining_depth - 1, bad_refines);
        auto child_id = upper_builder.build();
        base_builder.tree.nodes[node_idx].set_second_child(child_id);
        SPDLOG_DEBUG(base_builder.logger, "second child of {} is {}", node_idx,
                     child_id);
    }
    return node_idx;
}
}  // namespace rayli::vgrid::util
