#include <vgrid/TriCell.hpp>

namespace rayli::vgrid {
TriCell::TriCell(
  std::shared_ptr<rayli::surface::TriangularMesh> mesh,
  std::shared_ptr<std::vector<std::array<size_t, 2>>> cells_of_faces)
    : mesh(mesh), cells_of_faces(cells_of_faces) {
    assert(mesh->face_count() == cells_of_faces->size());
    face_count = cells_of_faces->size();
    logger = rayli::logging::get_logger("TriCell");
}

TriCell::RayIterator::RayIterator(const Self& grid, Ray ray, double tnear_,
                                  double tfar_)
    : grid(grid), ray(std::move(ray)), tnear(tnear_), tfar(tfar_), t1(0),
      t2(0) {
    face1 = face2 = grid.face_count;
    state = State::UNINITIALIZED;
    advance();
}

LinearSlice TriCell::RayIterator::operator*() const {
    const auto& mesh = *(grid.mesh);
    auto compute_geometry = [&mesh](const Ray& ray, const SurfaceSlice& slice) {
        Eigen::Vector3d p = ray(slice.t);
        auto face = mesh.get_face(slice.idx);
        auto v0 = face.v(0);
        Eigen::Vector3d dpdu = face.v(1) - v0;
        Eigen::Vector3d dpdv = face.v(2) - v0;
        double u = std::clamp((p - v0).dot(dpdu), 0., 1.);
        double v = std::clamp((p - v0).dot(dpdv), 0., 1.);

        Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
        Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
        return rayli::SurfaceGeometry{{u, v}, dpdu, dpdv, dndu, dndv};
    };

    switch (state) {
    case State::VOL1:
        return VolumeSlice{tnear, t1, volume};
        break;
    case State::VOL2:
        return VolumeSlice{t1, std::min(t2, tfar), volume};
    case State::VOL3:
        return VolumeSlice{t2, tfar, volume};
        break;
    case State::FACE1:
        return SurfaceSlice{t1, face1, compute_geometry};
        break;
    case State::FACE2:
        return SurfaceSlice{t2, face2, compute_geometry};
        break;
    default:
        return VolumeSlice{std::numeric_limits<double>::quiet_NaN(),
                           std::numeric_limits<double>::quiet_NaN(),
                           std::numeric_limits<size_t>::max()};
    }
}

void TriCell::RayIterator::advance() {
    switch (state) {
    case State::UNINITIALIZED:
        advance_face();
        if (face2 >= grid.face_count) {
            state = State::INVALID;
            break;
        }
        advance_face();
        if (state == State::FACE2) {
            volume = volume_id(2);
            if (is_valid_volume(volume)) {
                state = State::VOL2;
            }
        } else {
            state = State::VOL1;
            volume = volume_id(1);
            if (!is_valid_volume(volume)) {
                state = State::FACE1;
            }
        }
        break;
    case State::VOL1:
        state = State::FACE1;
        break;
    case State::VOL2:
        if (t2 > tfar) {
            state = State::INVALID;
            break;
        }
        state = State::FACE1;
        advance_face();
        break;
    case State::VOL3:
        state = State::INVALID;
        break;
    case State::FACE1:
        volume = volume_id(2);
        if (!is_valid_volume(volume)) {
            if (t2 > tfar) {
                state = State::INVALID;
            } else {
                advance_face();
            }
        } else {
            state = State::VOL2;
        }
        break;
    case State::FACE2:
        state = State::INVALID;
        /*
        state = State::VOL3;
        volume = volume_id(3);
        if (!is_valid_volume(volume)) {
            state = State::INVALID;
        }
        */
        break;
    case State::INVALID:
        break;
    }

    switch (state) {
    case State::UNINITIALIZED:
        SPDLOG_DEBUG(grid.logger, "-> UNINITIALIZED");
        break;
    case State::VOL1:
        SPDLOG_DEBUG(grid.logger, "-> VOL1");
        break;
    case State::VOL2:
        SPDLOG_DEBUG(grid.logger, "-> VOL2");
        break;
    case State::VOL3:
        SPDLOG_DEBUG(grid.logger, "-> VOL3");
        break;
    case State::FACE1:
        SPDLOG_DEBUG(grid.logger, "-> FACE1");
        break;
    case State::FACE2:
        SPDLOG_DEBUG(grid.logger, "-> FACE2");
        break;
    case State::INVALID:
        SPDLOG_DEBUG(grid.logger, "-> INVALID");
        break;
    }
}

void TriCell::RayIterator::advance_face() {
    double t = t2;
    size_t next_idx = grid.mesh->find_intersecting_face_idx(
      ray, tnear, std::numeric_limits<double>::infinity(), t);
    while (next_idx < grid.face_count && next_idx == face2) {
        tnear = std::nextafter(t, std::numeric_limits<double>::infinity());
        next_idx = grid.mesh->find_intersecting_face_idx(
          ray, tnear, std::numeric_limits<double>::infinity(), t);
    }
    if (next_idx >= grid.face_count) {
        state = State::FACE2;
    } else {
        face1 = face2;
        t1 = t2;
        face2 = next_idx;
        t2 = t;
    }
}

size_t TriCell::RayIterator::volume_id(int v) {
    if (face1 >= grid.face_count && face2 >= grid.face_count) {
        return std::numeric_limits<size_t>::max();
    }
    if (face1 >= grid.face_count || face2 >= grid.face_count) {
        auto [a, b] = (*grid.cells_of_faces)[std::min(face1, face2)];
        if (is_valid_volume(std::max(a, b))) {
            // can not decide
            return std::numeric_limits<size_t>::max();
        } else {
            return std::min(a, b);
        }
    }
    auto [a, b] = (*grid.cells_of_faces)[face1];
    auto [b2, c] = (*grid.cells_of_faces)[face2];
    if (is_valid_volume(a) && (a == b2 || a == c)) {
        std::swap(a, b);
    }
    if (is_valid_volume(c) && c == b) {
        std::swap(c, b2);
    }
    switch (v) {
    case 1:
        return a;
        break;
    case 2:
        return b;
        break;
    case 3:
        return c;
        break;
    }
    return std::numeric_limits<size_t>::max();
}
}  // namespace rayli::vgrid
