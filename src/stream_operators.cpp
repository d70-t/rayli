#include "stream_operators.hpp"

std::ostream& operator<<(std::ostream& stream, const VolumeSlice& slice) {
    stream << "VolumeSlice(" << slice.tnear << ", " << slice.tfar << ", "
           << slice.idx << ")";
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Teleport& slice) {
    stream << "Teleport(" << slice.t << ", " << slice.r << ")";
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const SurfaceSlice& slice) {
    stream << "SurfaceSlice(" << slice.t << ", " << slice.idx << ")";
    return stream;
}

std::ostream& operator<<(std::ostream& stream,
                         const LinearSliceTeleport& slice) {
    stream << "LinearSliceTeleport(";
    std::visit([&](const auto& s) { stream << s; }, slice);
    stream << ")";
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const LinearSlice& slice) {
    stream << "LinearSlice(";
    std::visit([&](const auto& s) { stream << s; }, slice);
    stream << ")";
    return stream;
}
