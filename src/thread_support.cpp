#include "thread_support.hpp"

#include <cstdlib>
#include <iostream>
#include <thread>

int get_env_int(const char* name) {
    char* val = std::getenv(name);
    if (!val) return -1;
    return std::atoi(val);
}

int cluster_aware_concurrency() {
    int task_cpus = get_env_int("SLURM_CPUS_PER_TASK");
    if (task_cpus > 0) {
        std::cerr << "concurrency is " << task_cpus
                  << " as determined by SLURM_CPUS_PER_TASK" << std::endl;
        return task_cpus;
    }
    int job_cpus = get_env_int("SLURM_JOB_CPUS_PER_NODE");
    if (job_cpus > 0) {
        std::cerr << "concurrency is " << job_cpus
                  << " as determined by SLURM_JOB_CPUS_PER_NODE" << std::endl;
        return job_cpus;
    }
    int node_cpus = get_env_int("SLURM_CPUS_ON_NODE");
    if (node_cpus > 0) {
        std::cerr << "concurrency is " << node_cpus
                  << " as determined by SLURM_CPUS_ON_NODE" << std::endl;
        return node_cpus;
    }
    int omp_cpus = get_env_int("OMP_NUM_THREADS");
    if (omp_cpus > 0) {
        std::cerr << "concurrency is " << omp_cpus
                  << " as determined by OMP_NUM_THREADS" << std::endl;
        return omp_cpus;
    }
    return std::thread::hardware_concurrency();
}

bool is_running_in_slurm() {
    int slurm_job_id = get_env_int("SLURM_JOB_ID");
    return slurm_job_id > 0;
}
