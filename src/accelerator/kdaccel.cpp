#include "accelerator/kdaccel.hpp"
#include "vgrid/util/sahtreebuilder.hpp"

#include "logging.hpp"
#include <numeric>

KDAccelerator::KDAccelerator(const std::vector<Bounds3d>& element_bounds,
                             size_t target_elements_per_node,
                             double intersection_cost, double traversal_cost,
                             double empty_bonus, int max_depth)
    : element_count(element_bounds.size()) {

    rayli::vgrid::util::RecursiveSAHTreeBuilder builder(
      tree, element_bounds, element_ids, target_elements_per_node,
      intersection_cost, traversal_cost, empty_bonus);
    builder.build(max_depth);
}
