#include "grid/ICONGrid.hpp"
#include <iostream>

namespace rayli::grid {

std::array<size_t, 3> ICONGrid::Cell::neighbor_cells() const {
    std::array<size_t, 3> out;
    const auto& edges = iedges();
    for (int i = 0; i < 3; ++i) {
        const auto& edge = grid.get_edge(edges[i]);
        size_t inner_id = edge.inner_cell().id();
        if (inner_id == id()) {
            out[i] = edge.outer_cell().id();
        } else {
            out[i] = inner_id;
        }
    }
    return out;
}

bool ICONGrid::Edge::intersect_plane(const Ray& ray, double tnear, double tfar,
                                     double& t) const {
    auto& n = normal();
    double dn = ray.d.dot(n);
    if (dn == 0) {  // ray is parallel
        return false;
    }
    t = -ray.o.dot(n) / dn;
    if (t < tnear || t > tfar) {
        return false;
    }
    return true;
}

bool ICONGrid::Edge::intersect(const Ray& ray, double tnear, double tfar,
                               double& t) const {
    if (!intersect_plane(ray, tnear, tfar, t)) {
        return false;
    }
    Eigen::Vector3d p = ray(t);
    double r = p.norm();
    if (r < grid.rmin || r > grid.rmax) {
        return false;
    }
    return is_between_vertices(p);
}

Bounds3d ICONGrid::Edge::get_bounds() const {
    auto [ia, ib] = vert_indices();
    const Eigen::Vector3d& a = grid.vertices[ia];
    const Eigen::Vector3d& b = grid.vertices[ib];
    const Eigen::Vector3d& n = normal();
    Bounds3d bounds(grid.rmin * a);
    bounds.extend_inplace(grid.rmin * b);
    bounds.extend_inplace(grid.rmax * a);
    bounds.extend_inplace(grid.rmax * b);
    // the outer arc might require slightly larger bounds
    for (int i = 0; i < 3; ++i) {
        Eigen::Vector3d axis = Eigen::Vector3d::Zero();
        axis(i) = 1;
        Eigen::Vector3d v = axis.cross(n);
        if (v.norm() == 0) continue;
        v.normalize();
        if (is_between_vertices(v)) {
            bounds.extend_inplace(grid.rmax * v);
        } else if (is_between_vertices(-v)) {
            bounds.extend_inplace(-grid.rmax * v);
        }
    }
    return bounds;
}

bool ICONGrid::Edge::is_between_vertices(const Eigen::Vector3d& p) const {
    Eigen::Vector3d pn = p.normalized();

    auto [ia, ib] = vert_indices();
    const Eigen::Vector3d& a = grid.vertices[ia];
    const Eigen::Vector3d& b = grid.vertices[ib];

    return (b.dot(a) < b.dot(pn)) && (b.cross(a).dot(b.cross(pn)) > 0);
}

size_t ICONGrid::find_cell_idx(const Eigen::Vector3d& point) const {
    size_t i;
    for (i = 0; i < cell_count(); ++i) {
        if (get_cell(i).contains_point(point)) {
            break;
        }
    }
    return i;
}

size_t ICONGrid::find_intersecting_edge_idx(const Ray& ray, double tnear,
                                            double tfar, double& t) const {
    if (edge_finding_accel) {
        return edge_finding_accel->find_intersecting_element(
          ray, tnear, tfar, t,
          [&](size_t element_id, const Ray& ray, double tnear, double tfar,
              double& tnext) {
              return get_edge(element_id).intersect(ray, tnear, tfar, tnext);
          });
    } else {
        size_t iout = edge_count();
        t = std::numeric_limits<double>::infinity();
        double tnext;
        for (size_t i = 0; i < edge_count(); ++i) {
            if (get_edge(i).intersect(ray, tnear, tfar, tnext)) {
                if (tnext < t) {
                    t = tnext;
                    iout = i;
                }
            }
        }
        return iout;
    }
}

void ICONGrid::repair_vertices() {
    for (auto& vertex : vertices) {
        vertex.normalize();
    }
}

void ICONGrid::repair_edge_cells() {
    edge_cells.resize(edge_count(), {cell_count(), cell_count()});
    for (size_t i = 0; i < cell_count(); ++i) {
        for (size_t j = 0; j < 3; ++j) {
            if (cells[i].edge_directions[j]) {
                edge_cells[cells[i].edges[j]].first = i;
            } else {
                edge_cells[cells[i].edges[j]].second = i;
            }
        }
    }
}

void ICONGrid::repair_edge_normals() {
    for (size_t i = 0; i < edge_count(); ++i) {
        edge_normals[i] = vertices[edge_vertices[i].first]
                            .cross(vertices[edge_vertices[i].second])
                            .normalized();
    }
}

void ICONGrid::repair_edge_orientations() {
    for (auto& cell : cells) {
        for (int i = 0; i < 3; ++i) {
            cell.edge_directions[i] = (((vertices[cell.vertices[i]] +
                                         vertices[cell.vertices[(i + 1) % 3]]) /
                                        2) -
                                       vertices[cell.vertices[(i + 2) % 3]])
                                        .dot(edge_normals[cell.edges[i]]) > 0;
        }
    }
}

void ICONGrid::build_accelerators() {
    std::vector<Bounds3d> edge_bounds;
    edge_bounds.reserve(edge_count());
    for (size_t i = 0; i < edge_count(); ++i) {
        edge_bounds.push_back(get_edge(i).get_bounds());
    }
    edge_finding_accel = std::make_unique<KDAccelerator>(edge_bounds);
}
}  // namespace rayli::grid
