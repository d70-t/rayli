#include "logging.hpp"
#include <spdlog/sinks/ansicolor_sink.h>
#include <spdlog/sinks/null_sink.h>

namespace rayli::logging {
/*
typedef spdlog::sink_ptr sink_ptr;
sink_ptr default_sink = nullptr;
std::mutex sink_creation_mutex;
static sink_ptr get_default_sink() {
    if (!default_sink) {
        std::lock_guard _(sink_creation_mutex);
        if (!default_sink) {
            default_sink =
              std::make_shared<spdlog::sinks::ansicolor_stdout_sink_mt>();
        }
    }
    return default_sink;
}
*/

std::mutex logger_creation_mutex;
logger_ptr get_logger(const std::string& name) {
    logger_ptr logger = spdlog::get(name);
    if (!logger) {
        std::lock_guard _(logger_creation_mutex);
        if (!logger) {
            logger =
              spdlog::create<spdlog::sinks::ansicolor_stdout_sink_mt>(name);
        }
    }
    return logger;
}
}  // namespace rayli::logging
