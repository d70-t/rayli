#include "rayli.hpp"
#include "rayli_version.hpp"

namespace rayli {
version_info version() { return {RAYLI_VERSION, RAYLI_GIT_REV}; }
}  // namespace rayli
