#include "PointSink.hpp"

PointSink::PointSink(const Eigen::Vector3d& location) : location(location) {}

Direction PointSink::attract(const Ray& ray) {
    return (location - ray.location).normalized();
}
