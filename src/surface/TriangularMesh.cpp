#include "surface/TriangularMesh.hpp"
#include "intersection/WoopTriangleIntersection.hpp"
#include <GeometryUtils.hpp>

#include <algorithm>
#include <map>
#include <numeric>

namespace rayli::surface {

bool TriangularMesh::Face::intersect(const Ray& ray, double tnear, double tfar,
                                     double& t) const {
    assert(valid());
    return rayli::intersection::woop_triangle(ray, *this, t, tnear, tfar);
}

TriangularMesh::Face TriangularMesh::Face::neighbor_face(int idx) const {
    assert(valid());
    assert(idx >= 0 && idx < 3);
    auto e = edge(idx);
    if (e.valid()) {
        auto f0 = e.face(0);
        if (f0.id() != face_id) {
            return f0;
        } else {
            return e.face(1);
        }
    } else {
        return mesh.get_face();
    }
}

Bounds3d TriangularMesh::Face::get_bounds() const {
    assert(valid());
    const std::array<size_t, 3>& vertices = mesh.faces[face_id].vertices;
    Bounds3d bounds(mesh.vertices[vertices[0]]);
    bounds.extend_inplace(mesh.vertices[vertices[1]]);
    bounds.extend_inplace(mesh.vertices[vertices[2]]);
    return bounds;
}

Bounds3d TriangularMesh::Edge::get_bounds() const {
    assert(valid());
    const std::array<size_t, 2>& vertices = mesh.edges[edge_id].vertices;
    Bounds3d bounds(mesh.vertices[vertices[0]]);
    bounds.extend_inplace(mesh.vertices[vertices[1]]);
    return bounds;
}

size_t TriangularMesh::find_intersecting_face_idx(const Ray& ray, double tnear,
                                                  double tfar,
                                                  double& t) const {

    if (face_finding_accel) {
        return face_finding_accel->find_intersecting_element(
          ray, tnear, tfar, t,
          [&](size_t element_id, const Ray& ray, double tnear, double tfar,
              double& tnext) {
              return get_face(element_id).intersect(ray, tnear, tfar, tnext);
          });
    } else {
        size_t iout = face_count();
        t = std::numeric_limits<double>::infinity();
        double tnext;
        for (size_t i = 0; i < face_count(); ++i) {
            if (get_face(i).intersect(ray, tnear, tfar, tnext)) {
                if (tnext < t) {
                    t = tnext;
                    iout = i;
                }
            }
        }
        return iout;
    }
}

void TriangularMesh::repair_normals() {
    SPDLOG_TRACE(logger, "repairing normals");
    for (auto& face : faces) {
        const auto& v0 = vertices[face.vertices[0]];
        const auto& v1 = vertices[face.vertices[1]];
        const auto& v2 = vertices[face.vertices[2]];
        face.normal = (v1 - v0).cross(v2 - v1).normalized();
    }
}

void TriangularMesh::repair_edges_with_renumbering() {
    constexpr size_t invalid_id = std::numeric_limits<size_t>::max();
    SPDLOG_TRACE(logger, "repairing edges with renumbering");
    size_t edge_id_counter = 0;
    {
        std::map<std::pair<size_t, size_t>, size_t> v2e;
        for (auto& face : faces) {
            for (int i = 0; i < 3; ++i) {
                size_t v0 = face.vertices[i];
                size_t v1 = face.vertices[(i + 1) % 3];
                if (v1 < v0) {
                    std::swap(v0, v1);
                }
                auto [it, success] = v2e.try_emplace({v0, v1}, edge_id_counter);
                if (success) {
                    ++edge_id_counter;
                }
                face.edges[i] = it->second;
                SPDLOG_TRACE(logger, "collected edge: ({}--{}) -> {}", v0, v1,
                             it->second);
            }
        }
    }

    edges.clear();
    edges.resize(edge_id_counter,
                 edge_t{{invalid_id, invalid_id}, {invalid_id, invalid_id}});
    SPDLOG_TRACE(logger, "total edge count: {}", edge_id_counter);
    for (size_t iface = 0; iface < faces.size(); ++iface) {
        const auto& face = faces[iface];
        for (int i = 0; i < 3; ++i) {
            size_t v0 = face.vertices[i];
            size_t v1 = face.vertices[(i + 1) % 3];
            if (v1 < v0) {
                std::swap(v0, v1);
            }
            auto& edge = edges[face.edges[i]];
            if (edge.faces[0] == invalid_id) {
                SPDLOG_TRACE(logger,
                             "edge {} has 1st face: {} with vertices({}--{})",
                             face.edges[i], iface, v0, v1);
                edge.faces[0] = iface;
                edge.vertices = {v0, v1};
            } else {
                SPDLOG_TRACE(logger,
                             "edge {} has 2nd face: {} with vertices({}--{})",
                             face.edges[i], iface, v0, v1);
                edge.faces[1] = iface;
            }
        }
    }
}

void TriangularMesh::repair() {
    repair_edges_with_renumbering();
    repair_normals();
}

void TriangularMesh::build_accelerators() {
    std::vector<Bounds3d> face_bounds;
    face_bounds.reserve(face_count());
    for (size_t i = 0; i < face_count(); ++i) {
        face_bounds.push_back(get_face(i).get_bounds());
    }
    face_finding_accel = std::make_unique<KDAccelerator>(face_bounds);
}

TriangularMesh TriangularMesh::from_vertices_and_triangles(
  std::vector<Eigen::Vector3d>& vertices,
  std::vector<std::array<size_t, 3>> triangles) {
    TriangularMesh mesh;
    mesh.vertices = std::move(vertices);

    mesh.faces.resize(triangles.size());
    std::transform(triangles.begin(), triangles.end(), mesh.faces.begin(),
                   [](const auto& tri) { return TriangularMesh::face_t{tri}; });
    mesh.repair();
    return mesh;
}

TriangularMesh TriangularMesh::from_vertices_and_triangles(
  const double* vertices, size_t vertex_count, const size_t* triangles,
  size_t triangle_count) {
    TriangularMesh mesh;
    mesh.vertices.resize(vertex_count);
    for (size_t i = 0; i < vertex_count; ++i) {
        mesh.vertices[i] = {vertices[3 * i + 0], vertices[3 * i + 1],
                            vertices[3 * i + 2]};
    }
    mesh.faces.resize(triangle_count);
    for (size_t i = 0; i < triangle_count; ++i) {
        mesh.faces[i] = TriangularMesh::face_t{
          {triangles[3 * i + 0], triangles[3 * i + 1], triangles[3 * i + 2]}};
    }
    mesh.repair();
    return mesh;
}

}  // namespace rayli::surface
