#include <progress.hpp>
#include <stdio.h>
#include <sys/ioctl.h>

#include <algorithm>
#include <iostream>

#include <thread_support.hpp>

ProgressBar::ProgressBar(long total)
    : done(0), start_time(std::chrono::system_clock::now()) {
    set_total(total);
    show_bar = !is_running_in_slurm();
}

void ProgressBar::set_total(long new_total) {
    total = new_total;
    granularity = std::max(1l, total / 200);
}

void ProgressBar::display() {
    if (show_bar) {
        display_bar();
    } else {
        display_text();
    }
}

void ProgressBar::display_bar() {
    using namespace std::chrono_literals;
    auto now = std::chrono::system_clock::now();
    if (old_done >= 0 && std::abs(done - old_done) < granularity &&
        now - old_time < 1s) {
        return;
    }
    old_time = now;
    constexpr size_t maxsize = 1024.;
    float fraction = float(done) / total;
    auto elapsed = now - start_time;
    auto time_to_go = (1 - fraction) * elapsed / fraction;
    auto seconds_to_go =
      std::chrono::duration_cast<std::chrono::duration<float>>(time_to_go)
        .count();

    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
    size_t cols = std::min<size_t>(std::max<size_t>(w.ws_col, 40), maxsize - 2);

    char buffer[maxsize];
    buffer[0] = '[';
    int barlen = (cols - 6 - 13);
    int ticks = fraction * barlen;
    for (int i = 0; i < ticks - 1; ++i) {
        buffer[i + 1] = '=';
    }
    if (done == total) {
        buffer[ticks + 1 - 1] = '"';
    } else {
        buffer[ticks + 1 - 1] = '>';
    }
    for (int i = ticks; i < barlen; ++i) {
        buffer[i + 1] = ' ';
    }
    buffer[barlen + 1] = ']';
    snprintf(buffer + barlen + 2, 7, "%4.0f%% ", fraction * 100);
    snprintf(buffer + barlen + 8, 15, "(%8.1fs)\r", seconds_to_go);
    std::cout << buffer;
    std::cout.flush();
    old_done = done;
}

void ProgressBar::display_text() {
    if (old_done >= 0 && std::abs(done - old_done) < granularity) {
        return;
    }
    float fraction = float(done) / total;
    auto now = std::chrono::system_clock::now();
    auto elapsed = now - start_time;
    auto time_to_go = (1 - fraction) * elapsed / fraction;
    auto seconds_to_go =
      std::chrono::duration_cast<std::chrono::duration<float>>(time_to_go)
        .count();
    std::cout << done << " / " << total << " (" << (fraction * 100) << "%"
              << ") " << seconds_to_go << " sec to go" << std::endl;
    old_done = done;
}
