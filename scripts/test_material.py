import pyrayli
import matplotlib.pylab as plt
import numpy as np
from test_atm import is_approx

def _main():
    ez = np.array([0.,0.,1.])
    m = pyrayli.LambertianMaterial(0.3)
    t = pyrayli.WorldBRDFTransform.from_n_and_dpdu(ez, [1, 0, 0])
    rng = pyrayli.make_rng()
    angles = np.deg2rad(np.linspace(0, 90, 360), dtype="float")
    outdirs = np.stack([np.sin(angles), np.zeros_like(angles), np.cos(angles)], axis=-1)
    bins = np.zeros(360,dtype="float")
    samplecount = 100000
    for i in range(samplecount):
        s = t.sample_f(m, ez, rng)
        if not is_approx(s.f, t.f(m, ez, s.d)):
            raise RuntimeError("f is inconsistent: {} {} {}".format(s.f, t.f(m, ez, s.d), s.d))
        angle = np.arccos(np.dot(s.d, ez))
        bins[int(np.rad2deg(angle)*4)] += 1

    binwidth = np.pi / len(bins)
    hemisphere_norm = np.pi
    bins /= samplecount * hemisphere_norm * binwidth

    print(t.world_to_local([0, 0, 1]))
    print(t.world_to_local([1, 0, 1]))

    plt.subplot(2, 1, 1)
    plt.plot(np.rad2deg(angles), [t.f(m, [0,0,1], o) for o in outdirs], label="fixed input")
    plt.plot(np.rad2deg(angles), [t.f(m, o, [0,0,1]) for o in outdirs], label="fixed output")
    plt.title("f")
    plt.legend()
    plt.subplot(2, 1, 2)
    plt.plot(np.rad2deg(angles), [t.pdf(m, [0,0,1], o) for o in outdirs], label="fixed input")
    plt.plot(np.rad2deg(angles), [t.pdf(m, o, [0,0,1]) for o in outdirs], label="fixed output")
    binangles = np.deg2rad(np.linspace(0,90,len(bins)) + 90./(2*len(bins)))
    plt.plot(np.rad2deg(binangles), bins/np.sin(binangles), label="sampling f directions")
    plt.title("pdf")
    plt.legend()
    plt.show()

if __name__ == "__main__":
    _main()
