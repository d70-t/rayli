import pyrayli
import numpy as np
import matplotlib.pylab as plt
import xarray as xr

def _main():
    rng = pyrayli.make_rng()
    scat = pyrayli.make_rayleigh() * 2
    #scat += pyrayli.make_hg(0.7) * 0.09

    wcds = xr.open_dataset("/home/tobi/Dokumente/Uni/MIM/libradtran/data/wc/mie/wc.sol.mie.cdf")
    wcds = wcds.isel(nlam=70, nreff=0, nphamat=0)
    # the mie tables may contain a couple of nan values.
    valid = np.isfinite(wcds.theta.data)
    wcds = wcds.isel(nthetamax=valid)
    scat += pyrayli.make_tabulated(np.cos(np.deg2rad(wcds.theta.data)), wcds.phase.data / 2) * 0.3

    bincount = 180
    samplecount = 400000
    bins = np.zeros(bincount)
    for i in range(samplecount):
        sample = scat.sample_f([1., 0., 0.], rng)
        angle = np.arccos(sample.d[0])
        b = int(bincount * angle / np.pi)
        bins[b] += sample.f / sample.pdf

    angles = np.linspace(0, np.pi, len(bins) + 1)
    angles = (angles[1:] + angles[:-1]) / 2
    plt.polar(angles, bins * bincount / (np.sin(angles) * samplecount * np.pi), label="{} samples".format(samplecount))
    plt.polar(angles, 2 * np.pi * np.array([scat.f([1., 0., 0.], [np.cos(a), np.sin(a), 0]) for a in angles]), label="analytic")
    plt.legend()
    plt.show()

if __name__ == '__main__':
    _main()
