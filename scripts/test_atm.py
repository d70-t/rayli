import numpy as np
import pyrayli
import xarray as xr

def is_approx(a, b, xtol=1e-9, rtol=1e-6):
    c = np.abs(a - b)
    if c > xtol:
        return False
    r = c / np.abs(a + b)
    if r > rtol:
        return False
    return True

def _main():
    samplecount = 40000
    wcds = xr.open_dataset("/project/meteo/work/Tobias.Koelling/libradtran_git/data/wc/mie/wc.sol.mie.cdf")
    wcds = wcds.isel(nlam=62, nreff=9, nphamat=0)
    valid = np.isfinite(wcds.theta.data)
    wcds = wcds.isel(nthetamax=valid)
    scat = pyrayli.make_tabulated(np.cos(np.deg2rad(wcds.theta.data)), wcds.phase.data / 2)
    scat2 = 2 * scat
    scat3 = scat2 + pyrayli.make_rayleigh()
    rng = pyrayli.make_rng()

    for name, s in [("mie", scat), ("2*mie", scat2), ("2*mie+rayleigh", scat3)]:
        print("testing {}".format(name))
        for i in range(samplecount):
            sample = s.sample_f([1., 0., 0.], rng)
            if not is_approx(sample.pdf, s.pdf([1., 0., 0.], sample.d)):
                print("pdf error:", sample.pdf, s.pdf([1., 0., 0.], sample.d))
                break
            if not is_approx(sample.f, s.f([1., 0., 0.], sample.d)):
                print("f error:", sample.f, s.f([1., 0., 0.], sample.d))
                break

if __name__ == "__main__":
    _main()
