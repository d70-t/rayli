import xarray as xr
import pyrayli
import numpy as np
import matplotlib.pylab as plt

def _main():
    angles = np.linspace(0, np.pi, 1801)
    outdirs = np.stack([np.cos(angles), np.sin(angles), np.zeros_like(angles)], axis=-1)
    
    rayleigh = pyrayli.make_rayleigh()
    hg0 = pyrayli.make_hg(0)
    hg085 = pyrayli.make_hg(0.85) * 0.1
    hgm05 = pyrayli.make_hg(-0.5)
    wcds = xr.open_dataset("/home/tobi/Dokumente/Uni/MIM/libradtran/data/wc/mie/wc.sol.mie.cdf")
    wcds = wcds.isel(nlam=70, nreff=0, nphamat=0)
    wc = pyrayli.make_tabulated(np.cos(np.deg2rad(wcds.theta.data)), wcds.phase.data / 2) * 0.3

    for scat, name in [(rayleigh, "rayleigh"),
                       (hg0, "HG g=0"),
                       (hg085, "HG g=0.85 / 10"),
                       (hgm05, "HG g=-0.5"),
                       (wc, "wc * 0.3")]:
        fs = np.array([scat.f([-1,0,0], o) for o in outdirs])
        plt.polar(angles, fs, label=name)
        print(np.trapz(fs*np.sin(angles), angles) * 2 * np.pi)
    plt.legend()
    plt.show()

if __name__ == "__main__":
    _main()
