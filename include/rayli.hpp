#pragma once

namespace rayli {
struct version_info {
    const char* numeric_version;
    const char* git_revision;
};

version_info version();
}  // namespace rayli
