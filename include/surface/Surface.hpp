#pragma once

#include "GeometryUtils.hpp"

template <typename G>
class Interaction;

template <typename G>
class Surface {
   public:
    virtual Bounds3d world_bounds() const = 0;
    virtual Interaction<G>* intersect(const Ray& r, double tnear, double tfar,
                                      std::pmr::memory_resource* m) const = 0;
};
