#pragma once

#include "Ray.hpp"

#include "GeometryUtils.hpp"
#include "accelerator/kdaccel.hpp"
#include "logging.hpp"

#include "Eigen.hpp"
#include <array>
#include <vector>

namespace rayli::surface {
struct TriangularMesh {
    struct face_t {
        std::array<size_t, 3> vertices;
        std::array<size_t, 3> edges;
        Eigen::Vector3d normal;
    };

    struct edge_t {
        std::array<size_t, 2> faces;
        std::array<size_t, 2> vertices;
    };

    class Edge;

    class Face {
       public:
        inline Face(const TriangularMesh& mesh, size_t face_id)
            : mesh(mesh), face_id(face_id) {}

        /** \brief id of the face inside its mesh */
        inline size_t id() const { return face_id; }

        /** \brief vertex of face */
        inline const Eigen::Vector3d& v(int idx) const {
            assert(valid());
            return mesh.vertices[mesh.faces[face_id].vertices[idx]];
        }

        /** \brief mean of face vertices */
        inline const Eigen::Vector3d c() const {
            assert(valid());
            return (v(0) + v(1) + v(2)) / 3;
        }

        /** \brief face normal vector */
        inline const Eigen::Vector3d& n() const {
            assert(valid());
            return mesh.faces[face_id].normal;
        }

        /** \brief surface area */
        inline double area() const {
            return 0.5 * (v(1) - v(0)).cross(v(2) - v(0)).norm();
        }

        /** \brief edge of face */
        Edge edge(int idx) const;

        /** \brief neighboring face */
        Face neighbor_face(int idx) const;

        /** \brief checks for ray intersection
         */
        bool intersect(const Ray& ray, double tnear, double tfar,
                       double& t) const;

        /** \brief axis aligned bounding box around face */
        Bounds3d get_bounds() const;

        /** \brief check if face is a valid face */
        inline bool valid() const { return face_id < mesh.face_count(); }

       private:
        const TriangularMesh& mesh;
        size_t face_id;
    };

    class Edge {
       public:
        inline Edge(const TriangularMesh& mesh, size_t edge_id)
            : mesh(mesh), edge_id(edge_id) {}

        /** \brief id of the face inside its mesh */
        inline size_t id() const { return edge_id; }

        /** \brief Vertex of Edge */
        inline const Eigen::Vector3d& v(int idx) {
            assert(valid());
            assert(idx >= 0 && idx < 2);
            size_t vertex_id = mesh.edges[edge_id].vertices[idx];
            assert(vertex_id < mesh.vertex_count());
            return mesh.vertices[vertex_id];
        }

        /** \brief Face of Edge */
        inline Face face(int idx) {
            assert(valid());
            assert(idx >= 0 && idx < 2);
            return {mesh, mesh.edges[edge_id].faces[idx]};
        }

        /** \brief axis aligned bounding box around edge */
        Bounds3d get_bounds() const;

        /** \brief check if edge is a valid edge */
        inline bool valid() const { return edge_id < mesh.edge_count(); }

       private:
        const TriangularMesh& mesh;
        size_t edge_id;
    };

    /** \brief number of vertices in mesh */
    inline size_t vertex_count() const { return vertices.size(); }

    /** \brief number of faces in mesh */
    inline size_t face_count() const { return faces.size(); }
    /** \brief create invalid face */
    inline Face get_face() const { return Face(*this, face_count()); }
    /** \brief get face of given id */
    inline Face get_face(size_t face_id) const { return Face(*this, face_id); }
    size_t find_intersecting_face_idx(const Ray& ray, double tnear, double tfar,
                                      double& t) const;

    /** \brief number of edges in mesh */
    inline size_t edge_count() const { return edges.size(); }
    /** \brief create invalid edge */
    inline Edge get_edge() const { return Edge(*this, edge_count()); }
    /** \brief get edge of given id */
    inline Edge get_edge(size_t edge_id) const { return Edge(*this, edge_id); }

    /** \brief repairs face normals
     *
     * Needs only face vertices.
     */
    void repair_normals();

    /** \brief repairs edge information
     *
     * Needs only face vertices.
     * Updates edges of faces and faces of edges.
     */
    void repair_edges_with_renumbering();

    /** \brief repairs mesh from vertex locations and triangles only.
     *
     * \note edges will be numbered in undefined order after this operation.
     */
    void repair();

    /** \brief builds acceleration structures for faster lookups
     *
     * \note changing the mesh invalidates acceleration structures. If mesh is
     * changed without rebuilding acceleration structures afterwards, bad things
     * can happen!
     */
    void build_accelerators();

    static TriangularMesh
    from_vertices_and_triangles(std::vector<Eigen::Vector3d>& vertices,
                                std::vector<std::array<size_t, 3>> triangles);

    static TriangularMesh from_vertices_and_triangles(const double* vertices,
                                                      size_t vertex_count,
                                                      const size_t* triangles,
                                                      size_t triangle_count);

    std::vector<Eigen::Vector3d> vertices = {};
    std::vector<face_t> faces = {};
    std::vector<edge_t> edges = {};

    std::unique_ptr<KDAccelerator> face_finding_accel = nullptr;

    rayli::logging::logger_ptr logger =
      rayli::logging::get_logger("TriangularMesh");
};

inline TriangularMesh::Edge TriangularMesh::Face::edge(int idx) const {
    assert(valid());
    return {mesh, mesh.faces[face_id].edges[idx]};
}
}  // namespace rayli::surface
