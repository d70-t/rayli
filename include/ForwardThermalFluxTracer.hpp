#pragma once
#include "Ray.hpp"
#include "overloaded.hpp"
#include "thread_support.hpp"
#include <VarianceReduction.hpp>
#include <brdf/worldbrdf.hpp>
#include <material.hpp>
#include <vgrid/GridIterator.hpp>

#include <algorithm>
#include <iostream>
#include <limits>
#include <random>
#include <thread>
#include <variant>

template <typename R, typename  EmisssionTexture, typename ExtTexture, typename ScatTexture,
          typename SurfIndirectTexture, typename Recorder,
          typename VR = rayli::vr::NoVarianceReduction>
struct ForwardThermalEventHandler {
    R& rng;
    Ray r;
    const EmisssionTexture& emission;
    const ExtTexture& extinction;
    const ScatTexture& scat;
    const SurfIndirectTexture& surf_indirect;
    Recorder& rec;
    VR vr = {};
    bool change_direction = true;
    double L = 0;
    size_t scatter_count = 0;
    double tau_remain;

    void operator()(const Teleport& t) {
      r = t.r;
    }

    void operator()(const VolumeSlice& v) {
        auto scatterer = scat(v.idx);
        double s = v.tfar - v.tnear;
        double k_scat = scatterer.sigma_scat();
        double dtau = s * k_scat;
        bool do_scatter = dtau > tau_remain;

        double k_abs = extinction(v.idx) - k_scat;

        if (do_scatter) {
            s *= tau_remain / dtau;
        }

        const double tauabs = k_abs * s;
        const double B1 = emission({v.idx, r(v.tnear)});  // planck at start of ray
        const double B2 = emission({v.idx, r(v.tfar)});  // planck at end of ray
        double L0;
        if (tauabs > 1e-10) {
            const double tm1 = expm1(-tauabs);
            L0 = L * (tm1 + 1) + (B2 - B1) - (B1 - (B2 - B1) / tauabs) * tm1;
        } else {
            L0 = L * (1 - tauabs) + (B1 + B2) * .5 * tauabs;
        }
        rec.on_volabs(v.idx, L - L0);
        L = L0;

        if (do_scatter) {
            auto scat_sample = scatterer.sample_f(-r.d, rng);
            L *= scat_sample.f / scat_sample.pdf / k_scat;
            r = {r(v.tnear + s), scat_sample.d};
            change_direction = true;
            rec.on_scattering(r);
            ++scatter_count;
        } else {
            tau_remain -= dtau;
        }
    }

    void operator()(const SurfaceSlice& s) {
        rec.on_enter_surface(r, s, L);
        Eigen::Vector3d p = r(s.t);
        auto brdf = surf_indirect({s.idx, p});

        if (bool(brdf.flags & MaterialFlags::transparent)) {
            rec.on_leave_surface(r, s, L);
        } else {
            auto geometry = s.compute_geometry(r);
            auto transform = WorldBRDFTransform::from_n_and_dpdu(
              geometry.shading.n, geometry.shading.dpdu);

            if (bool(brdf.flags & MaterialFlags::emitting)) {
                auto scat_sample = transform.sample_f(brdf, -r.d, rng);
                auto Ag = scat_sample.f / scat_sample.pdf;   // albedo
                auto emis = transform.L(brdf, scat_sample.d);  // emission
                L = L * Ag + emis;
                r = {p + scat_sample.d * 1e-6, scat_sample.d};  // TODO, maybe somehow prevent the use of
                                                                // epsilon here, but necessary not to be
                                                                // jumping up and down around surface layers
                change_direction = true;
                rec.on_scattering(r);
                rec.on_leave_surface(r, s, L);
                ++scatter_count;
            } else {
                L = 0;
            }
        }
    }
};

template <typename Grid, typename VR = rayli::vr::NoVarianceReduction>
class ForwardThermalFluxTracer {
   public:
    ForwardThermalFluxTracer(const Grid& grid, VR vr = {})
        : grid(grid), vr(vr) {}
    template <typename RaySource, typename TerminationCriteria,
              typename EmissionTexture, typename ExtTexture, typename ScatTexture,
              typename SurfIndirectTexture, typename Recorder>
    void solve(const RaySource& source, TerminationCriteria crit,
               const EmissionTexture& emission, const ExtTexture& extinction, const ScatTexture& scat,
               const SurfIndirectTexture& surf_indirect, Recorder& rec,
               size_t num_threads = 0) {
        if (!num_threads) {
            num_threads = cluster_aware_concurrency();
        }
        std::vector<Recorder> recorders;
        std::generate_n(std::back_inserter(recorders), num_threads,
                        [&]() { return rec.clone(); });
        std::vector<std::thread> pool;

        for (size_t thread_id = 0; thread_id < recorders.size(); ++thread_id) {
            pool.push_back(std::thread([&, thread_id]() {
                solve_single(
                  source, [=](size_t i) { return crit(i, thread_id); },
                  emission, extinction, scat, surf_indirect, recorders[thread_id]);
            }));
        }
        for (auto& thread : pool) {
            thread.join();
        }
        for (auto& subrec : recorders) {
            rec.join(subrec);
        }
    }

    template <typename RaySource, typename TerminationCriteria,
              typename EmissionTexture, typename ExtTexture, typename ScatTexture,
              typename SurfIndirectTexture, typename Recorder>
    void solve_single(const RaySource& source, TerminationCriteria crit,
                      const EmissionTexture& emission, const ExtTexture& extinction, const ScatTexture& scat,
                      const SurfIndirectTexture& surf_indirect, Recorder& rec) {
        std::random_device rd;
        std::mt19937_64 rng(rd());
        std::exponential_distribution<double> tau_dist(1);

        for (size_t i = 0; crit(i); ++i) {
            Ray r = source.generate_ray(rng);
            rec.on_new_ray(r);
            auto handler =
              ForwardThermalEventHandler<std::mt19937_64, EmissionTexture, ExtTexture,
                                         ScatTexture, SurfIndirectTexture,
                                         Recorder, VR>{
                rng, r, emission, extinction, scat, surf_indirect, rec, vr};
            typename Grid::RayIterator it = {};
            while (handler.change_direction) {
                handler.change_direction = false;
                handler.tau_remain = tau_dist(rng);
                auto path = grid.walk_along(
                  handler.r, 0, std::numeric_limits<double>::infinity(), it);
                for (it = path.begin(); it != path.end(); ++it) {
                    handle(handler, *it);
                    if (handler.change_direction) break;
                }
            }
            rec.on_ray_finish();
        }
    }

   private:
    const Grid& grid;
    VR vr;
};
