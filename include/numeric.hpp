#pragma once

#include <cmath>

/** \brief solves quadratic equation
 *
 * \f[
 * a x^2 + b x + c = 0
 * \f]
 *
 * \param r0 result value for \f$ x \f$ with smaller value
 * \param r1 result result value for \f$ x \f$ with larger value
 * \return ``true`` if results have been found, ``false`` otherwise
 */
template <typename Scalar>
bool solve_quadratic(Scalar a, Scalar b, Scalar c, Scalar* r0, Scalar* r1) {
    double discrim = (double)b * (double)b - 4 * (double)a * (double)c;
    if (discrim < 0) return false;
    double rootDiscrim = std::sqrt(discrim);

    double q;
    if (b < 0)
        q = -.5 * (b - rootDiscrim);
    else
        q = -.5 * (b + rootDiscrim);
    *r0 = q / a;
    *r1 = c / q;
    if (*r0 > *r1) std::swap(*r0, *r1);
    return true;
}
