#pragma once

#include <Ray.hpp>
#include <atm.hpp>

#include <random>

namespace rayli::vr {
class VRBase {
   public:
    bool allow_local_estimate() const { return true; }

    template <typename HitObject, typename SCN, typename G>
    bool modify_scatter(const HitObject& hit_object, const SCN& scn,
                        double& Lpath, double k_ext, Ray& r, G& rng) const {
        return false;
    }

    template <typename ST, typename HitObject, typename TH>
    double before_scatter(ST& st, Ray r, double& Lpath,
                          const HitObject& hit_object, double k_ext,
                          TH make_th) {
        return 0;
    }

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        return 0;
    }
};

class NoVarianceReduction : public VRBase {};

class RussianRoulette {
   public:
    RussianRoulette(double Lcrit, double pkill) : Lcrit(Lcrit), pkill(pkill) {}
    template <typename G>
    double operator()(double L, G& g) {
        std::bernoulli_distribution should_kill(pkill);
        if (L < Lcrit) {
            if (should_kill(g)) {
                return 0;
            } else {
                return L / (1 - pkill);
            }
        }
        return L;
    }

   private:
    double Lcrit, pkill;
};

class RussianRouletteVR : public VRBase {
   public:
    RussianRouletteVR(RussianRoulette roulette = {1e-3, .1})
        : roulette(roulette) {}

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        Lpath = roulette(Lpath, st.rng);
        return 0;
    }

   private:
    RussianRoulette roulette;
};

template <typename G>
class DirectionalImportanceSamplingVR : public VRBase {
   public:
    DirectionalImportanceSamplingVR(double p_dis = 0.1, double g = 0.9)
        : p_dis(p_dis), directional_scatterer(g) {}

    template <typename HitObject, typename SCN>
    bool modify_scatter(const HitObject& hit_object, const SCN& scn,
                        double& Lpath, double k_ext, Ray& r, G& rng) const {
        // TODO: maybe formulate this by anisotropic scatterer
        ;
        auto target = scn.sink.sample_from(rng, r.o);

        DirectedSample sample;

        if (std::bernoulli_distribution(p_dis)(rng)) {
            // TODO: check which distribution is good
            sample = directional_scatterer.sample_f(-target.d, rng);
            // sample = hit_object.sample_f(-target.d, rng);
            // if(!(abs(sample.pdf - directional_scatterer.pdf(-target.d,
            // sample.d)) < 1e-5)) {
            //    std::cout << "ddis / sample pdf: " << std::setprecision(15) <<
            //    sample.pdf << " recalc: " <<
            //    directional_scatterer.pdf(-target.d, sample.d) << "\n"; throw
            //    std::runtime_error("missmatching pdf");
            //}
            auto p1 = sample.pdf;
            auto p2 = hit_object.pdf(-r.d, sample.d);
            sample.pdf = p_dis * p1 + (1 - p_dis) * p2;
            sample.f = hit_object.f(-r.d, sample.d);
        } else {
            sample = hit_object.sample_f(-r.d, rng);
            // if(!(abs(sample.pdf - hit_object.pdf(-r.d, sample.d)) < 1e-5)) {
            //    std::cout << "normal / sample pdf: " << sample.pdf << "
            //    recalc: " << hit_object.pdf(-r.d, sample.d) << "\n"; std::cout
            //    << "ray direction " << (-r.d).transpose() << "\n"; std::cout
            //    << "sample direction " << (-sample.d).transpose() << "\n";
            //    std::cout << "sigma_scat " << hit_object.sigma_scat() << "\n";
            //    throw std::runtime_error("missmatching pdf");
            //}
            auto p1 = directional_scatterer.pdf(-target.d, sample.d);
            // auto p1 = hit_object.pdf(-target.d, sample.d);
            auto p2 = sample.pdf;
            sample.pdf = p_dis * p1 + (1 - p_dis) * p2;
            // sample.f = sample.f;
        }

        Lpath *= sample.f / (sample.pdf * k_ext);
        r.d = sample.d;
        return true;
    }

   private:
    double p_dis;
    HGScat<G> directional_scatterer;
};

class MaxScatterVr : public VRBase {
   public:
    MaxScatterVr(size_t max_scatter, size_t le_start = 0)
        : max_scatter(max_scatter), le_start(le_start) {}

    bool allow_local_estimate() const { return scatter_count >= le_start; }

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        ++scatter_count;
        if (scatter_count >= max_scatter) {
            Lpath = 0;
        }
        return 0;
    }

   private:
    size_t max_scatter, le_start;
    size_t scatter_count = 0;
};

template <typename DIS, typename OtherVR = NoVarianceReduction>
class NLRVR : public VRBase {
   public:
    NLRVR(size_t n, size_t first_cp, size_t le_per_cp, DIS dis,
          OtherVR other_vr = {})
        : n(n), first_cp(first_cp), le_per_cp(le_per_cp), dis(dis),
          other_vr(other_vr) {
        assert(le_per_cp > 0);
        assert(le_per_cp <= n);
    }

    bool allow_local_estimate() const { return scatter_count < first_cp; }

    template <typename ST, typename HitObject, typename TH>
    double before_scatter(ST& st, Ray r, double Lpath,
                          const HitObject& hit_object, double k_ext,
                          TH make_th) {
        // TODO: Directional Sampling?
        if ((scatter_count >= first_cp - 1) &&
            ((scatter_count - (first_cp - 1)) % le_per_cp == 0)) {
            // std::cerr << "splitting path\n";
            size_t first_le = 0;
            if (scatter_count > first_cp - 1) {
                first_le = n - le_per_cp;
            }
            auto scat_sample = hit_object.sample_f(-r.d, st.rng);
            Lpath *= scat_sample.f / (scat_sample.pdf * k_ext);
            r.d = scat_sample.d;
            auto handler =
              make_th(Lpath, r, dis + other_vr + MaxScatterVr{n, first_le});
            st.run_handler(handler);
            // std::cerr << "split done\n";
            return handler.L();
        } else {
            return 0;
        }
    }

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        ++scatter_count;
        return 0;
    }

   private:
    size_t n, first_cp, le_per_cp;
    size_t scatter_count = 0;
    DIS dis;
    OtherVR other_vr;
};

template <typename SCAT>
class PBSVR : public VRBase {
   public:
    PBSVR(size_t nsplit, double max_weight, SCAT directional_scatterer)
        : nsplit(nsplit), max_weight(max_weight),
          directional_scatterer(directional_scatterer) {}

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        auto target = st.scn.sink.sample_from(st.rng, r.o);
        double L = 0;
        if (Lpath * directional_scatterer.f(-r.d, target.d) > max_weight) {
            Lpath /= nsplit;
            // std::cout << "split! new_weight: " << Lpath << " f: " <<
            // directional_scatterer.f(-r.d, target.d) << "\n";

            for (size_t i = 0; i < nsplit - 1; ++i) {
                auto handler = make_th(Lpath, r, vr);
                st.run_handler(handler);
                L += handler.L();
            }
        }
        return L;
    }

   private:
    size_t nsplit;
    double max_weight;
    SCAT directional_scatterer;
};

template <typename A, typename B>
class AddVR : public VRBase {
   public:
    AddVR(A a, B b) : a(a), b(b) {}

    bool allow_local_estimate() const {
        return a.allow_local_estimate() && b.allow_local_estimate();
    }

    template <typename ST, typename VR, typename TH>
    double after_scatter(ST& st, Ray r, double& Lpath, const VR& vr,
                         TH make_th) {
        double L = a.after_scatter(st, r, Lpath, vr, make_th);
        L += b.after_scatter(st, r, Lpath, vr, make_th);
        return L;
    }

    template <typename HitObject, typename SCN, typename G>
    bool modify_scatter(const HitObject& hit_object, const SCN& scn,
                        double& Lpath, double k_ext, Ray& r, G& rng) {
        if (a.modify_scatter(hit_object, scn, Lpath, k_ext, r, rng)) {
            return true;
        } else {
            return b.modify_scatter(hit_object, scn, Lpath, k_ext, r, rng);
        }
    }

   private:
    A a;
    B b;
};

template <typename A, typename B,
          typename = std::enable_if<std::is_base_of<VRBase, A>::value>,
          typename = std::enable_if<std::is_base_of<VRBase, B>::value>>
AddVR<A, B> operator+(A a, B b) {
    return {a, b};
}
}  // namespace rayli::vr
