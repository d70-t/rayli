#pragma once

#include <cmath>

constexpr double INV_PI = 1. / M_PI;
constexpr double INV_2PI = 1. / (2. * M_PI);
