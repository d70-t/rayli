#pragma once

#include <vgrid/GridIterator.hpp>

#include <iostream>

std::ostream& operator<<(std::ostream& stream, const VolumeSlice& slice);
std::ostream& operator<<(std::ostream& stream, const Teleport& slice);
std::ostream& operator<<(std::ostream& stream, const SurfaceSlice& slice);
std::ostream& operator<<(std::ostream& stream, const LinearSliceTeleport& slice);
std::ostream& operator<<(std::ostream& stream, const LinearSlice& slice);
