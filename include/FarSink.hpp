#pragma once

#include "RaySink.hpp"

template <typename G>
class FarSink : public RaySink<G> {
   public:
    FarSink(const Eigen::Vector3d& direction)
        : direction(direction.normalized()) {}
    SinkSample sample_from(G& rng, const Eigen::Vector3d from) const {
        return {direction, std::numeric_limits<double>::infinity(), 1.};
    }

   private:
    Eigen::Vector3d direction;
};
