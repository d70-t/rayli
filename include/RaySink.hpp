#pragma once

#include "Ray.hpp"

#include "Eigen.hpp"

struct SinkSample {
    Eigen::Vector3d d;
    double distance;
    double intensity;
};

template <typename G>
class RaySink {
   public:
    typedef G rng_t;

    virtual ~RaySink() {}

    virtual SinkSample sample_from(G& rng,
                                   const Eigen::Vector3d from) const = 0;
};
