#pragma once

#include "utils.hpp"
#include <cmath>

namespace rayli::microphysics {

struct ext_reff {
    double ext = 0;
    double reff = 0;  ///< \brief droplet effective radius [µm]

    bool operator==(const ext_reff& other) const {
        return ext == other.ext && reff == other.reff;
    }
    bool is_empty() const { return ext == 0 || reff == 0; }
};

struct LWC_reff {
    double LWC = 0;   ///< \brief liquid water content [g/m³]
    double reff = 0;  ///< \brief droplet effective radius [µm]

    bool operator==(const LWC_reff& other) const {
        return LWC == other.LWC && reff == other.reff;
    }
    bool is_empty() const { return LWC == 0 || reff == 0; }
};

template <typename IStream>
IStream& operator>>(IStream& istream, ext_reff& m) {
    istream >> m.ext >> m.reff;
    return istream;
}

template <typename IStream>
IStream& operator>>(IStream& istream, LWC_reff& m) {
    istream >> m.LWC >> m.reff;
    return istream;
}
}  // namespace rayli::microphysics

namespace std {
template <>
struct hash<rayli::microphysics::ext_reff> {
    size_t operator()(const rayli::microphysics::ext_reff& v) const {
        size_t h = std::hash<double>()(v.ext);
        hash_combine(h, v.reff);
        return h;
    }
};
template <>
struct hash<rayli::microphysics::LWC_reff> {
    size_t operator()(const rayli::microphysics::LWC_reff& v) const {
        size_t h = std::hash<double>()(v.LWC);
        hash_combine(h, v.reff);
        return h;
    }
};
}  // namespace std

namespace rayli::microphysics {
constexpr double MOLAR_MASS_DRY_AIR =
  0.028964;  ///< \brief Molar mass of dry air [kg/mol]
constexpr double MOLAR_MASS_WATER_VAPOR =
  0.018016;  ///< \brief Molar mass of water vapor [kg/mol]
constexpr double GAS_CONSTANT =
  8.314;  ///< \brief Universal gas constant [J/(kg mol)]
constexpr double DENSITY_WATER = 1e3;  ///< \brief density of water [kg/m³]

/** \brief saturation pressure of water
 *
 * \param temperature temperature [ĸ]
 * \return saturation pressure [Pa]
 */
inline double saturation_vapor_pressure_water(double temperature) {
    return 6.1078 * std::pow(10., 7.5 * temperature / (temperature + 237.3));
}

/** \brief density of air
 *
 * \param air_pressure absolute air pressure [Pa]
 * \param humidity relative humidity [0...1]
 * \param temperature temperature [ĸ]
 * \return air density [kg / m³]
 */
inline double density_air(double air_pressure, double humidity,
                   double temperature) {
    double p_vapor = humidity * saturation_vapor_pressure_water(temperature);
    double p_dry = air_pressure - p_vapor;
    return (p_dry * MOLAR_MASS_DRY_AIR + p_vapor * MOLAR_MASS_WATER_VAPOR) /
           (GAS_CONSTANT * temperature);
}

/** \brief density of air
 *
 * \param air_pressure absolute air pressure [Pa]
 * \param specific_humidity specific humidity [kg/kg]
 * \param temperature temperature [ĸ]
 * \return air density [kg/m³]
 *
 * \f[
 * \rho_{\rm air} = \frac{M_{\rm w} M_{\rm d}}{(1-s) M_{\rm w} + s M_{\rm d}}
 * \frac{p_{\rm air}}{R T}
 * \f]
 */
inline double density_air_specific(double air_pressure,
                            double specific_humidity,
                            double temperature) {
    return (MOLAR_MASS_WATER_VAPOR * MOLAR_MASS_DRY_AIR * air_pressure) /
           (((1 - specific_humidity) * MOLAR_MASS_WATER_VAPOR +
             specific_humidity * MOLAR_MASS_DRY_AIR) *
            GAS_CONSTANT * temperature);
}

/** \brief reff from LWC and droplet density
 *
 * This method follows Bugliaro et al. (2011)
 *
 * \sa https://www.atmos-chem-phys.net/11/5603/2011/acp-11-5603-2011.html
 *
 * \param lwc liquid qater content [kg/m³]
 * \param n water droplet density [1/m³]
 * \param k ratio between volumetric radius and effective radius
 * \return effective radius [µm]
 */
inline double reff_from_lwc_n(double lwc, double n, double k = 0.75) {
    if (lwc <= 1e-8 || n <= 1e2) return 0;
    double reff =
      std::pow(0.75 * lwc / (M_PI * k * n * DENSITY_WATER), 1. / 3.) * 1e6;
    if (reff < 2.5) return 2.5;
    if (reff > 60) return 60;
    return reff;
}
}  // namespace rayli::microphysics
