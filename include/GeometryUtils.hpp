#pragma once

#include <limits>
#include <optional>

#include "Eigen.hpp"
#include "Ray.hpp"
#include "numeric.hpp"

template <typename N>
struct Bounds3 {
    typedef Eigen::Matrix<N, 3, 1> Vector;

    Bounds3() = default;
    Bounds3(const Vector& lower, const Vector& upper)
        : lower(lower), upper(upper) {}
    Bounds3(const Vector& point) : lower(point), upper(point) {}

    Vector lower;
    Vector upper;
    bool intersect(const Ray& ray, N* tnear = nullptr,
                   N* tfar = nullptr) const {
        static_assert(std::numeric_limits<N>::has_infinity,
                      "intersection code depends on infinity");
        static_assert(std::numeric_limits<N>::has_quiet_NaN,
                      "intersection code depends on NaN");
        N t0 = tnear ? *tnear : -std::numeric_limits<N>::infinity();
        N t1 = tfar ? *tfar : std::numeric_limits<N>::infinity();
        for (int i = 0; i < 3; ++i) {
            N invDir = 1 / ray.d(i);
            N tt0 = (lower(i) - ray.o(i)) * invDir;
            N tt1 = (upper(i) - ray.o(i)) * invDir;
            if (tt0 > tt1) std::swap(tt0, tt1);
            if (tt0 > t0) t0 = tt0;
            if (tt1 < t1) t1 = tt1;
            if (t0 > t1) return false;
        }
        if (tnear) *tnear = t0;
        if (tfar) *tfar = t1;
        return true;
    }

    N surface_area() const {
        Vector d = upper - lower;
        return 2 * (d(0) * d(1) + d(1) * d(2) + d(2) * d(0));
    }

    int maximum_extent() const {
        Vector d = upper - lower;
        N max = d(0);
        int max_idx = 0;
        for (int i = 1; i < 3; ++i) {
            if (max < d(i)) {
                max = d(i);
                max_idx = i;
            }
        }
        return max_idx;
    }

    void extend_inplace(const Vector& other) {
        for (int i = 0; i < 3; ++i) {
            if (other(i) < lower(i)) lower(i) = other(i);
            if (other(i) > upper(i)) upper(i) = other(i);
        }
    }

    void extend_inplace(const Bounds3<N>& other) {
        for (int i = 0; i < 3; ++i) {
            if (other.lower(i) < lower(i)) lower(i) = other.lower(i);
            if (other.upper(i) > upper(i)) upper(i) = other.upper(i);
        }
    }

    bool contains(const Vector& point) const {
        for (int i = 0; i < 3; ++i) {
            if (point(i) < lower(i) || point(i) > upper(i)) {
                return false;
            }
        }
        return true;
    }
};

template <typename N>
inline std::ostream& operator<<(std::ostream& s, const Bounds3<N>& bounds) {
    Eigen::IOFormat flat_fmt(Eigen::FullPrecision, Eigen::DontAlignCols, ", ",
                             ",", "", "", "[", "]");
    s << "<Bounds3 " << bounds.lower.format(flat_fmt) << " "
      << bounds.upper.format(flat_fmt) << ">";
    return s;
}

enum class EdgeType { start, end };

template <typename N>
struct BoundEdge {
    BoundEdge(N t, size_t id, bool starting)
        : BoundEdge(t, id, starting ? EdgeType::start : EdgeType::end) {}
    BoundEdge(N t, size_t id, EdgeType type) : t(t), id(id), type(type) {}

    N t;
    size_t id;
    EdgeType type;
};

/** \brief finds both intersections of a ray and a ball
 *
 * \param r ball radius
 * \param ray ray to test intersection for
 * \param t1 resulting ray distance (smaller value)
 * \param t2 resulting ray distance (larger value)
 * \return ``true`` if results have been found, ``false`` otherwise
 */
template <typename Scalar>
bool ray_ball_intersection(Scalar r, const Ray& ray, Scalar* t1, Scalar* t2) {
    Scalar a = ray.d.squaredNorm();
    Scalar b = 2 * ray.d.dot(ray.o);
    Scalar c = ray.o.squaredNorm() - r * r;
    return solve_quadratic(a, b, c, t1, t2);
}

/** \brief finds first intersection of ray and ball (if any)
 *
 * \param r ball radius
 * \param ray ray to test intersection for
 * \param tnear lowest allowable ray distance
 * \param tfar biggest allowable ray distance
 * \return intersection distance (``t``) if it exists, ``std::nullopt``
 * otherwise
 */
template <typename Scalar>
std::optional<double> first_ray_ball_intersection(
  Scalar r, const Ray& ray, Scalar tnear = 0,
  Scalar tfar = std::numeric_limits<Scalar>::infinity()) {
    Scalar t1, t2;
    if (!ray_ball_intersection(r, ray, &t1, &t2)) {
        return std::nullopt;
    }
    if (t1 > tfar || t1 < tnear) {
        return std::nullopt;
    }
    return t1;
}

/** \brief finds second intersection of ray and ball (if any)
 *
 * \param r ball radius
 * \param ray ray to test intersection for
 * \param tnear lowest allowable ray distance
 * \param tfar biggest allowable ray distance
 * \return intersection distance (``t``) if it exists, ``std::nullopt``
 * otherwise
 */
template <typename Scalar>
std::optional<double> second_ray_ball_intersection(
  Scalar r, const Ray& ray, Scalar tnear = 0,
  Scalar tfar = std::numeric_limits<Scalar>::infinity()) {
    Scalar t1, t2;
    if (!ray_ball_intersection(r, ray, &t1, &t2)) {
        return std::nullopt;
    }
    if (t2 > tfar || t2 < tnear) {
        return std::nullopt;
    }
    return t2;
}

template <typename Vector>
Vector any_perpendicular(Vector v) {
    if (v(2) == 0 && v(1) == 0) {
        return {0, 1, 0};
    } else {
        return Vector{0, v(2), -v(1)}.normalized();
    }
}

template <typename Vector>
int max_vector_component(const Vector& vector) {
    double a = std::abs(vector(0));
    double b = std::abs(vector(1));
    double c = std::abs(vector(2));
    if (a > b) {
        if (a > c) {
            return 0;
        } else {
            return 2;
        }
    } else {
        if (b > c) {
            return 1;
        } else {
            return 2;
        }
    }
}

template <typename Vector>
Vector permute(const Vector& v, int kx, int ky, int kz) {
    return Vector{v(kx), v(ky), v(kz)};
}

template <typename Scalar>
Scalar deg2rad(Scalar d) {
    return d * M_PI / 180.;
}

template <typename Scalar>
Scalar rad2deg(Scalar d) {
    return d * 180. / M_PI;
}

template <typename Scalar>
Eigen::Matrix<Scalar, 3, 3>
rotation_from_cos_sin_axis(Scalar c, Scalar s,
                           const Eigen::Matrix<Scalar, 3, 1> axis) {
    Eigen::Matrix<Scalar, 3, 3> out;
    out(0, 0) = c + axis(0) * axis(0) * (1 - c);
    out(1, 0) = axis(1) * axis(0) * (1 - c) + axis(2) * s;
    out(2, 0) = axis(2) * axis(0) * (1 - c) - axis(1) * s;

    out(0, 1) = axis(0) * axis(1) * (1 - c) - axis(2) * s;
    out(1, 1) = c + axis(1) * axis(1) * (1 - c);
    out(2, 1) = axis(2) * axis(1) * (1 - c) + axis(0) * s;

    out(0, 2) = axis(0) * axis(2) * (1 - c) + axis(1) * s;
    out(1, 2) = axis(1) * axis(2) * (1 - c) - axis(0) * s;
    out(2, 2) = c + axis(2) * axis(2) * (1 - c);
    return out;
}

template <typename Scalar>
Eigen::Matrix<Scalar, 3, 3>
rotation_from_cos_axis(Scalar c, const Eigen::Matrix<Scalar, 3, 1> axis) {
    return rotation_from_cos_sin_axis(c, sqrt(1 - c * c), axis);
}

template <typename T, typename OP>
int argbinop3(const T& t0, const T& t1, const T& t2, OP op) {
    if (op(t0, t1)) {
        if (op(t0, t2)) {
            return 0;
        } else {
            return 2;
        }
    } else {
        if (op(t1, t2)) {
            return 1;
        } else {
            return 2;
        }
    }
}

template <typename T>
int argmin3(const T& t0, const T& t1, const T& t2) {
    return argbinop3(t0, t1, t2, [](const T& a, const T& b) { return a < b; });
}

template <typename T>
int argmax3(const T& t0, const T& t1, const T& t2) {
    return argbinop3(t0, t1, t2, [](const T& a, const T& b) { return a > b; });
}

using Bounds3d = Bounds3<double>;
using BoundEdged = BoundEdge<double>;
