#pragma once

#include "BRDF.hpp"
#include "constants.hpp"
#include "distributions.hpp"

template <typename G>
class LambertianBRDF : public BRDF<G> {
   public:
    LambertianBRDF(double r) : r(r) {}
    template <typename MEMR>
    static LambertianBRDF<G>* create(double r, MEMR* m) {
        std::pmr::polymorphic_allocator<LambertianBRDF<G>> brdf_allocator(m);
        auto brdf = brdf_allocator.allocate(1);
        brdf_allocator.construct(brdf, r);
        return brdf;
    }

    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const override {
        if (same_hemisphere(in_direction, out_direction)) {
            return r * INV_PI;
        } else {
            return 0;
        }
    }

   private:
    double r;
};
