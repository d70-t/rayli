#pragma once

#include "constants.hpp"
#include "distributions.hpp"
#include "helper_types.hpp"

inline double abs_cos_theta(const Eigen::Vector3d& v) { return std::abs(v(2)); }
inline bool same_hemisphere(const Eigen::Vector3d& v,
                            const Eigen::Vector3d& w) {
    return v(2) * w(2) > 0;
}

template <typename G>
class BRDF {
   public:
    /** \brief evaluates the BRDF for a given pair of directions
     *
     * \f[
     *      f_r(\omega_i, \omega_r) = \frac{d L_r(\omega_r)}{d E_i(\omega_i)} =
     * \frac{d L_r(\omega_r)}{L_i(\omega_i) \cos \Theta_i d \omega_i}
     * \f]
     *
     * \note directions must be given in scattering coordinates.
     * \node there is an assymetry between in- and out direction.
     *
     * \return fraction of the light scattered.
     */
    virtual double f(const Eigen::Vector3d& in_direction,
                     const Eigen::Vector3d& out_direction) const = 0;

    /** \brief evaluates the probability of scattering
     *
     * The default implementation is for uniform sampling of the scattering
     * hemisphere.
     *
     * \note directions must be given in scattering coordinates.
     *
     * \note if one of pdf and sample_f is overridden, the other should be
     * overridden too!
     */
    virtual double pdf(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const {
        if (same_hemisphere(in_direction, out_direction)) {
            return abs_cos_theta(in_direction) * INV_PI;
        } else {
            return 0;
        }
    }

    /** \brief samples the BRDF for a given incident direction
     *
     * This method can be used by sampling integrators to optimally find new ray
     * directions, in order not to waste cycles on unlikely directions. Also,
     * for specular reflection, it is infinitely unlikely to radomly sample the
     * correct reflection direction.
     *
     * \note directions must be given in scattering coordinates.
     *
     * \note if one of pdf and sample_f is overridden, the other should be
     * overridden too!
     */
    virtual DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                                    G& rng) const {
        auto u = uniform_sample_nd<2, double>(rng);
        DirectedSample out;
        out.d = cosine_sample_hemisphere(u);
        if (in_direction(2) < 0) {
            out.d(2) *= -1;
        }
        out.pdf = pdf(in_direction, out.d);
        out.f = f(in_direction, out.d);
        return out;
    }
};
