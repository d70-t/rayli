#pragma once

#include "Eigen.hpp"
#include "helper_types.hpp"

/** \brief
 *
 * BRDF transformation in world coordinates
 */
class WorldBRDFTransform {
   public:
    inline WorldBRDFTransform(const Eigen::Vector3d& s,
                              const Eigen::Vector3d& t,
                              const Eigen::Vector3d& n) {
        m.row(0) = s;
        m.row(1) = t;
        m.row(2) = n;
    }

    static inline WorldBRDFTransform
    from_n_and_dpdu(const Eigen::Vector3d& n, const Eigen::Vector3d& dpdu) {
        return {dpdu.normalized(), n.cross(dpdu).normalized(), n.normalized()};
    }

    inline Eigen::Vector3d world_to_local(const Eigen::Vector3d& v) const {
        return m * v;
    }

    inline Eigen::Vector3d local_to_world(const Eigen::Vector3d& v) const {
        return m.transpose() * v;
    }

    template <typename B>
    double f(const B& brdf, const Eigen::Vector3d& in_direction_world,
             const Eigen::Vector3d& out_direction_world) const {
        // note that in shading coordinates, both directions are facing outward,
        Eigen::Vector3d in_direction = world_to_local(in_direction_world);
        Eigen::Vector3d out_direction = world_to_local(out_direction_world);

        /*
         * this section might become relevant if transmission and shading
         * coordinates are fully supported.
         *
        // this extra check is necessary, if shading coordinates differ from
        // geometric corrdinates, in which case a computation in local (shading)
        // coordinates would yield an errorneous result.
        bool reflect =
          in_direction_world.dot(ng) * out_direction_world.dot(ng) > 0;
        */
        return brdf.f(in_direction, out_direction);
    }

    template <typename B>
    double L(const B& brdf, const Eigen::Vector3d& out_direction_world) const {
        // note that in shading coordinates, both directions are facing outward,
        Eigen::Vector3d out_direction = world_to_local(out_direction_world);
        return brdf.L(out_direction);
    }

    template <typename B>
    double pdf(const B& brdf, const Eigen::Vector3d& in_direction_world,
               const Eigen::Vector3d& out_direction_world) const {
        // note that in shading coordinates, both directions are facing outward,
        // so the in direction has to be flipped
        Eigen::Vector3d in_direction = world_to_local(in_direction_world);
        Eigen::Vector3d out_direction = world_to_local(out_direction_world);
        return brdf.pdf(in_direction, out_direction);
    }

    template <typename B, typename G>
    DirectedSample sample_f(const B& brdf,
                            const Eigen::Vector3d& in_direction_world,
                            G& rng) const {
        DirectedSample out;
        Eigen::Vector3d in_direction = world_to_local(in_direction_world);
        out = brdf.sample_f(in_direction, rng);
        out.d = local_to_world(out.d);
        return out;
    }

   private:
    Eigen::Matrix3d m;  ///< \brief shading coordinate system
};
