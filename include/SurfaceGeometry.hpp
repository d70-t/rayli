#pragma once

#include "Eigen.hpp"

namespace rayli {
struct SurfaceGeometry {
    inline SurfaceGeometry(const Eigen::Vector2d& uv,
                           const Eigen::Vector3d& dpdu,
                           const Eigen::Vector3d& dpdv,
                           const Eigen::Vector3d& dndu,
                           const Eigen::Vector3d& dndv)
        : n(dpdu.cross(dpdv).normalized()), uv(uv), dpdu(dpdu), dpdv(dpdv),
          dndu(dndu), dndv(dndv) {
        reset_shading_geometry();
    }

    inline void reset_shading_geometry() {
        shading.n = n;
        shading.dpdu = dpdu;
        shading.dpdv = dpdv;
        shading.dndu = dndu;
        shading.dndv = dndv;
    }

    Eigen::Vector3d n;   ///< \brief surface normal at interaction
    Eigen::Vector2d uv;  ///< \brief UV coordinates of the interaction
    Eigen::Vector3d
      dpdu;  ///< \brief partial derivative of the 3d interaction point wrt. u
    Eigen::Vector3d
      dpdv;  ///< \brief partial derivative of the 3d interaction point wrt. u
    Eigen::Vector3d dndu;
    Eigen::Vector3d dndv;

    /** \brief alternative facts for shading geometry
     *
     * To support local modifications of the surface (e.g. bump mapping), a
     * separate set of coodinates (the shading coordinates) can be used. They
     * default to the geometric counterparts as defined above.
     */
    struct {
        Eigen::Vector3d n;
        Eigen::Vector3d dpdu;
        Eigen::Vector3d dpdv;
        Eigen::Vector3d dndu;
        Eigen::Vector3d dndv;
    } shading;
};
}  // namespace rayli
