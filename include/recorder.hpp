#pragma once
#include <Ray.hpp>
#include <vgrid/GridIterator.hpp>

/** \brief estimate variance of sample mean
 *  \param data sum of all data values
 *  \param data2 sum of all squared data values
 *  \param count number of summed samples
 */
double sample_mean_var_estimate(double data, double data2, double count) {
    return (count * data2 - data * data) / (count * count * (count - 1));
}

template <typename SourceID>
class NullRecorder {
   public:
    inline void on_new_ray(const Ray& r, SourceID source_id) {}
    inline void on_volabs(size_t volume_id, double absorption) {}
    inline void on_surface(const Ray& r, const SurfaceSlice& s, double L) {}
    inline void on_surface_le(const Ray& r, const SurfaceSlice& s, double L) {}
    inline void on_scattering(const Ray& new_ray) {}
    inline void on_ray_finish(double intensity) {}
};

class ImageRecorder : public NullRecorder<Eigen::Vector2d> {
   public:
    inline ImageRecorder(size_t sx, size_t sy) : sx(sx), sy(sy), idx(0) {
        data.resize(sx * sy);
        data2.resize(sx * sy);
        count.resize(sx * sy);
    }
    inline void on_new_ray(const Ray& r, Eigen::Vector2d source_id) {
        idx = pixel2idx(source_id);
    }
    inline void on_ray_finish(double intensity) {
        count[idx] += 1;
        data[idx] += intensity;
        data2[idx] += intensity * intensity;
    }

    ImageRecorder fork() const { return ImageRecorder{sx, sy}; }

    std::vector<ImageRecorder> split(size_t count) const {
        return std::vector<ImageRecorder>(count, ImageRecorder{sx, sy});
    }

    void join(ImageRecorder&& child) {
        assert(sx == child.sx && sy == child.sy);
        for (size_t i = 0; i < data.size(); ++i) {
            data[i] += child.data[i];
            data2[i] += child.data2[i];
            count[i] += child.count[i];
        }
    }

    inline size_t pixel2idx(const Eigen::Vector2d& pixel) const {
        size_t pix_idx = size_t(pixel(0) * sx) * sy + size_t(pixel(1) * sy);
        assert(pix_idx < data.size());
        return pix_idx;
    }

    inline Eigen::Vector2d idx2pixel(size_t pix_idx) const {
        size_t x = pix_idx / sy;
        size_t y = pix_idx % sy;
        return {(0.5 + x) / sx, (0.5 + y) / sy};
    }

    std::vector<double> radiance() const {
        std::vector<double> out(sx * sy);
        std::transform(begin(data), end(data), begin(count), begin(out),
                       [](auto d, auto c) { return d / c; });
        return out;
    }

    std::vector<double> radiance_std() const {
        std::vector<double> out(sx * sy);
        for (size_t i = 0; i < sx * sy; ++i) {
            out[i] =
              sqrt(sample_mean_var_estimate(data[i], data2[i], count[i]));
        }
        return out;
    }

    size_t sx, sy, idx;
    std::vector<double> data, data2;
    std::vector<size_t> count;
};

class PointRecorder : public NullRecorder<size_t> {
   public:
    inline void on_ray_finish(double intensity) {
        count += 1;
        data += intensity;
        data2 += intensity * intensity;
    }

    PointRecorder fork() const { return {}; }

    std::vector<PointRecorder> split(size_t count) const {
        return std::vector<PointRecorder>(count, PointRecorder{});
    }

    void join(PointRecorder&& child) {
        count += child.count;
        data += child.data;
        data2 += child.data2;
    }

    inline double e() const { return data / count; }
    inline double sigma() const {
        return sqrt(sample_mean_var_estimate(data, data2, count));
    }

    double data = 0;
    double data2 = 0;
    size_t count = 0;
};

template <typename SourceID>
class FirstSurfacePropertyRecorder : public NullRecorder<SourceID> {
   public:
    inline void on_new_ray(const Ray& r, SourceID source_id) {
        first = true;
        current_surface_properties = {source_id};
    }

    inline void on_surface(const Ray& r, const SurfaceSlice& s, double L) {
        if (!first) return;
        first = false;
        auto geometry = s.compute_geometry(r);
        current_surface_properties.surface_id = s.idx;
        if (r.d.dot(geometry.n) < 0) {
            current_surface_properties.normal = geometry.n;
        } else {
            current_surface_properties.normal = -geometry.n;
        }
        current_surface_properties.hitpoint = r(s.t);
    }

    inline void on_ray_finish(double intensity) {
        surface_properties.push_back(current_surface_properties);
    }

    std::vector<FirstSurfacePropertyRecorder<SourceID>>
    split(size_t count) const {
        return std::vector<FirstSurfacePropertyRecorder<SourceID>>(count);
    }

    void join(FirstSurfacePropertyRecorder<SourceID>&& child) {
        std::copy(child.surface_properties.begin(),
                  child.surface_properties.end(),
                  std::back_inserter(surface_properties));
    }

    struct SurfaceProperties {
        SourceID source_id;
        size_t surface_id = std::numeric_limits<size_t>::max();
        Eigen::Vector3d normal = {0., 0., 0.};
        Eigen::Vector3d hitpoint = {0., 0., 0.};
    };

    SurfaceProperties current_surface_properties;
    std::vector<SurfaceProperties> surface_properties;
    bool first = false;
};

template <typename SourceID>
class LESurfRecorder : public NullRecorder<SourceID> {
   public:
    LESurfRecorder(const Eigen::Vector2d& center, const Eigen::Vector2d& size,
                   size_t nx, size_t ny)
        : center(center), size(size), nx(nx), ny(ny), data(nx * ny) {}

    void on_surface_le(const Ray& r, const SurfaceSlice& s, double L) {
        auto geometry = s.compute_geometry(r);
        Eigen::Vector2d p = geometry.uv - center;
        int ix = nx * (0.5 + p(0) / (2 * size(0)));
        int iy = ny * (0.5 + p(1) / (2 * size(1)));
        if (ix >= 0 && size_t(ix) < nx && iy >= 0 && size_t(iy) < ny) {
            data[ix * ny + iy] += L;
        }
    }

    std::vector<LESurfRecorder<SourceID>> split(size_t count) const {
        return std::vector<LESurfRecorder<SourceID>>(
          count, LESurfRecorder<SourceID>{center, size, nx, ny});
    }

    void join(LESurfRecorder<SourceID>&& child) {
        assert(nx == child.nx && ny == child.ny);
        for (size_t i = 0; i < data.size(); ++i) {
            data[i] += child.data[i];
        }
    }

    const std::vector<double>& get_data() const { return data; }

   private:
    Eigen::Vector2d center, size;
    size_t nx, ny;
    std::vector<double> data;
};
