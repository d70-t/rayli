#pragma once

#include "vgrid/KDTree.hpp"

#include <memory>

template <typename It>
struct iterator_pair {
    It begin() const { return a; };
    It end() const { return b; };
    It a, b;
};

template <typename Container>
auto container2iterpair(Container& c) {
    return iterator_pair<decltype(c.begin())>(c.begin(), c.end());
}

class KDAccelerator {
   public:
    typedef rayli::vgrid::KdTree<size_t> Tree;

    KDAccelerator(const std::vector<Bounds3d>& element_bounds,
                  size_t target_elements_per_node = 4,
                  double intersection_cost = 80, double traversal_cost = 1,
                  double empty_bonus = 0.1, int max_depth = -1);

    template <typename F>
    size_t find_intersecting_element(const Ray& ray, double tnear, double tfar,
                                     double& t,
                                     F element_intersection_function) const;

    inline const Tree& get_tree() const { return tree; }
    inline const std::vector<std::vector<size_t>>& get_element_ids() const {
        return element_ids;
    }

   private:
    Tree tree;
    size_t element_count;
    std::vector<std::vector<size_t>> element_ids;
};

#include "kdaccel_impl.hpp"
