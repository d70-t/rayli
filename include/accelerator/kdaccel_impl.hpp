#pragma once

template <typename F>
size_t KDAccelerator::find_intersecting_element(
  const Ray& ray, double tnear, double tfar, double& t,
  F element_intersection_function) const {
    auto [element_id, tmax] = tree.walk<std::pair<size_t, double>>(
      ray, tnear, tfar,
      {element_count, std::numeric_limits<double>::infinity()},
      [&](auto acc, size_t element_ofs, const Ray& ray, double tnear_local,
          double tfar_local, bool& stop) -> std::pair<size_t, double> {
          auto [best_id, tmax] = acc;
          if (tmax < tnear_local) {
              // there was an intersection closer than this box, no need
              // to check here and later
              stop = true;
              return acc;
          }
          for (size_t element_id : element_ids[element_ofs]) {
              double t_cur;
              if (element_intersection_function(element_id, ray, tnear, tfar,
                                                t_cur)) {
                  if (t_cur < tmax) {
                      tmax = t_cur;
                      best_id = element_id;
                      // intentionally no break as there can still be
                      // closer ones
                  }
              }
          }
          return {best_id, tmax};
      });
    t = tmax;
    return element_id;
}
