#pragma once

#include <Ray.hpp>
#include <RaySink.hpp>
#include <VarianceReduction.hpp>
#include <brdf/worldbrdf.hpp>
#include <material.hpp>
#include <thread_support.hpp>
#include <vgrid/GridIterator.hpp>

#include <iostream>
#include <random>
#include <thread>
#include <variant>

constexpr double eps = 1e-9;

struct NoSink {};
static auto nosink = NoSink{};

template <typename Grid, typename ExtTexture, typename ScatTexture,
          typename SurfTexture, typename Sink = NoSink>
struct Scn {
    typedef Grid grid_t;
    typedef ExtTexture ext_tt;
    typedef ScatTexture scat_tt;
    typedef SurfTexture surf_tt;
    typedef Sink sink_t;

    Scn(const Grid& grid, const ExtTexture& ext, const ScatTexture& scat,
        const SurfTexture& surf)
        : grid(grid), ext(ext), scat(scat), surf(surf), sink(nosink) {}

    Scn(const Grid& grid, const ExtTexture& ext, const ScatTexture& scat,
        const SurfTexture& surf, const Sink& sink)
        : grid(grid), ext(ext), scat(scat), surf(surf), sink(sink) {}

    const Grid& grid;
    const ExtTexture& ext;
    const ScatTexture& scat;
    const SurfTexture& surf;
    const Sink& sink;

    static constexpr bool has_sink() {
        return !std::is_convertible<sink_t, NoSink>::value;
    }
};

template <typename SCN, typename Recorder,
          template <typename, typename> typename TraceHandler>
class SingleTracer {
   public:
    typedef std::mt19937_64 rng_t;
    typedef SCN scn_t;
    typedef Recorder rec_t;

    SingleTracer(const SCN& scn, Recorder& rec) : scn(scn), rec(rec) {
        std::random_device rd;
        rng.seed(rd());
    }

    template <typename Handler>
    void run_handler(Handler& h) {
        while (h.new_direction()) {
            h.initialize_walk();
            for (auto event :
                 scn.grid.walk_along(h.r, h.walk_start(), h.walk_end())) {
                handle(h, event);
                if (!h.continue_direction()) break;
            }
        }
    }

    template <typename RaySource, typename VR = rayli::vr::NoVarianceReduction>
    void solve(RaySource& source, VR vr = {}) {
        while (source.has_more_rays()) {
            auto [r, source_id] = source.generate_ray(rng);
            rec.on_new_ray(r, source_id);
            auto handler = TraceHandler{*this, r, vr};
            run_handler(handler);
            rec.on_ray_finish(handler.L());
        }
    }

    const SCN& scn;
    Recorder& rec;
    rng_t rng;
};

template <typename ST>
class LocalEstimateTraceHandler {
   public:
    ST& st;
    Ray r;
    double epsilon = 0;
    double Lpath;

    template <typename F>
    LocalEstimateTraceHandler(ST& st, const Eigen::Vector3d& o, F calc_Lpath,
                              double epsilon_ = 0.)
        : st{st} {
        target = st.scn.sink.sample_from(st.rng, o);
        r = {o, target.d};
        epsilon = epsilon_;
        Lpath = calc_Lpath(r.d);
    }

    constexpr bool new_direction() const { return tau < 0; }

    constexpr bool continue_direction() const {
        return tau < std::numeric_limits<double>::infinity() && Lpath > 0;
    }

    void initialize_walk() { tau = 0; }

    void operator()(const Teleport& t) {
      r = t.r;
    }

    void operator()(const VolumeSlice& v) {
        double s = v.tfar - v.tnear;
        double k_ext = st.scn.ext(v.idx);
        tau += s * k_ext;
    }

    void operator()(const SurfaceSlice& s) {
        st.rec.on_surface_le(r, s, std::exp(-tau) * Lpath);
        auto brdf = st.scn.surf(s.idx);
        if (!bool(brdf.flags & MaterialFlags::transparent)) {
            tau = std::numeric_limits<double>::infinity();
        }
    }

    constexpr double walk_start() const { return epsilon; }
    constexpr double walk_end() const { return target.distance; }

    constexpr double L() const {
        return std::exp(-tau) * target.intensity * Lpath;
    }

   private:
    SinkSample target;
    double tau = -1;
};

template <typename ST, typename VR = rayli::vr::NoVarianceReduction>
class ExtinctionTraceHandler {
   public:
    ST& st;
    Ray r;
    VR vr;
    ExtinctionTraceHandler(ST& st, const Ray& r, VR vr = {}, double Lpath = 1)
        : st(st), r(r), vr(vr), Lpath(Lpath) {}

    constexpr bool new_direction() const {
        return change_direction && Lpath > 0;
    }
    constexpr bool continue_direction() const {
        return !change_direction && Lpath > 0;
    }

    void initialize_walk() {
        change_direction = false;
        std::exponential_distribution<double> tau_dist(1);
        tau_remain = tau_dist(st.rng);
    }

    void operator()(const Teleport& t) {
      r = t.r;
    }

    void operator()(const VolumeSlice& v) {
        double s = v.tfar - v.tnear;
        double k_ext = st.scn.ext(v.idx);
        double dtau = s * k_ext;
        bool do_scatter = dtau > tau_remain;

        if (do_scatter) {
            s *= tau_remain / dtau;
            r.o = r(v.tnear + s);
            auto scatterer = st.scn.scat(v.idx);

            local_estimate(scatterer, k_ext);
            Ltot += vr.before_scatter(
              st, r, Lpath, scatterer, k_ext,
              [&](double Lpath, const Ray& r, auto sub_vr) {
                  return ExtinctionTraceHandler<ST, decltype(sub_vr)>(
                    st, r, sub_vr, Lpath);
              });

            scatter(scatterer, k_ext);

            Ltot += vr.after_scatter(
              st, r, Lpath, vr, [&](double Lpath, const Ray& r, auto sub_vr) {
                  return ExtinctionTraceHandler<ST, decltype(sub_vr)>(
                    st, r, sub_vr, Lpath);
              });
        } else {
            tau_remain -= dtau;
        }
    }

    void operator()(const SurfaceSlice& s) {
        st.rec.on_surface(r, s, Lpath);
        auto brdf = st.scn.surf(s.idx);
        if (!bool(brdf.flags & MaterialFlags::transparent)) {
            Eigen::Vector3d p = r(s.t);
            auto geometry = s.compute_geometry(r);
            auto transform = WorldBRDFTransform::from_n_and_dpdu(
              geometry.shading.n, geometry.shading.dpdu);

            if constexpr (ST::scn_t::has_sink()) {
                if (bool(brdf.flags & MaterialFlags::diffuse)) {
                    auto le_handler = LocalEstimateTraceHandler(
                      st, p,
                      [&](const Eigen::Vector3d& d) {
                          // TODO: check if direction order is valid for Forward
                          // Tracing
                          return Lpath * transform.f(brdf, d, -r.d);
                      },
                      eps);
                    st.run_handler(le_handler);
                    Ltot += le_handler.L();
                }
            }

            if (bool(brdf.flags &
                     (MaterialFlags::diffuse | MaterialFlags::specular))) {
                // TODO: handle transform argument correctly
                /*Ltot += vr.before_scatter(
                  st, r, Lpath, transform, 1.,
                  [&](double Lpath, const Ray& r, auto sub_vr) {
                      return ExtinctionTraceHandler<ST, decltype(sub_vr)>(
                        st, r, sub_vr, Lpath);
                  });
                */

                auto scat_sample = transform.sample_f(brdf, -r.d, st.rng);
                Lpath *= scat_sample.f / scat_sample.pdf;
                r = {p - r.d * eps, scat_sample.d};
                st.rec.on_scattering(r);
                change_direction = true;

                Ltot += vr.after_scatter(
                  st, r, Lpath, vr,
                  [&](double Lpath, const Ray& r, auto sub_vr) {
                      return ExtinctionTraceHandler<ST, decltype(sub_vr)>(
                        st, r, sub_vr, Lpath);
                  });
            } else {
                Lpath = 0;
            }
        }
    }

    constexpr double walk_start() const { return eps; }
    constexpr double walk_end() const {
        return std::numeric_limits<double>::infinity();
    }

    constexpr double L() const { return Ltot; }

   protected:
    bool change_direction = true;
    double Ltot = 0;  ///< total power transmitted through all paths
    double Lpath;     ///< power transmitted on the currently traced path
    double tau_remain;

    template <typename HitObject>
    void local_estimate(const HitObject& hit_object, double k_ext) {
        if constexpr (ST::scn_t::has_sink()) {
            if (!vr.allow_local_estimate()) {
                // std::cerr << "local estimate denied\n";
                return;
            }
            // std::cerr << "performing estimate\n";
            auto le_handler = LocalEstimateTraceHandler(
              st, r.o - r.d * eps, [&](const Eigen::Vector3d& d) {
                  return Lpath * hit_object.f(-r.d, d) / k_ext;
              });
            st.run_handler(le_handler);
            Ltot += le_handler.L();
        }
    }

    template <typename HitObject>
    void scatter(const HitObject& hit_object, double k_ext) {
        st.rec.on_scattering(r);
        if constexpr (ST::scn_t::has_sink()) {
            if (!vr.modify_scatter(hit_object, st.scn, Lpath, k_ext, r,
                                   st.rng)) {
                scatter_normal(hit_object, k_ext);
            }
        } else {
            scatter_normal(hit_object, k_ext);
        }
        change_direction = true;
    }

    template <typename HitObject>
    void scatter_normal(const HitObject& hit_object, double k_ext) {
        auto scat_sample = hit_object.sample_f(-r.d, st.rng);
        Lpath *= scat_sample.f / (scat_sample.pdf * k_ext);
        r.d = scat_sample.d;
    }
};

template <typename ST, typename VR = rayli::vr::NoVarianceReduction>
class ScatterTraceHandler : public ExtinctionTraceHandler<ST, VR> {
   public:
    using ExtinctionTraceHandler<ST, VR>::st;
    using ExtinctionTraceHandler<ST, VR>::r;

    ScatterTraceHandler(ST& st, const Ray& r, VR vr = {})
        : ExtinctionTraceHandler<ST, VR>(st, r, vr) {}

    void initialize_walk() {
        ExtinctionTraceHandler<ST, VR>::initialize_walk();
        tau_abs = 0;
    }

    using ExtinctionTraceHandler<ST, VR>::operator();

    void operator()(const VolumeSlice& v) {
        double s = v.tfar - v.tnear;
        auto scatterer = st.scn.scat(v.idx);
        double k_sca = scatterer.sigma_scat();
        double k_ext = st.scn.ext(v.idx);
        double dtau = s * k_sca;
        bool do_scatter = dtau > tau_remain;

        if (do_scatter) {
            s *= tau_remain / dtau;
        }

        tau_abs += (k_ext - k_sca) * s;

        if (do_scatter) {
            r.o = r(v.tnear + s);
            Lpath *= exp(-tau_abs);

            local_estimate(scatterer, k_sca);
            scatter(scatterer, k_sca);
        } else {
            tau_remain -= dtau;
        }
    }

   protected:
    using ExtinctionTraceHandler<ST, VR>::tau_remain;
    using ExtinctionTraceHandler<ST, VR>::Lpath;
    using ExtinctionTraceHandler<ST, VR>::local_estimate;
    using ExtinctionTraceHandler<ST, VR>::scatter;
    double tau_abs = 0;
};

template <typename SCN, typename VR = rayli::vr::NoVarianceReduction,
          template <typename, typename> typename TraceHandler = ExtinctionTraceHandler>
class PathTracer {
   public:
    PathTracer(const SCN& scn, VR vr = {}) : scn(scn), vr(vr) {}

    template <typename RaySource, typename Recorder>
    void solve(RaySource& source, Recorder& rec) {
        auto st = SingleTracer<SCN, Recorder, TraceHandler>{scn, rec};
        st.solve(source, vr);
    }

    template <typename RaySource, typename Recorder>
    void solve_par(RaySource& source, Recorder& rec, size_t num_threads = 0) {
        if (!num_threads) {
            num_threads = cluster_aware_concurrency();
        }
        auto sources = source.split(num_threads);
        auto recorders = rec.split(num_threads);
        std::vector<std::thread> pool;
        for (size_t i = 0; i < num_threads; ++i) {
            auto& source = sources[i];
            auto& rec = recorders[i];
            pool.push_back(std::thread([&]() {
                auto st = SingleTracer<SCN, Recorder, TraceHandler>{scn, rec};
                st.solve(source, vr);
            }));
        }
        for (size_t i = 0; i < num_threads; ++i) {
            pool[i].join();
            rec.join(std::move(recorders[i]));
        }
    }

   private:
    const SCN& scn;
    VR vr;
};

template <typename SCN, typename VR = rayli::vr::NoVarianceReduction>
class ScatterPathTracer : public PathTracer<SCN, VR, ScatterTraceHandler> {
   public:
    ScatterPathTracer(const SCN& scn, VR vr = {})
        : PathTracer<SCN, VR, ScatterTraceHandler>(scn, vr) {}
};
