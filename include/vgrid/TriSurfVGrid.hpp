#pragma once

#include <Ray.hpp>
#include <surface/TriangularMesh.hpp>

class TriSurfVGrid {
   public:
    typedef TriSurfVGrid Self;
    struct RayPath;

    TriSurfVGrid(std::shared_ptr<rayli::surface::TriangularMesh> mesh);

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{*this, ray, tnear, tfar};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice& reference;
        typedef std::input_iterator_tag iterator_category;

        inline RayIterator(const Self& grid, Ray ray, double tnear_,
                           double tfar_)
            : grid(grid), ray(std::move(ray)), tnear(tnear_), tfar(tfar_),
              t(0) {
            valid = true;
            face_idx = grid.mesh->face_count();
            advance();
        }

        inline RayIterator(const Self& grid) : grid(grid) { valid = false; }

        const LinearSlice operator*() const {
            auto face = grid.mesh->get_face(face_idx);
            return SurfaceSlice{
              t, face_idx, [face](const Ray& ray, const SurfaceSlice& slice) {
                  Eigen::Vector3d p = ray(slice.t);
                  auto v0 = face.v(0);
                  Eigen::Vector3d dpdu = face.v(1) - v0;
                  Eigen::Vector3d dpdv = face.v(2) - v0;
                  double u = std::clamp((p - v0).dot(dpdu), 0., 1.);
                  double v = std::clamp((p - v0).dot(dpdv), 0., 1.);

                  Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
                  Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
                  return rayli::SurfaceGeometry{{u, v}, dpdu, dpdv, dndu, dndv};
              }};
        }

        bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        RayIterator& operator++() {
            if (valid) {
                advance();
            }
            return *this;
        }

        inline bool is_valid() const { return valid; }

       private:
        const Self& grid;
        Ray ray;
        double tnear, tfar, t;
        size_t face_idx;
        bool valid;

        void advance();
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar);
        }

        inline RayIterator end() const { return RayIterator(grid); }
    };

   private:
    std::shared_ptr<rayli::surface::TriangularMesh> mesh;
};
