#pragma once
#include <vgrid/CartesianBase.hpp>

#include <vector>

namespace rayli::vgrid {
class MysticCloud : public CartesianBase<MysticCloud> {
   public:
    using RayPath = typename CartesianBase<MysticCloud>::RayPath;

    MysticCloud(double dx, double dy, size_t Nx, size_t Ny,
                std::vector<double> z_lev_);

    inline double gx(size_t ix) const { return ix * dx; }
    inline double gy(size_t iy) const { return iy * dy; }
    inline double gz(size_t iz) const { return z_lev[iz]; }

    std::array<size_t, 3>
    find_grid_coordinates(const Eigen::Vector3d& x,
                          const Eigen::Vector3d& direction) const;

   private:
    double dx;
    double dy;
    std::vector<double> z_lev;
};
}  // namespace rayli::vgrid
