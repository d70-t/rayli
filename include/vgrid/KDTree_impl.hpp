#pragma once

template <typename Value>
KdNode<Value> KdNode<Value>::create_leaf(const Value& value) {
    KdNode<Value> node;
    node.set_leaf();
    node.u.leaf.value = value;
    return node;
}

template <typename Value>
KdNode<Value> KdNode<Value>::create_split(unsigned char axis, double split) {
    KdNode<Value> node;
    node.set_inner(axis);
    node.u.internal.split = split;
    return node;
}

template <typename Value>
template <size_t size>
void KdTree<Value>::TodoManager<size>::enqueue(
  const typename KdTree<Value>::Node* node, double tnear, double tfar) {
    todo[pos].node = node;
    todo[pos].tnear = tnear;
    todo[pos].tfar = tfar;
    ++pos;
}

template <typename Value>
template <size_t size>
typename KdTree<Value>::template TodoManager<size>::Todo&
KdTree<Value>::TodoManager<size>::pop() {
    --pos;
    return todo[pos];
}
