#pragma once
#include "grid/ICONGrid.hpp"
#include "logging.hpp"

#include "GridIterator.hpp"
#include "constants.hpp"
#include "ringbuffer.hpp"

#include "vgrid/WedgeGrid.hpp"

namespace rayli::vgrid {
constexpr size_t ICON_MAX_WALK_ITERATIONS = 1000000;

class UniformVerticalGrid {
   public:
    UniformVerticalGrid() = delete;
    UniformVerticalGrid(std::vector<double> levels_)
        : levels(std::move(levels_)) {
        assert(levels.size() >= 2);
        logger = rayli::logging::get_logger("UniformVerticalGrid");
    }
    inline double ground() { return levels.front(); }
    inline double toa() { return levels.back(); }
    inline size_t level_count() { return levels.size(); }
    inline size_t layer_count() { return level_count() - 1; }
    inline size_t volume_base_id(size_t cell_id) {
        return cell_id * layer_count();
    }
    inline size_t volume_id(size_t cell_id, size_t layer_id) {
        return cell_id * layer_count() + layer_id;
    }
    inline size_t horizontal_face_id(size_t cell_id, size_t level_id) {
        return cell_id * level_count() + level_id;
    }
    inline size_t vertical_face_id(size_t edge_id, size_t layer_id,
                                   size_t cell_count, size_t edge_count) {
        // note that there is space for one ghost cell
        return (cell_count + 1) * levels.size() + layer_id * edge_count +
               edge_id;
    }
    /** \brief finds layer containing z
     *
     * \note it is assumed that z is between (including) ground() and toa().
     */
    size_t find_layer(size_t cell_id, double z);
    /** \brief finds layer containing z
     *
     * \note it is assumed that z is between (including) ground() and toa().
     *
     * \note this method assumes that the given start_layer
     *       contains z in one of the neighbor cells.
     */
    inline size_t find_layer(size_t cell_id, double z, size_t start_layer) {
        assert(start_layer < layer_count());
        return start_layer;
    };
    /** \brief finds next layer by intersecting ray with adjacent levels
     *
     * \return pair of next layer id and intersection t
     */
    std::pair<size_t, double> find_next_layer(size_t cell_id,
                                              size_t start_layer,
                                              const Ray& ray, double tnear,
                                              double tfar);
    inline double level_below(size_t layer_id) {
        assert(layer_id < layer_count());
        return levels[layer_id];
    }
    inline double level_above(size_t layer_id) {
        assert(layer_id < layer_count());
        return levels[layer_id + 1];
    }
    inline bool layer_is_valid(size_t layer_id) {
        return layer_id < layer_count();
    }
    inline const std::vector<double>& get_levels() const { return levels; }

   private:
    std::vector<double> levels;
    rayli::logging::logger_ptr logger;
};

template <typename VerticalGrid = UniformVerticalGrid>
class ICON {
   public:
    typedef ICON<VerticalGrid> Self;
    struct RayPath;

    ICON(std::shared_ptr<rayli::grid::ICONGrid> grid,
         std::shared_ptr<VerticalGrid> vgrid = nullptr)
        : grid(grid), vertical_grid(vgrid) {
        logger = rayli::logging::get_logger("ICON");
        assert(grid);
        if (!vertical_grid) {
            vertical_grid = std::make_shared<UniformVerticalGrid>(
              std::vector<double>{grid->rmin, grid->rmax});
        }
        assert(grid->rmin == vertical_grid->ground());
        assert(grid->rmax == vertical_grid->toa());
    }

    size_t layer_count() const { return vertical_grid->layer_count(); };
    size_t volume_count() const {
        return (grid->cell_count() + 1) * vertical_grid->layer_count();
    };
    size_t surface_count() const {
        return (grid->cell_count() + 1) * vertical_grid->level_count() +
               grid->edge_count() * vertical_grid->layer_count();
    };

    RayPath
    walk_along(const Ray& ray, double tnear, double tfar,
               std::optional<size_t> last_volume_id = std::nullopt) const {
        return RayPath{*this, ray, tnear, tfar, last_volume_id};
    }

    explicit operator WedgeGrid() {
        const auto& svertices = grid->vertices;
        const auto& levels = vertical_grid->get_levels();
        std::vector<Eigen::Vector3d> vertices(svertices.size() * levels.size());
        for (size_t i = 0; i < svertices.size(); ++i) {
            for (size_t j = 0; j < levels.size(); ++j) {
                vertices[i * levels.size() + j] = svertices[i] * levels[j];
            }
        }
        const auto& scells = grid->cells;
        size_t nlayers = vertical_grid->layer_count();
        std::vector<std::array<size_t, 6>> cells(scells.size() * nlayers);
        for (size_t i = 0; i < scells.size(); ++i) {
            size_t va = scells[i].vertices[0] * levels.size();
            size_t vb = scells[i].vertices[1] * levels.size();
            size_t vc = scells[i].vertices[2] * levels.size();
            for (size_t j = 0; j < nlayers; ++j) {
                cells[i * nlayers + j] = {va + j,     vb + j,     vc + j,
                                          va + j + 1, vb + j + 1, vc + j + 1};
            }
        }
        std::vector<std::array<size_t, 3>> faces(scells.size() * levels.size());
        for (size_t i = 0; i < scells.size(); ++i) {
            size_t va = scells[i].vertices[0] * levels.size();
            size_t vb = scells[i].vertices[1] * levels.size();
            size_t vc = scells[i].vertices[2] * levels.size();
            for (size_t j = 0; j < levels.size(); ++j) {
                faces[i * levels.size() + j] = {va + j, vb + j, vc + j};
            }
        }
        return WedgeGrid{std::move(vertices), std::move(cells),
                         std::move(faces)};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid, Ray ray, double tnear_, double tfar_)
            : grid(grid), ray(std::move(ray)), tnear(tnear_), tfar(tfar_),
              cell(grid.grid->get_cell()) {

            if (!test_spherical_shell_interaction()) return;
            if (!initialize_state_from_unknown_location()) return;

            valid = true;

            SPDLOG_TRACE(grid.logger,
                         "number of cells: {}, starting cell is: {}, "
                         "vertical layer is: {}",
                         grid.grid->cell_count(), cell.id(), current_layer);

            std::tie(next_layer, tfar_next_level) =
              grid.vertical_grid->find_next_layer(cell.id(), current_layer, ray,
                                                  tnear, tfar);
            SPDLOG_TRACE(grid.logger, "next vertical layer will be {} @ t={}",
                         next_layer, tfar_next_level);

            emit_volume();
        }

        RayIterator(const Self& grid, Ray ray, double tnear_, double tfar_,
                    size_t last_volume_id)
            : grid(grid), ray(std::move(ray)), tnear(tnear_), tfar(tfar_),
              cell(grid.grid->get_cell(last_volume_id /
                                       grid.vertical_grid->layer_count())) {

            if (!test_spherical_shell_interaction()) return;

            if (cell.is_valid() && tnear == 0) {
                if (!initialize_state_from_known_location(last_volume_id))
                    return;
            } else {
                if (!initialize_state_from_unknown_location()) return;
            }

            valid = true;
            SPDLOG_TRACE(grid.logger,
                         "number of cells: {}, starting cell is: {}, "
                         "vertical layer is: {}",
                         grid.grid->cell_count(), cell.id(), current_layer);

            std::tie(next_layer, tfar_next_level) =
              grid.vertical_grid->find_next_layer(cell.id(), current_layer, ray,
                                                  tnear, tfar);
            SPDLOG_TRACE(grid.logger, "next vertical layer will be {} @ t={}",
                         next_layer, tfar_next_level);

            emit_volume();
        }
        RayIterator(const Self& grid)
            : grid(grid), cell(grid.grid->get_cell()) {
            valid = false;
        }

        const LinearSlice& operator*() const { return slice_queue.front(); }

        bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        RayIterator& operator++() {
            if (!slice_queue.empty()) {
                slice_queue.pop();
            }
            if (valid && slice_queue.empty()) {
                advance();
            }
            return *this;
        }

        bool is_valid() const { return !slice_queue.empty() || valid; }

       private:
        bool test_spherical_shell_interaction() {
            double t1, t2;
            if (!ray_ball_intersection(grid.grid->rmax, ray, &t1, &t2)) {
                // ray misses entire grid
                SPDLOG_DEBUG(grid.logger, "misses entire grid");
                return false;
            }
            SPDLOG_TRACE(grid.logger, "outer sphere intersection at t: {}, {}",
                         t1, t2);
            if (t1 > tnear) {
                enters_through_toa = true;
                SPDLOG_DEBUG(grid.logger, "enters through TOA");
            }
            if (t2 < tfar) {
                leaves_through_toa = true;
                SPDLOG_DEBUG(grid.logger, "maybe leaves through TOA");
            }
            tfar = std::min(tfar, t2);
            tnear = std::max(tnear, t1);
            if (ray_ball_intersection(grid.grid->rmin, ray, &t1, &t2)) {
                if (t1 > 0 && t1 < tfar) {
                    tfar = t1;
                    leaves_through_ground = true;
                    leaves_through_toa = false;
                    SPDLOG_DEBUG(grid.logger, "leaves through ground");
                }
                if (t1 < 0 && t2 > tnear) {
                    // cover the case when ray starts inside of sphere...
                    // maybe this is not always watned
                    tnear = t2;
                    enters_through_ground = true;
                    enters_through_toa = false;
                    SPDLOG_TRACE(
                      grid.logger,
                      "ray started inside of sphere, setting tnear to {}",
                      tnear);
                }
            }
            if (tfar <= tnear) {
                SPDLOG_DEBUG(grid.logger, "tfar ({}) < tnear ({})", tfar,
                             tnear);
                return false;
            }
            return true;
        }

        bool initialize_state_from_unknown_location() {
            double t;
            bool may_hit_spherical_face_first = false;

            auto first_edge =
              grid.grid->find_intersecting_edge(ray, tnear, tfar, t);
            SPDLOG_TRACE(grid.logger, "first edge id: {}, t: {}",
                         first_edge.id(), t);
            if (!first_edge.is_valid()) {
                // if ray passes vertically trough a single cell, it does
                // not hit any wall
                cell = grid.grid->find_cell(ray(tnear));
                tfar_next_edge = std::numeric_limits<double>::infinity();
                edge_next = grid.grid->edge_count();
                // does not matter, there is no next edge
                edge_next_dir = false;
                may_hit_spherical_face_first = true;
            } else {
                edge_next_dir = ray.d.dot(first_edge.normal()) > 0;
                cell = first_edge.get_cell(edge_next_dir);

                tfar_next_edge = t;
                edge_next = first_edge.id();
                t = tnear;
                may_hit_spherical_face_first = true;
            }
            current_layer =
              grid.vertical_grid->find_layer(cell.id(), ray(tnear).norm());

            if (may_hit_spherical_face_first) {
                if (enters_through_toa) {
                    emit_spherical_face(tnear, cell.id(),
                                        grid.vertical_grid->layer_count());
                } else if (enters_through_ground) {
                    emit_spherical_face(tnear, cell.id(), 0);
                }
            } else {
                emit_radial_face(tnear, first_edge.id(), current_layer);
            }

            return true;
        }

        bool initialize_state_from_known_location(size_t last_volume_id) {
            // can restart from continuation
            tfar_next_edge = std::numeric_limits<double>::infinity();
            edge_next = grid.grid->edge_count();
            int j = -1;
            for (auto ie : cell.iedges()) {
                ++j;
                double tnext;
                if (grid.grid->get_edge(ie).intersect_plane(
                      ray, tnear, tfar_next_edge, tnext)) {
                    if (tnext != tnear) {
                        // catch special case where ray enters through edge
                        // intersection
                        tfar_next_edge = tnext;
                        edge_next = ie;
                        edge_next_dir = cell.edge_directions()[j];
                    }
                }
            }
            current_layer =
              last_volume_id - grid.vertical_grid->volume_base_id(cell.id());
            return true;
        }

        void advance() {
            tnear = tfar_next();
            // note these two if's (instead of if-else),
            // this is to cover the case where the ray crosses cell and
            // level at once, however, the check must be done beforehand, as the
            // cross_* functions already change the iterator's state
            bool crosses_edge = tfar_next_edge <= tfar_next_level;
            bool crosses_level = tfar_next_level <= tfar_next_edge;
            SPDLOG_TRACE(grid.logger, "cross edge: {}, cross level: {}",
                         crosses_edge, crosses_level);
            if (crosses_edge) cross_edge();
            if (valid && crosses_level) cross_level();
            if (valid) emit_volume();

            if (++iteration_count >= ICON_MAX_WALK_ITERATIONS) {
                grid.logger->warn(
                  "trapped ray: {} cell id: {} "
                  "current layer: {} next layer: {} "
                  "tnear: {} tfar_next_edge: {} tfar_next_level: "
                  "{}",
                  ray, cell.id(), current_layer, next_layer, tnear,
                  tfar_next_edge, tfar_next_level);
                throw std::runtime_error("maximum number of iterations "
                                         "reached, ray seems to be trapped!");
            }
            return;
        }

        void emit_volume() {
            slice_queue.push(VolumeSlice{
              tnear, tfar_next(),
              grid.vertical_grid->volume_id(cell.id(), current_layer)});

            SPDLOG_TRACE(grid.logger,
                         "cell: {}, layer: {}, tnear: {}, "
                         "tfar_next_edge: {}, tfar_next_level: {}",
                         cell.id(), current_layer, tnear, tfar_next_edge,
                         tfar_next_level);
        }

        void emit_spherical_face(double t, size_t cell_id, size_t level_id) {
            slice_queue.push(SurfaceSlice{
              t, grid.vertical_grid->horizontal_face_id(cell_id, level_id),
              [](const Ray& ray, const SurfaceSlice& slice) {
                  Eigen::Vector3d p = ray(slice.t);
                  double r = p.norm();
                  double inv_r = 1. / r;
                  double rz = std::sqrt(p(0) * p(0) + p(1) * p(1));
                  double inv_rz = 1. / rz;

                  double phi = std::atan2(p(1), p(0));
                  double cos_phi = p(0) * inv_rz;
                  double sin_phi = p(1) * inv_rz;
                  double theta = std::acos(p(2) * inv_r);

                  Eigen::Vector2d uv = {std::clamp(0.5 + phi * INV_2PI, 0., 1.),
                                        std::clamp(theta * INV_PI, 0., 1.)};

                  Eigen::Vector3d dpdu =
                    2 * M_PI * Eigen::Vector3d{-p(1), p(0), 0};
                  Eigen::Vector3d dpdv =
                    M_PI * Eigen::Vector3d{cos_phi * p(2), sin_phi * p(2),
                                           -r * std::sin(theta)};

                  Eigen::Vector3d dndu = dpdu * inv_r;
                  Eigen::Vector3d dndv = dpdv * inv_r;
                  return rayli::SurfaceGeometry{uv, dpdu, dpdv, dndu, dndv};
              }});
        }

        void emit_radial_face(double t, size_t edge_id, size_t layer_id) {
            const auto& gridref = grid;
            slice_queue.push(SurfaceSlice{
              t,
              grid.vertical_grid->vertical_face_id(edge_id, layer_id,
                                                   grid.grid->cell_count(),
                                                   grid.grid->edge_count()),
              [edge_id, layer_id, &gridref](const Ray& ray,
                                            const SurfaceSlice& slice) {
                  double z_below = gridref.vertical_grid->level_below(layer_id);
                  double z_above = gridref.vertical_grid->level_above(layer_id);
                  double dz = z_above - z_below;

                  Eigen::Vector3d p = ray(slice.t);
                  double r = p.norm();
                  double inv_r = 1. / r;
                  Eigen::Vector3d pnorm = p * inv_r;

                  auto edge = gridref.grid->get_edge(edge_id);
                  auto a = edge.a();
                  auto b = edge.b();
                  double ab = std::acos(a.dot(b));
                  double ap = std::acos(a.dot(pnorm));

                  Eigen::Vector2d uv = {(r - z_below) / dz, ap / ab};

                  Eigen::Vector3d dpdu = pnorm * dz;
                  Eigen::Vector3d dpdv = pnorm.cross(edge.normal()) * ab * r;

                  Eigen::Vector3d dndu = Eigen::Vector3d::Zero();
                  Eigen::Vector3d dndv = Eigen::Vector3d::Zero();
                  return rayli::SurfaceGeometry{uv, dpdu, dpdv, dndu, dndv};
              }});
        }

        void update(const rayli::grid::ICONGrid::Edge& current_edge) {
            tfar_next_edge = std::numeric_limits<double>::infinity();
            if (!cell.is_valid()) {
                // TODO: check if it is possible to re-enter grid at later time
                return;
            }
            edge_next = grid.grid->edge_count();
            int j = -1;
            for (auto ie : cell.iedges()) {
                ++j;
                if (ie == current_edge.id()) {
                    continue;
                }
                double tnext;
                if (grid.grid->get_edge(ie).intersect_plane(
                      ray, tnear, tfar_next_edge, tnext)) {
                    if (tnext != tnear) {
                        // catch special case where ray enters through
                        // edge intersection
                        tfar_next_edge = tnext;
                        edge_next = ie;
                        edge_next_dir = cell.edge_directions()[j];
                    }
                }
            }
        }
        void cross_edge() {
            SPDLOG_TRACE(grid.logger, "crossing edge, change cell");
            auto edge = grid.grid->get_edge(edge_next);
            if (!edge.is_valid()) {
                cell = grid.grid->get_cell();
                SPDLOG_TRACE(grid.logger,
                             "leaving grid through side (edge invalid)");
                leave();
                return;
            }
            emit_radial_face(tfar_next_edge, edge.id(), current_layer);
            cell = edge.get_cell(!edge_next_dir);
            // if levels have shifted between layers, correct for it here
            current_layer = grid.vertical_grid->find_layer(
              cell.id(), ray(tnear).norm(), current_layer);
            update(edge);
        }
        void cross_level() {
            SPDLOG_TRACE(grid.logger, "crossing level, change layer ({} -> {})",
                         current_layer, next_layer);
            bool next_layer_is_valid =
              grid.vertical_grid->layer_is_valid(next_layer);
            auto level = current_layer;
            if (next_layer_is_valid) {
                if (next_layer > current_layer) ++level;
            } else {
                if (level != 0) ++level;
            }
            emit_spherical_face(tfar_next_level, cell.id(), level);
            if (!next_layer_is_valid) {
                SPDLOG_TRACE(grid.logger, "stopped walk, because next_layer "
                                          "became invalid");
                leave();
                return;
            }
            current_layer = next_layer;
            std::tie(next_layer, tfar_next_level) =
              grid.vertical_grid->find_next_layer(cell.id(), current_layer, ray,
                                                  tnear, tfar);
        }

        void leave() { valid = false; }

        double tfar_next() const {
            return std::min(tfar_next_edge, tfar_next_level);
        }

        const Self& grid;
        Ray ray;
        double tnear, tfar;
        bool valid = false;

        rayli::grid::ICONGrid::Cell cell;
        size_t current_layer;
        size_t next_layer;
        double tfar_next_edge;
        double tfar_next_level;
        size_t edge_next;
        bool edge_next_dir = false;
        bool leaves_through_ground = false;
        bool leaves_through_toa = false;
        bool enters_through_ground = false;
        bool enters_through_toa = false;

        Ringbuffer<LinearSlice, 3> slice_queue = {};

        size_t iteration_count = 0;
    };
    friend class RayIterator;

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;
        std::optional<size_t> last_volume_id;

        RayIterator begin() const {
            if (last_volume_id) {
                return RayIterator(grid, ray, tnear, tfar,
                                   last_volume_id.value());
            } else {
                return RayIterator(grid, ray, tnear, tfar);
            }
        }

        RayIterator end() const { return RayIterator(grid); }
    };

   private:
    std::shared_ptr<rayli::grid::ICONGrid> grid;
    std::shared_ptr<VerticalGrid> vertical_grid;
    rayli::logging::logger_ptr logger;
};
}  // namespace rayli::vgrid
