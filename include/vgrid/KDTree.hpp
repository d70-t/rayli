#pragma once

#include "GeometryUtils.hpp"
#include "Ray.hpp"
#include "vgrid/GridIterator.hpp"

#include <iostream>

#include <vector>

namespace rayli::vgrid {

template <typename Value>
struct KdNode {
    static KdNode<Value> create_leaf(const Value& value);
    static KdNode<Value> create_split(unsigned char axis, double split);
    static KdNode<Value> create_xsplit(double split) {
        return create_split(0, split);
    }
    static KdNode<Value> create_ysplit(double split) {
        return create_split(1, split);
    }
    static KdNode<Value> create_zsplit(double split) {
        return create_split(2, split);
    }

    union u {
        struct internal {
            double split;
            size_t second_child;
        } internal;
        struct leaf {
            Value value;
        } leaf;
    } u;
    unsigned char kind_axis = 0;

    inline unsigned char axis() const { return kind_axis & 0x03; }
    inline bool is_leaf() const { return kind_axis & 0x04; }
    inline bool is_inner() const { return !is_leaf(); }
    inline void set_leaf() { kind_axis |= 0x04; }
    inline void set_inner(unsigned char ax) {
        kind_axis = (kind_axis & 0xf8) | (ax & 0x03);
    }
    inline void set_second_child(size_t child_id) {
        assert(is_inner());
        u.internal.second_child = child_id;
    }
};

template <typename Value>
class KdTree {
   public:
    typedef KdTree<Value> Self;
    typedef KdNode<Value> Node;

    /** \brief stack for KdTree nodes to visit in future
     */
    template <size_t size>
    class TodoManager {
       public:
        struct Todo {
            const Node* node;
            double tnear, tfar;
        };
        void enqueue(const Node* node, double tnear, double tfar);
        Todo& pop();
        bool empty() const { return pos == 0; }

       private:
        std::array<Todo, size> todo;
        size_t pos = 0;
    };

    KdTree() = default;
    KdTree(Bounds3d bounds, std::vector<Node> nodes)
        : bounds(bounds), nodes(std::move(nodes)) {}

    template <typename RET, typename STEP, typename CONT>
    RET walk(const Ray& ray, double tnear, double tfar, RET accumulator,
             CONT continuation, STEP step) const {
        for (auto slice : this->walk_along(ray, tnear, tfar)) {
            bool stop = false;
            accumulator = step(accumulator, slice.idx, ray, slice.tnear,
                               slice.tfar, continuation, stop);
            if (stop) break;
        }
        return accumulator;
    }

    template <typename RET, typename STEP>
    RET walk(const Ray& ray, double tnear, double tfar, RET accumulator,
             STEP step) const {
        for (auto slice : this->walk_along(ray, tnear, tfar)) {
            bool stop = false;
            accumulator =
              step(accumulator, slice.idx, ray, slice.tnear, slice.tfar, stop);
            if (stop) break;
        }
        return accumulator;
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef VolumeSlice value_type;
        typedef value_type* pointer;
        typedef value_type& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& tree, Ray ray, double tnear_, double tfar_)
            : tree(tree), ray(ray), tnear(tnear_), tfar(tfar_) {
            if (!tree.bounds.intersect(ray, &tnear, &tfar)) return;
            invDir = {1. / ray.d(0), 1. / ray.d(1), 1. / ray.d(2)};
            node = &tree.nodes[0];
            advance_to_next_leaf();
        }

        RayIterator(const Self& tree) : tree(tree) {}

        value_type operator*() const {
            return VolumeSlice{tnear, tfar, node->u.leaf.value};
        }

        RayIterator& operator++() {
            if (todo.empty()) {
                node = nullptr;
            } else {
                auto& next = todo.pop();
                node = next.node;
                tnear = next.tnear;
                tfar = next.tfar;
                advance_to_next_leaf();
            }
            return *this;
        }

        RayIterator operator++(int) {
            RayIterator out = *this;
            (*this)++;
            return out;
        }

        bool operator!=(const RayIterator& other) const {
            if (is_valid() != other.is_valid()) {
                return true;
            }
            if (!(is_valid() && other.is_valid())) {
                return false;
            }
            return (node != other.node) || (ray != other.ray) ||
                   (tnear != other.tnear) || (tfar != other.tfar);
        }

        bool operator==(const RayIterator& other) const {
            return !(*this == other);
        }

        bool is_valid() const { return bool(node); }

       private:
        void advance_to_next_leaf() {
            while (is_valid() && node->is_inner()) {
                int axis = node->axis();

                double axis_aligned_distance =
                  node->u.internal.split - ray.o(axis);
                double tsplit;
                if (axis_aligned_distance == 0) {
                    // otherwise a ray which is exactly inside the split and
                    // parallel will create tsplit == nan (because invDir is
                    // inf) and produce wrong results.
                    tsplit = 0;
                } else {
                    tsplit = axis_aligned_distance * invDir(axis);
                }

                // check which of the two children has to be visited first
                bool lower_first =
                  (ray.o(axis) < node->u.internal.split) ||
                  (ray.o(axis) == node->u.internal.split && ray.d(axis) <= 0);
                const Node *first_child, *second_child;
                if (lower_first) {
                    first_child = node + 1;
                    second_child = &tree.nodes[node->u.internal.second_child];
                } else {
                    first_child = &tree.nodes[node->u.internal.second_child];
                    second_child = node + 1;
                }
                // avoid looking at children which can not be reached
                if (tsplit > tfar || tsplit <= 0) {
                    node = first_child;
                } else if (tsplit < tnear) {
                    node = second_child;
                } else {
                    todo.enqueue(second_child, tsplit, tfar);
                    node = first_child;
                    tfar = tsplit;
                }
            }
        }

        const Self& tree;
        Ray ray;
        double tnear, tfar;
        Eigen::Vector3d invDir;
        TodoManager<64> todo;
        const Node* node = nullptr;
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef VolumeSlice value_type;
        typedef VolumeSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& tree;
        Ray ray;
        double tnear, tfar;

        RayIterator begin() const {
            return RayIterator(tree, ray, tnear, tfar);
        }

        RayIterator end() const { return RayIterator(tree); }
    };

    RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{*this, ray, tnear, tfar};
    }

    size_t add_node(const Node& node) {
        size_t idx = nodes.size();
        nodes.push_back(node);
        return idx;
    }

    size_t add_leaf(const Value& value) {
        return add_node(Node::create_leaf(value));
    }

    size_t add_split(unsigned char axis, double split) {
        return add_node(Node::create_split(axis, split));
    }

    size_t add_xsplit(double split) { return add_split(0, split); }

    size_t add_ysplit(double split) { return add_split(1, split); }

    size_t add_zsplit(double split) { return add_split(2, split); }

    Bounds3d bounds;
    std::vector<Node> nodes;
};

#include "KDTree_impl.hpp"
}  // namespace rayli::vgrid
