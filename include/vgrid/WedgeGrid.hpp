#pragma once
#include <Eigen.hpp>
#include <array>
#include <vector>

namespace rayli::vgrid {

class WedgeGrid {
   public:
    WedgeGrid(std::vector<Eigen::Vector3d> vertices,
              std::vector<std::array<size_t, 6>> cells,
              std::vector<std::array<size_t, 3>> faces);

    std::vector<Eigen::Vector3d> vertices;
    std::vector<std::array<size_t, 6>> cells;
    std::vector<std::array<size_t, 3>> faces;
};
}  // namespace rayli::vgrid
