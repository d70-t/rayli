#pragma once
#include <Eigen.hpp>
#include <Ray.hpp>
#include <array>
#include <logging.hpp>
#include <memory>
#include <surface/TriangularMesh.hpp>
#include <vector>
#include <vgrid/GridIterator.hpp>

namespace rayli::vgrid {

class TriCell {
   public:
    typedef TriCell Self;
    struct RayPath;

    TriCell(std::shared_ptr<rayli::surface::TriangularMesh> mesh,
            std::shared_ptr<std::vector<std::array<size_t, 2>>> cells_of_faces);

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{*this, ray, tnear, tfar};
    }

    inline double area_of_face(size_t face_idx) const {
        return mesh->get_face(face_idx).area();
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid, Ray ray, double tnear_, double tfar_);

        inline RayIterator(const Self& grid) : grid(grid) {
            state = State::INVALID;
        }

        LinearSlice operator*() const;

        inline bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        inline bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        inline RayIterator& operator++() {
            advance();
            return *this;
        }

        inline bool is_valid() const { return state != State::INVALID; }

       private:
        const Self& grid;
        Ray ray;
        double tnear, tfar, t1, t2;
        size_t face1, face2, volume;

        enum class State {
            UNINITIALIZED,
            VOL1,
            FACE1,
            VOL2,
            FACE2,
            VOL3,
            INVALID
        };

        State state;

        void advance();
        void advance_face();
        size_t volume_id(int v);
        inline static bool is_valid_volume(size_t volume_id) {
            return volume_id < std::numeric_limits<size_t>::max();
        }
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar);
        }

        inline RayIterator end() const { return RayIterator(grid); }
    };

   private:
    std::shared_ptr<rayli::surface::TriangularMesh> mesh;
    std::shared_ptr<std::vector<std::array<size_t, 2>>> cells_of_faces;
    size_t face_count;
    rayli::logging::logger_ptr logger;
};

}  // namespace rayli::vgrid
