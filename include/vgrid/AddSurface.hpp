#pragma once

#include <Ray.hpp>

namespace rayli::vgrid {

struct RestrictSlice {
    double tnear, tfar;
    VolumeSlice operator()(const VolumeSlice& s) {
        return VolumeSlice{std::max(tnear, s.tnear), std::min(tfar, s.tfar),
                           s.idx};
    }

    SurfaceSlice operator()(const SurfaceSlice& s) {
        assert(tnear <= s.t && tfar >= s.t);
        return s;
    }

    LinearSlice operator()(const LinearSlice& s) {
        return std::visit(
          [this](const auto& s) -> LinearSlice { return operator()(s); }, s);
    }
};

template <typename T>
constexpr bool is_surface_slice(const T& s) {
    if constexpr (std::is_same<T, SurfaceSlice>::value) {
        return true;
    } else {
        return std::holds_alternative<SurfaceSlice>(s);
    }
}

template <typename T>
constexpr const SurfaceSlice& get_surface_slice(const T& s) {
    if constexpr (std::is_same<T, SurfaceSlice>::value) {
        return s;
    } else {
        return std::get<SurfaceSlice>(s);
    }
}

template <typename VG, typename SG>
class AddSurface {
   public:
    typedef AddSurface<VG, SG> Self;
    struct RayPath;

    AddSurface(const VG& vg, const SG& sg) : vg(vg), sg(sg) {}
    RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{vg.walk_along(ray, tnear, tfar),
                       sg.walk_along(ray, tnear, tfar)};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(typename VG::RayIterator vgit_,
                    typename SG::RayIterator sgit_)
            : vgit(vgit_), sgit(sgit_) {
            set_volume_t();
            if (sgit.is_valid() && !is_surface_slice(*sgit)) {
                advance_surface();
            } else {
                set_surface_t();
            }
            t = std::min(tnv, ts);
        }

        const LinearSlice operator*() const {
            if (vgit.is_valid()) {
                if (sgit.is_valid()) {
                    if (t == ts) {
                        return *sgit;
                    } else {
                        return RestrictSlice{t, ts}(*vgit);
                    }
                } else {
                    return RestrictSlice{
                      t, std::numeric_limits<double>::infinity()}(*vgit);
                }
            } else {
                return *sgit;
            }
        }

        bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        RayIterator& operator++() {
            if (vgit.is_valid()) {
                if (sgit.is_valid()) {
                    if (tfv < ts) {
                        advance_volume();
                        t = std::min(tnv, ts);
                    } else if (t < ts) {
                        t = ts;
                    } else if (t == ts) {
                        advance_surface();
                    }
                } else {
                    advance_volume();
                    t = tnv;
                }
            } else {
                advance_surface();
                t = ts;
            }
            return *this;
        }

        bool is_valid() const { return vgit.is_valid() || sgit.is_valid(); }

       private:
        void advance_volume() {
            if (vgit.is_valid()) {
                ++vgit;
                set_volume_t();
            }
        }
        void set_volume_t() {
            if (vgit.is_valid()) {
                auto v = *vgit;
                tnv = slice_tnear(v);
                tfv = slice_tfar(v);
            } else {
                tnv = std::numeric_limits<double>::infinity();
                tfv = std::numeric_limits<double>::infinity();
            }
        }

        void advance_surface() {
            ++sgit;
            while (sgit.is_valid() && !is_surface_slice(*sgit)) {
                ++sgit;
            }
            set_surface_t();
        }

        void set_surface_t() {
            if (sgit.is_valid()) {
                ts = get_surface_slice(*sgit).t;
            } else {
                ts = std::numeric_limits<double>::infinity();
            }
        }

        typename VG::RayIterator vgit;
        typename SG::RayIterator sgit;
        double t, tnv, tfv, ts;
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        typename VG::RayPath vgpath;
        typename SG::RayPath sgpath;

        inline RayIterator begin() const {
            return RayIterator(vgpath.begin(), sgpath.begin());
        }

        inline RayIterator end() const {
            return RayIterator(vgpath.end(), sgpath.end());
        }
    };

   private:
    const VG& vg;
    const SG& sg;
};
}  // namespace rayli::vgrid
