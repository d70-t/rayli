#pragma once
#include <vgrid/CartesianBase.hpp>

#include <vector>

namespace rayli::vgrid {
class EquidistantCartesian : public CartesianBase<EquidistantCartesian> {
   public:
    using RayPath = typename CartesianBase<EquidistantCartesian>::RayPath;

    /**
     *
     * \param number of grid boxes per dimension
     * \param d size of each grid box
     * \param o origin of the (0, 0, 0) gridbox)
     */
    EquidistantCartesian(std::array<size_t, 3> shape = {1, 1, 1},
                         Eigen::Vector3d d = {1, 1, 1},
                         Eigen::Vector3d o = {0, 0, 0})
        : CartesianBase<EquidistantCartesian>(
            {o, o + Eigen::Vector3d{d[0] * shape[0], d[1] * shape[1],
                                    d[2] * shape[2]}},
            shape),
          d(d), o(o) {}

    inline double gx(size_t ix) const { return o(0) + ix * d(0); }
    inline double gy(size_t iy) const { return o(1) + iy * d(1); }
    inline double gz(size_t iz) const { return o(2) + iz * d(2); }

    std::array<size_t, 3>
    find_grid_coordinates(const Eigen::Vector3d& x,
                          const Eigen::Vector3d& direction) const;

   private:
    Eigen::Vector3d d, o;
};
}  // namespace rayli::vgrid
