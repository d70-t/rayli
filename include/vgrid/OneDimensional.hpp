#pragma once
#include <vgrid/CartesianBase.hpp>

#include <vector>

namespace rayli::vgrid {
class OneDimensional : public CartesianBase<OneDimensional> {
   public:
    using RayPath = typename CartesianBase<OneDimensional>::RayPath;

    /**
     *
     * \param levels levels from grid base to grid top
     */
    OneDimensional(std::vector<double> levels)
        : CartesianBase<OneDimensional>(
            {{-std::numeric_limits<double>::infinity(),
              -std::numeric_limits<double>::infinity(), levels.front()},
             {std::numeric_limits<double>::infinity(),
              std::numeric_limits<double>::infinity(), levels.back()}},
            {1, 1, levels.size() - 1}),
          levels(levels) {}

    inline double gx(size_t ix) const {
        if (ix == 0) {
            return -std::numeric_limits<double>::infinity();
        } else {
            return std::numeric_limits<double>::infinity();
        }
    }
    inline double gy(size_t iy) const {
        if (iy == 0) {
            return -std::numeric_limits<double>::infinity();
        } else {
            return std::numeric_limits<double>::infinity();
        }
    }
    inline double gz(size_t iz) const { return levels[iz]; }

    std::array<size_t, 3>
    find_grid_coordinates(const Eigen::Vector3d& x,
                          const Eigen::Vector3d& direction) const;

   private:
    std::vector<double> levels;
};
}  // namespace rayli::vgrid
