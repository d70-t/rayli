/** \brief A vgrid describes the geometric outline of some structured 3D
 * dataset.
 *
 * Generally, a grid must provide an interator interface to support walking
 * through it along a straight line.
 *
 * The grid must support a `walk_along(const Ray &ray, double tnear, double
 * tfar)` method.
 */
namespace rayli::vgrid {}
