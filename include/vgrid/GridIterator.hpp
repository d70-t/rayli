#pragma once
#include "Ray.hpp"
#include "SurfaceGeometry.hpp"
#include "overloaded.hpp"

#include <cstddef>
#include <functional>
#include <variant>

struct VolumeSlice {
    double tnear;
    double tfar;
    size_t idx;

    VolumeSlice() = default;

    inline VolumeSlice(double tnear, double tfar, size_t idx)
        : tnear(tnear), tfar(tfar), idx(idx) {
        assert(tnear <= tfar);
    }

    inline bool operator==(const VolumeSlice& other) const {
        return tnear == other.tnear && tfar == other.tfar && idx == other.idx;
    }

    inline bool operator!=(const VolumeSlice& other) const {
        return !((*this) == other);
    }

    inline VolumeSlice reindex(size_t new_index) const {
        return {tnear, tfar, new_index};
    }
};

class SurfaceSlice {
   public:
    double t;
    size_t idx;

    SurfaceSlice(
      double t, size_t idx,
      std::function<rayli::SurfaceGeometry(const Ray&, const SurfaceSlice&)>
        geometry_f)
        : t(t), idx(idx), geometry_f(geometry_f) {}

    inline bool operator==(const SurfaceSlice& other) const {
        return t == other.t && idx == other.idx;
    }

    inline bool operator!=(const SurfaceSlice& other) const {
        return !((*this) == other);
    }

    inline rayli::SurfaceGeometry compute_geometry(const Ray& ray) const {
        return geometry_f(ray, *this);
    }

    inline SurfaceSlice reindex(size_t new_index) const {
        return {t, new_index, geometry_f};
    }

   private:
    std::function<rayli::SurfaceGeometry(const Ray&, const SurfaceSlice&)>
      geometry_f;
};

class Teleport {
   public:
    double t;
    Ray r;
};

using LinearSliceTeleport = std::variant<VolumeSlice, SurfaceSlice, Teleport>;
using LinearSlice = std::variant<VolumeSlice, SurfaceSlice>;

template<typename Slice>
double slice_tnear(const Slice& slice) {
    return std::visit(overloaded{[](const VolumeSlice& s) { return s.tnear; },
                                 [](const SurfaceSlice& s) { return s.t; },
                                 [](const Teleport& s) { return s.t; }},
                      slice);
}

template<typename Slice>
double slice_tfar(const Slice& slice) {
    return std::visit(overloaded{[](const VolumeSlice& s) { return s.tfar; },
                                 [](const SurfaceSlice& s) { return s.t; },
                                 [](const Teleport& s) { return s.t; }},
                      slice);
}

template <class... Args>
struct variant_cast_proxy
{
    std::variant<Args...> v;

    template <class... ToArgs>
    operator std::variant<ToArgs...>() const
    {
        return std::visit([](auto&& arg) -> std::variant<ToArgs...> { return arg ; },
                          v);
    }
};

template <class... Args>
auto variant_cast(const std::variant<Args...>& v) -> variant_cast_proxy<Args...>
{
    return {v};
}

template <class... Args>
auto variant_cast(std::variant<Args...>&& v) -> variant_cast_proxy<Args...>
{
    return {std::forward<std::variant<Args...>>(v)};
}

template <class T>
auto variant_cast(T&& v) -> variant_cast_proxy<typename std::remove_cv<typename std::remove_reference<T>::type>::type>
{
    return {std::forward<T>(v)};
}

template <typename H, typename... Es>
auto handle(H&& handler, std::variant<Es...>&& event) {
    return std::visit(std::forward<H>(handler), std::forward<std::variant<Es...>>(event));
}

template <typename H, typename... Es>
auto handle(H&& handler, const std::variant<Es...>& event) {
    return std::visit(std::forward<H>(handler), event);
}

template <typename H, typename... Es>
auto handle(H&& handler, std::variant<Es...>& event) {
    return std::visit(std::forward<H>(handler), event);
}

template <typename H, typename E>
auto handle(H&& handler, E&& event) {
    return std::forward<H>(handler)(std::forward<E>(event));
}
