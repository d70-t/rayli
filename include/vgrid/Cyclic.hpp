#pragma once

#include <GeometryUtils.hpp>
#include <Ray.hpp>
#include <vgrid/GridIterator.hpp>

namespace rayli::vgrid {

template <typename G>
class Cyclic {
   public:
    typedef Cyclic<G> Self;
    struct RayPath;
    class RayIterator;

    Cyclic(const G& base, const Bounds3d bounds) : base(base), bounds(bounds) {}

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar,
                              const RayIterator& last_iterator = {}) const {
        return RayPath{*this, ray, tnear, tfar, last_iterator};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSliceTeleport value_type;
        typedef LinearSliceTeleport* pointer;
        typedef value_type reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid, Ray ray, double tnear_, double tfar_,
                    const RayIterator& last_iterator)
            : grid(&grid) {

            invDir = 1. / ray.d.array();

            current_sheet = get_sheet(ray(tnear_), ray.d);
            Eigen::Vector3d offset = offset_to_base_sheet(current_sheet);

            // local ray will be inside the cyclic domain starting
            // where the tracing of the original ray would have begun
            local_ray = Ray{ray(tnear_) + offset, ray.d};
            teleport = {tnear_, local_ray};
            tnear = 0;
            tfar = tfar_ - tnear_;
        }

        RayIterator() = default;

        reference operator*() const {
            assert(is_valid());
            if(teleport) return *teleport;
            return variant_cast(*baseit);
        }

        inline bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        inline bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        inline RayIterator& operator++() {
            if(teleport) {
                teleport = std::nullopt;
                init_subgrid();
            } else {
                ++baseit;
                if(!baseit.is_valid()) {
                    do_teleport();
                }
            }
            return *this;
        }

        inline RayIterator operator++(int) {
            RayIterator out = *this;
            ++(*this);
            return out;
        }

        inline bool is_valid() const { return grid; }

       private:
        const Self* grid;
        Eigen::Array3d invDir;
        Ray local_ray;
        std::array<int64_t, 3> current_sheet;
        double tnear, tfar, tnext;
        int next_face;
        std::optional<Teleport> teleport = std::nullopt;
        size_t consecutive_teleports = 0;

        typename G::RayIterator baseit;

        void do_teleport() {
            assert(is_valid());
            if(!std::isfinite(tnext)) {
              grid = nullptr;
              return;
            }
            local_ray.o = local_ray(tnext); // translate point to edge of domain
            auto dx = grid->bounds.upper[next_face] - grid->bounds.lower[next_face];
            if (local_ray.d(next_face) < 0) {
                --current_sheet[next_face];
                local_ray.o[next_face] += dx;
            } else {
                ++current_sheet[next_face];
                local_ray.o[next_face] -= dx;
            }

            teleport = {tnext, local_ray};

            tfar -= tnext;
            tnear = 0;
        }

        std::array<int64_t, 3> get_sheet(const Eigen::Vector3d& p,
                                         const Eigen::Vector3d& d) const {
            std::array<int64_t, 3> res;
            for (int i = 0; i < 3; ++i) {
                auto lo = grid->bounds.lower(i);
                auto hi = grid->bounds.upper(i);
                if (std::isinf(lo) || std::isinf(hi)) {
                    res[i] = 0;
                } else {
                    res[i] = std::floor((p(i) - lo) / (hi - lo));
                    if (d(i) < 0 && res[i] * (hi - lo) + lo == p(i)) {
                        --res[i];
                    }
                }
            }

            return res;
        }

        Eigen::Vector3d
        offset_to_base_sheet(const std::array<int64_t, 3>& sheet) const {
            Eigen::Vector3d res;
            for (int i = 0; i < 3; ++i) {
                auto lo = grid->bounds.lower(i);
                auto hi = grid->bounds.upper(i);
                if (std::isinf(lo) || std::isinf(hi)) {
                    res[i] = 0;
                } else {
                    res[i] = sheet[i] * (lo - hi);
                }
            }
            return res;
        }

        void init_subgrid() {
            // TODO: maybe only one bound is necessary, which can be precomputed
            Eigen::Array3d t0 =
              (grid->bounds.lower - local_ray.o).array() * invDir;
            Eigen::Array3d t1 =
              (grid->bounds.upper - local_ray.o).array() * invDir;
            // Eigen::Vector3d tmin = t0.min(t1);
            Eigen::Array3d tmax = t0.max(t1);
            next_face = argmin3(tmax[0], tmax[1], tmax[2]);
            tnext = std::min(tmax[next_face], tfar);
            if (std::isnan(tnext)) {
                grid = nullptr;
                return;
            }

            auto path = grid->base.walk_along(local_ray, tnear, tnext, baseit);
            baseit = path.begin();
            if (!baseit.is_valid()) {

                { // we are outside of base grid and fly outwards,
                  // i.e. cycling does not make sense because we will never end up hitting anything
                  auto rayloc = local_ray(tnear);
                  for (size_t i=0; i<3; ++i) {
                    auto lo = grid->bounds.lower(i);
                    auto hi = grid->bounds.upper(i);
                    if (std::isinf(lo) || std::isinf(hi)) {
                        auto inner_lo = grid->base.bounds.lower(i);
                        auto inner_hi = grid->base.bounds.upper(i);
                        if (rayloc[i] > inner_hi && local_ray.d[i] >= 0) {
                          grid = nullptr;
                          return;
                        }
                        if (rayloc[i] < inner_lo && local_ray.d[i] <= 0) {
                          grid = nullptr;
                          return;
                        }
                    }
                  }
                }
                if (consecutive_teleports < 1000) {
                    do_teleport();
                    ++consecutive_teleports;
                } else {
                    grid = nullptr;
                }
            } else {
              consecutive_teleports = 0;
            }
        }
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSliceTeleport value_type;
        typedef LinearSliceTeleport& reference;
        typedef const LinearSliceTeleport& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;
        RayIterator last_iterator;

        inline RayIterator begin() const {
            return {grid, ray, tnear, tfar, last_iterator};
        }

        inline RayIterator end() const { return RayIterator(); }
    };

   private:
    const G& base;
    Bounds3d bounds;
};
}  // namespace rayli::vgrid
