#pragma once

#include <Ray.hpp>
#include <vgrid/GridIterator.hpp>

namespace rayli::vgrid {
/** \brief axis aligned surface
 */
class TestboxVGrid {
   public:
    typedef TestboxVGrid Self;
    struct RayPath;

    TestboxVGrid(Eigen::Vector3d corner, Eigen::Vector3d extent,
                 std::array<size_t, 3> box_count)
        : corner(corner), extent(extent), box_count(box_count) {}

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{*this, ray, tnear, tfar};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef SurfaceSlice value_type;
        typedef value_type* pointer;
        typedef value_type& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid, Ray ray, double tnear, double tfar);

        inline RayIterator() : t(std::numeric_limits<double>::quiet_NaN()) {}

        const value_type operator*() const;

        inline bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        inline bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        inline RayIterator& operator++() {
            t = std::numeric_limits<double>::quiet_NaN();
            return *this;
        }

        inline bool is_valid() const { return std::isfinite(t); }

       private:
        double t;
        size_t surface_id = std::numeric_limits<size_t>::max();
        size_t axis = 0;
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef SurfaceSlice value_type;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar);
        }

        inline RayIterator end() const { return RayIterator(); }
    };

   private:
    Eigen::Vector3d corner, extent;
    std::array<size_t, 3> box_count;
};
}  // namespace rayli::vgrid
