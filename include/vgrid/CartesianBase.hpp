#pragma once

#include <GeometryUtils.hpp>
#include <Ray.hpp>
#include <utils.hpp>
#include <vgrid/GridIterator.hpp>

#include <array>

namespace rayli::vgrid {
template <typename Derived>
class CartesianBase {
   public:
    struct RayPath;
    class RayIterator;

    CartesianBase(Bounds3d bounds, std::array<size_t, 3> shape)
        : _bounds(bounds), _shape(shape) {}

    RayPath walk_along(const Ray& ray, double tnear, double tfar,
                       const RayIterator& = {}) const {
        // TODO: initial RayIterator should be passed to RayPath for faster
        // continuation
        return RayPath{cast(), ray, tnear, tfar};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Derived& grid, Ray ray, double tnear, double tfar)
            : grid(&grid), ray(ray), tnear(tnear), tfar(tfar) {
            if (!initialize_start_position()) return;
            calculate_next_position();
        }
        RayIterator() {}

        reference operator*() const {
            return VolumeSlice{tnear, tfar_next, current_volume_id()};
        }
        inline bool operator!=(const RayIterator& other) const {
            return valid || other.valid;
        }
        inline bool operator==(const RayIterator& other) const {
            return !operator!=(other);
        }
        RayIterator& operator++() {
            if (advance_to_next()) {
                calculate_next_position();
            }
            return *this;
        }
        RayIterator operator++(int) {
            RayIterator out = *this;
            ++(*this);
            return out;
        }

        inline bool is_valid() const { return valid; }

       private:
        const Derived* grid;
        Ray ray;
        double tnear, tfar;
        bool valid = false;

        std::array<size_t, 3> ipos, ipos_next;
        double tfar_next;
        std::array<double, 3> tfar_next_per_axis;

        bool initialize_start_position();
        bool calculate_next_position();
        bool advance_to_next();
        size_t current_volume_id() const;
    };
    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Derived& grid;
        Ray ray;
        double tnear, tfar;
        std::optional<size_t> last_volume_id;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar);
        }

        inline RayIterator end() const { return RayIterator(); }
    };

    const Bounds3d& bounds() const { return _bounds; }
    const std::array<size_t, 3> shape() const { return _shape; }

    double gpos(size_t i, int axis) const {
        if (axis == 0) return cast().gx(i);
        if (axis == 1) return cast().gy(i);
        if (axis == 2) return cast().gz(i);
        return std::numeric_limits<double>::quiet_NaN();
    }

   private:
    Bounds3d _bounds;
    std::array<size_t, 3> _shape;

    Derived& cast() { return *static_cast<Derived*>(this); }
    const Derived& cast() const { return *static_cast<const Derived*>(this); }
};

template <typename Derived>
bool CartesianBase<Derived>::RayIterator::initialize_start_position() {
    if (!grid->bounds().intersect(ray, &tnear, &tfar)) {
        return false;
    }
    if (tnear > tfar) {
        return false;
    }
    Eigen::Vector3d invDir(1. / ray.d(0), 1. / ray.d(1), 1. / ray.d(2));

    ipos = grid->find_grid_coordinates(ray(tnear), ray.d);
    for (int i = 0; i < 3; ++i) {
        if (ipos[i] >= grid->shape()[i]) {
            ipos[i] = grid->shape()[i] - 1;
        }
    }
    return true;
}

template <typename Derived>
bool CartesianBase<Derived>::RayIterator::calculate_next_position() {
    auto [ix, iy, iz] = ipos;
    if (!(ix < grid->shape()[0] && iy < grid->shape()[1] &&
          iz < grid->shape()[2])) {
        valid = false;
        return false;
    }

    ipos_next = {ix + sgn(ray.d(0)), iy + sgn(ray.d(1)), iz + sgn(ray.d(2))};

    for (int i = 0; i < 3; ++i) {
        if (ipos[i] != ipos_next[i] && ray.d(i) != 0) {
            // they are equal if ray is parallel and
            // will not cross any border in this dimension
            auto iborder_next = ipos_next[i];
            if (ray.d(i) < 0) {
                iborder_next += 1;
            }
            assert(iborder_next >= 0 && iborder_next <= grid->shape()[i]);
            tfar_next_per_axis[i] =
              (grid->gpos(iborder_next, i) - ray.o(i)) / ray.d(i);
        } else {
            tfar_next_per_axis[i] = std::numeric_limits<double>::infinity();
        }
    }
    tfar_next =
      *std::min_element(tfar_next_per_axis.begin(), tfar_next_per_axis.end());
    if (tfar_next >= tfar) {
        tfar_next = tfar;
    }
    // assert(tfar_next > tnear);
    if (tfar_next > tnear) {
        valid = true;
    } else {
        valid = false;
    }
    return valid;
}
template <typename Derived>
bool CartesianBase<Derived>::RayIterator::advance_to_next() {
    if (tfar_next >= tfar) {
        valid = false;
        return false;
    }
    for (int i = 0; i < 3; ++i) {
        // in some cases, a ray might cross in a corner of the box.
        // In this case, more than one ipos is incremented.
        if (tfar_next_per_axis[i] == tfar_next) {
            ipos[i] = ipos_next[i];
        }
    }
    tnear = tfar_next;
    return true;
}

template <typename Derived>
size_t
CartesianBase<Derived>::RayIterator::RayIterator::current_volume_id() const {
    return (ipos[0] * grid->shape()[1] + ipos[1]) * grid->shape()[2] + ipos[2];
}

}  // namespace rayli::vgrid
