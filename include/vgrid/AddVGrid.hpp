#pragma once

#include <optional>
#include <utility>
#include <variant>

#include <overloaded.hpp>
#include <vgrid/GridIterator.hpp>

namespace rayli::vgrid {

// TODO: general indexers need templatized LinearSlice
template <typename IA, typename IB>
struct VariantSurfaceIndexer {
    using a_type = IA;
    using b_type = IB;
    using value_type = std::variant<IA, IB>;

    value_type surface_a(IA&& ia) const {
        return {std::in_place_index<0>, std::forward<IA>(ia)};
    }
    value_type surface_b(IB&& ib) const {
        return {std::in_place_index<1>, std::forward<IB>(ib)};
    }
};

template <typename IA, typename IB>
struct PairOptionalVolumeIndexer {
    using a_type = IA;
    using b_type = IB;
    using value_type = std::pair<std::optional<IA>, std::optional<IB>>;

    value_type volume_a(IA&& ia) const {
        return {{std::forward<IA>(ia)}, std::nullopt};
    }
    value_type volume_b(IB&& ib) const {
        return {std::nullopt, {std::forward<IB>(ib)}};
    }
    value_type volume_ab(IA&& ia, IB&& ib) const {
        return {{std::forward<IA>(ia)}, {std::forward<IB>(ib)}};
    }
};

/** \brief adds two vgrid into a combined vgrid
 *
 * \tparam A first vgrid
 * \tparam B second vgrid
 * \tparam SurfaceIndexer indexer for combining surface indices
 * \tparam VolumeIndexer indexer for combining volume indices
 */
template <typename A, typename B, typename SurfaceIndexer,
          typename VolumeIndexer>
class AddVGrid {
   public:
    typedef AddVGrid<A, B, SurfaceIndexer, VolumeIndexer> Self;
    struct RayPath;

    AddVGrid(const A& a, const B& b, SurfaceIndexer&& si, VolumeIndexer&& vi)
        : a(a), b(b), si(std::forward<SurfaceIndexer>(si)),
          vi(std::forward<VolumeIndexer>(vi)) {}

    RayPath walk_along(const Ray& ray, double tnear, double tfar) const {
        return RayPath{a.walk_along(ray, tnear, tfar),
                       b.walk_along(ray, tnear, tfar), this};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef LinearSlice* pointer;
        typedef LinearSlice& reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(typename A::RayIterator ait_, typename B::RayIterator bit_,
                    const Self* grid_)
            : ait(ait_), bit(bit_), grid(grid_) {
            ++(*this);
        }

        const LinearSlice& operator*() const { return *current; }

        bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        RayIterator& operator++() {
            auto a = pop_a();
            auto b = pop_b();
            if (!a && !b) {
                current = std::nullopt;
            } else if (!a) {
                current = reindex_b(*b);
            } else if (!b) {
                current = reindex_a(*a);
            } else {
                current = merge(*a, *b);
            }
            return *this;
        }

        bool is_valid() const { return bool(current); }

       private:
        std::optional<LinearSlice> pop_a() {
            std::optional<LinearSlice> ret = std::nullopt;
            if (anext) {
                ret.swap(anext);
            } else if (ait.is_valid()) {
                ret = *ait;
                ++ait;
            }
            return ret;
        }

        std::optional<LinearSlice> pop_b() {
            std::optional<LinearSlice> ret = std::nullopt;
            if (bnext) {
                ret.swap(bnext);
            } else if (bit.is_valid()) {
                ret = *bit;
                ++bit;
            }
            return ret;
        }

        LinearSlice merge(const LinearSlice& a, const LinearSlice& b) {
            return std::visit(
              overloaded{
                [this](const SurfaceSlice& sa,
                       const SurfaceSlice& sb) -> LinearSlice {
                    if (sa.t < sb.t) {
                        bnext = sb;
                        return reindex_a(sa);
                    } else {
                        anext = sa;
                        return reindex_b(sb);
                    }
                },
                [this](const VolumeSlice& va,
                       const SurfaceSlice& sb) -> LinearSlice {
                    if (va.tfar < sb.t) {
                        bnext = sb;
                        return reindex_a(va);
                    } else if (va.tnear < sb.t) {
                        anext = VolumeSlice{sb.t, va.tfar, va.idx};
                        bnext = sb;
                        return reindex_a(VolumeSlice{va.tnear, sb.t, va.idx});
                    } else {
                        anext = va;
                        return reindex_b(sb);
                    }
                },
                [this](const SurfaceSlice& sa,
                       const VolumeSlice& vb) -> LinearSlice {
                    if (vb.tfar < sa.t) {
                        anext = sa;
                        return reindex_b(vb);
                    } else if (vb.tnear < sa.t) {
                        bnext = VolumeSlice{sa.t, vb.tfar, vb.idx};
                        anext = sa;
                        return reindex_b(VolumeSlice{vb.tnear, sa.t, vb.idx});
                    } else {
                        bnext = vb;
                        return reindex_a(sa);
                    }
                },
                [this](const VolumeSlice& va,
                       const VolumeSlice& vb) -> LinearSlice {
                    if (va.tfar <= vb.tnear) {
                        bnext = vb;
                        return reindex_a(va);
                    } else if (vb.tfar <= va.tnear) {
                        anext = va;
                        return reindex_b(vb);
                    } else if (va.tnear < vb.tnear) {
                        anext = VolumeSlice{vb.tnear, va.tfar, va.idx};
                        bnext = vb;
                        return reindex_a(va);
                    } else if (vb.tnear < va.tnear) {
                        bnext = VolumeSlice{va.tnear, vb.tfar, vb.idx};
                        anext = va;
                        return reindex_b(vb);
                    } else if (va.tfar < vb.tfar) {
                        bnext = VolumeSlice{va.tfar, vb.tfar, vb.idx};
                        return VolumeSlice{va.tnear, va.tfar,
                                           index_ab(va.idx, vb.idx)};
                    } else if (vb.tfar < va.tfar) {
                        anext = VolumeSlice{vb.tfar, va.tfar, va.idx};
                        return VolumeSlice{vb.tnear, vb.tfar,
                                           index_ab(va.idx, vb.idx)};
                    } else {
                        return VolumeSlice{va.tnear, va.tfar,
                                           index_ab(va.idx, vb.idx)};
                    }
                }},
              a, b);
        }

        auto index_ab(const typename VolumeIndexer::a_type& ia,
                      const typename VolumeIndexer::b_type& ib) {
            return grid->vi.volume_ab(ia, ib);
        }

        VolumeSlice reindex_a(const VolumeSlice& va) {
            return va.reindex(grid->vi.volume_a(va.idx));
        }

        VolumeSlice reindex_b(const VolumeSlice& vb) {
            return vb.reindex(grid->vi.volume_b(vb.idx));
        }

        SurfaceSlice reindex_a(const SurfaceSlice& sa) {
            return sa.reindex(grid->si.surface_a(sa.idx));
        }

        SurfaceSlice reindex_b(const SurfaceSlice& sb) {
            return sb.reindex(grid->si.surface_b(sb.idx));
        }

        LinearSlice reindex_a(const LinearSlice& la) {
            return std::visit(
              overloaded{[this](const VolumeSlice& va) -> LinearSlice {
                             return this->reindex_a(va);
                         },
                         [this](const SurfaceSlice& sa) -> LinearSlice {
                             return this->reindex_a(sa);
                         }},
              la);
        }

        LinearSlice reindex_b(const LinearSlice& lb) {
            return std::visit(
              overloaded{[this](const VolumeSlice& vb) -> LinearSlice {
                             return this->reindex_b(vb);
                         },
                         [this](const SurfaceSlice& sb) -> LinearSlice {
                             return this->reindex_b(sb);
                         }},
              lb);
        }

        typename A::RayIterator ait;
        typename B::RayIterator bit;
        const Self* grid;
        std::optional<LinearSlice> current;
        std::optional<LinearSlice> anext;
        std::optional<LinearSlice> bnext;
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef LinearSlice& reference;
        typedef const LinearSlice& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        typename A::RayPath apath;
        typename B::RayPath bpath;
        const Self* grid;

        inline RayIterator begin() const {
            return RayIterator(apath.begin(), bpath.begin(), grid);
        }

        inline RayIterator end() const {
            return RayIterator(apath.end(), bpath.end(), grid);
        }
    };

   private:
    const A& a;
    const B& b;
    SurfaceIndexer si;
    VolumeIndexer vi;
};
}  // namespace rayli::vgrid
