#include <cstddef>
#include <vgrid/KDTree.hpp>

#include <logging.hpp>

namespace rayli::vgrid::util {
/** \brief builds a kd tree using SAH cost model
 *
 * The SAH (%Surface Area Heuristic) cost model tries to estimate how much cost
 * will be involved by tracing through the constructed kd tree at each potential
 * split. It will further generate the tree with the least tracing cost.
 *
 * The cost for checking intersections in an existing node is given by:
 * \f[
 * t_{\rm isect} N
 * \f]
 * where \f$N\f$ is the number of elements in the node and \f$t_{\rm isect}\f$
 * is the cost for an intersection computation (`intersection_cost`).
 *
 * The cost for checking intersections after a node split is estimated as:
 * \f[
 * t_{\rm trav} + (1 - b_e) (p_A N_A t_{\rm isect} + p_B N_B t_{\rm isect})
 * \f]
 * where \f$t_{\rm trav}\f$ is the cost of traversing a splitted node
 * (`traversal_cost`), \f$b_e\f$ is a bonus for empty nodes,
 * which can be traversed very fast (`empty_bonus`),
 * \f$p_A\f$ and \f$p_B\f$ are the probabilities that a ray hitting the outer
 * node will also hit the subnode \f$A\f$ or \f$B\f$ respectively. These
 * probabilities are estimated by the ratio of surface areas:
 * \f[
 * p_i = \frac{{\rm Surface}({\rm node}~i)}{{\rm Surface}({\rm outer~node})}
 * \f]
 *
 * \f$N_A\f$ and \f$N_B\f$ are the number of elements contained in each
 * subnodes.
 */
class RecursiveSAHTreeBuilder {
   public:
    typedef rayli::vgrid::KdTree<size_t> Tree;

    RecursiveSAHTreeBuilder(Tree& tree,
                            const std::vector<Bounds3d>& element_bounds,
                            std::vector<std::vector<size_t>>& element_ids,
                            size_t target_elements_per_node = 8,
                            double intersection_cost = 80,
                            double traversal_cost = 1,
                            double empty_bonus = 0.1);
    size_t build(int max_depth = -1);

   private:
    size_t init_leaf(std::vector<size_t>&& elements);
    size_t init_leaf(size_t* elements, size_t element_count);

    Tree& tree;
    const std::vector<Bounds3d>& element_bounds;
    std::vector<std::vector<size_t>>& element_ids;
    size_t target_elements_per_node;
    double intersection_cost, traversal_cost, empty_bonus;
    rayli::logging::logger_ptr logger;

    // working memory
    std::array<std::vector<BoundEdged>, 3> bound_edges;

    friend class _RecursiveSAHTreeBuilder;
};
}  // namespace rayli::vgrid::util
