#pragma once

#include <Eigen/Dense>
#include <GeometryUtils.hpp>
#include <Ray.hpp>
#include <accelerator/kdaccel.hpp>
#include <vgrid/GridIterator.hpp>

#include <cmath>
#include <cstddef>
#include <vector>

namespace rayli::vgrid {

template <typename T>
class IndexWithBool {
   public:
    static_assert(std::numeric_limits<T>::radix == 2);
    constexpr const static T bool_mask =
      (T(1) << (std::numeric_limits<T>::digits - 1));
    constexpr const static T value_mask = bool_mask - 1;
    IndexWithBool() : IndexWithBool(std::numeric_limits<T>::max(), true) {}
    inline IndexWithBool(T v, bool b) {
        storage = (v & value_mask) | (b ? bool_mask : 0);
    }
    inline T v() const { return storage & value_mask; }
    inline bool b() const { return storage & bool_mask; }

   private:
    T storage;
};

struct SidedIntersectResult {
    double t;
    bool along_normal;
};

class ConvexPolytopes {
   public:
    typedef ConvexPolytopes Self;
    struct RayPath;
    class RayIterator;

    struct Plane {
        Eigen::Vector3d n;
        double d;

        inline SidedIntersectResult intersect(const Ray& r) const {
            SidedIntersectResult res = {std::numeric_limits<double>::infinity(),
                                        true};
            double ofs = n.dot(r.d);
            if (ofs == 0) {
                return res;
            }
            res.t = distance(r.o) / ofs;
            res.along_normal = ofs > 0;
            return res;
        };

        inline double distance(const Eigen::Vector3d& p) const {
            return d - n.dot(p);
        };
    };

    struct PolytopeIntersectionResult {
        IndexWithBool<size_t> plane1 = {};
        IndexWithBool<size_t> plane2 = {};
        double t1 = -std::numeric_limits<double>::infinity();
        double t2 = std::numeric_limits<double>::infinity();
    };

    struct PolytopeIntersectionResultSingle {
        IndexWithBool<size_t> plane = {};
        double t = std::numeric_limits<double>::infinity();
    };

    struct PolytopeSearchResult {
        size_t polytope = std::numeric_limits<size_t>::max();
        PolytopeIntersectionResult intersection;
    };

    class Polytope {
       public:
        Polytope(const ConvexPolytopes& cp, size_t idx) : cp(cp), idx(idx) {}

        inline size_t first_plane_idx() const {
            assert(cp.polytope_plane_indices.size() > idx + 1);
            return cp.polytope_plane_indices[idx];
        }

        inline size_t last_plane_idx() const {
            assert(cp.polytope_plane_indices.size() > idx + 1);
            return cp.polytope_plane_indices[idx + 1];
        }

        inline size_t face_count() const {
            return last_plane_idx() - first_plane_idx();
        }

        inline const auto& plane_of_face(size_t face_index) const {
            return cp.planes_of_polytopes[first_plane_idx() + face_index];
        }

        inline const auto& get_plane(size_t face_index) const {
            return cp.planes[plane_of_face(face_index).v()];
        }

        inline size_t first_vertex_idx() const {
            assert(cp.polytope_vertex_indices.size() > idx + 1);
            return cp.polytope_vertex_indices[idx];
        }

        inline size_t last_vertex_idx() const {
            assert(cp.polytope_vertex_indices.size() > idx + 1);
            return cp.polytope_vertex_indices[idx + 1];
        }

        inline size_t vertex_count() const {
            return last_vertex_idx() - first_vertex_idx();
        }

        inline const auto& get_vertex(size_t vertex_index) const {
            return cp.vertices[cp.vertices_of_polytopes[first_vertex_idx() +
                                                        vertex_index]];
        }

        inline double distance_to_plane(IndexWithBool<size_t> plane_index,
                                        const Eigen::Vector3d& p) const {
            return cp.planes[plane_index.v()].distance(p) *
                   (plane_index.b() ? 1 : -1);
        }

        inline double distance_to_face(size_t face_index,
                                       const Eigen::Vector3d& p) const {
            const auto& plane_index = plane_of_face(face_index);
            return distance_to_plane(plane_index, p);
        }

        inline SidedIntersectResult
        intersect_plane(IndexWithBool<size_t> plane_index, const Ray& r) const {
            return cp.planes[plane_index.v()].intersect(r);
        }

        inline SidedIntersectResult intersect_face(size_t face_index,
                                                   const Ray& r) const {
            const auto& plane_index = plane_of_face(face_index);
            return intersect_plane(plane_index, r);
        }

        bool includes(const Eigen::Vector3d& p) const;
        PolytopeIntersectionResult intersect(const Ray& r) const;
        PolytopeIntersectionResultSingle intersect2(const Ray& r) const;
        Bounds3d get_bounds() const;

       private:
        const ConvexPolytopes& cp;
        size_t idx;
    };

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar,
                              const RayIterator& last_iterator = {}) const {
        return RayPath{*this, ray, tnear, tfar, last_iterator};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef LinearSlice value_type;
        typedef value_type* pointer;
        typedef value_type reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid_, Ray ray_, double tnear_, double tfar_,
                    const RayIterator& last_iterator);

        inline RayIterator() { invalidate(); }

        reference operator*() const;

        inline bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        inline bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        inline RayIterator& operator++() {
            advance();
            return *this;
        }

        inline RayIterator operator++(int) {
            RayIterator out = *this;
            advance();
            return out;
        }

        inline bool is_valid() const { return state != State::EXIT; }

       private:
        enum class State { ENTER, ON_FACE, IN_VOLUME, EXIT };
        const Self* grid = nullptr;
        Ray ray;

        double tnear = std::numeric_limits<double>::signaling_NaN();
        double tfar = std::numeric_limits<double>::signaling_NaN();

        State state;
        PolytopeSearchResult current_polytope;
        size_t last_polytope_id = std::numeric_limits<size_t>::max();

        void initialize();
        void initialize_from_other(const RayIterator& other);
        void advance();
        inline void invalidate() { state = State::EXIT; }
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef LinearSlice value_type;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;
        RayIterator last_iterator;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar, last_iterator);
        }

        inline RayIterator end() const { return RayIterator(); }
    };

    inline size_t polytope_count() const {
        return polytope_plane_indices.size() - 1;
    }

    inline Polytope get_polytope(size_t i) const { return {*this, i}; }
    PolytopeSearchResult find_first_polytope(
      const Ray& r, double tnear = 0,
      double tfar = std::numeric_limits<double>::infinity(),
      size_t exclude_polytope = std::numeric_limits<size_t>::max()) const;

    void repair_polytopes_of_planes();
    void repair_bounds();

    /** \brief builds acceleration structures for faster lookups
     *
     * \note changing the grid invalidates acceleration structures. If grid is
     * changed without rebuilding acceleration structures afterwards, bad things
     * can happen!
     */
    void build_accelerators();

    std::vector<Plane> planes;
    std::vector<Eigen::Vector3d> face_dpdu;
    std::vector<Eigen::Vector3d> vertices;
    // Face normals point outwards, if plane normal points inwards, b() of the
    // index will be false
    std::vector<IndexWithBool<size_t>> planes_of_polytopes;
    std::vector<size_t> vertices_of_polytopes;
    std::vector<size_t> polytope_plane_indices;
    std::vector<size_t> polytope_vertex_indices;
    // the first polytope is the one with plane normal aligned to face normal
    // (outwards), the second one is reversed
    std::vector<std::array<size_t, 2>> polytopes_of_planes;

    std::unique_ptr<KDAccelerator> polytope_finding_accel = nullptr;
    bool is_convex =
      false;  ///< If the whole grid is convex, no additional intersection test
              ///< is necessary if ray hits void area
    Bounds3d bounds;
};

/** \brief creates a polytope grid from hexahedra
 *
 * The 8 indices of the hexahedral corners must be given
 * in the following order:
 *         7----6
 *        /|   /|
 *       / |  / |
 *      4----5  |
 *      |  3-|--2
 *      | /  | /
 *      |/   |/
 *      0----1
 */
ConvexPolytopes polytopes_from_hexahedra(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 8>>& vertices_of_hexahedra);

ConvexPolytopes polytopes_from_wedges(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 6>> vertices_of_wedges);

ConvexPolytopes polytopes_from_wedges_with_facedefinitions(
  const std::vector<Eigen::Vector3d>& vertices,
  const std::vector<std::array<size_t, 5>>& faces_of_wedges,
  const std::vector<std::array<size_t, 4>>& vertices_of_faces);
}  // namespace rayli::vgrid
