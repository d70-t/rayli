#pragma once

#include <vector>

#include <Ray.hpp>
#include <GeometryUtils.hpp>
#include <vgrid/GridIterator.hpp>

namespace rayli::vgrid {
/** \brief axis aligned surface
 */
class BallVGrid {
   public:
    typedef BallVGrid Self;
    struct RayPath;
    class RayIterator;

    BallVGrid(Eigen::Vector3d center, std::vector<double> radii_)
        : center(center), radii(std::move(radii_)) {
          bounds = {center - radii.back() * Eigen::Vector3d{1,1,1}, center + radii.back() * Eigen::Vector3d{1,1,1}};
        }

    inline RayPath walk_along(const Ray& ray, double tnear, double tfar,
                              const RayIterator& = {}) const {
        return RayPath{*this, ray, tnear, tfar};
    }

    class RayIterator {
       public:
        typedef std::ptrdiff_t difference_type;
        typedef VolumeSlice value_type;
        typedef value_type* pointer;
        typedef value_type reference;
        typedef std::input_iterator_tag iterator_category;

        RayIterator(const Self& grid_, Ray ray_, double tnear_, double tfar_);

        inline RayIterator() : grid(nullptr), ray({}) {
            tnear = 0;
            tfar = 0;
            iclose = 0;
            iin = 0;
            iout = 0;
            inbound = false;
            invalidate();
        }

        reference operator*() const;

        inline bool operator!=(const RayIterator& other) const {
            return is_valid() || other.is_valid();
        }

        inline bool operator==(const RayIterator& other) const {
            return !((*this) != other);
        }

        inline RayIterator& operator++() {
            advance();
            return *this;
        }

        inline RayIterator operator++(int) {
            RayIterator out = *this;
            advance();
            return out;
        }

        inline bool is_valid() const { return std::isfinite(tnext); }

       private:
        const Self* grid;
        Ray ray;
        double tnear, tfar, tnext;
        size_t iclose, iin, iout;
        bool inbound;

        void advance();
        double intersect_t_next(size_t i);
        inline void invalidate() {
            tnext = std::numeric_limits<double>::quiet_NaN();
        }
    };

    struct RayPath {
        typedef std::ptrdiff_t difference_type;
        typedef std::size_t size_type;
        typedef VolumeSlice value_type;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef RayIterator iterator;
        typedef RayIterator const_iterator;

        const Self& grid;
        Ray ray;
        double tnear, tfar;

        inline RayIterator begin() const {
            return RayIterator(grid, ray, tnear, tfar);
        }

        inline RayIterator end() const { return RayIterator(); }
    };

    Bounds3d bounds;

   private:
    Eigen::Vector3d center;
    std::vector<double> radii;
};
}  // namespace rayli::vgrid
