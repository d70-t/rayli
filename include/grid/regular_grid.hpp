#pragma once
#include <cstddef>
#include <memory>
#include <numeric>
#include <vector>
#include <array>

namespace rayli::grid {

template <size_t N, typename A, typename B>
void copy_shuffled(A& a, const B& b) {}

template <size_t N, typename A, typename B, typename T, typename... Args>
void copy_shuffled(A& a, const B& b, T t, Args... args) {
    a[N - (sizeof...(Args) + 1)] = b[t];
    copy_shuffled<N>(a, b, args...);
}

template <size_t NDim, typename Scalar>
class RegularGrid {
   public:
    typedef RegularGrid<NDim, Scalar> This;

    RegularGrid() { data = std::make_shared<std::vector<Scalar>>(); }

    size_t shape(size_t dim) const { return sizes[dim]; }
    size_t size() const {
        size_t s = 1;
        for (auto si : sizes) {
            s *= si;
        }
        return s;
    }

    template <typename... Args>
    void resize(Args... args) {
        static_assert(sizeof...(Args) == NDim,
                      "number of sizes does not match number of dimensions");
        data->resize((1 * ... * args));
        this->update_size(args...);
    }

    template <typename... Args>
    Scalar& operator()(Args... args) {
        static_assert(sizeof...(Args) == NDim,
                      "number of indices does not match number of dimensions");
        return get(base_ofs, args...);
    }

    template <typename... Args>
    const Scalar& operator()(Args... args) const {
        return (*const_cast<This*>(this))(args...);
    }

    template <typename... Args>
    This transpose(Args... args) {
        static_assert(
          sizeof...(Args) == NDim,
          "number of arguments does not match number of dimensions");
        This out(data);
        out.base_ofs = base_ofs;
        copy_shuffled<NDim>(out.sizes, sizes, args...);
        copy_shuffled<NDim>(out.element_ofs, element_ofs, args...);
        return out;
    }

    RegularGrid<NDim - 1, Scalar> slice(size_t dim, size_t selection) {
        RegularGrid<NDim - 1, Scalar> out(data);
        size_t j = 0;
        for (size_t i = 0; i < NDim; ++i) {
            if (i != dim) {
                out.sizes[j] = sizes[i];
                out.element_ofs[j] = element_ofs[i];
                ++j;
            }
        }
        out.base_ofs = base_ofs + selection * element_ofs[dim];
        return out;
    }

    class Iterator {
       public:
        typedef ptrdiff_t difference_type;
        typedef Scalar value_type;
        typedef Scalar* pointer;
        typedef Scalar& reference;
        typedef std::input_iterator_tag iterator_category;

        Iterator(This& grid, std::array<size_t, NDim> idx)
            : grid(grid), idx(idx) {}

        reference operator*() {
            return (*grid.data)[std::inner_product(
              idx.begin(), idx.end(), grid.element_ofs, grid.base_ofs)];
        }

        Iterator& operator++() {
            for (size_t i = NDim - 1; i < NDim; --i) {
                if (++idx[i] < grid.sizes[i]) {
                    break;
                } else {
                    if (i > 0) {
                        idx[i] = 0;
                    }
                }
            }
            return *this;
        }

        Iterator operator++(int) {
            Iterator that = *this;
            ++(*this);
            return that;
        }

        bool operator==(const Iterator& other) {
            for (size_t i = 0; i < NDim; ++i) {
                if (idx[i] != other.idx[i]) {
                    return false;
                }
            }
            return true;
        }

        bool operator!=(const Iterator& other) { return !((*this) == other); }

       private:
        This& grid;
        std::array<size_t, NDim> idx;
    };

    friend class Iterator;

    Iterator begin() { return Iterator(*this, {}); }
    const Iterator begin() const { return const_cast<This*>(this)->begin(); }

    Iterator end() {
        std::array<size_t, NDim> past_the_end = {};
        past_the_end[0] = sizes[0];
        return Iterator(*this, past_the_end);
    }

    const Iterator end() const { return const_cast<This*>(this)->end(); }

   private:
    RegularGrid(std::shared_ptr<std::vector<Scalar>>& data) : data(data) {}

    template <typename T, typename... Args>
    void update_size(T size, Args... args) {
        sizes[NDim - (1 + sizeof...(Args))] = size;
        update_size(args...);
    }

    template <typename T>
    void update_size(T size) {
        sizes[NDim - 1] = size;
        calculate_offsets_from_size();
    }

    void calculate_offsets_from_size() {
        size_t ofs = 1;
        for (size_t i = (NDim - 1); i < NDim; --i) {
            element_ofs[i] = ofs;
            ofs *= shape(i);
        }
    }

    template <typename T, typename... Args>
    Scalar& get(size_t base, T i, Args... args) {
        auto dim_idx = NDim - (sizeof...(Args) + 1);
        return get(base + element_ofs[dim_idx] * i, args...);
    }
    Scalar& get(size_t base) { return (*data)[base]; }

    std::shared_ptr<std::vector<Scalar>> data;
    size_t sizes[NDim];
    size_t element_ofs[NDim];
    size_t base_ofs = 0;

    friend class RegularGrid<NDim + 1, Scalar>;
};
}  // namespace rayli::grid
