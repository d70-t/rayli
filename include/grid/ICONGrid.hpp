#pragma once

#include <algorithm>
#include <map>
#include <memory>
#include <vector>

#include "Eigen.hpp"
#include "Ray.hpp"
#include "accelerator/kdaccel.hpp"

namespace rayli::grid {

struct ICONGrid {
    struct cell_t {
        std::array<size_t, 3> vertices;
        std::array<size_t, 3> edges;
        std::array<bool, 3> edge_directions;  ///< true -> facing outwards

        bool contains_point(const ICONGrid& grid,
                            const Eigen::Vector3d& point) const {
            for (size_t i = 0; i < 3; ++i) {
                if ((grid.edge_normals[edges[i]].dot(point) *
                     (edge_directions[i] ? -1 : 1)) < 0) {
                    return false;
                }
            }
            return true;
        }
    };

    /** \brief helper class to provide a better cell API
     */
    class Cell {
       public:
        inline Cell(const ICONGrid& grid, size_t cell_id)
            : grid(grid), cell_id(cell_id) {}
        Cell& operator=(const Cell& other) {
            cell_id = other.cell_id;
            return *this;
        }
        inline bool is_valid() const { return cell_id < grid.cell_count(); };
        inline bool contains_point(const Eigen::Vector3d& point) {
            assert(is_valid());
            return grid.cells[cell_id].contains_point(grid, point);
        }

        inline const std::array<size_t, 3>& ivertices() const {
            assert(is_valid());
            return grid.cells[cell_id].vertices;
        }

        inline const std::array<size_t, 3>& iedges() const {
            assert(is_valid());
            return grid.cells[cell_id].edges;
        }

        inline const std::array<bool, 3>& edge_directions() const {
            assert(is_valid());
            return grid.cells[cell_id].edge_directions;
        }

        std::array<size_t, 3> neighbor_cells() const;

        inline size_t id() const { return cell_id; }

       private:
        const ICONGrid& grid;
        size_t cell_id;
    };

    /** \brief helper class to provide better edge API
     */
    class Edge {
       public:
        inline Edge(const ICONGrid& grid, size_t edge_id)
            : grid(grid), edge_id(edge_id) {}

        inline bool is_valid() const { return edge_id < grid.edge_count(); };
        inline const Eigen::Vector3d& normal() const {
            assert(edge_id < grid.edge_normals.size());
            return grid.edge_normals[edge_id];
        }
        inline const std::pair<size_t, size_t>& vert_indices() const {
            assert(edge_id < grid.edge_vertices.size());
            return grid.edge_vertices[edge_id];
        }

        inline const Eigen::Vector3d& a() const {
            return grid.vertices[vert_indices().first];
        }

        inline const Eigen::Vector3d& b() const {
            return grid.vertices[vert_indices().second];
        }

        inline Cell inner_cell() const {
            assert(edge_id < grid.edge_cells.size());
            return grid.get_cell(grid.edge_cells[edge_id].first);
        }

        inline Cell outer_cell() const {
            assert(edge_id < grid.edge_cells.size());
            return grid.get_cell(grid.edge_cells[edge_id].second);
        }

        inline Cell get_cell(bool inner) const {
            if (inner) {
                return inner_cell();
            } else {
                return outer_cell();
            }
        }

        inline size_t id() const { return edge_id; }

        /** \brief checks for ray-edgeplane intersection
         *
         * \note This method only checks for an intersection with the plane,
         *       no check for triangle bounds is performed.
         */
        bool intersect_plane(const Ray& ray, double tnear, double tfar,
                             double& t) const;

        /** \brief checks for ray-edge intersection
         */
        bool intersect(const Ray& ray, double tnear, double tfar,
                       double& t) const;

        /** \brief calculate the 3d bounds of the edge face
         */
        Bounds3d get_bounds() const;

        /** \brief checks if given point is between the two vertices
         *
         * \param p point which must be on the plane between the
         *          two edge vertices.
         */
        bool is_between_vertices(const Eigen::Vector3d& p) const;

       private:
        const ICONGrid& grid;
        size_t edge_id;
    };

    inline Cell get_cell() const { return {*this, cell_count()}; }
    inline Cell get_cell(size_t cell_id) const { return {*this, cell_id}; }
    inline Edge get_edge() const { return {*this, edge_count()}; }
    inline Edge get_edge(size_t edge_id) const { return {*this, edge_id}; }

    inline size_t vertex_count() const { return vertices.size(); }
    inline size_t cell_count() const { return cells.size(); }
    inline size_t edge_count() const { return edge_normals.size(); }

    /** \brief search for grid cell index containing the given point
     *
     * TODO: create an acceleration structure for this.
     */
    size_t find_cell_idx(const Eigen::Vector3d& point) const;

    inline Cell find_cell(const Eigen::Vector3d& point) const {
        return get_cell(find_cell_idx(point));
    }

    /** \brief search for first edge intersecting with given ray
     *
     * TODO: create an acceleration structure for this.
     */
    size_t find_intersecting_edge_idx(const Ray& ray, double tnear, double tfar,
                                      double& t) const;

    inline Edge find_intersecting_edge(const Ray& ray, double tnear,
                                       double tfar, double& t) const {
        return get_edge(find_intersecting_edge_idx(ray, tnear, tfar, t));
    }

    /** \brief normalizes vertices
     *
     * \note vertices must already be available but don't need to be normalized
     */
    void repair_vertices();
    void repair_edge_normals();
    void repair_edge_orientations();
    /** \brief repairs edge neighbor cells
     *
     * \note this functioncion needs correct edge_orientations
     */
    void repair_edge_cells();

    void build_accelerators();

    std::vector<Eigen::Vector3d> vertices;
    std::vector<Eigen::Vector3d> edge_normals;
    std::vector<std::pair<size_t, size_t>> edge_vertices;
    std::vector<std::pair<size_t, size_t>> edge_cells;
    std::vector<cell_t> cells;

    std::unique_ptr<KDAccelerator> edge_finding_accel = nullptr;

    double rmin, rmax;
};

template <typename VertIter, typename TriIter>
std::unique_ptr<ICONGrid> create_icon_grid_from_vertices_and_triangles(
  VertIter vert_begin, VertIter vert_end, TriIter tri_begin, TriIter tri_end,
  double rmin, double rmax) {
    auto grid = std::make_unique<ICONGrid>();
    grid->vertices.reserve(std::distance(vert_begin, vert_end));
    std::copy(vert_begin, vert_end, std::back_inserter(grid->vertices));
    size_t edgecounter = 0;
    std::map<std::pair<size_t, size_t>, size_t> edge_map;
    for (auto it = tri_begin; it != tri_end; ++it) {
        ICONGrid::cell_t cell;
        for (int i = 0; i < 3; ++i) {
            int j = i + 1;
            if (j == 3) j = 0;
            size_t v1 = (*it)[i];
            size_t v2 = (*it)[j];
            cell.vertices[i] = v1;
            bool outwards = false;
            if (v1 > v2) {
                std::swap(v1, v2);
                outwards = true;
            }
            auto [em_it, is_new] = edge_map.try_emplace({v1, v2}, edgecounter);
            if (is_new) {
                ++edgecounter;
            }
            cell.edges[i] = em_it->second;
            cell.edge_directions[i] = outwards;
        }
        grid->cells.push_back(cell);
    }
    grid->edge_normals.resize(edgecounter);
    grid->edge_vertices.resize(edgecounter);
    for (auto& edge : edge_map) {
        const auto& v1 = grid->vertices[edge.first.first];
        const auto& v2 = grid->vertices[edge.first.second];
        grid->edge_normals[edge.second] = v1.cross(v2).normalized();
        grid->edge_vertices[edge.second] = edge.first;
    }

    grid->rmin = rmin;
    grid->rmax = rmax;

    grid->repair_vertices();
    grid->repair_edge_cells();

    grid->build_accelerators();
    return grid;
}
}  // namespace rayli::grid
