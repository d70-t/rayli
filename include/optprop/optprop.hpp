#pragma once

namespace rayli::optprop {

struct HG {
    double ext = 0;
    double g = 0;
    double ssa = 0;

    bool operator==(const HG& other) const {
        return ext == other.ext && g == other.g && ssa == other.ssa;
    }

    bool is_empty() const { return ext == 0; }
};

template <typename IStream>
IStream& operator>>(IStream& istream, HG& m) {
    istream >> m.ext >> m.g >> m.ssa;
    return istream;
}
}  // namespace rayli::optprop

namespace std {
template <>
struct hash<rayli::optprop::HG> {
    size_t operator()(const rayli::optprop::HG& v) const {
        size_t h = std::hash<double>()(v.ext);
        hash_combine(h, v.g);
        hash_combine(h, v.ssa);
        return h;
    }
};
}  // namespace std
