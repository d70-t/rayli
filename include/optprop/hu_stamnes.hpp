#pragma once

#include "exceptions.hpp"
#include "microphysics.hpp"
#include "optprop/optprop.hpp"

#include <fmt/format.h>
#include <vector>

namespace rayli::optprop::hu_stamnes {
struct ABC {
    double a, b, c;

    double eval(double reff) const;
};

ABC operator+(const ABC& a, const ABC& b);
ABC operator*(double a, const ABC& b);
ABC operator*(const ABC& a, double b);

struct ECG {
    ABC ext;
    ABC coalbedo;
    ABC g;
};

ECG operator+(const ECG& a, const ECG& b);
ECG operator*(double a, const ECG& b);
ECG operator*(const ECG& a, double b);

template <typename EL>
struct ReffRange {
    double reff_min;
    double reff_max;
    EL data;
};

struct MonochromaticTable {
    std::vector<ReffRange<ECG>> reff_ranges;
};

/** \brief Table for Optical Properties calculation following Hu and Stamnes
 *
 * \ref:
 * http://journals.ametsoc.org/doi/pdf/10.1175/1520-0442%281993%29006%3C0728%3AAAPOTR%3E2.0.CO%3B2
 *
 * Units are:
 * * [µm] for wavelength
 * * [µm] for effective radius
 * * [g m⁻³] for liquid water content
 * * [m⁻³] for extinction coefficient beta_ext
 */
struct Table {
    std::vector<double> wavelength;
    std::vector<ReffRange<std::vector<ECG>>> reff_ranges;

    MonochromaticTable
    interpolate_to_wavelength(double selected_wavelength) const;
};

rayli::optprop::HG get_optprop(const ECG& params,
                               const rayli::microphysics::LWC_reff& mp);

rayli::optprop::HG get_optprop(const ECG& params,
                               const rayli::microphysics::ext_reff& mp);

template <typename Microphysics>
rayli::optprop::HG get_optprop(const MonochromaticTable& table,
                               const Microphysics& mp) {
    for (const auto& rr : table.reff_ranges) {
        if (mp.reff >= rr.reff_min && mp.reff < rr.reff_max) {
            return get_optprop(rr.data, mp);
        }
    }
    throw rayli::exceptions::InvalidArgument(
      fmt::format("supplied reff ({}) is not in valid range", mp.reff));
}
}  // namespace rayli::optprop::hu_stamnes
