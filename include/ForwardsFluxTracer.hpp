#pragma once
#include "Ray.hpp"
#include "overloaded.hpp"
#include "thread_support.hpp"
#include <VarianceReduction.hpp>
#include <brdf/worldbrdf.hpp>
#include <material.hpp>
#include <vgrid/GridIterator.hpp>

#include <algorithm>
#include <iostream>
#include <limits>
#include <random>
#include <thread>
#include <variant>

template <typename R, typename ExtTexture, typename ScatTexture,
          typename SurfDirectTexture, typename SurfIndirectTexture,
          typename Recorder, typename VR = rayli::vr::NoVarianceReduction>
struct ForwardEventHandler {
    R& rng;
    Ray r;
    const ExtTexture& extinction;
    const ScatTexture& scat;
    const SurfDirectTexture& surf_direct;
    const SurfIndirectTexture& surf_indirect;
    Recorder& rec;
    VR vr = {};
    bool change_direction = true;
    double L = 1;
    size_t scatter_count = 0;
    double tau_remain;

    void operator()(const Teleport& t) {
      r = t.r;
    }

    void operator()(const VolumeSlice& v) {
        auto scatterer = scat(v.idx);
        double s = v.tfar - v.tnear;
        double k_scat = scatterer.sigma_scat();
        double dtau = s * k_scat;
        bool do_scatter = dtau > tau_remain;

        double k_abs = extinction(v.idx) - k_scat;

        if (do_scatter) {
            s *= tau_remain / dtau;
        }

        double tr_abs = std::exp(-k_abs * s);
        rec.on_volabs(v.idx, L * (1 - tr_abs));
        L *= tr_abs;

        if (do_scatter) {
            auto scat_sample = scatterer.sample_f(-r.d, rng);
            L *= scat_sample.f / scat_sample.pdf / k_scat;
            r = {r(v.tnear + s), scat_sample.d};
            change_direction = true;
            rec.on_scattering(r);
            ++scatter_count;
            // vr.after_scatter(L, rng); // TODO: find out what to do here
        } else {
            tau_remain -= dtau;
        }
    }

    void operator()(const SurfaceSlice& s) {
        rec.on_enter_surface(r, s, L);
        auto brdf =
          (scatter_count == 0) ? surf_direct(s.idx) : surf_indirect(s.idx);

        if (bool(brdf.flags & MaterialFlags::transparent)) {
            rec.on_leave_surface(r, s, L);
        } else {
            Eigen::Vector3d p = r(s.t);
            auto geometry = s.compute_geometry(r);
            auto transform = WorldBRDFTransform::from_n_and_dpdu(
              geometry.shading.n, geometry.shading.dpdu);

            if (bool(brdf.flags &
                     (MaterialFlags::diffuse | MaterialFlags::specular))) {
                auto scat_sample = transform.sample_f(brdf, -r.d, rng);
                L *= scat_sample.f / scat_sample.pdf;
                r = {
                  p + scat_sample.d * 1e-6,
                  scat_sample.d};  // TODO, maybe somehow prevent the use of
                                   // epsilon here, but necessary not to be
                                   // jumping up and down around surface layers
                change_direction = true;
                rec.on_scattering(r);
                rec.on_leave_surface(r, s, L);
                ++scatter_count;
                // vr.after_scatter(L, rng); // TODO: find out what to do here
            } else {
                L = 0;
            }
        }
    }
};

template <typename Grid, typename VR = rayli::vr::NoVarianceReduction>
class ForwardsFluxTracer {
   public:
    ForwardsFluxTracer(const Grid& grid, VR vr = {}) : grid(grid), vr(vr) {}
    template <typename RaySource, typename TerminationCriteria,
              typename ExtTexture, typename ScatTexture,
              typename SurfDirectTexture, typename SurfIndirectTexture,
              typename Recorder>
    void solve(const RaySource& source, TerminationCriteria crit,
               const ExtTexture& extinction, const ScatTexture& scat,
               const SurfDirectTexture& surf_direct,
               const SurfIndirectTexture& surf_indirect, Recorder& rec,
               size_t num_threads = 0) {
        if (!num_threads) {
            num_threads = cluster_aware_concurrency();
        }
        std::vector<Recorder> recorders;
        std::generate_n(std::back_inserter(recorders), num_threads,
                        [&]() { return rec.clone(); });
        std::vector<std::thread> pool;

        for (size_t thread_id = 0; thread_id < recorders.size(); ++thread_id) {
            pool.push_back(std::thread([&, thread_id]() {
                solve_single(
                  source, [=](size_t i) { return crit(i, thread_id); },
                  extinction, scat, surf_direct, surf_indirect,
                  recorders[thread_id]);
            }));
        }
        for (auto& thread : pool) {
            thread.join();
        }
        for (auto& subrec : recorders) {
            rec.join(subrec);
        }
    }

    template <typename RaySource, typename TerminationCriteria,
              typename ExtTexture, typename ScatTexture,
              typename SurfDirectTexture, typename SurfIndirectTexture,
              typename Recorder>
    void solve_single(const RaySource& source, TerminationCriteria crit,
                      const ExtTexture& extinction, const ScatTexture& scat,
                      const SurfDirectTexture& surf_direct,
                      const SurfIndirectTexture& surf_indirect, Recorder& rec) {
        std::random_device rd;
        std::mt19937_64 rng(rd());
        std::exponential_distribution<double> tau_dist(1);

        for (size_t i = 0; crit(i); ++i) {
            Ray r = source.generate_ray(rng);
            rec.on_new_ray(r);
            auto handler =
              ForwardEventHandler<std::mt19937_64, ExtTexture, ScatTexture,
                                  SurfDirectTexture, SurfIndirectTexture,
                                  Recorder, VR>{
                rng, r, extinction, scat, surf_direct, surf_indirect, rec, vr};
            typename Grid::RayIterator it = {};
            while (handler.change_direction) {
                handler.change_direction = false;
                handler.tau_remain = tau_dist(rng);
                auto path = grid.walk_along(
                  handler.r, 0, std::numeric_limits<double>::infinity(), it);
                for (it = path.begin(); it != path.end(); ++it) {
                    handle(handler, *it);
                    if (handler.change_direction || handler.L == 0) break;
                }
            }
            rec.on_ray_finish();
        }
    }

   private:
    const Grid& grid;
    VR vr;
};
