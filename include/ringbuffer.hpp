#pragma once
#include <array>

template <typename T, size_t size>
class Ringbuffer {
    static_assert(size > 0);

   public:
    T& front() { return data[read]; }
    const T& front() const { return data[read]; }
    void pop() {
        read = (read + 1) % size;
        _full = false;
    }
    void push(const T& value) {
        data[write] = value;
        inc_write();
    }
    void push(T&& value) {
        data[write] = std::move(value);
        inc_write();
    }
    bool empty() const { return read == write && !_full; }
    bool full() const { return _full; }

   private:
    void inc_write() {
        write = (write + 1) % size;
        if (read == write) {
            _full = true;
        }
    }
    size_t read = 0, write = 0;
    std::array<T, size> data;
    bool _full = false;
};
