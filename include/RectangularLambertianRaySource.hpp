#include <Eigen/Dense>
#include <GeometryUtils.hpp>
#include <Ray.hpp>
#include <distributions.hpp>
#include <random>

class RectangularLambertianRaySource {
   public:
    RectangularLambertianRaySource(const Eigen::Vector3d& o,
                                   const Eigen::Vector3d& e1,
                                   const Eigen::Vector3d& e2)
        : o(o), e1(e1), e2(e2) {}

    inline double area() const { return e1.cross(e2).norm(); }
    inline double effective_area() const { return area(); }

    template <typename G>
    Ray generate_ray(G& rng) const {
        // TODO direction needs to be rotated on the area, for now hardcoded to
        // be on the horizontal slab
        //return Ray{ Eigen::Vector3d{210,210,501}, Eigen::Vector3d{0,0,-1}.normalized()};
        auto u = uniform_sample_nd<2, double>(rng);
        auto d = cosine_sample_hemisphere(u);
        d[2] = -d[2];

        auto r = std::uniform_real_distribution<double>();
        Eigen::Vector3d p = o + r(rng) * e1 + r(rng) * e2;
        return Ray{p,d};
    }

    template <typename Stream>
    friend Stream& operator<<(Stream&, const RectangularLambertianRaySource&);

   private:
    Eigen::Vector3d o, e1, e2;
};

template <typename Stream>
Stream& operator<<(Stream& out, const RectangularLambertianRaySource& s) {
    out << "<RectangularLambertianRaySource @ " << s.o.transpose() << " -> "
        << " e1: " << s.e1.transpose()
        << " e2: " << s.e2.transpose() << ">"
        << " effective area: " << s.effective_area();
    return out;
}
