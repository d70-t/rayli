#pragma once

namespace rayli::traits {
template <typename T>
bool is_zero(const T& value) {
    return value == 0;
}
}  // namespace rayli::traits
