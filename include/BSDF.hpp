#pragma once

#include "brdf/BRDF.hpp"

/** \brief Describes the full surface scattering properties at a single surface
 * interaction
 */
template <typename G>
class BSDF {
   public:
    BSDF(const Eigen::Vector3d& s, const Eigen::Vector3d& t,
         const Eigen::Vector3d& n) {
        m.row(0) = s;
        m.row(1) = t;
        m.row(2) = n;
    }

    Eigen::Vector3d world_to_local(const Eigen::Vector3d& v) const {
        return m * v;
    }

    Eigen::Vector3d local_to_world(const Eigen::Vector3d& v) const {
        return m.transpose() * v;
    }

    double f(const Eigen::Vector3d& in_direction_world,
             const Eigen::Vector3d& out_direction_world) const {
        if (!has_brdf()) {
            return 0;
        }
        // note that in shading coordinates, both directions are facing outward,
        // so the in direction has to be flipped
        Eigen::Vector3d in_direction = world_to_local(-in_direction_world);
        Eigen::Vector3d out_direction = world_to_local(out_direction_world);

        /*
         * this section might become relevant if transmission and shading
         * coordinates are fully supported.
         *
        // this extra check is necessary, if shading coordinates differ from
        // geometric corrdinates, in which case a computation in local (shading)
        // coordinates would yield an errorneous result.
        bool reflect =
          -in_direction_world.dot(ng) * out_direction_world.dot(ng) > 0;
        */
        return brdf->f(in_direction, out_direction);
    }

    double pdf(const Eigen::Vector3d& in_direction_world,
               const Eigen::Vector3d& out_direction_world) const {
        if (!has_brdf()) {
            return 0;
        }
        // note that in shading coordinates, both directions are facing outward,
        // so the in direction has to be flipped
        Eigen::Vector3d in_direction = world_to_local(-in_direction_world);
        Eigen::Vector3d out_direction = world_to_local(out_direction_world);
        return brdf->pdf(in_direction, out_direction);
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction_world,
                            G& rng) const {
        DirectedSample out;
        if (!has_brdf()) {
            out.d = in_direction_world;
            out.pdf = 0;
            out.f = 0;
            return out;
        }
        Eigen::Vector3d in_direction = world_to_local(-in_direction_world);
        out = brdf->sample_f(in_direction, rng);
        out.d = local_to_world(out.d);
        return out;
    }

    void add_brdf(BRDF<G>* brdf_) { brdf = brdf_; }

   private:
    inline bool has_brdf() const { return bool(brdf); }

    // Eigen::Vector3d ng;  ///< \brief geometric normal
    Eigen::Matrix3d m;  ///< \brief shading coordinate system
    BRDF<G>* brdf;
};
