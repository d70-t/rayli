#pragma once
#include <stdexcept>

namespace rayli::exceptions {
class InvalidArgument : public std::invalid_argument {
   public:
    inline InvalidArgument(const std::string& what)
        : std::invalid_argument(what) {}
};
}  // namespace rayli::exceptions
