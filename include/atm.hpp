#pragma once
#include "Eigen.hpp"
#include "GeometryUtils.hpp"
#include "helper_types.hpp"

#include <memory>
#include <random>

template <typename G>
class Scat {
   public:
    typedef G rng_t;
    virtual ~Scat(){};
    virtual double sigma_scat() const = 0;
    virtual double f(const Eigen::Vector3d& in_direction,
                     const Eigen::Vector3d& out_direction) const = 0;
    virtual double pdf(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const = 0;
    virtual DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                                    G& rng) const = 0;
};

template <typename G>
class VacuumScat : public Scat<G> {
   public:
    typedef G rng_t;
    double sigma_scat() const override { return 0.; }

    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const override {
        return 0.;
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const override {
        return 0.;
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            G& rng) const override {
        return {-in_direction, 1., 1.};
    }
};

template <typename T, typename G>
class MuDependentScat : public Scat<G> {
   public:
    typedef G rng_t;
    T& cast() { return *static_cast<T*>(this); }

    const T& cast() const { return *static_cast<const T*>(this); }

    double sigma_scat() const override { return 1.; }

    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const override {
        double mu = -in_direction.dot(out_direction);
        return f_spehre_normed(mu);
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const override {
        double mu = -in_direction.dot(out_direction);
        return f_spehre_normed(mu);  // in this case, f == pdf
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            G& rng) const override {
        double mu = cast().sample_mu(rng);

        std::uniform_real_distribution<double> dist2(0, 2 * M_PI);
        double phi = dist2(rng);
        auto p =
          Eigen::AngleAxis(phi, in_direction) * any_perpendicular(in_direction);
        double f_out = f_spehre_normed(mu);
        return {(rotation_from_cos_axis(-mu, p) * in_direction).normalized(),
                f_out, f_out};
    }

   private:
    double f_spehre_normed(double mu) const {
        // integration over mu only and integration over the whole sphere
        // differs by a factor of 2 pi
        return cast().f(mu) * (1. / (2. * M_PI));
    }
};

template <typename G>
class HGScat : public MuDependentScat<HGScat<G>, G> {
   public:
    typedef G rng_t;
    HGScat(double g) : MuDependentScat<HGScat<G>, G>(), g(g) {
        assert(g >= -1);
        assert(g <= 1);
    }

    using MuDependentScat<HGScat<G>, G>::f;
    double f(double mu) const {
        double g2 = g * g;
        double foo = (1 + g2 - 2 * g * mu);
        return (1 - g2) / (2 * std::sqrt(foo * foo * foo));
    }

    double sample_mu(G& rng) const {
        std::uniform_real_distribution<double> dist(-1, 1);
        double s = dist(rng);
        if (g == 0) {
            return s;
        } else {
            double g2 = g * g;
            double h = (1 - g2) / (1 + g * s);
            return (1 + g2 - h * h) / (2 * g);
        }
    }

   private:
    double g;
};

template <typename G>
class RayleighScat : public MuDependentScat<RayleighScat<G>, G> {
   public:
    typedef G rng_t;
    RayleighScat() : MuDependentScat<RayleighScat<G>, G>() {}

    using MuDependentScat<RayleighScat<G>, G>::f;
    double f(double mu) const { return (3. / 8.) * (1 + mu * mu); }

    double sample_mu(G& rng) const {
        std::uniform_real_distribution<double> dist(0, 1);
        double r = dist(rng);
        double q = 4 * r - 2;
        double d = 1 + (q * q);
        double u = std::cbrt(q + std::sqrt(d));
        return u - 1. / u;
    }
};

template <typename G>
class TabulatedMuScat : public MuDependentScat<TabulatedMuScat<G>, G> {
   public:
    typedef G rng_t;
    TabulatedMuScat(std::vector<double> mu, std::vector<double> f)
        : MuDependentScat<TabulatedMuScat<G>, G>(), mu_points(std::move(mu)),
          f_values(std::move(f)) {
        dist = {mu_points.begin(), mu_points.end(), f_values.begin()};
    }

    using MuDependentScat<TabulatedMuScat<G>, G>::f;
    double f(double mu) const {
        assert(mu >= mu_points.front() && mu <= mu_points.back());
        auto it = std::lower_bound(mu_points.begin(), mu_points.end(), mu);
        if (it == mu_points.end()) {
            return f_values.back();
        }
        if (it == mu_points.begin()) {
            return f_values.front();
        }
        auto it2 = it--;
        auto substep = (mu - *it) / (*it2 - *it);
        auto fit = f_values.begin() + std::distance(mu_points.begin(), it);
        auto fit2 = fit + 1;
        return (1. - substep) * *fit + substep * *fit2;
    }

    double sample_mu(G& rng) const { return dist(rng); }

   private:
    std::vector<double> mu_points, f_values;
    mutable std::piecewise_linear_distribution<double> dist;
};

template <typename T>
class ScaleAtm : public Scat<typename T::rng_t> {
   public:
    typedef typename T::rng_t rng_t;
    ScaleAtm(T a, double scale) : a(a), scale(scale) {}

    constexpr double sigma_scat() const override {
        return scale * a.sigma_scat();
    }

    constexpr double f(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const override {
        return scale * a.f(in_direction, out_direction);
    }

    constexpr double pdf(const Eigen::Vector3d& in_direction,
                         const Eigen::Vector3d& out_direction) const override {
        return a.pdf(in_direction, out_direction);
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            rng_t& rng) const override {
        return scale * a.sample_f(in_direction, rng);
    }

    ScaleAtm<T> operator*(double f) { return {a, scale * f}; }

   private:
    T a;
    double scale;
};

template <typename A, typename B,
          typename = std::enable_if<
            std::is_same<typename A::rng_t, typename B::rng_t>::value>>
class AddAtm : public Scat<typename A::rng_t> {
   public:
    typedef typename A::rng_t rng_t;
    AddAtm(A a, B b) : a(a), b(b) {}

    constexpr double sigma_scat() const override {
        return a.sigma_scat() + b.sigma_scat();
    }

    constexpr double f(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const override {
        return a.f(in_direction, out_direction) +
               b.f(in_direction, out_direction);
    }

    constexpr double pdf(const Eigen::Vector3d& in_direction,
                         const Eigen::Vector3d& out_direction) const override {
        double s_a = a.sigma_scat();
        double s_b = b.sigma_scat();
        if (s_a == 0) {
            return b.pdf(in_direction, out_direction);
        } else if (s_b == 0) {
            return a.pdf(in_direction, out_direction);
        } else {
            return (a.sigma_scat() * a.pdf(in_direction, out_direction) +
                    b.sigma_scat() * b.pdf(in_direction, out_direction)) /
                   sigma_scat();
        }
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            rng_t& rng) const override {
        double s_a = a.sigma_scat();
        double s_b = b.sigma_scat();
        if (s_a == 0) {
            return b.sample_f(in_direction, rng);
        } else if (s_b == 0) {
            return a.sample_f(in_direction, rng);
        } else {
            double p_a = s_a / (s_a + s_b);
            if (std::bernoulli_distribution(p_a)(rng)) {
                auto sample = a.sample_f(in_direction, rng);
                sample.f += b.f(in_direction, sample.d);
                sample.pdf =
                  p_a * sample.pdf + (1 - p_a) * b.pdf(in_direction, sample.d);
                return sample;
            } else {
                auto sample = b.sample_f(in_direction, rng);
                sample.f += a.f(in_direction, sample.d);
                sample.pdf =
                  p_a * a.pdf(in_direction, sample.d) + (1 - p_a) * sample.pdf;
                return sample;
            }
        }
    }

   private:
    A a;
    B b;
};

template <typename T, typename = std::enable_if<
                        std::is_base_of<Scat<typename T::rng_t>, T>::value>>
ScaleAtm<T> operator*(T a, double f) {
    return {a, f};
}

template <typename T, typename = std::enable_if<
                        std::is_base_of<Scat<typename T::rng_t>, T>::value>>
ScaleAtm<T> operator*(double f, T a) {
    return {a, f};
}

template <
  typename A, typename B,
  typename = std::enable_if<std::is_base_of<Scat<typename A::rng_t>, A>::value>,
  typename = std::enable_if<std::is_base_of<Scat<typename B::rng_t>, B>::value>,
  typename =
    std::enable_if<std::is_same<typename A::rng_t, typename B::rng_t>::value>>
AddAtm<A, B> operator+(A a, B b) {
    return {a, b};
}

template <typename G>
class Atm : public Scat<G> {
   public:
    typedef G rng_t;
    template <typename T,
              typename = std::enable_if<std::is_base_of<Scat<G>, T>::value>>
    Atm(T a)
        : impl(std::make_shared<typename std::remove_reference_t<T>>(
            std::move(a))) {}

    constexpr double sigma_scat() const override { return impl->sigma_scat(); }

    constexpr double f(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const override {
        return impl->f(in_direction, out_direction);
    }

    constexpr double pdf(const Eigen::Vector3d& in_direction,
                         const Eigen::Vector3d& out_direction) const override {
        return impl->pdf(in_direction, out_direction);
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            rng_t& rng) const override {
        return impl->sample_f(in_direction, rng);
    }

   private:
    std::shared_ptr<Scat<G>> impl;
};

template <typename T,
          typename =
            std::enable_if<std::is_base_of<Scat<typename T::rng_t>, T>::value>>
class RefAtm : public Scat<typename T::rng_t> {
   public:
    typedef typename T::rng_t rng_t;
    RefAtm(T& a) : impl(a) {}

    constexpr double sigma_scat() const override { return impl.sigma_scat(); }

    constexpr double f(const Eigen::Vector3d& in_direction,
                       const Eigen::Vector3d& out_direction) const override {
        return impl.f(in_direction, out_direction);
    }

    constexpr double pdf(const Eigen::Vector3d& in_direction,
                         const Eigen::Vector3d& out_direction) const override {
        return impl.pdf(in_direction, out_direction);
    }

    DirectedSample sample_f(const Eigen::Vector3d& in_direction,
                            rng_t& rng) const override {
        return impl.sample_f(in_direction, rng);
    }

   private:
    T& impl;
};
