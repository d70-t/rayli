#pragma once

#include "RaySink.hpp"

template <typename G>
class PointSink : public RaySink<G> {
   public:
    PointSink(const Eigen::Vector3d& location) : location(location) {}
    SinkSample sample_from(G& rng, const Eigen::Vector3d from) const {
        Eigen::Vector3d dir = location - from;
        return {dir.normalized(), dir.norm(), 1.};
    }

   private:
    Eigen::Vector3d location;
};
