#pragma once

#include "Eigen.hpp"

struct Ray {
    Eigen::Vector3d o;  ///< origin
    Eigen::Vector3d d;  ///< direction
    /** \brief returns ray at time t
     */
    inline Eigen::Vector3d operator()(double t) const { return o + d * t; }

    bool operator==(const Ray& other) const {
        return (o == other.o) && (d == other.d);
    }

    bool operator!=(const Ray& other) const { return !(*this == other); }
};

inline std::ostream& operator<<(std::ostream& s, const Ray& ray) {
    Eigen::IOFormat flat_fmt(Eigen::FullPrecision, Eigen::DontAlignCols, ", ",
                             ",", "", "", "[", "]");
    s << "<Ray " << ray.o.format(flat_fmt) << " " << ray.d.format(flat_fmt)
      << ">";
    return s;
}
