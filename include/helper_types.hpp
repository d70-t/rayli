#pragma once

#include "Eigen.hpp"
#include <type_traits>

struct DirectedSample {
    Eigen::Vector3d d;
    double f;
    double pdf;
};

template <typename T, typename = std::enable_if<std::is_arithmetic<T>::value>>
DirectedSample operator*(T f, const DirectedSample& s) {
    return {s.d, f * s.f, s.pdf};
}

template <typename T, typename = std::enable_if<std::is_arithmetic<T>::value>>
DirectedSample operator*(const DirectedSample& s, T f) {
    return f * s;
}
