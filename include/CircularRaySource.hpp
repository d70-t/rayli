#include <Eigen/Dense>
#include <GeometryUtils.hpp>
#include <Miniball.hpp>
#include <Ray.hpp>
#include <sampling.hpp>

class CircularRaySource {
   public:
    CircularRaySource(const Eigen::Vector3d& o, const Eigen::Vector3d& d,
                      const Eigen::Vector3d& e1, const Eigen::Vector3d& e2,
                      double r)
        : o(o), d(d), e1(e1), e2(e2), r(r) {}

    inline double area() const { return M_PI * r * r; }
    inline double effective_area() const {
        return area() * abs(e1.cross(e2).normalized().dot(d.normalized()));
    }

    template <typename G>
    Ray generate_ray(G& rng) const {
        Eigen::Vector2d p = r * uniform_sample_disk(rng);
        return Ray{o + p(0) * e1 + p(1) * e2, d};
    }

    template <typename Stream>
    friend Stream& operator<<(Stream&, const CircularRaySource&);

   private:
    Eigen::Vector3d o, d, e1, e2;
    double r;
};

template <typename Stream>
Stream& operator<<(Stream& out, const CircularRaySource& s) {
    out << "<CircularRaySource @ " << s.o.transpose() << " -> "
        << s.d.transpose() << " r: " << s.r << ">";
    return out;
}

/** \brief create an appropriate ray source illuminating all points
 *
 * \param first iterator to the first point
 * \param last iterator to the last point
 *
 * \return A CircularRaySource capaple of illuminating the convex hull around
 * the given points
 */
template <typename It>
CircularRaySource
ray_source_from_sun_direction(It first, It last,
                              const Eigen::Vector3d& sun_direction) {
    Eigen::Vector3d se = any_perpendicular(sun_direction);
    Eigen::Vector3d sn = sun_direction.cross(se).normalized();
    std::vector<std::array<double, 2>> disk_points;
    using category = typename std::iterator_traits<It>::iterator_category;
    if constexpr (std::is_same_v<category, std::random_access_iterator_tag>) {
        disk_points.reserve(last - first);
    }
    double toa = -std::numeric_limits<double>::infinity();
    for (; first != last; ++first) {
        const auto& point = *first;
        disk_points.push_back({sn.dot(point), se.dot(point)});
        toa = std::max(toa, point.dot(sun_direction));
    }
    Miniball::Miniball mb(2, disk_points.begin(), disk_points.end());

    return CircularRaySource(2 * toa * sun_direction + mb.center()[0] * sn +
                               mb.center()[1] * se,
                             -sun_direction, sn, se, sqrt(mb.squared_radius()));
}
