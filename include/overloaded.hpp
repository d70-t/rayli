#pragma once

// nice trick from http://en.cppreference.com/w/cpp/utility/variant/visit

template <class... Ts>
struct overloaded : Ts... {
    using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;
