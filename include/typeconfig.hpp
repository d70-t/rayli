#pragma once

#include <memory_resource.hpp>
#include <random>

struct DefaultTypes {
    typedef std::mt19937_64 RNG;
    // typedef std::pmr::monotonic_buffer_resource MEMR;
    typedef monotonic_buffer_resource MEMR;
};

namespace rayli {
typedef std::mt19937_64 RNG;
}
