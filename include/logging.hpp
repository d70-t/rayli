#pragma once

#include <fmt/ostream.h>
#include <spdlog/spdlog.h>
#include <string>

namespace rayli::logging {
typedef std::shared_ptr<spdlog::logger> logger_ptr;
logger_ptr get_logger(const std::string& name);
}  // namespace rayli::logging
