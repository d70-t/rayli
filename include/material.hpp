#pragma once

#include "bitmask_operators.hpp"
#include "constants.hpp"
#include "distributions.hpp"

#include "brdf/BRDF.hpp"  // TODO: needs to be unified

#include <variant>

enum class MaterialFlags {
    transparent = 1,
    specular = 2,
    diffuse = 4,
    emitting = 8,
};

template <>
struct enable_bitmask_operators<MaterialFlags> {
    static constexpr bool enable = true;
};

class BaseMaterial {
  public:
    double L(const Eigen::Vector3d& out_direction) const { return 0; }
};

class LambertianMaterial : public BaseMaterial {
   public:
    LambertianMaterial(double r) : r(r) {}

    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const {
        if (same_hemisphere(in_direction, out_direction)) {
            return r * abs_cos_theta(in_direction) * INV_PI;
        } else {
            return 0;
        }
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const {
        if (same_hemisphere(in_direction, out_direction)) {
            return abs_cos_theta(in_direction) * INV_PI;
        } else {
            return 0;
        }
    }

    template <typename G>
    DirectedSample sample_f(const Eigen::Vector3d& in_direction, G& rng) const {
        auto u = uniform_sample_nd<2, double>(rng);
        DirectedSample out;
        out.d = cosine_sample_hemisphere(u);
        if (in_direction(2) < 0) {
            out.d(2) *= -1;
        }
        out.pdf = pdf(in_direction, out.d);
        out.f = f(in_direction, out.d);
        return out;
    }

    static constexpr MaterialFlags flags = MaterialFlags::diffuse;

   private:
    double r;
};


class ThermalLambertianMaterial : public BaseMaterial {
   public:
    ThermalLambertianMaterial(double r, double B) : r(r), B(B) {}

    double L(const Eigen::Vector3d& out_direction) const {
      return (1.-r) * B;
    }

    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const {
        if (same_hemisphere(in_direction, out_direction)) {
            return r * abs_cos_theta(in_direction) * INV_PI;
        } else {
            return 0;
        }
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const {
        if (same_hemisphere(in_direction, out_direction)) {
            return abs_cos_theta(in_direction) * INV_PI;
        } else {
            return 0;
        }
    }

    template <typename G>
    DirectedSample sample_f(const Eigen::Vector3d& in_direction, G& rng) const {
        auto u = uniform_sample_nd<2, double>(rng);
        DirectedSample out;
        out.d = cosine_sample_hemisphere(u);
        if (in_direction(2) < 0) {
            out.d(2) *= -1;
        }
        out.pdf = pdf(in_direction, out.d);
        out.f = f(in_direction, out.d);
        return out;
    }

    static constexpr MaterialFlags flags = MaterialFlags::emitting;

   private:
    double r;
    double B;
};


class MirrorMaterial : public BaseMaterial {
   public:
    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const {
        return 0;
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const {
        return 0;
    }

    template <typename G>
    DirectedSample sample_f(const Eigen::Vector3d& in_direction, G& rng) const {
        DirectedSample out;
        out.d = -in_direction;
        out.d(2) *= -1;
        out.pdf = 1;
        out.f = 1;
        return out;
    }

    static constexpr MaterialFlags flags = MaterialFlags::specular;
};

class NoMaterial : public BaseMaterial {
   public:
    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const {
        return 0;
    }

    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const {
        return 0;
    }

    template <typename G>
    DirectedSample sample_f(const Eigen::Vector3d& in_direction, G& rng) const {
        DirectedSample out;
        out.d = -in_direction;
        out.pdf = 1;
        out.f = 1;
        return out;
    }

    static constexpr MaterialFlags flags =
      MaterialFlags::specular | MaterialFlags::transparent;
};

template <typename... Ts>
class VariantMaterial {
   public:
    template <typename T, typename = std::enable_if_t<
                            (std::is_convertible<T, Ts>::value || ...)>>
    VariantMaterial(T&& material) : impl(std::forward<T>(material)) {
        flags = std::visit([](const auto& impl) { return impl.flags; }, impl);
    }
    double L(const Eigen::Vector3d& out_direction) const {
        return std::visit(
          [&](const auto& impl) { return impl.L(out_direction); },
          impl);
    }
    double f(const Eigen::Vector3d& in_direction,
             const Eigen::Vector3d& out_direction) const {
        return std::visit(
          [&](const auto& impl) { return impl.f(in_direction, out_direction); },
          impl);
    }
    double pdf(const Eigen::Vector3d& in_direction,
               const Eigen::Vector3d& out_direction) const {
        return std::visit(
          [&](const auto& impl) {
              return impl.pdf(in_direction, out_direction);
          },
          impl);
    }
    template <typename G>
    DirectedSample sample_f(const Eigen::Vector3d& in_direction, G& rng) const {
        return std::visit(
          [&](const auto& impl) { return impl.sample_f(in_direction, rng); },
          impl);
    }

    MaterialFlags flags;

   private:
    std::variant<Ts...> impl;
};
