#pragma once
#include <cstddef>
#include <functional>

/** \brief signum function
 *
 * \ref
 * https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c#4609795
 */
template <typename T>
int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

/** \brief add additional values to a hash
 *
 * This follows boost's hash combine and tries to avoid collisions better than
 * simple xor.
 */
template <typename T>
void hash_combine(size_t& seed, T const& v) {
    seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}
