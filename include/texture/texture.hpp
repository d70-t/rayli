#pragma once

#include <memory>
#include <type_traits>
#include <vector>

template <typename Value, typename Index>
class Texture {
   public:
    typedef Value value_t;
    typedef Index index_t;
    virtual Value operator()(Index) const = 0;
};

template <typename Index>
class IdTexture : public Texture<Index, Index> {
   public:
    Index operator()(Index idx) const override { return idx; }
};

template <typename Index, typename Value>
class ConstantTexture : public Texture<Value, Index> {
   public:
    ConstantTexture(Value value) : value(value) {}
    Value operator()(Index) const override { return value; }

   private:
    Value value;
};

template <typename Index, typename Value>
auto constant_texture(Value&& value) {
    return ConstantTexture<Index, Value>(std::forward<Value>(value));
}

template <typename BaseTexture, typename F,
          typename Value = decltype(
            std::declval<F>()(std::declval<typename BaseTexture::value_t>())),
          typename Index = typename BaseTexture::index_t>
class MapTexture : public Texture<Value, Index> {

   public:
    MapTexture(F f, BaseTexture base) : f(f), base(base) {}
    Value operator()(Index i) const override { return f(base(i)); }

   private:
    F f;
    BaseTexture base;
};

template <typename Index, typename F,
          typename Value = decltype(std::declval<F>()(std::declval<Index>()))>
class FunctionTexture : public Texture<Value, Index> {
   public:
    FunctionTexture(F f) : f(f) {}
    Value operator()(Index i) const override { return f(i); }

   private:
    F f;
};

template <typename Index, typename F>
auto function_texture(F&& f) {
    return FunctionTexture<Index, F>(std::forward<F>(f));
}

template <typename TEX1, typename TEX2, typename F,
          typename Value =
            decltype(std::declval<F>()(std::declval<typename TEX1::value_t>(),
                                       std::declval<typename TEX2::value_t>())),
          typename Index = typename std::enable_if_t<
            std::is_same_v<typename TEX1::index_t, typename TEX2::index_t>,
            typename TEX1::index_t>>
class BinOpTexture : public Texture<Value, Index> {
   public:
    BinOpTexture(TEX1 tex1, TEX2 tex2, F f) : tex1(tex1), tex2(tex2), f(f) {}
    Value operator()(Index idx) const override {
        return f(tex1(idx), tex2(idx));
    }

   private:
    TEX1 tex1;
    TEX2 tex2;
    F f;
};

template <typename Value>
class SharedVectorTexture : public Texture<Value, size_t> {
   public:
    SharedVectorTexture(std::vector<Value> data)
        : data(std::make_shared<std::vector<Value>>(std::move(data))) {}
    SharedVectorTexture(std::shared_ptr<std::vector<Value>> data)
        : data(data) {}
    Value operator()(size_t idx) const override { return (*data)[idx]; }

   private:
    std::shared_ptr<std::vector<Value>> data;
};

template <typename Value>
class ForeignVectorTexture : public Texture<Value, size_t> {
   public:
    ForeignVectorTexture(const Value* data) : data(data) {}
    Value operator()(size_t idx) const override { return data[idx]; }

   private:
    const Value* data;
};

template <class T>
inline constexpr bool has_value_t(typename T::value_t*) {
    return true;
}
template <class T>
inline constexpr bool has_value_t(...) {
    return false;
}
template <class T>
inline constexpr bool has_index_t(typename T::index_t*) {
    return true;
}
template <class T>
inline constexpr bool has_index_t(...) {
    return false;
}

template <typename T>
inline constexpr bool is_texture() {
    if constexpr (!std::is_class_v<T>) {
        return false;
    } else if constexpr (!(has_value_t<T>(0) && has_index_t<T>(0))) {
        return false;
    } else if constexpr (!std::is_base_of_v<
                           Texture<typename T::value_t, typename T::index_t>,
                           T>) {
        return false;
    }
    return true;
}

template <typename T>
inline constexpr bool is_texture_v = is_texture<T>();

template <typename T1, typename T2>
inline constexpr bool is_binop_texture_input() {
    return is_texture_v<T1> || is_texture_v<T2>;
}

template <typename MAYBE_TEX1, typename MAYBE_TEX2, typename F>
auto mk_binop_texture(MAYBE_TEX1 tex1, MAYBE_TEX2 tex2, F f) {
    if constexpr (is_texture_v<MAYBE_TEX1> && is_texture_v<MAYBE_TEX2>) {
        return BinOpTexture(tex1, tex2, f);
    } else if constexpr (is_texture_v<MAYBE_TEX1>) {
        return BinOpTexture(
          tex1, ConstantTexture<typename MAYBE_TEX1::index_t, MAYBE_TEX2>(tex2),
          f);
    } else if constexpr (is_texture_v<MAYBE_TEX2>) {
        return BinOpTexture(
          ConstantTexture<typename MAYBE_TEX2::index_t, MAYBE_TEX1>(tex1), tex2,
          f);
    }
}

template <typename TEX1, typename TEX2,
          typename = std::enable_if_t<is_texture_v<TEX1> || is_texture_v<TEX2>>>
auto operator+(TEX1 tex1, TEX2 tex2) {
    return mk_binop_texture(tex1, tex2, [](auto a, auto b) { return a + b; });
}

template <typename TEX1, typename TEX2,
          typename = std::enable_if_t<is_texture_v<TEX1> || is_texture_v<TEX2>>>
auto operator-(TEX1 tex1, TEX2 tex2) {
    return mk_binop_texture(tex1, tex2, [](auto a, auto b) { return a - b; });
}

template <typename TEX1, typename TEX2,
          typename = std::enable_if_t<is_texture_v<TEX1> || is_texture_v<TEX2>>>
auto operator*(TEX1 tex1, TEX2 tex2) {
    return mk_binop_texture(tex1, tex2, [](auto a, auto b) { return a * b; });
}

template <typename TEX1, typename TEX2,
          typename = std::enable_if_t<is_texture_v<TEX1> || is_texture_v<TEX2>>>
auto operator/(TEX1 tex1, TEX2 tex2) {
    return mk_binop_texture(tex1, tex2, [](auto a, auto b) { return a / b; });
}
