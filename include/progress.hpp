#pragma once
#include <atomic>
#include <chrono>
#include <thread>

class NullProgressReporter {
   public:
    inline void set_total(long new_total) {}
    inline void set_done(long new_done) {}
    inline void operator++() {}
    inline void operator--() {}
};

template <typename P>
class RefReporter {
   public:
    inline RefReporter(P& p) : p(p) {}
    inline void set_total(long new_total) { p.set_total(new_total); }
    inline void set_done(long new_done) { p.set_done(new_done); }
    inline void operator++() { ++p; }
    inline void operator--() { --p; }

   private:
    P& p;
};

class ProgressBar {
   public:
    ProgressBar(long total = 1);
    void set_total(long new_total);
    inline void operator++() { ++done; }
    inline void operator--() { --done; }
    void display();
    void display_bar();
    void display_text();
    inline RefReporter<ProgressBar> reporter() { return *this; }
    template <typename F>
    void display_while(F f) {
        std::atomic<bool> ready = false;
        auto t = std::thread([&]() {
            f();
            ready = true;
        });
        while (!ready) {
            display();
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1ms);
        }
        t.join();
    }

   private:
    long total = 1;
    std::atomic<long> done;
    std::chrono::system_clock::time_point start_time;
    std::chrono::system_clock::time_point old_time;
    long granularity = 1;
    long old_done = -1;
    bool show_bar = true;
};
