#include <Eigen/Dense>
#include <GeometryUtils.hpp>
#include <Ray.hpp>

class RectangularRaySource {
   public:
    RectangularRaySource(const Eigen::Vector3d& o, const Eigen::Vector3d& d,
                         const Eigen::Vector3d& e1, const Eigen::Vector3d& e2)
        : o(o), d(d), e1(e1), e2(e2) {}

    inline double area() const { return e1.cross(e2).norm(); }
    inline double effective_area() const {
        return area() * abs(e1.cross(e2).normalized().dot(d.normalized()));
    }

    template <typename G>
    Ray generate_ray(G& rng) const {
        auto dist = std::uniform_real_distribution<double>();
        return Ray{o + dist(rng) * e1 + dist(rng) * e2, d};
    }

    template <typename Stream>
    friend Stream& operator<<(Stream&, const RectangularRaySource&);

   private:
    Eigen::Vector3d o, d, e1, e2;
};

template <typename Stream>
Stream& operator<<(Stream& out, const RectangularRaySource& s) {
    out << "<RectangularRaySource @ " << s.o.transpose() << " -> "
        << s.d.transpose() << " e1: " << s.e1.transpose()
        << " e2: " << s.e2.transpose() << ">";
    return out;
}
