#pragma once
#include <Eigen.hpp>
#include <progress.hpp>
#include <logging.hpp>

Eigen::Matrix3d lookat(Eigen::Vector3d to, Eigen::Vector3d up) {
    Eigen::Matrix3d out;
    out.col(2) = to.normalized();
    out.col(0) = to.cross(up).normalized();
    out.col(1) = out.col(0).cross(to).normalized();
    return out;
}

class RandomPixelSelector {
   public:
    template <typename G>
    Eigen::Vector2d get_normalized_pixel(G& rng) {
        double rx = dist(rng);
        double ry = dist(rng);
        return {rx, ry};
    }

   private:
    std::uniform_real_distribution<double> dist;
};

class ListedPixelSelector {
   public:
    ListedPixelSelector(const std::vector<Eigen::Vector2d>& pixels)
        : pixels(pixels), dist(0, pixels.size() - 1) {
        assert(pixels.size() > 0);
    }

    template <typename G>
    Eigen::Vector2d get_normalized_pixel(G& rng) {
        return pixels[dist(rng)];
    }

   private:
    const std::vector<Eigen::Vector2d>& pixels;
    std::uniform_int_distribution<size_t> dist;
};

class WeightedListedPixelSelector {
   public:
    WeightedListedPixelSelector(const std::vector<Eigen::Vector2d>& pixels,
                                const std::vector<double> weights)
        : pixels(pixels), dist(begin(weights), end(weights)) {
        assert(pixels.size() > 0);
        assert(pixels.size() == weights.size());
    }

    template <typename G>
    Eigen::Vector2d get_normalized_pixel(G& rng) {
        return pixels[dist(rng)];
    }

   private:
    const std::vector<Eigen::Vector2d>& pixels;
    std::discrete_distribution<size_t> dist;
};

template <typename PixelSelector = RandomPixelSelector,
          typename ProgressReporter = NullProgressReporter>
class SimpleCamera {
   public:
    SimpleCamera(const Eigen::Vector3d& location,
                 const Eigen::Matrix3d orientation, double fovx, double fovy,
                 size_t rays, PixelSelector pixel_selector = {},
                 ProgressReporter reporter = {})
        : location(location), orientation(orientation), x0(-fovx / 2),
          y0(-fovy / 2), fovx(fovx), fovy(fovy), rays(rays),
          pixel_selector(pixel_selector), reporter(reporter) {}

    bool has_more_rays() const { return rays != 0; }

    template <typename G>
    std::pair<Ray, Eigen::Vector2d> generate_ray(G& rng) {
        Eigen::Vector2d pixel = pixel_selector.get_normalized_pixel(rng);
        auto d = Eigen::Vector3d(x0 + pixel(0) * fovx, y0 + pixel(1) * fovy, 1)
                   .normalized();
        if (rays > 0) --rays;
        ++reporter;
        return {{location, orientation * d}, pixel};
    }

    std::vector<SimpleCamera> split(size_t count) const {
        return std::vector<SimpleCamera>(count, {location, orientation, fovx,
                                                 fovy, rays / count,
                                                 pixel_selector, reporter});
    }

   private:
    Eigen::Vector3d location;
    Eigen::Matrix3d orientation;
    double x0, y0, fovx, fovy;
    size_t rays;
    PixelSelector pixel_selector;
    ProgressReporter reporter;
};

template <typename PixelSelector = RandomPixelSelector,
          typename ProgressReporter = NullProgressReporter>
class ParallelCamera {
   public:
    ParallelCamera(const Eigen::Vector3d& location,
                   const Eigen::Matrix3d orientation, double fovx, double fovy,
                   size_t rays, PixelSelector pixel_selector = {},
                   ProgressReporter reporter = {})
        : rays(rays), pixel_selector(pixel_selector), reporter(reporter) {
        ex = orientation * Eigen::Vector3d::UnitX() * fovx;
        ey = orientation * Eigen::Vector3d::UnitY() * fovy;
        ez = orientation * Eigen::Vector3d::UnitZ();
        corner = location - ex / 2 - ey / 2;
    }

    ParallelCamera(const Eigen::Vector3d& corner, const Eigen::Vector3d& ex,
                   const Eigen::Vector3d& ey, const Eigen::Vector3d& ez,
                   size_t rays, PixelSelector pixel_selector = {},
                   ProgressReporter reporter = {})
        : corner(corner), ex(ex), ey(ey), ez(ez), rays(rays),
          pixel_selector(pixel_selector), reporter(reporter) {}

    bool has_more_rays() const { return rays != 0; }

    template <typename G>
    std::pair<Ray, Eigen::Vector2d> generate_ray(G& rng) {
        Eigen::Vector2d pixel = pixel_selector.get_normalized_pixel(rng);
        if (rays > 0) --rays;
        ++reporter;
        return {{corner + ex * pixel(0) + ey * pixel(1), ez}, pixel};
    }

    std::vector<ParallelCamera> split(size_t count) const {
        return std::vector<ParallelCamera>(
          count, {corner, ex, ey, ez, rays / count, pixel_selector, reporter});
    }

   private:
    Eigen::Vector3d corner;
    Eigen::Vector3d ex, ey, ez;
    size_t rays;
    PixelSelector pixel_selector;
    ProgressReporter reporter;
};

template <typename PixelSelector = RandomPixelSelector,
          typename ProgressReporter = NullProgressReporter>
class MysticPanoramaCamera {
   public:
    MysticPanoramaCamera(const Eigen::Vector3d& location, double umu,
                         double phi, double phi_1, double phi_2, double theta_1,
                         double theta_2, size_t rays,
                         PixelSelector pixel_selector = {},
                         ProgressReporter reporter = {})
        : location(location),
          orientation(
            Eigen::AngleAxis(deg2rad(180. - phi), Eigen::Vector3d::UnitZ()) *
            Eigen::AngleAxis(acos(-umu), Eigen::Vector3d::UnitX())),
          phi_0(deg2rad(phi_1)), dphi(deg2rad(phi_2 - phi_1)),
          theta_0(deg2rad(theta_1)), dtheta(deg2rad(theta_2 - theta_1)),
          rays(rays), pixel_selector(pixel_selector), reporter(reporter) {}

    bool has_more_rays() const { return rays != 0; }

    template <typename G>
    std::pair<Ray, Eigen::Vector2d> generate_ray(G& rng) {
        Eigen::Vector2d pixel = pixel_selector.get_normalized_pixel(rng);
        double phi = phi_0 + pixel(0) * dphi;
        double theta = theta_0 + pixel(1) * dtheta;
        Eigen::Vector3d d = {-sin(phi) * sin(theta), -cos(phi) * sin(theta),
                             -cos(theta)};
        Eigen::Vector3d d_out = orientation * d;
        if (rays > 0) --rays;
        ++reporter;
        return {{location, d_out}, pixel};
    }

    std::vector<MysticPanoramaCamera> split(size_t count) const {
        return std::vector<MysticPanoramaCamera>(
          count, {location, orientation, phi_0, dphi, theta_0, dtheta,
                  rays / count, pixel_selector, reporter});
    }

   private:
    MysticPanoramaCamera(const Eigen::Vector3d& location,
                         const Eigen::Matrix3d& orientation, double phi_0,
                         double dphi, double theta_0, double dtheta,
                         size_t rays, PixelSelector pixel_selector = {},
                         ProgressReporter reporter = {})
        : location(location), orientation(orientation), phi_0(phi_0),
          dphi(dphi), theta_0(theta_0), dtheta(dtheta), rays(rays),
          pixel_selector(pixel_selector), reporter(reporter) {}

    Eigen::Vector3d location;
    Eigen::Matrix3d orientation;
    double phi_0, dphi, theta_0, dtheta;
    size_t rays;
    PixelSelector pixel_selector;
    ProgressReporter reporter;
};

template <typename PixelSelector = RandomPixelSelector,
          typename ProgressReporter = NullProgressReporter>
class VisIt3DCamera {
   public:
    VisIt3DCamera(const Eigen::Vector3d& view_normal,
                  const Eigen::Vector3d& focus, const Eigen::Vector3d& view_up,
                  double view_angle, double parallel_scale,
                  const Eigen::Vector2d& image_pan, double image_zoom,
                  bool perspective, size_t nx, size_t ny, size_t rays,
                  PixelSelector pixel_selector = {},
                  ProgressReporter reporter = {})
        : view_normal(view_normal.normalized()), view_up(view_up.normalized()),
          perspective(perspective),
          rays(rays), pixel_selector(pixel_selector), reporter(reporter) {

        // zoom taken from VTK manual
        if (perspective) {
            view_angle /= image_zoom;
        }
        parallel_scale /= image_zoom;
        auto logger = rayli::logging::get_logger("VisIt 3D Camera");

        double fovy2 = std::tan(M_PI * view_angle / 360);
        double fovx2 = fovy2 * (nx - 1) / (ny - 1);

        location = view_normal.normalized() * (parallel_scale / fovy2) + focus;

        logger->info("camera location: {}", location.transpose());
        logger->info("view normal: {}", view_normal.transpose());
        logger->info("view up: {}", view_up.transpose());

        view_right = view_normal.cross(view_up).normalized();

        sx = 2 * fovx2;
        sy = 2 * fovy2;
        x0 = -fovx2 * (1 + 2 * image_zoom * image_pan(0));
        y0 = -fovy2 * (1 + 2 * image_zoom * image_pan(1));

        spy = 2 * parallel_scale / (ny - 1);
        spx = -spy;
        double xp0 = -spx / 2.;
        double yp0 = -spy / 2.;
        left_bottom_corner = location + xp0 * view_right + yp0 * view_up;
        logger->info("left bottom corner: {}", left_bottom_corner.transpose());
        logger->info("xp0: {}, yp0: {}", xp0, yp0);
    }

    bool has_more_rays() const { return rays != 0; }

    template <typename G>
    std::pair<Ray, Eigen::Vector2d> generate_ray(G& rng) {
        Eigen::Vector2d pixel = pixel_selector.get_normalized_pixel(rng);
        if (perspective) {
            auto d = (-(x0 + sx * pixel(0)) * view_right + (y0 + sy * pixel(1)) * view_up -
                      view_normal)
                       .normalized();
            return {{location, d}, pixel};
        } else {
            auto loc = left_bottom_corner + (spx * pixel(0)) * view_right +
                       (spy * pixel(1)) * view_up;
            return {{loc, -view_normal}, pixel};
        }
    }

    std::vector<VisIt3DCamera> split(size_t count) const {
        return std::vector<VisIt3DCamera>(
          count, {location, left_bottom_corner, view_normal, view_up, view_right, perspective, sx, sy, x0, y0, spx, spy, rays / count, pixel_selector, reporter});
    }

   private:
   VisIt3DCamera(
       const Eigen::Vector3d& location,
       const Eigen::Vector3d& left_bottom_corner,
       const Eigen::Vector3d& view_normal,
       const Eigen::Vector3d& view_up,
       const Eigen::Vector3d& view_right,
       bool perspective,
       double sx,
       double sy,
       double x0,
       double y0,
       double spx,
       double spy,
       size_t rays,
       PixelSelector pixel_selector,
       ProgressReporter reporter) :
   location(location), left_bottom_corner(left_bottom_corner), view_normal(view_normal), view_up(view_up), view_right(view_right), perspective(perspective), sx(sx), sy(sy), x0(x0), y0(y0), spx(spx), spy(spy), rays(rays), pixel_selector(pixel_selector), reporter(reporter) {}

    Eigen::Vector3d location;
    Eigen::Vector3d left_bottom_corner;
    Eigen::Vector3d view_normal, view_up, view_right;
    bool perspective;
    double sx, sy;
    double x0, y0;
    double spx, spy;
    size_t rays;
    PixelSelector pixel_selector;
    ProgressReporter reporter;
};

template <typename ProgressReporter = NullProgressReporter>
class PointCamera {
   public:
    PointCamera(const Eigen::Vector3d& location,
                const Eigen::Vector3d& direction, size_t rays,
                ProgressReporter reporter = {})
        : location(location), direction(direction), rays(rays),
          reporter(reporter) {}

    bool has_more_rays() const { return rays != 0; }

    template <typename G>
    std::pair<Ray, size_t> generate_ray(G& rng) {
        if (rays > 0) --rays;
        ++reporter;
        return {{location, direction}, 0};
    }

    std::vector<PointCamera> split(size_t count) const {
        return std::vector<PointCamera>(
          count, {location, direction, rays / count, reporter});
    }

   private:
    Eigen::Vector3d location, direction;
    size_t rays;
    ProgressReporter reporter;
};
