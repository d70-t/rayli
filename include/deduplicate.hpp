#pragma once

#include <unordered_map>

/** \brief converts an iterator of values into and index iterator and a
 * deduplicated iterator
 *
 * \param first iterator to the start of the data
 * \param last iterator to the end of the data
 * \param out_idx iterator to the stored indices (must be at least of size
 * std::distance(first, last)
 * \param out_data iterator to the stored data (must be able to store the number
 * of unique elements between first and last)
 */
template <typename InputIt, typename OutputIdxIt, typename OutputDataIt>
void deduplicate(InputIt first, InputIt last, OutputIdxIt out_idx,
                 OutputDataIt out_data) {
    std::unordered_map<typename InputIt::value_type, size_t> val2idx;
    size_t current_idx = 0;
    auto iit = first;
    for (; iit != last; ++iit) {
        auto current_data = *iit;
        auto [it, is_new] = val2idx.try_emplace(current_data, current_idx);
        if (is_new) {
            ++current_idx;
            *out_data++ = current_data;
        }
        *out_idx++ = it->second;
    }
}
