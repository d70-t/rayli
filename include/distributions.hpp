#pragma once

#include <random>

#include "Eigen.hpp"

// many of these functions come from PBRT www.pbrt.org

template <size_t size, typename Scalar, typename G>
Eigen::Matrix<Scalar, size, 1> uniform_sample_nd(G& rng) {
    Eigen::Matrix<Scalar, size, 1> out;

    std::uniform_real_distribution<Scalar> dist(0, 1);
    for (size_t i = 0; i < size; ++i) {
        out(i) = dist(rng);
    }
    return out;
}

template <typename Scalar>
Eigen::Matrix<Scalar, 2, 1>
uniform_sample_disk(const Eigen::Matrix<Scalar, 2, 1>& u) {
    Scalar r = std::sqrt(u(0));
    Scalar theta = 2 * M_PI * u(1);
    return {r * std::cos(theta), r * std::sin(theta)};
}

template <typename Scalar>
Eigen::Matrix<Scalar, 2, 1>
concentric_sample_disk(const Eigen::Matrix<Scalar, 2, 1>& u) {
    // Map uniform random numbers to $[-1,1]^2$
    Eigen::Matrix<Scalar, 2, 1> uOffset =
      Scalar{2} * u - Eigen::Matrix<Scalar, 2, 1>{1, 1};

    // Handle degeneracy at the origin
    if (uOffset(0) == 0 && uOffset(1) == 0) return {0, 0};

    // Apply concentric mapping to point
    Scalar theta, r;
    if (std::abs(uOffset(0)) > std::abs(uOffset(1))) {
        r = uOffset(0);
        theta = (M_PI / 4) * (uOffset(1) / uOffset(0));
    } else {
        r = uOffset(1);
        theta = (M_PI / 2) - (M_PI / 4) * (uOffset(0) / uOffset(1));
    }
    return r * Eigen::Matrix<Scalar, 2, 1>{std::cos(theta), std::sin(theta)};
}

template <typename Scalar>
Eigen::Matrix<Scalar, 3, 1>
cosine_sample_hemisphere(const Eigen::Matrix<Scalar, 2, 1>& u) {
    auto d = concentric_sample_disk(u);
    Scalar z = std::sqrt(std::max(Scalar{0}, 1 - d.squaredNorm()));
    return {d(0), d(1), z};
}
