#pragma once

#include <Eigen.hpp>
#include <random>

template <typename G>
Eigen::Vector2d uniform_sample_disk(G& rng) {
    auto dist = std::uniform_real_distribution(0., 1.);
    double r = std::sqrt(dist(rng));
    double theta = 2 * M_PI * dist(rng);
    return {r * std::cos(theta), r * std::sin(theta)};
}

template <typename G>
Eigen::Vector2d concentric_sample_disk(G& rng) {
    auto dist = std::uniform_real_distribution(-1., std::nextafter(1., 2.));
    auto x = dist(rng);
    auto y = dist(rng);
    if (x == 0 && y == 0) {
        return {0., 0.};
    }
    double theta, r;
    if (std::abs(x) > std::abs(y)) {
        r = x;
        theta = (M_PI / 4) * (y / x);
    } else {
        r = y;
        theta = (M_PI / 2) - (M_PI / 4) * (x / y);
    }
    return r * Eigen::Vector2d{std::cos(theta), std::sin(theta)};
}
