#include <GeometryUtils.hpp>
#include <Ray.hpp>

namespace rayli::intersection {

/** \brief Ray-Triangle intersection following Woop et al. 2013
 *
 * \sa http://jcgt.org/published/0002/01/05/
 * \sa PBRT Book v3, Pages 157ff
 */
template <typename Triangle, typename Scalar = double>
bool woop_triangle(const Ray& ray, const Triangle& triangle, Scalar& t,
                   Scalar tnear = 0,
                   Scalar tfar = std::numeric_limits<Scalar>::infinity()) {
    typedef Eigen::Matrix<Scalar, 3, 1> Vector;
    // translate ray and triangles such that coordinate system origin is
    // ray origin (this is of course implicit for the ray)
    Vector v0 = triangle.v(0) - ray.o;
    Vector v1 = triangle.v(1) - ray.o;
    Vector v2 = triangle.v(2) - ray.o;

    // permute dimensions such that ray direction is largest along z
    int kz = max_vector_component(ray.d);
    int kx = kz + 1;
    if (kx == 3) kx = 0;
    int ky = kx + 1;
    if (ky == 3) ky = 0;
    Vector d = permute(ray.d, kx, ky, kz);
    v0 = permute(v0, kx, ky, kz);
    v1 = permute(v1, kx, ky, kz);
    v2 = permute(v2, kx, ky, kz);

    // shear transformation to align ray on positive z axis
    // shear of z dimension is delayed as it might not be needed
    Scalar sx = -d(0) / d(2);
    Scalar sy = -d(1) / d(2);

    v0(0) += sx * v0(2);
    v0(1) += sy * v0(2);
    v1(0) += sx * v1(2);
    v1(1) += sy * v1(2);
    v2(0) += sx * v2(2);
    v2(1) += sy * v2(2);

    // now the intersection must be somewhere on the z-axis, if any
    // this corresponds to the point (0, 0) in the projection on the x-y-plane
    // to check if this point is inside the triangle, the singned distances
    // between this point and the edges can be used. In this specific coordinate
    // frame, they are simple to calculate and called (e0, e1, e2):
    Scalar e0 = v1(0) * v2(1) - v1(1) * v2(0);
    Scalar e1 = v2(0) * v0(1) - v2(1) * v0(0);
    Scalar e2 = v0(0) * v1(1) - v0(1) * v1(0);

    if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e2 > 0)) {
        // if not all signs are equal, the ray misses the triangle
        return false;
    }
    Scalar det = e0 + e1 + e2;
    if (det == 0) {
        // hitting parallel through face, reported as false but neighbor with
        // different orientation will report true
        return false;
    }

    // the final t value can be found by barycentric interpolation of the
    // transformed vertex z values, which now have to be computed:
    Scalar sz = Scalar{1} / d(2);
    v0(2) *= sz;
    v1(2) *= sz;
    v2(2) *= sz;

    Scalar tscaled = e0 * v0(2) + e1 * v1(2) + e2 * v2(2);
    double t_intersect = tscaled / det;
    if (t_intersect < tnear || t_intersect > tfar) {
        return false;
    }
    t = t_intersect;
    return true;
}
}  // namespace rayli::intersection
