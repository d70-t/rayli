#include <GeometryUtils.hpp>
#include <Ray.hpp>

#include <numeric>

namespace rayli::intersection {

/** \brief Ray-Triangle intersection following Ertl
 *
 * \sa http://www.iue.tuwien.ac.at/phd/ertl/node114.html
 */
template <typename Triangle, typename Scalar = double>
bool ertl_triangle(const Ray& ray, const Triangle& triangle, Scalar& t,
                   Scalar tnear = 0,
                   Scalar tfar = std::numeric_limits<Scalar>::infinity()) {
    typedef Eigen::Matrix<Scalar, 3, 1> Vector;

    std::array<Scalar, 3> signed_areas;
    for (int i0 = 0; i0 < 3; ++i0) {
        int i1 = (i0 + 1) % 3;
        int i2 = (i1 + 1) % 3;
        Vector x1 = triangle.v(i1);
        Vector x2 = triangle.v(i2);
        // fix anticomutativity according to:
        // http://www.iue.tuwien.ac.at/phd/ertl/node114.html
        Scalar sign = 1;
        if (!(x1(0) < x2(0) ||
              (x1(0) == x2(0) &&
               (x1(1) < x2(1) || (x1(1) == x2(1) && x1(2) < x2(2)))))) {
            std::swap(x1, x2);
            sign = -1;
        }
        signed_areas[i0] = sign * (x1 - ray.o).cross(x2 - ray.o).dot(ray.d);
    }
    if (std::all_of(signed_areas.begin(), signed_areas.end(),
                    [](auto a) { return a <= 0; }) ||
        std::all_of(signed_areas.begin(), signed_areas.end(),
                    [](auto a) { return a >= 0; })) {
        // ray intersects triangle
        double inv_total_area =
          1. / std::accumulate(signed_areas.begin(), signed_areas.end(), 0);
        Vector p = Vector::Zero();
        for (int i = 0; i < 3; ++i) {
            p += (signed_areas[i] * inv_total_area) * triangle.v(i);
        }
        Vector d = p - ray.o;
        int idx = max_vector_component(ray.d);
        t = d[idx] / ray.d[idx];
        return t >= tnear && t <= tfar;
    }
    return false;
}
}  // namespace rayli::intersection
