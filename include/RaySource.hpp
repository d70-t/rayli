#pragma once

#include "Ray.hpp"

template <typename G, typename Recorder>
class RaySource {
   public:
    typedef G rng_t;
    typedef Recorder recorder_t;

    virtual ~RaySource() {}

    virtual Recorder& get_recorder() = 0;
    virtual Ray create(G g, size_t ix, size_t iy) = 0;
};
