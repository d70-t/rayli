RayLi Ray based Light transport {#mainpage}
===============================

The RayLi Light transport system aims to become an atmospheric radiance solver.
As such, it is focussed on "participating medium" however, surface are also supported.

[![pipeline status](https://gitlab.lrz.de/ri96kit/rayli/badges/master/pipeline.svg)](https://gitlab.lrz.de/ri96kit/rayli/commits/master) [![coverage report](https://gitlab.lrz.de/ri96kit/rayli/badges/master/coverage.svg)](https://gitlab.lrz.de/ri96kit/rayli/commits/master)

![cloud 3d effects](figures/cloud_3d_effects.png)

Output created by `examples/cloud_3d_effects.cpp`.


## Project roots

The system is based on [PBRT](http://www.pbrt.org/) for its structure and [libRadtran](http://libradtran.org) for important additions for atmospheric simulation.

## Basic Structure

RayLi is designed as a ray tracing library with a lot of template usage. This approach allows to build your own application specific ray tracer, the compiler will be able to remove code wich is not used in your application. For that reason, there is no central executable, but every user is expected to build his/her own executables. A few examples can be found in the `examples` directory.

A basic building block in RayLi is the volume grid (`vgrid`).
A `vgrid` describes a set of enclosed volumina in space, each identified by a unique id.
The volumina of a `vgrid` can be separated by surfaces, which again are identified by different unique ids.
After a `vgrid` has been set up, a `Ray` can be traced through it using the `walk_along` method, leading to a series of `VolumeSlice` and `SurfaceSlice`, which describe which volumes and surfaces are crossed by the `Ray` in order.

This can be used to implement Monte-Carlo pathtracing, for example as done by the `PathTracer`, which implements backward path tracing through a `vgrid`.
In addition to the geometry information provided by the grid, the tracer also needs optical properties of the atmosphere and the surfaces.
These can be provided by `texture` objects, which can roughtly be imagined as arrays, but can also be evaluated lazily.

## Compilation and Docs

RayLi can be build with and without the conan package manager. In both cases, the build can be performed out of tree and will use CMake as build tool.

### without conan
Building without conan requires to install several dependencies by hand.
For the core library, this is [Eigen](http://eigen.tuxfamily.org), [fmt](https://fmt.dev/) and [spdlog](https://github.com/gabime/spdlog).
Currently Eigen is searched via `find_package`, while fmt and spdlog are automatically fetched from Github during the build.
Most of the examples additionally require [netCDF C++](http://unidata.github.io/netcdf-cxx4/index.html).
To build without conan, please do the following:

```bash
mkdir build
cd build
cmake ..
make
make doc
```

You will find the generated Doxygen-based documentation inside the `html` folder.

#### tests
If you want to build and execute tests as well, this is a good way to do it:

```bash
mkdir build
cd build
cmake .. -DRAYLI_BUILD_TESTS=ON -DRAYLI_BUILD_FOR_COVERAGE=ON -DRAYLI_BUILD_ASAN=ON -DRAYLI_BUILD_UBSAN=ON
make
bin/run_test
```

#### CMake options

| option | values | description |
| ---- | ---------- | --- |
| `RAYLI_USE_CONAN` | `ON` / `OFF` | enable / disable conan (should be set automatically) |
| `RAYLI_BUILD_SHARED` | `ON` / `OFF` | if RayLi should be build as shared library |
| `RAYLI_BUILD_PYTHON_BINDINGS` | `ON` / `OFF` | if python bindings should be generated (requires shared build) |
| `PYBIND11_PYTHON_VERSION` | [python version number](https://pybind11.readthedocs.io/en/stable/compiling.html#configuration-variables) | which python version should be used for the bindings |
| `RAYLI_BUILD_IO` | `ON` / `OFF` | if input / output routines should be build (may require additional libraries for file formats) |
| `RAYLI_BUILD_TESTS` | `ON` / `OFF` | if tests should be built |
| `RAYLI_BUILD_BENCHMARK` | `ON` / `OFF` | if benchmarks should be built |
| `RAYLI_BUILD_FOR_COVERAGE` | `ON` / `OFF` | build with annotations for coverage report |
| `RAYLI_BUILD_ASAN` | `ON` / `OFF` | build with [address sanitizer](https://clang.llvm.org/docs/AddressSanitizer.html) enabled |
| `RAYLI_BUILD_UBSAN` | `ON` / `OFF` | build with [undefined behaviour sanitizer](https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html) enabled |


### with conan
Building RayLi with conan allows to automatically install add dependencies and may be the easiest method to build RayLi with all components.
However setting up conan initially can be a bit of a burden as well and not all dependecies are (yet) available on conan-center.
In order to build via conan, please create a separate build folder and build it as follows:

```bash
mkdir build
cd build
conan install ..
conan build ..
make doc
```

The conan build exposes some of the CMAKE options as conan build options as well, they can be added to the conan command line as:

```bash
conan install .. -o rayli:<name>=<value>
```

The following options are available:

| name | values | description |
| ---- | ---------- | --- |
| `test` | `True` / `False` | `RAYLI_BUILD_TESTS` |
| `asan` | `True` / `False` | `RAYLI_BUILD_TESTS` |
| `ubsan` | `True` / `False` | `RAYLI_BUILD_TESTS` |
| `coverage` | `True` / `False` | `RAYLI_BUILD_TESTS` |
| `python_bindings` | `True` / `False` | `RAYLI_BUILD_TESTS` |
| `python_version` | `2.7` / `3.5` / `3.6` / `3.7` / `3.8` / `auto` | `PYBIND11_PYTHON_VERSION` |
