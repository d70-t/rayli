# BackwardsPathTracer

The BackwardsPathTracer is a [Monte-Carlo](https://en.wikipedia.org/wiki/Monte_Carlo_integration) style integrator for the radiative transfer equation.
It uses the [reciprocity of light](https://en.wikipedia.org/wiki/Reciprocity_%28electromagnetism%29) and follows the light path backwards from the sensor / camera to the light source(es).

Thus, the integral which the BackwardsPathTracer solves is the radiance \f$ L \f$, which is received by a Sensor from direction \f$ \omega \f$.
This is the integral of all differential radiances directed to the sensor at the location \f$ t \f$ along the infinitely long path from the sensor, multiplied by the transmission from the respective location \f$ t \f$ back to the sensor (at location \f$ 0 \f$):
\f[
L(0, \omega) = \int_0^\infty T_{t \rightarrow 0}~\frac{dL_o(t, \omega)}{dt} dt
\f]
This can be expanded to:
\f[
L(0, \omega) = \int_0^\infty\int_0^t \sigma_t(t')~dt' \left( L_e(t, \omega) + \int_{\mathcal{S}^2} \sigma_s(t) \mathcal{P}(t, \omega, \omega') L(t, \omega') d\omega' \right) dt
\f]
where \f$ L_e(t, \omega) \f$ is radiance emitted at location \f$ t \f$ in the direction of the sensor and \f$ L(t, \omega') \f$ is the radiance incident to scattering events at location \f$ t \f$ and is recursively solved by the BackwardsPathTracer again.

Currently it is assumed that a volume differential either is a light emitting source \f$ \left( L_e(t, \omega) \neq 0 \right) \f$ or is scattering light \f$ \left( \sigma_s(t) \neq 0 \right) \f$, but not both at the same time.
Also is is assumed that only a few sources exist.
These restrictions might be lifted in future, but are reasonable for atmospheric scenes with solar illumination.
There is only one sun and the scattering of light from other sources than the sun at the sun's surface is negligible.

These two cases split up into primary rays and shadow rays (local estimates):
\f[
L(0, \omega) = L_{\rm primary}(0, \omega) + L_{\rm le}(0, \omega)
\f]
\f[
L_{\rm primary}(0, \omega) = \int_0^\infty T_{t \rightarrow 0}~\int_{\mathcal{S}^2} \sigma_s(t) \mathcal{P}(t, \omega, \omega') L(t, \omega') d\omega' dt
\f]
\f[
L_{\rm le}(0, \omega) = \sum_{e=1}^{N_e} \int_0^\infty T_{t \rightarrow 0}~L_e(t_e, \omega)\delta(t - t_e) dt = \sum_{e=1}^{N_e} T_{t_e \rightarrow 0}~L_e(t_e, \omega)
\f]
The \f$ \delta \f$ expresses that the emission of the single source happens at exactly one location \f$ t_e \f$ along the path, instead of beeing distributed along the path.

## Optical Path Length

Much of the following argumentation is done in the *optical path space*, so distances expressed in optical space are not counted in units of *meters* but in *optical thickness units*.
Optical path space can be understood as a homogeneous medium, where the [extinction](@ref extinction) \f$ \sigma_t \f$ is constant.
To convert *physical space* into optical space, the *physical distances* are extended or contracted while decreasing ord increasing the local *extinction* accordingly.
For a given distance \f$ d \f$ and a spatially vaying distribution of *extinction* \f$ \sigma_t(t) \f$ the corresponding *optical path lengh* (or *thickness*) \f$ \tau \f$ is given by:
\f[
\tau = \int_0^d \sigma_t(t) dt
\f]
If the *physical space* is discretized along the path into sections of constant *extinction*, the integral can be written as a sum:
\f[
\tau = \sigma_{t,1} \Delta{}t_1 + \sigma_{t,2} \Delta{}t_2 + ...
\f]
Where \f$ \sigma_{t,i} \f$ is the *extinction* in section \f$ i \f$ and \f$ t_i \f$ is the *physical* distance travelled in section \f$ i \f$.

## Extinction

[Extinction](@ref extinction) (out scattering and absorption) is handled differently on primary rays and local estimates (shadow rays).
The distinction to be made is that for primary rays, the ray's destination is not yet known, so we are free to choose an appropriate sampling pdf for the traced distance.
For shadow rays, the destinaion (the light source) is known.
Thus we ask for the *transmission* along a fully specified path and are not free to choose a sampling pdf.

### Primary rays

For primary rays, *extinction* is covered by randomly generating a free optical path length \f$ \tau' \f$ according to an [exponential distribution](http://en.cppreference.com/w/cpp/numeric/random/exponential_distribution):
\f[
P(\tau') = e^{-\tau'} \quad \forall x \in [0..\infty]
\f]
The expected value of this distribution is:
\f[
E[P(\tau')] = \int_0^\infty \tau' e^{-\tau'} d\tau' = 1
\f]
This corresponds to the expectance that on average a photon travels one optical thickness before being scattered or absorbed.
This probability distribution also exactly describes the *Bouguer-Lambert-Beer's Law* in the sense that the probability of generating an optical path length \f$ \tau' \f$ is equal to the *transmittance* from the Ray's starting point to a point at distance \f$ \tau' \f$.
Subsequently, if the travelling distance of generated Rays (each corresponding to an equal share of radiance) is distributed in this fashion, each point \f$ \tau' \f$ is expected to receive it's share of radiance according to the transmittance from the Ray's start until the point \f$ \tau' \f$.

To convert this location \f$ \tau' \f$ in *optical space* back into physical space, a walk (or more specifically a Volume::propagate) through the volume is performed.
Basically, this happens by inspecting each of the summands of the equation for \f$ \tau \f$ given [above](#Optical Path Length) in order while counting how much *optical* and *pysical* distance has been travelled so far.
Details of this approach are suspect of the individual Volume implementations.

This approach is a form of [importance sampling](@ref importancesampling) of the *transmittance* part of the radiative transfer equation.
In this case, the *transmittance* distribution can be sampled perfectly (the CDF is known analytically) and thus all path weights become equal to 1.

Sampling values of \f$ \tau \f$ can also be expressed by a variable transform:
\f[
L_\textrm{pri}(0, \omega) = \int_0^\infty dt ~ T_{t \rightarrow 0} ~ \frac{dL(t,\omega)}{dt}
\f]
\f[
d\tau = \sigma_t(t) dt
\f]
\f[
L_\textrm{pri}(0, \omega) = \int_0^\infty d\tau ~ e^{-\tau} \frac{1}{\sigma_t(t(\tau))} ~ \frac{dL(t(\tau),\omega)}{dt}
\f]
Using the standard Monte Carlo estimator, this transforms to:
\f[
L_\textrm{pri}(0, \omega) \approx \frac{1}{N} \sum_{i=1}^N \frac{e^{-\tau_i}}{e^{-\tau_i} \sigma_t(t(\tau_i))} ~ \frac{dL(t(\tau_i),\omega)}{dt} = \frac{1}{N} \sum_{i=1}^N \frac{1}{\sigma_t(t(\tau_i))}\frac{dL(t(\tau_i),\omega)}{dt}
\f]

### Local Estimates

For local estimates, the *transmittance* along the path must be accounted for as path weight.

## In Scattering

[In scattering](@ref inscattering) is the complicated part of backwards path tracing. 
It involves an integral over all incoming light at the observed scattering location:
\f[
\frac{dL(t, \omega)}{dt} = \int_{\mathcal{S}^2} \sigma_s(t) \mathcal{P}(t, \omega, \omega') L(t, \omega') d\omega'
\f]
where \f$ \mathcal{P}(t, \omega, \omega') \f$ is the scattering phase function at location \f$ t \f$.
To simplify this forumla a bit, \f$ \sigma_s \f$ can be absorbed into the phase function:
\f[
f(t, \omega, \omega') = \sigma_s(t) \mathcal{P}(t, \omega, \omega')
\f]
\f[
\frac{dL(t, \omega)}{dt} = \int_{\mathcal{S}^2} f(t, \omega, \omega') L(t, \omega') d\omega'
\f]
The incident radiance \f$ L(t, \omega') \f$ can be seen as the sum of direct lighting and indirect (scattered) lighting:
\f[
L(t, \omega') = L_d(t, \omega') + L_s(t, \omega')
\f]
This allows to split the in-scattering integral into two parts.
\f[
\frac{dL(t, \omega)}{dt} = \int_{\mathcal{S}^2} f(t, \omega, \omega') L_d(t, \omega') d\omega' + \int_{\mathcal{S}^2} f(t, \omega, \omega') L_s(t, \omega') d\omega'
\f]
For the case of a point light source (which may be infinitely far away), the angular distribution of the direct lighting term is a delta distribution:
\f[
\int_{\mathcal{S}^2} f(t, \omega, \omega') L_d(t, \omega') \delta(\omega_e - \omega') d\omega' = f(t, \omega, \omega_e) L_d(t, \omega_e)
\f]
where \f$ \omega_e \f$ is the direction from scattering location to light source.
Together with the transmission from emitter to scattering location, this term forms the *local estimate*:
\f[
L_d(t, \omega_e) = T_{p_e \rightarrow t} L_e(p_e, \omega_e)
\f]
Where \f$ T_{p_e \rightarrow t} \f$ is the transmission from the location \f$ p_e \f$ of the emitter to the scattering location \f$ t \f$ and \f$ L_e(p_e, \omega_e) \f$ is the emission of the light source at location \f$ p_e \f$ in direction \f$ \omega_e \f$.
If the light source is not a point source, this integral can be solved using [multiple importance sampling](@ref mis).

The other integral over scattered lighting is more involved.
Generally, the incoming radiance from other scattering events \f$ L_s \f$ is not known in advance¹, so [importance sampling](@ref importancesampling) with (phase-function-dependent) pdf \f$ p_{t, \omega}(\omega') \f$ is only applied over the scattering function \f$ f(t, \omega, \omega') \f$, leading to the following estimator:
\f[
\int_{\mathcal{S}^2} f(t, \omega, \omega') L_s(t, \omega') d\omega' \approx \frac{1}{N} \sum_{i=1}^N \frac{f(t, \omega, \omega'_i)}{p_{t, \omega}(\omega'_i)} L_s(t, \omega'_i)
\f]
Typically, \f$ N \f$ is chosen to be 1, in order not to exponentially increase the amount of traced rays.
However in some conditions (e.g. possibility of spikes as described in VROOM paper), a larger value for \f$ N \f$ can be chosen.
This is typically called splitting.

The full estimator for \f$ L_\textrm{pri} \f$ now reads:
\f[
L_\textrm{pri}(0, \omega) \approx \frac{1}{N} \sum_{i=1}^N \frac{1}{\sigma_t(t)}\left(\frac{1}{M} \sum_{j=1}^M \frac{f(t, \omega, \omega'_j)}{p_{t, \omega}(\omega'_j)} L_s(t, \omega'_j) + f(t, \omega, \omega_e) T_{p_e \rightarrow t} L_e(p_e, \omega_e) \right)
\f]

If more than one scattering species is present, \f$ f(t, \omega, \omega') \f$ has to be replaced by a sum over all species:
\f[
f(t, \omega, \omega') = \sum_k f_k(t, \omega, \omega')
\f]
However, to allow efficient [monte carlo integration](@ref mcint), a method efficiently generate samples for the new function must be provided as well.
How this can be done in a convenient way is shown in [calculations on scattering functions](@ref scatfcalc).

## Footnotes

¹ There are approximative methods like [photon mapping](https://de.wikipedia.org/wiki/Photon_Mapping), which generate a map of indirect lighting before the actual raytracing step.
Also [VROOM](https://www.sciencedirect.com/science/article/pii/S0022407310003791) includes some heuristics which increase sampling efficiency for indirect lighting and thus reduce variance.

