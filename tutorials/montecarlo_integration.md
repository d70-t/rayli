Monte Carlo Integration
=======================

# Monte Carlo Integration {#mcint}

The goal of [Monte-Carlo](https://en.wikipedia.org/wiki/Monte_Carlo_integration) integration is to solve integrals by drawing samples from a specially crafted probability distribution.
This method is very effective for higher dimensional integrals, but for simplicity, we start with a 1D version: \f$ \int_a^b f(x) dx \f$ over the domain \f$ [a, b] \f$.
Using \f$ N \f$ samples from a random uniform distribution \f$ X_i \in [a, b] \f$ an estimator for the integral value can be found as:
\f[
F_N = \frac{b - a}{N} \sum_{i=1}^{N} f(X_i)
\f]
The expected value of \f$ E[F_N] \f$ is equal to the integrals value:
\f{eqnarray*}{
E[F_N] & = & E\left[ \frac{b - a}{N} \sum_{i=1}^{N} f(X_i)  \right] \\
& = & \frac{b - a}{N} \sum_{i=1}^{N} E\left[ f(X_i) \right] \\
& = & \frac{b - a}{N} \sum_{i=1}^{N} \int_a^b f(x) p(x) dx \\
& \stackrel{p(x) = 1/(b-a)}{=} & \frac{1}{N} \sum_{i=1}^{N} \int_a^b f(x) dx \\
& = & \int_a^b f(x) dx
\f}
This allows to approximate the integrals value by generating random samples and evaluating \f$ f \f$ only at some sample points, sidestepping the need to explicitly evaluate the integral.
Also the quality of the approximation can be improved, simply by generating and evaluating more samples.

An important generalization of this method is to change the probability distribution from uniform to any arbitrary distribution \f$ p(x) \f$:
\f[
F_N = \frac{1}{N} \sum_{i=1}^N \frac{f(X_i)}{p(X_i)}
\f]

Thus, to evaluate an integral of the form \f$ F = \int f(x) dx \f$, it is required to generate samples \f$ X_i \f$ and to evaluate both, the integrand \f$ f(X_i) \f$ and the sampling distribution \f$ p(X_i) \f$ at the location of the generated sample.
While a common case is to evaluate all of these three quantities at once, importance sampling (see below) and other applications can require to evaluate only some of these results.
The interface for a Monte Carlo integable function should thus allow for all of these cases.

## Efficiency of Monte Carlo integration

The *efficiency* \f$ \epsilon[F] \f$ of an estimator \f$ F \f$ can be defined as:
\f[
\epsilon[F] = \frac{1}{V[F] T[F]}
\f]
where \f$ V[F] \f$ is the variance and \f$ T[F] \f$ is the running time of the estimator.
Thus creating a better Monte Carlo estimator can be accieved by either reducing the variance or reducing the running time.

## Importance Sampling {#importancesampling}

Importance sampling is a method of improving the efficiency of [Monte-Carlo](https://en.wikipedia.org/wiki/Monte_Carlo_integration) integrators by reducing its variance, preferably without increasing the running time.
The general idea is to create many samples in important (larger valued) regions of the integrand, while skipping samples in less important regions.

A possible choice for the sampling pdf is one that is proportional to the integrand: \f$ p(x) \propto f(x) \f$ or \f$ p(x) = c f(x) \f$.
As probability distributions need to be normalized, this can be solved for \f$ c \f$:
\f[
c = \frac{1}{\int f(x) dx}
\f]
Inserting this into the definition of the Monte Carlo estimator shows that using this pdf, a single sample is sufficient to find the exact solution of the integral:
\f{eqnarray*}{
F_N & = & \frac{1}{N} \sum_{i=1}^N \frac{f(X_i)}{\frac{1}{\int f(x) dx} f(X_i)} \\
& = & \frac{1}{N} \sum_{i=1}^N \int f(x) dx \\
& = & \int f(x) dx
\f}
The value of the estimator is the correct solution of the integral, independently of the number of samples and as such, a single sample is enough to solve the integral.
This however does not come without a caveat.
Generating samples out of a specially crafted probability distribution from a (generally available) uniform distribution efficiently (important to not increase the running time) requires the knowledge of the inverse cumulative probability distribution.
The cumulative probability distribution is defined as:
\f[
{\rm CDF}[x] = \int_{-\infty}^x p(x') dx' = c \int_{-\infty}^x f(x') dx'
\f]
And thus already requires the solution of the integral in question.

However even generating samples from probability distributions similar to \f$ c f(x) \f$ yields results with lower variance as generating them from an arbitrary pdf.
Fortunately, for many physically interesting integrands, it is still possible to guess such a pdf.

## Multiple Importance Sampling {#mis}

There are cases where more than one potentially good strategy of generating samples exists.
A notable case is if the integrand \f$ f(x) \f$ is composed of multiple components (e.g. sum of mutliple scatterers, product of light source and scattering phase function).
In these cases, a strategy to combine samples from multiple distributions must be found which should reduce the total variance as much as possible, without generating too much extra work.
Eric Veach studied multiple solutions to this problems in Chapter 9 of his [thesis](https://graphics.stanford.edu/courses/cs348b-03/papers/veach-chapter9.pdf).

Generally, the problem consists of one integrand \f$ f(x) \f$ and \f$ n \f$ sampling distributions \f$ p_1(x) ... p_n(x) \f$.
He distingushes two solutions:

### The multi-sample estimator {#multimis}

\f[
F = \sum_{i=1}^n \frac{1}{n_i} \sum_{j=1}^{n_i} w_i(X_{ij}) \frac{f(X_{ij})}{p_i(X_{ij})}
\f]

In this case, \f$ n_i \ge 1 \f$ samples: \f$ X_{i1} ... X_{in_i} \f$ are taken from the \f$ i \f$-th distribution and \f$ f(x) \f$ is evaluated for all of them.
Using the weights \f$ w_i(x) \f$ the function evaluations from each sample are combined in a way to reduce the overall variance.
The weights must obey the following conditions:
\f[
\sum_{i=1}^n w_i(x) = 1 \quad \textrm{whenever} \quad f(x) \ne 0
\f]
\f[
w_i(x) = 0 \quad \textrm{whenever} \quad p_i(x) = 0
\f]
which implies that at every point with \f$ f(x) \ne 0 \f$, at least one sampling distribution must be able to generate samples with finite probability.
One way to choose the weights is the power heuristic with \f$ \beta = 2 \f$, which according to Veach's thesis shows very good results:
\f[
w_i(x) = \frac{(n_i p_i(x))^\beta}{\sum_k (n_k p_k(x))^\beta}
\f]

A downside of the multi-sample estimator is the requirement of generating more than one sample.
In case of Monte Carlo path tracing, this would quickly result in an exponential growth of ray paths.
To circumvent this problem, there is also the one-sample model:

### The one-sample model {#singlemis}

In this model, one of the sampling distributions (call it \f$ I \f$) is randomly selected according to probabilities \f$ c_1 ... c_n \f$ and a single sample is taken from this distribution.
The integral is then estimated as:
\f[
F = \frac{w_I(X_I)f(X_I)}{c_Ip_I(X_I)}
\f]
In this case, Veach shows that the balance heuristic provides optimal variance:
\f[
w_i(x) = \frac{c_ip_i(x)}{\sum_k c_kp_k(x)}
\f]
