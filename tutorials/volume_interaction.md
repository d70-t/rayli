Volume Interactions
===================

# Volume Interactions {#volumeinteractions}

Volume Interaction can be categorized into 3 main processes:

* Absorption (Removal of light along path and conversion into different form of Energy)
* Emission (Volume particals produce light)
* Scattering (Light changes direction)

## Absorption {#absorption}

Absorption can be described by the absorption cross section \f$ \sigma_a \f$, which is the probability of absorbing light along a unit distance through the medium. The differential radiance can thus be described by:
\f[
dL = -\sigma_a L_i dt
\f]
Where \f$ dL \f$ is the differential change in radiance, \f$ L_i \f$ is the incoming radiance and \f$ dt \f$ is the differential of travelled time / distance.
This equation can be solved for a path to the integral form:
\f[
L_o = L_i e^{-\int_0^d \sigma_a(t) dt}
\f]

## Emission {#emission}

Emission along the path is \f$ dL_o = L_e dt \f$. It is currently not supported in rayli.

## Out Scattering {#outscattering}

Out Scattering is the removal of light from the observed path by changing the propagation direction of the light out of the observed path. Similarly to absorption, this can be described by the *scattering cross section* \f$ \sigma_s \f$, which describes the probability of scattering per unit length of travelled path. The radiance differential is accordingly:
\f[
dL = -\sigma_s L_i dt
\f]

## Extinction {#extinction}

\f$ \sigma_s \f$ and \f$ \sigma_a \f$ are commonly described together as *attenuation* or *extinction*:
\f[
\sigma_t = \sigma_a + \sigma_s
\f]
Related quantities are the *single scattering albedo* \f$ \rho \f$:
\f[
\rho = \frac{\sigma_s}{\sigma_t}
\f]
And the *mean free path* \f$ 1/\sigma_t \f$ describing the average distance which light can travel before it hits any particle.

Extinction along a differental path can thus be described as:
\f[
\frac{dL_o}{dt} = -\sigma_t L_i
\f]
In its integral form, it yields the *transmittance*:
\f[
T = e^{-\int_0^d \sigma_t(t) dt}
\f]
The *transmittance* has some useful properties:
* The transmittance from a point to itself (distance = 0) is 1.
* It is multiplicative, meaning if light goes from \f$ A \f$ through \f$ B \f$ to \f$ C \f$, the transmittance \f$ T_{AC} = T_{BC} T_{AB} \f$.

The exponent (without the negation) is also called optical thickness \f$ \tau \f$:
\f[
\tau = \int_0^d \sigma_t(t) dt
\f]
Or for a homogeneous medium:
\f[
\tau = \sigma_t d
\f]
Finally, the *transmittance* can be written in the form of *Bouguer-Lambert-Beer's Law*:
\f[
T = e^{-\tau}
\f]

## In Scattering {#inscattering}

While out scattering removes light, in scattering adds light to the observed path. The ammount of light added to the path is described by a scattering phase function \f$ p(\omega, \omega') \f$. 
It describes how much incoming radiance from direction \f$ \omega' \f$ is visible in the outgoing direction \f$ \omega \f$.
As any scattered light leaving the point must come from *somewhere*, the scattering phase function has to be normalized:
\f[
\int_{\mathcal{S}^2} p(\omega, \omega') d\omega' = \int_0^{2\pi} \int_0^\pi p(\omega, \omega') \sin\theta d\theta d\phi = 1
\f]

The change in radiance due to in scattering can subsequently be described by:
\f[
\frac{dL_o(\omega)}{dt} = \sigma_s \int_{\mathcal{S}^2} p(\omega, \omega') L_i(\omega') d\omega'
\f]
Where \f$ \sigma_s \f$ is again the *scattering cross section*, because only the part of the light which actually scatters can contribute to the outgoing radiance. 

# Phase Functions

## Isotropic scattering

The simplest phase function is the one for *isotropic scattering*.
Isotropic scattering means if light is scattered, every possible outgoing direction is equally probable.
The *phase function* must thus be constant.
Because of the normalization requirement, there can only one such function:
\f[
p_{\rm isotropic}(\omega, \omega') = \frac{1}{4\pi}
\f]
