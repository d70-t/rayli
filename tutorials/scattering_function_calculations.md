Scattering Function Calculations
================================

# Scattering Function Calculations {#scatfcalc}

Scattering functions \f$ f \f$ are used in integrals like:
\f[
\int_{\mathcal{S}^2} f(t, \omega, \omega') L(t, \omega') d\omega'
\f]
which are subject to [Monte Carlo Integration](@ref mcint).
To be usable in MC integration, the function \f$ f \f$ must additionally provide a method to draw samples \f$ X \f$ from a probability distribution \f$ p \f$ efficiently. \f$ p \f$ should be chosen in a way that the variance of resulting estimator is reduced.
The questions which arises is how can the sampling function and \f$ p \f$ be implemented efficiently for \f$ f = f_1 + f_2 \f$?
To answer this question, let's have a look at the estimator for the integral:
\f[
\int_{\mathcal{S}^2} \left( f_1(t, \omega, \omega') + f_2(t, \omega, \omega') \right) L(t, \omega') d\omega'
\f]
which is:
\f[
\frac{1}{N} \sum_{i=1}^N \frac{f_1(t, \omega, \omega'_i) + f_2(t, \omega, \omega'_i)}{p_{t, \omega}(\omega'_i)} L(t, \omega'_i)
\f]
First, we look at the case of \f$ N = 1 \f$, naming the chosen sample \f$ \omega'_I \f$:
\f[
\frac{f_1(t, \omega, \omega'_I) + f_2(t, \omega, \omega'_I)}{p_{t, \omega}(\omega'_I)} L(t, \omega'_I)
\f]
A solution can be found with the help of the [one-sample model](@ref singlemis) for multiple importance sampling, which suggests to randomly select one probability distribution according to some weights \f$ c_1 \f$ and \f$ c_2 \f$.
Then the estimator can be replaced by:
\f[
\frac{w_I(\omega'_I)(f_1(t, \omega, \omega'_I) + f_2(t, \omega, \omega'_I))}{c_Ip_{I,t,\omega}(\omega'_I)} L(t, \omega'_i)
\f]
yielding an implementation for \f$ p_{t,\omega}(\omega'_I) \f$:
\f[
p_{t,\omega}(\omega'_I) = \frac{c_Ip_{I,t,\omega}(\omega'_I)}{w_I(\omega'_I)} = \frac{c_Ip_{I,t,\omega}(\omega'_I) \sum_k c_kp_{k,t,\omega}(\omega'_I)}{c_Ip_{I,t,\omega}(\omega'_I)} =  \sum_k c_kp_{k,t,\omega}(\omega'_I)
\f]
For scattering functions, a natural weighting is the *scattering cross section* \f$ \sigma_s \f$.
So the weights can be chosen according to the following rule:
\f[
c_i \propto \sigma_{s,i}
\f]
\f[
\sum_i c_i = 1
\f]
\f[
\Rightarrow c_i = \frac{\sigma_{s,i}}{\sum_i \sigma_{s,i}}
\f]

## special case: perfectly sampled scattering function

In many cases, the scattering functions can be sampled perfectly, in particular because in an isotropic medium, the scattering function is only dependent on \f$ \mu = cos(\omega, \omega') \f$ and thus a 1D function which allows a relatively cheap tabulation and subsequently the inverse of the cumulative distribution can always be calculated.
In particular, now the scattering function reads:
\f[
f_i(t, \omega, \omega') = \sigma_{s,i} p_{i,t,\omega}(\omega')
\f]
If now the joint sampling distribution is chosen naturally as described above, the Monte Carlo Estimator reads:
\f[
\frac{1}{N} \sum_{i=1}^N \frac{\sum_{j=1}^M \sigma_{s,j} p_{j,t,\omega}(\omega'_i)}{\sum_{k=1}^M c_k p_{k,t,\omega}(\omega'_i)} L(t, \omega'_i) = \frac{1}{N} \sum_{i=1}^N \sum_{j=1}^M \sigma_{s,j} L(t, \omega'_i)
\f]
Thus in this case, the addition of scattering functions can be implemented by randomly selecting one scatterer with weights accorting to their contribution to the total scattering coefficient.
Afterwards, the sum of all scattering coefficient is used as coefficient for the scattering event.
