# BRDF coordinates

BRDFs are evaluated in a local coordinate system, where the z axis
is perpendicular to the surface and in and out directions are both
facing away from the surface.
