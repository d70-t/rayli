import os
from conans import ConanFile, CMake
VERSION = open(os.path.join(os.path.dirname(__file__), "version.txt")).read().strip()

class RayliConan(ConanFile):
    name = "rayli"
    version = VERSION
    license = "GPL"
    url = "https://gitlab.lrz.de/ri96kit/rayli"
    description = "Ray based Light Simulator"

    settings = "os", "compiler", "build_type", "arch"
    options = {
        "test": [True, False],
        "asan": [True, False],
        "ubsan": [True, False],
        "coverage": [True, False],
        "python_bindings": [True, False],
        "python_version": ["2.7", "3.5", "3.6", "3.7", "3.8", "auto"],
        "shared": [True, False],
         }
    requires = (
        "eigen/[>=3.3.4]",
        "spdlog/[>=1.0]",
        "fmt/[>=5.0.0]",
        "netcdf-cxx/[>=4.3]@mim/stable",
        "msgpack/[>=3.1.1]",
        )
    build_requires = ()
    generators = "cmake", "ycm", "virtualenv"
    default_options = (
        "test=False",
        "asan=False",
        "ubsan=False",
        "coverage=False",
        "python_bindings=True",
        "python_version=auto",
        "shared=True",
        )
    exports = "version.txt"
    exports_sources = "*", "!build*"


    def configure(self):
        if not self.options.shared:
            self.options.python_bindings = False

        self.options["hdf5"].shared = self.options.shared
        self.options["netcdf"].shared = self.options.shared
        self.options["netcdf-cxx"].shared = self.options.shared
        self.options["zlib"].shared = self.options.shared

    def requirements(self):
        if self.options.python_bindings:
            self.requires("pybind11/[>=2.2.3]")

    def build(self):
        cmake = CMake(self)
        defs = {
            "RAYLI_BUILD_TESTS": self.options.test,
            "RAYLI_BUILD_ASAN": self.options.asan,
            "RAYLI_BUILD_UBSAN": self.options.ubsan,
            "RAYLI_BUILD_FOR_COVERAGE": self.options.coverage,
            "RAYLI_BUILD_SHARED": self.options.shared,
            "RAYLI_BUILD_PYTHON_BINDINGS": self.options.python_bindings,
        }
        if self.options.python_version != "auto":
            defs["PYBIND11_PYTHON_VERSION"] = self.options.python_version
        cmake.configure(defs=defs)
        cmake.build()
        if self.options.test:
            self.run("bin/run_test")

    def package(self):
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="io/include")
        if self.options.shared:
            self.copy("*.dll", dst="bin", keep_path=False)
            self.copy("*.dylib*", dst="lib", keep_path=False)
            self.copy("*.so", dst="lib", keep_path=False)
        else:
            self.copy("*.lib", dst="lib", keep_path=False)
            self.copy("*.a", dst="lib", keep_path=False)
        self.copy("rayliy", dst="bin", keep_path=False)

    def package_info(self):
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.cpp_info.libs = ["rayli_common", "rayli_io", "stdc++fs"]
