# Terms in different communities

The RayLi light transport system is based on [PBRT](http://www.pbrt.org/) for its structure and [libRadtran](http://libradtran.org) for important additions for atmospheric simulation.
These two projects come from different communities (rendering and atmospheric radiative transfer) and thus sometime use different terms for roughly the same ideas. 
This table is an attempt to relate roughly similar terms in the two field.
The table might not always be accurate but should still assist in finding similarities.

| PBRT | libRadtran |
| ---- | ---------- |
| shadow ray | local estimate |
| participating medium | atmosphere |
| ray | photon (small parts of it) |
| Spectrum | [ALIS](http://dx.doi.org/10.1016/j.jqsrt.2011.03.018), spectral part |
| MIS (multiple importance sampling) on scattering events | [VROOM](http://dx.doi.org/10.1016/j.jqsrt.2010.10.005) DDIS (detector directional importance sampling) |
| splitting + shadow ray | [VROOM](http://dx.doi.org/10.1016/j.jqsrt.2010.10.005) NLE (n-tuple local estimate) |
| | [VROOM](http://dx.doi.org/10.1016/j.jqsrt.2010.10.005) PBS (prediction based splitting) |
| | [VROOM](http://dx.doi.org/10.1016/j.jqsrt.2010.10.005) PBRR (prediction based russian roulette) |
| | [VROOM](http://dx.doi.org/10.1016/j.jqsrt.2010.10.005) CS-VIS (circum-solar virtual importance sampling) |

