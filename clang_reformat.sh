#!/bin/bash

set -e

if [ "$1" == "check" ]; then
    OPTS="--Werror --dry-run"
else
    OPTS="-i"
fi

for folder in src include io c_wrapper test benchmark; do
    find $folder -iname "*.hpp" -o -iname "*.h" -o -iname "*.cpp" -exec clang-format -style=file $OPTS {} +
done
