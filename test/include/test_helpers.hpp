#pragma once
#include <stream_operators.hpp>
#include <vgrid/GridIterator.hpp>

#include <sstream>
#include <type_traits>
#include <utility>

template <typename T>
class Eq : public Catch::MatcherBase<T> {
    T truth;

   public:
    Eq(T truth) : truth(truth) {}

    bool match(const T& test) const override { return test == truth; }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "is " << truth;
        return ss.str();
    }
};

template <typename T>
auto as_matcher(T t) {
    if constexpr (std::is_base_of_v<Catch::Matchers::Impl::MatcherUntypedBase,
                                    T>) {
        return t;
    } else {
        return Eq<T>{t};
    }
}

template <typename T>
class VariantGetter {
    const T* ptr;

   public:
    template <typename V>
    VariantGetter(const V& v) : ptr(std::get_if<T>(&v)) {}
    const T* get() const { return ptr; }
};

template <typename T, typename M>
class VariantWith : public Catch::MatcherBase<VariantGetter<T>> {
    M matcher;

   public:
    VariantWith(M matcher) : matcher(matcher) {}

    bool match(const VariantGetter<T>& v) const override {
        if (auto p = v.get(); p) {
            return matcher.match(*p);
        } else {
            return false;
        }
    }

    virtual std::string describe() const override {
        return std::string("contains ") + matcher.toString();
    }
};

template <typename T, typename M>
auto variant_with(M matcher) {
    return VariantWith<T, decltype(as_matcher(matcher))>{as_matcher(matcher)};
}

template <typename MTNear, typename MTFar, typename MIdx>
class VolumeSliceMatcher : public Catch::MatcherBase<VolumeSlice> {
    MTNear tnear_matcher;
    MTFar tfar_matcher;
    MIdx idx_matcher;

   public:
    VolumeSliceMatcher(MTNear tnear_matcher, MTFar tfar_matcher,
                       MIdx idx_matcher)
        : tnear_matcher(tnear_matcher), tfar_matcher(tfar_matcher),
          idx_matcher(idx_matcher) {}

    bool match(const VolumeSlice& vs) const override {
        return tnear_matcher.match(vs.tnear) && tfar_matcher.match(vs.tfar) &&
               idx_matcher.match(vs.idx);
    }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "with tnear " << tnear_matcher.toString() << ", tfar "
           << tfar_matcher.toString() << ", idx " << idx_matcher.toString();
        return ss.str();
    }
};

class VecMatcher : public Catch::MatcherBase<Eigen::Vector3d> {
  Eigen::Vector3d ref;
  double atol;

  public:
    VecMatcher(Eigen::Vector3d ref, double atol = 1e-8) :
      ref(ref), atol(atol) {}

    bool match(const Eigen::Vector3d& val) const override {
      return (val - ref).cwiseAbs().maxCoeff() < atol;
    }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "expected " << ref.transpose() << " with absolute tolerance < " << atol;
        return ss.str();
    }
};

template <typename Mo, typename Md>
class RayMatcher : public Catch::MatcherBase<Ray> {
    Mo o_matcher;
    Md d_matcher;

   public:
    RayMatcher(Mo o_matcher, Md d_matcher)
        : o_matcher(o_matcher), d_matcher(d_matcher) {}

    bool match(const Ray& r) const override {
        return o_matcher.match(r.o) && d_matcher.match(r.d);
    }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "with o" << o_matcher.toString() << ", d " << d_matcher.toString();
        return ss.str();
    }
};


template <typename MT, typename MRay>
class TeleportMatcher : public Catch::MatcherBase<Teleport> {
    MT t_matcher;
    MRay ray_matcher;

   public:
    TeleportMatcher(MT t_matcher, MRay ray_matcher)
        : t_matcher(t_matcher), ray_matcher(ray_matcher) {}

    bool match(const Teleport& t) const override {
        return t_matcher.match(t.t) && ray_matcher.match(t.r);
    }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "with t " << t_matcher.toString() << ", ray "
           << ray_matcher.toString();
        return ss.str();
    }
};

template <typename MT, typename MIdx>
class SurfaceSliceMatcher : public Catch::MatcherBase<SurfaceSlice> {
    MT t_matcher;
    MIdx idx_matcher;

   public:
    SurfaceSliceMatcher(MT t_matcher, MIdx idx_matcher)
        : t_matcher(t_matcher), idx_matcher(idx_matcher) {}

    bool match(const SurfaceSlice& ss) const override {
        return t_matcher.match(ss.t) && idx_matcher.match(ss.idx);
    }

    virtual std::string describe() const override {
        std::ostringstream ss;
        ss << "with t " << t_matcher.toString() << ", idx "
           << idx_matcher.toString();
        return ss.str();
    }
};

template <typename T>
class MatchableSequence {
   public:
    template <typename U>
    MatchableSequence(const U& seq) {
        std::copy(seq.begin(), seq.end(), std::back_inserter(data));
    }

    bool match() const { return false; }

    std::vector<T> data;
};

template <typename M, typename S, std::size_t... idx>
bool match_all(const M& m, const S& s, std::index_sequence<idx...>) {
    return (std::get<idx>(m).match(s.data[idx]) & ... & true);
}

template <typename T, typename... Matchers>
class ElementsAre_ : public Catch::MatcherBase<MatchableSequence<T>> {
    std::tuple<Matchers...> matchers;

   public:
    ElementsAre_(Matchers... matchers) : matchers(matchers...){};

    bool match(const MatchableSequence<T>& seq) const override {
        return match_all(matchers, seq, std::index_sequence_for<Matchers...>{});
    }

    virtual std::string describe() const override {
        return std::apply(
          [](auto... matchers) {
              return (std::string("ElementsAre ") + ... +
                      (matchers.describe() + ", "));
          },
          matchers);
    }
};

template <typename T, typename... Matchers>
auto ElementsAre(Matchers... matchers) {
    return ElementsAre_<T, decltype(as_matcher(matchers))...>(
      as_matcher(matchers)...);
}

template <typename T, typename... Matchers>
auto VariantElementsAre(Matchers... matchers) {
    return ElementsAre_<T, decltype(as_matcher(variant_with<T>(matchers)))...>(
      as_matcher(variant_with<T>(matchers))...);
}

inline auto approx_vol_slice(double tnear, double tfar, size_t idx) {
    using namespace Catch::Matchers;
    return VolumeSliceMatcher(WithinRel(tnear), WithinRel(tfar), Eq(idx));
}

inline auto approx_vol_slice_variant(double tnear, double tfar, size_t idx) {
    return variant_with<VolumeSlice>(approx_vol_slice(tnear, tfar, idx));
}

inline auto approx_teleport(double t, Ray r, double atol=1e-8) {
    using namespace Catch::Matchers;
    return TeleportMatcher(WithinRel(t), RayMatcher(VecMatcher(r.o, atol), VecMatcher(r.d, atol)));
}

inline auto approx_teleport_variant(double t, Ray r, double atol=1e-8) {
    return variant_with<Teleport>(approx_teleport(t, r, atol));
}

inline auto approx_surf_slice(double t, size_t idx) {
    using namespace Catch::Matchers;
    return SurfaceSliceMatcher(WithinRel(t), Eq(idx));
}

inline auto approx_surf_slice_variant(double t, size_t idx) {
    return variant_with<SurfaceSlice>(approx_surf_slice(t, idx));
}

template <typename G>
void assert_is_grid(const G& g) {
    Ray r{{0, 0, 0}, {1, 0, 0}};
    auto path = g.walk_along(r, 0, std::numeric_limits<double>::infinity());
    auto start = path.begin();

    {
        using RayIterator = typename std::remove_cv<
          typename std::remove_reference<decltype(start)>::type>::type;
        using value_type =
          typename std::iterator_traits<RayIterator>::value_type;
        using reference = typename std::iterator_traits<RayIterator>::reference;
        // requirements from LegacyIterator
        static_assert(std::is_copy_constructible<RayIterator>::value,
                      "RayIterator is not copy constructible");
        static_assert(std::is_copy_assignable<RayIterator>::value,
                      "RayIterator is not copy assignable");
        static_assert(std::is_destructible<RayIterator>::value,
                      "RayIterator is not destructible");
        static_assert(std::is_swappable<RayIterator>::value,
                      "RayIterator is not swappable");
        /* TODO: check iterator traits */
        static_assert(std::is_same<RayIterator&, decltype(++start)>::value,
                      "RayIterator is not incrementable");

        // requirements from LegacyInputIterator
        static_assert(
          std::is_convertible<decltype(start == start), bool>::value,
          "RayIterator ist not equality comparable");
        static_assert(
          std::is_convertible<decltype(start != start), bool>::value,
          "!= does not work on RayIterator");
        static_assert(std::is_same<decltype(*start), reference>::value,
                      "dereferencing RayIterator does not lead to reference");
        static_assert(
          std::is_convertible<decltype(*start), value_type>::value,
          "dereferencing RayIterator: not convertible to value_type");
        static_assert(
          std::is_convertible<decltype(*start++), value_type>::value,
          "dereferencing RayIterator++: not convertible to value_type");
    }
}
