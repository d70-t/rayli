#include <catch2/catch.hpp>
#include <test_helpers.hpp>

#include "deduplicate.hpp"

#include <iterator>
#include <vector>

TEST_CASE("deduplicate deduplicate_a_vector") {
    std::vector<double> data = {1., 3., 4., 1., 1., 4., 0.};
    std::vector<size_t> idx;
    std::vector<double> dedup;

    deduplicate(data.begin(), data.end(), std::back_inserter(idx),
                std::back_inserter(dedup));
    REQUIRE_THAT(idx, ElementsAre<double>(0, 1, 2, 0, 0, 2, 3));
    REQUIRE_THAT(dedup, ElementsAre<double>(1., 3., 4., 0.));
}
