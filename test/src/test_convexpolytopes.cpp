#include <catch2/catch.hpp>
#include <test_helpers.hpp>

#include <Ray.hpp>
#include <vgrid/ConvexPolytopes.hpp>

using namespace rayli::vgrid;

TEST_CASE("convex_polytopes_plane intersect") {
    auto r = Ray{{0, 0, 1}, {0, 0, 1}};
    auto p = ConvexPolytopes::Plane{{0, 0, 1}, 3};
    REQUIRE(p.intersect(r).t == 2.);
}

TEST_CASE("convex_polytopes_plane intersect_tilted") {
    auto r = Ray{{0, 0, 1}, {0, 0, 1}};
    auto p = ConvexPolytopes::Plane{Eigen::Vector3d{0, 1, 1}.normalized(), 3};
    REQUIRE(p.intersect(r).t == 3. * sqrt(2.) - 1);
}

TEST_CASE("convex_polytopes_plane intersection_at_infinity_if_parallel") {
    auto r_inside = Ray{{0, 0, 3}, {0, 1, 0}};
    auto r_outside = Ray{{0, 0, 2}, {0, 1, 0}};
    auto p = ConvexPolytopes::Plane{Eigen::Vector3d{0, 0, 1}.normalized(), 3};
    REQUIRE(p.intersect(r_inside).t == std::numeric_limits<double>::infinity());
    REQUIRE(p.intersect(r_outside).t ==
            std::numeric_limits<double>::infinity());
}

TEST_CASE("convex_polytopes_plane distance_tilted") {
    auto p = ConvexPolytopes::Plane{Eigen::Vector3d{0, 1, 1}.normalized(), 3};
    REQUIRE(p.distance({0., 0., 0.}) == 3.);
    REQUIRE(p.distance({0., 0., -1.}) == 3. + sqrt(.5));
}

TEST_CASE("convex_polytopes_plane negative_distance") {
    auto p = ConvexPolytopes::Plane{{1, 0, 0}, -3};
    REQUIRE(p.distance({0., 0., 0.}) == -3.);
}

ConvexPolytopes unit_box() {
    auto grid = polytopes_from_hexahedra({{0., 0., 0.},
                                          {1., 0., 0.},
                                          {1., 1., 0.},
                                          {0., 1., 0.},
                                          {0., 0., 1.},
                                          {1., 0., 1.},
                                          {1., 1., 1.},
                                          {0., 1., 1.}},
                                         {{0, 1, 2, 3, 4, 5, 6, 7}});
    grid.is_convex = true;
    return grid;
}

ConvexPolytopes tilted_box() {
    auto grid = polytopes_from_hexahedra({{0., 0., 0.},
                                          {1., 0., 0.},
                                          {1., 1., 0.},
                                          {0., 1., 0.},
                                          {-1., 0., 1.},
                                          {0., 0., 1.},
                                          {0., 1., 1.},
                                          {-1., 1., 1.}},
                                         {{0, 1, 2, 3, 4, 5, 6, 7}});
    grid.is_convex = true;
    return grid;
}

ConvexPolytopes small_fish() {
    Eigen::Vector3d o = {0., 0., 0.};
    Eigen::Vector3d a = {1., 0., 0.};
    Eigen::Vector3d b = {0.5, sqrt(1. - 0.25), 0.};
    Eigen::Vector3d c = {0., 0., 1.};

    auto grid = polytopes_from_wedges(
      {o, o + c, o + a, o + a + c, o + b, o + b + c, o + a + b, o + a + b + c,
       o - a + 2 * b, o - a + 2 * b + c, o + 2 * b, o + 2 * b + c},
      {{0, 2, 4, 1, 3, 5},
       {2, 6, 4, 3, 7, 5},
       {6, 10, 4, 7, 11, 5},
       {4, 10, 8, 5, 11, 9}});
    grid.is_convex = true;
    return grid;
}

ConvexPolytopes two_small_fishes() {
    Eigen::Vector3d o1 = {0., 0., 0.};
    Eigen::Vector3d a = {1., 0., 0.};
    Eigen::Vector3d b = {0.5, sqrt(1. - 0.25), 0.};
    Eigen::Vector3d c = {0., 0., 1.};

    Eigen::Vector3d o2 = o1 + 4 * a;

    return polytopes_from_wedges({o1,
                                  o1 + c,
                                  o1 + a,
                                  o1 + a + c,
                                  o1 + b,
                                  o1 + b + c,
                                  o1 + a + b,
                                  o1 + a + b + c,
                                  o1 - a + 2 * b,
                                  o1 - a + 2 * b + c,
                                  o1 + 2 * b,
                                  o1 + 2 * b + c,
                                  o2,
                                  o2 + c,
                                  o2 + a,
                                  o2 + a + c,
                                  o2 + b,
                                  o2 + b + c,
                                  o2 + a + b,
                                  o2 + a + b + c,
                                  o2 - a + 2 * b,
                                  o2 - a + 2 * b + c,
                                  o2 + 2 * b,
                                  o2 + 2 * b + c},
                                 {
                                   {0, 2, 4, 1, 3, 5},
                                   {2, 6, 4, 3, 7, 5},
                                   {6, 10, 4, 7, 11, 5},
                                   {4, 10, 8, 5, 11, 9},

                                   {12, 14, 16, 13, 15, 17},
                                   {14, 18, 16, 15, 19, 17},
                                   {18, 22, 16, 19, 23, 17},
                                   {16, 22, 20, 17, 23, 21},
                                 });
    // note, this grid is not convex!
}

TEST_CASE("convex_polytopes is_grid") {
    auto grid = unit_box();
    assert_is_grid(grid);
}

TEST_CASE("convex_polytopes default_iterator_is_invalid") {
    auto it = rayli::vgrid::ConvexPolytopes::RayIterator{};
    REQUIRE(it.is_valid() == false);
}

TEST_CASE("convex_polytopes inside_polytope") {
    auto cp = unit_box();
    auto box = cp.get_polytope(0);
    REQUIRE(box.includes({.5, .5, .5}));
    REQUIRE(box.includes({0, 0, 0}));
    REQUIRE(box.includes({0, 1, 0}));
    REQUIRE(!box.includes({1.5, .5, .5}));
    REQUIRE(!box.includes({-.5, .5, .5}));
    REQUIRE(!box.includes({.5, 10, .5}));
    REQUIRE(!box.includes({.5, -10, .5}));
    REQUIRE(!box.includes({.5, .2, 1.5}));
}

TEST_CASE("convex_polytopes intersect_from_outside") {
    auto cp = unit_box();
    auto box = cp.get_polytope(0);
    auto r = Ray{{0, 0, -1}, {0, 0, 1}};
    auto intersection = box.intersect(r);
    REQUIRE(intersection.t1 == Approx(1.));
    REQUIRE(intersection.t2 == Approx(2.));
}

TEST_CASE("convex_polytopes pass_by_box") {
    auto cp = unit_box();
    auto box = cp.get_polytope(0);
    auto r = Ray{{10, 0, -1}, {0, 0, 1}};
    auto intersection = box.intersect(r);
    REQUIRE(intersection.t1 == -std::numeric_limits<double>::infinity());
    REQUIRE(intersection.t2 == std::numeric_limits<double>::infinity());
}

TEST_CASE("convex_polytopes leave_box") {
    auto cp = unit_box();
    auto box = cp.get_polytope(0);
    auto r = Ray{{0, 0.5, 0}, {0, 1, 0}};
    auto intersection = box.intersect(r);
    REQUIRE(intersection.t1 == Approx(-.5));
    REQUIRE(intersection.t2 == Approx(.5));
}

TEST_CASE("convex_polytopes intersect_tilted_from_outside") {
    auto cp = tilted_box();
    auto box = cp.get_polytope(0);
    auto r = Ray{{0.5, 0.5, -1}, {0, 0, 1}};
    auto intersection = box.intersect(r);
    REQUIRE(intersection.t1 == 1.);
    REQUIRE(intersection.t2 == 1.5);
}

TEST_CASE("convex_polytopes intersect_wedge_horizontal") {
    auto cp = small_fish();
    auto wedge = cp.get_polytope(0);
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto intersection = wedge.intersect(r);
    REQUIRE(intersection.t1 == Approx(0.25));
    REQUIRE(intersection.t2 == Approx(0.75));
}

TEST_CASE("convex_polytopes find_first_polytope") {
    auto cp = small_fish();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto res = cp.find_first_polytope(r);
    REQUIRE(res.polytope == 0);
    REQUIRE(res.intersection.plane1.v() == 3);
    REQUIRE(res.intersection.plane2.v() == 2);
    REQUIRE(res.intersection.t1 == Approx(0.25));
    REQUIRE(res.intersection.t2 == Approx(0.75));
}

TEST_CASE("convex_polytopes iterate_fish_horizontal") {
    auto cp = small_fish();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    REQUIRE_THAT(
      path, ElementsAre<LinearSlice>(approx_surf_slice_variant(0.25, 3),
                                     approx_vol_slice_variant(0.25, 0.75, 0),
                                     approx_surf_slice_variant(0.75, 2),
                                     approx_vol_slice_variant(0.75, 1.25, 1),
                                     approx_surf_slice_variant(1.25, 6)));
}

TEST_CASE("convex_polytopes restart_from_face") {
    auto cp = small_fish();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    auto it = path.begin();
    auto path2 = cp.walk_along(r, 0.25, 100000, it);
    REQUIRE_THAT(
      path2, ElementsAre<LinearSlice>(approx_vol_slice_variant(0.25, 0.75, 0),
                                      approx_surf_slice_variant(0.75, 2),
                                      approx_vol_slice_variant(0.75, 1.25, 1),
                                      approx_surf_slice_variant(1.25, 6)));
}

TEST_CASE("convex_polytopes restart_from_volume") {
    auto cp = small_fish();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    auto it = path.begin();
    ++it;
    auto path2 = cp.walk_along(r, 0.25, 100000, it);
    REQUIRE_THAT(
      path2, ElementsAre<LinearSlice>(approx_vol_slice_variant(0.25, 0.75, 0),
                                      approx_surf_slice_variant(0.75, 2),
                                      approx_vol_slice_variant(0.75, 1.25, 1),
                                      approx_surf_slice_variant(1.25, 6)));
}

TEST_CASE("convex_polytopes iterate_fish_horizontal_with_accel") {
    auto cp = small_fish();
    cp.build_accelerators();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    REQUIRE_THAT(
      path, ElementsAre<LinearSlice>(approx_surf_slice_variant(0.25, 3),
                                     approx_vol_slice_variant(0.25, 0.75, 0),
                                     approx_surf_slice_variant(0.75, 2),
                                     approx_vol_slice_variant(0.75, 1.25, 1),
                                     approx_surf_slice_variant(1.25, 6)));
}

TEST_CASE("convex_polytopes leave_fish_horizontal") {
    auto cp = small_fish();
    auto r = Ray{{1, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(approx_vol_slice_variant(0, 0.25, 1),
                                          approx_surf_slice_variant(0.25, 6)));
}

TEST_CASE("convex_polytopes surface_geometry") {
    auto cp = small_fish();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    auto it = path.begin();
    auto surf = std::get<SurfaceSlice>(*it);
    auto geometry = surf.compute_geometry(r);
    REQUIRE(geometry.n(0) == Approx(0.8660254037844386));
    REQUIRE(geometry.n(1) == Approx(-0.5));
    REQUIRE(geometry.n(2) == Approx(0));
}

TEST_CASE("convex_polytopes iterate_two_fishes_horizontal") {
    auto cp = two_small_fishes();
    auto r = Ray{{0, 0.5 * sqrt(0.75), 0.5}, {1, 0, 0}};
    auto path = cp.walk_along(r, 0, 100000);
    REQUIRE_THAT(
      path, ElementsAre<LinearSlice>(approx_surf_slice_variant(0.25, 3),
                                     approx_vol_slice_variant(0.25, 0.75, 0),
                                     approx_surf_slice_variant(0.75, 2),
                                     approx_vol_slice_variant(0.75, 1.25, 1),
                                     approx_surf_slice_variant(1.25, 6),
                                     approx_surf_slice_variant(4.25, 20),
                                     approx_vol_slice_variant(4.25, 4.75, 4),
                                     approx_surf_slice_variant(4.75, 19),
                                     approx_vol_slice_variant(4.75, 5.25, 5),
                                     approx_surf_slice_variant(5.25, 23)));
}

TEST_CASE("convex_polytopes unit_box_bbox") {
    auto cp = unit_box();
    auto box = cp.get_polytope(0);
    auto bounds = box.get_bounds();
    REQUIRE(bounds.lower == Eigen::Vector3d(0, 0, 0));
    REQUIRE(bounds.upper == Eigen::Vector3d(1, 1, 1));
}

TEST_CASE("convex_polytopes tilted_box_bbox") {
    auto cp = tilted_box();
    auto box = cp.get_polytope(0);
    auto bounds = box.get_bounds();
    REQUIRE(bounds.lower == Eigen::Vector3d(-1, 0, 0));
    REQUIRE(bounds.upper == Eigen::Vector3d(1, 1, 1));
}

TEST_CASE("index_with_bool object_size") {
    REQUIRE(sizeof(unsigned int) == sizeof(IndexWithBool<unsigned int>));
    REQUIRE(sizeof(size_t) == sizeof(IndexWithBool<size_t>));
}

TEST_CASE("index_with_bool store_and_retrieve") {
    REQUIRE(3 == IndexWithBool(3, true).v());
    REQUIRE(3 == IndexWithBool(3, false).v());
    REQUIRE(true == IndexWithBool(3, true).b());
    REQUIRE(false == IndexWithBool(3, false).b());
}
