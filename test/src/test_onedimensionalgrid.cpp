#include <catch2/catch.hpp>
#include <stream_operators.hpp>
#include <test_helpers.hpp>

#include <vgrid/OneDimensional.hpp>

using namespace rayli::vgrid;

TEST_CASE("one_dimensional_grid iterate_vertical") {
    auto grid = OneDimensional{{1., 2.}};

    Ray ray{{0., 0., 3.}, {0, 0, -1}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{1., 2., 0})));
}

TEST_CASE("one_dimensional_grid iterate_horizontal_outside") {
    auto grid = OneDimensional{{1., 2.}};

    Ray ray{{0., 0., 3.}, {1, 0, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000), ElementsAre<LinearSlice>());
}

TEST_CASE("one_dimensional_grid leave_down") {
    auto grid = OneDimensional{{1., 2.}};

    Ray ray{{0., 0., 1.5}, {0, 0, -1}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{0., 0.5, 0})));
}

TEST_CASE("one_dimensional_grid leave_up") {
    auto grid = OneDimensional{{1., 2.}};

    Ray ray{{0., 0., 1.5}, {0, 0, 1}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{0., 0.5, 0})));
}

TEST_CASE("one_dimensional_grid leave_up_diag") {
    auto grid = OneDimensional{{1., 2.}};

    Ray ray{{0., 0., 1.5}, Eigen::Vector3d{0, 1, 1}.normalized()};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                   VolumeSlice{0., sqrt(2) * 0.5, 0})));
}
