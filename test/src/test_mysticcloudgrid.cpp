#include <catch2/catch.hpp>

#include "test_helpers.hpp"
#include <stream_operators.hpp>
#include <vgrid/MysticCloud.hpp>

using namespace rayli::vgrid;

TEST_CASE("mystic_cloud_grid is_grid") {
    MysticCloud grid(1., 1., 5, 2, {0., 1.});
    assert_is_grid(grid);
}

TEST_CASE("mystic_cloud_grid iterate_horizontal") {
    MysticCloud grid(1., 1., 5, 2, {0., 1.});

    Ray ray{{0, 0, 0.5}, {1, 0, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{0., 1., 0}),
                   variant_with<VolumeSlice>(VolumeSlice{1., 2., 2}),
                   variant_with<VolumeSlice>(VolumeSlice{2., 3., 4}),
                   variant_with<VolumeSlice>(VolumeSlice{3., 4., 6}),
                   variant_with<VolumeSlice>(VolumeSlice{4., 5., 8})));
}

TEST_CASE("mystic_cloud_grid iterate_vertical") {
    MysticCloud grid(1., 1., 2, 2, {0., 1., 3., 6.});

    Ray ray{{0.5, 0.5, -0.5}, {0, 0, 1}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{0.5, 1.5, 0}),
                   variant_with<VolumeSlice>(VolumeSlice{1.5, 3.5, 1}),
                   variant_with<VolumeSlice>(VolumeSlice{3.5, 6.5, 2})));
}

TEST_CASE("mystic_cloud_grid iterate_vertical_shifted") {
    MysticCloud grid(1., 1., 2, 2, {1., 2., 4., 7.});

    Ray ray{{0.5, 0.5, 0.5}, {0, 0, 1}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{0.5, 1.5, 0}),
                   variant_with<VolumeSlice>(VolumeSlice{1.5, 3.5, 1}),
                   variant_with<VolumeSlice>(VolumeSlice{3.5, 6.5, 2})));
}

TEST_CASE("mystic_cloud_grid iterate_tilted") {
    MysticCloud grid(2., 2., 2, 1, {0., 1., 2., 3.});

    Ray ray{{1., 1., 0.}, Eigen::Vector3d{1., 0, 1.5}.normalized()};
    double a = sqrt(1. + 4. / 9.);
    double b = sqrt(1.5 * 1.5 + 1.);
    double d = 2 * b;
    double c = d - a;

    std::vector<VolumeSlice> slices;
    for (auto slice : grid.walk_along(ray, 0, 100000)) {
        slices.push_back(std::get<VolumeSlice>(slice));
    }

    REQUIRE_THAT(slices,
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(approx_vol_slice(0., a, 0)),
                   variant_with<VolumeSlice>(approx_vol_slice(a, b, 1)),
                   variant_with<VolumeSlice>(approx_vol_slice(b, c, 4)),
                   variant_with<VolumeSlice>(approx_vol_slice(c, d, 5))));
}
