#include <catch2/catch.hpp>

#include "Eigen.hpp"
#include "test_helpers.hpp"
#include "vgrid/BallVGrid.hpp"
#include "vgrid/Cyclic.hpp"

TEST_CASE("cyclicgrid is_grid") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(basegrid, {{-2, -2, -2}, {2, 2, 2}});
    assert_is_grid(grid);
}

TEST_CASE("cyclicgrid iterate_through_one_center") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(basegrid, {{-2, -2, -2}, {2, 2, 2}});

    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    std::vector<LinearSliceTeleport> path_begin;
    std::copy_n(path.begin(), 6, std::back_inserter(path_begin));

    REQUIRE_THAT(path_begin,
                 ElementsAre<LinearSliceTeleport>(
                   approx_teleport_variant(0., Ray{{2, 0, 0}, {-1, 0, 0}}),
                   approx_vol_slice_variant(1., 3., 0),
                   approx_teleport_variant(4., Ray{{2, 0, 0}, {-1, 0, 0}}),
                   approx_vol_slice_variant(1., 3., 0),
                   approx_teleport_variant(4., Ray{{2, 0, 0}, {-1, 0, 0}}),
                   approx_vol_slice_variant(1., 3., 0)));
}

TEST_CASE("cyclicgrid iterate_diagonal") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(basegrid, {{-2, -2, -2}, {2, 2, 2}});

    Ray ray{{10, -10, -10}, Eigen::Vector3d{-1, 1, 1}.normalized()};
    auto path = grid.walk_along(ray, 0, 100000);

    std::vector<LinearSliceTeleport> path_begin;
    std::copy_n(path.begin(), 10, std::back_inserter(path_begin));

    double cd = 4 * std::sqrt(3.);
    double c1 = 2 * std::sqrt(3.);
    REQUIRE_THAT(path_begin,
                 ElementsAre<LinearSliceTeleport>(
                   approx_teleport_variant(0., Ray{{2, -2, -2}, ray.d}),
                   approx_vol_slice_variant(c1 - 1., c1 + 1., 0),
                   approx_teleport_variant(cd, Ray{{2, -2, -2}, ray.d}, 10),
                   approx_teleport_variant(0, Ray{{2, -2, -2}, ray.d}, 10),
                   approx_teleport_variant(0, Ray{{2, -2, -2}, ray.d}),
                   approx_vol_slice_variant(c1 - 1., c1 + 1., 0),
                   approx_teleport_variant(cd, Ray{{2, -2, -2}, ray.d}, 10),
                   approx_teleport_variant(0, Ray{{2, -2, -2}, ray.d}, 10),
                   approx_teleport_variant(0, Ray{{2, -2, -2}, ray.d}),
                   approx_vol_slice_variant(c1 - 1., c1 + 1., 0)));
}

TEST_CASE("cyclicgrid terminate_if_grid_can_not_be_reached") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{0, 0, 10}, {0, 0, 1}};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>());
}

TEST_CASE("cyclicgrid terminate_if_grid_can_not_be_reached_parallel") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{0, 0, 10}, {1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>());
}

TEST_CASE("cyclicgrid terminate_if_grid_can_not_be_reached_tilted") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{2, 0, 0}, Eigen::Vector3d{0.1, 0, 1.}.normalized()};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>());
}

TEST_CASE("cyclicgrid terminate_if_in_bounds") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{2, 2, 0}, Eigen::Vector3d{0, 0, 1.}.normalized()};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>());
}

TEST_CASE("cyclicgrid terminate_if_in_bounds_diagonal") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    double c1 = 2 * std::sqrt(2.);

    SECTION("checking the ray terminates after teleport in x-axis") {
        Ray ray{{0, 0, 0}, Eigen::Vector3d{1, 0, 1.}.normalized()};
        auto path = grid.walk_along(ray, 0, 100000);
        REQUIRE_THAT(path,
                     ElementsAre<LinearSliceTeleport>(
                       approx_teleport_variant(0., Ray{{0, 0, 0}, ray.d}),
                       approx_vol_slice_variant(0, 1, 0),
                       approx_teleport_variant(c1, Ray{{-2, 0, 2}, ray.d})));
    }

    SECTION("checking the ray terminates after teleport in y-axis") {
        Ray ray{{0, 0, 0}, Eigen::Vector3d{0, 1, 1.}.normalized()};
        auto path = grid.walk_along(ray, 0, 100000);
        REQUIRE_THAT(path,
                     ElementsAre<LinearSliceTeleport>(
                       approx_teleport_variant(0., Ray{{0, 0, 0}, ray.d}),
                       approx_vol_slice_variant(0, 1, 0),
                       approx_teleport_variant(c1, Ray{{0, -2, 2}, ray.d})));
    }
}

TEST_CASE("cyclicgrid terminate_if_ray_sneaks_between_volumes") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{2, 2, 0}, Eigen::Vector3d{1, 0, 0.}.normalized()};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>());
}

TEST_CASE("cyclicgrid shift_to_negative_indices") {
    auto basegrid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    auto grid = rayli::vgrid::Cyclic(
      basegrid, {{-2, -2, -std::numeric_limits<double>::infinity()},
                 {2, 2, std::numeric_limits<double>::infinity()}});

    Ray ray{{-3.5, 0, 0}, {0, 0, 1}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path, ElementsAre<LinearSliceTeleport>(
                         approx_teleport_variant(0., Ray{{.5, 0, 0}, ray.d}),
                         approx_vol_slice_variant(0., sqrt(0.75), 0)));
}
