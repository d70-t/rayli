#include <catch2/catch.hpp>

#include <grid/regular_grid.hpp>

using namespace rayli::grid;

TEST_CASE("RegularGrid views") {
    RegularGrid<3, double> grid;
    grid.resize(2, 3, 4);
    grid(0, 1, 0) = 3.0;

    SECTION("simple view") {
        RegularGrid view(grid);
        REQUIRE(view(0, 1, 0) == 3.0);
    }

    SECTION("transposed view") {
        auto view = grid.transpose(1, 0, 2);
        REQUIRE(view(1, 0, 0) == 3.0);
        REQUIRE(view(0, 0, 1) == 0.0);
    }

    SECTION("sliced view") {
        auto view = grid.slice(1, 1);
        REQUIRE(view(0, 0) == 3.0);
        REQUIRE(view(0, 1) == 0.0);
    }
}

TEST_CASE("RegularGrid iterator") {
    RegularGrid<2, double> grid;
    grid.resize(2, 3);
    grid(0, 1) = 4.0;
    std::vector<double> v(grid.begin(), grid.end());
    REQUIRE(v[1] == 4.0);
}
