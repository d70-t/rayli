#include <catch2/catch.hpp>

#include "ringbuffer.hpp"

TEST_CASE("ringbuffer initially_empty") {
    Ringbuffer<int, 5> buf;
    REQUIRE(buf.empty() == true);
    REQUIRE(buf.full() == false);
}

TEST_CASE("ringbuffer full_after_push") {
    Ringbuffer<int, 3> buf;
    buf.push(3);
    REQUIRE(buf.empty() == false);
    REQUIRE(buf.full() == false);
    buf.push(2);
    buf.push(7);
    REQUIRE(buf.empty() == false);
    REQUIRE(buf.full() == true);
}

TEST_CASE("ringbuffer push_and_pop") {
    Ringbuffer<int, 3> buf;
    buf.push(4);
    buf.push(2);
    REQUIRE(buf.front() == 4);
    buf.pop();
    buf.push(7);
    REQUIRE(buf.full() == false);
    REQUIRE(buf.front() == 2);
    buf.pop();
    buf.push(1);
    buf.push(9);
    REQUIRE(buf.full() == true);
    REQUIRE(buf.front() == 7);
    buf.pop();
    REQUIRE(buf.front() == 1);
    buf.pop();
    REQUIRE(buf.front() == 9);
    buf.pop();
    REQUIRE(buf.empty() == true);
}
