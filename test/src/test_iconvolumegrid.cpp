#include "Eigen.hpp"
#include "vgrid/ICON.hpp"
#include <catch2/catch.hpp>

template <typename Slices>
void print_slices(const Slices& slices) {
    for (auto& slice : slices) {
        std::visit(overloaded{[](const SurfaceSlice& s) {
                                  std::cout << "surf " << s.idx << " " << s.t
                                            << "\n";
                              },
                              [](const VolumeSlice& s) {
                                  std::cout << "vol " << s.idx << " " << s.tnear
                                            << " -> " << s.tfar << "\n";
                              }},
                   slice);
    }
}

TEST_CASE("icon_volume_grid iterate_vertical") {
    // spdlog::set_level(spdlog::level::trace);
    std::vector<Eigen::Vector3d> vertices = {
      {1, 0, -0.1}, {1, 1, 0}, {1, -0.1, 1}};
    std::vector<std::array<size_t, 3>> triangles{{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(), 1,
      2);

    rayli::vgrid::ICON<> iconvolume(std::move(grid));

    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    auto path = iconvolume.walk_along(ray, 0, 100000);
    auto slices = std::vector<LinearSlice>(path.begin(), path.end());
    REQUIRE(slices.size() == 3);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 8.);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 1);
    REQUIRE(std::get<VolumeSlice>(slices[1]) == (VolumeSlice{8., 9., 0}));
    REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 9.);
    REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 0);
}

TEST_CASE("icon_volume_grid iterate_vertical_outside") {
    // spdlog::set_level(spdlog::level::trace);
    std::vector<Eigen::Vector3d> vertices = {
      {-1, 0, -0.1}, {-1, -1, 0}, {-1, -0.1, 1}};
    std::vector<std::array<size_t, 3>> triangles{{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(), 1,
      3);
    auto vertgrid = std::make_shared<rayli::vgrid::UniformVerticalGrid>(
      std::vector<double>{1, 2, 3});

    rayli::vgrid::ICON<> iconvolume(std::move(grid), vertgrid);

    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    auto path = iconvolume.walk_along(ray, 0, 100000);
    auto slices = std::vector<LinearSlice>(path.begin(), path.end());
    REQUIRE(slices.size() == 5);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 7.);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 5);
    REQUIRE(std::get<VolumeSlice>(slices[1]) == (VolumeSlice{7., 8., 3}));
    REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 8.);
    REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 4);
    REQUIRE(std::get<VolumeSlice>(slices[3]) == (VolumeSlice{8., 9., 2}));
    REQUIRE(std::get<SurfaceSlice>(slices[4]).t == 9.);
    REQUIRE(std::get<SurfaceSlice>(slices[4]).idx == 3);
}

TEST_CASE("icon_volume_grid iterate_leaving_domain") {
    // spdlog::set_level(spdlog::level::trace);
    std::vector<Eigen::Vector3d> vertices = {
      {1, -0.1, 0}, {1, 0.1, 0}, {1, 0, 1}};
    std::vector<std::array<size_t, 3>> triangles{{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(),
      10001, 10003);
    auto vertgrid = std::make_shared<rayli::vgrid::UniformVerticalGrid>(
      std::vector<double>{10001, 10002, 10003});

    rayli::vgrid::ICON<> iconvolume(std::move(grid), vertgrid);

    Ray ray{{10003, 0, 0.5}, Eigen::Vector3d{-1, 0, -1}.normalized()};
    auto path = iconvolume.walk_along(ray, 0, 100000);
    auto slices = std::vector<LinearSlice>(path.begin(), path.end());
    REQUIRE(slices.size() == 7);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 2);
    REQUIRE(std::get<VolumeSlice>(slices[1]).idx == 1);
    REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 9);
    REQUIRE(std::get<VolumeSlice>(slices[3]).idx == 3);
    REQUIRE(std::get<SurfaceSlice>(slices[4]).idx == 4);
    REQUIRE(std::get<VolumeSlice>(slices[5]).idx == 2);
    REQUIRE(std::get<SurfaceSlice>(slices[6]).idx == 3);
}

TEST_CASE("icon_volume_grid iterate_entering_domain") {
    // spdlog::set_level(spdlog::level::trace);
    std::vector<Eigen::Vector3d> vertices = {
      {1, -0.1, 0}, {1, 0.1, 0}, {1, 0, 1}};
    std::vector<std::array<size_t, 3>> triangles{{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(),
      10001, 10003);
    auto vertgrid = std::make_shared<rayli::vgrid::UniformVerticalGrid>(
      std::vector<double>{10001, 10002, 10003});

    rayli::vgrid::ICON<> iconvolume(std::move(grid), vertgrid);

    Ray ray{{10003, 0, -0.5}, Eigen::Vector3d{-1, 0, 1}.normalized()};
    auto path = iconvolume.walk_along(ray, 0, 100000);
    auto slices = std::vector<LinearSlice>(path.begin(), path.end());
    REQUIRE(slices.size() == 7);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 5);
    REQUIRE(std::get<VolumeSlice>(slices[1]).idx == 3);
    REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 9);
    REQUIRE(std::get<VolumeSlice>(slices[3]).idx == 1);
    REQUIRE(std::get<SurfaceSlice>(slices[4]).idx == 1);
    REQUIRE(std::get<VolumeSlice>(slices[5]).idx == 0);
    REQUIRE(std::get<SurfaceSlice>(slices[6]).idx == 0);
}

TEST_CASE("icon_volume_grid hit_vertical_surface") {
    // spdlog::set_level(spdlog::level::trace);
    std::vector<Eigen::Vector3d> vertices = {{0, 1, 1}, {-1, 0, 1}, {0, -1, 1}};
    std::vector<std::array<size_t, 3>> triangles{{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(), 1,
      2);

    rayli::vgrid::ICON<> iconvolume(std::move(grid));

    Ray ray{{10, 0, 1.5}, {-1, 0, 0}};
    auto path = iconvolume.walk_along(ray, 0, 100000);
    auto slices = std::vector<LinearSlice>(path.begin(), path.end());
    REQUIRE(slices.size() == 5);
    REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 3);
    REQUIRE(std::get<VolumeSlice>(slices[1]).idx == 1);
    REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 10.);
    /* surfacees:
     * 0, 1: grid bottom and top
     * 2, 3: out of grid layers
     * 4, 5, 6: vertical faces
     */
    REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 6);
    auto geometry = std::get<SurfaceSlice>(slices[2]).compute_geometry(ray);
    REQUIRE(geometry.uv.isApprox(Eigen::Vector2d{0.5, 0.5}));
    REQUIRE(geometry.dpdu == Eigen::Vector3d::UnitZ());
    REQUIRE(geometry.dpdv.isApprox(Eigen::Vector3d::UnitY() * M_PI / 2. * 1.5));
}
