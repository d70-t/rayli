#include <catch2/catch.hpp>

#include "io/hu_stamnes.hpp"
#include "microphysics.hpp"
#include "optprop/hu_stamnes.hpp"

using namespace rayli::io::hu_stamnes;
using namespace rayli::optprop::hu_stamnes;

TEST_CASE("hu_stamnes load_table") {
    auto table = load_table("../data/wc/");
    REQUIRE(table.wavelength.size() > 0u);
    CHECK(table.wavelength[0] == 0.290);
    CHECK(table.reff_ranges[0].reff_min == 2.5);
    CHECK(table.reff_ranges[2].reff_max == 60);
    CHECK(table.reff_ranges[1].data[4].ext.a == 1.64);
}

TEST_CASE("hu_stamnes convert_lwc_redd") {
    auto table = load_table("../data/wc/");
    auto table544 = table.interpolate_to_wavelength(.544);

    rayli::microphysics::LWC_reff mp = {1., 10.};
    auto op = get_optprop(table544, mp);
    REQUIRE(op.ext == Approx(0.154899).margin(0.01));
    REQUIRE(op.ssa == Approx(1 - 3.640694e-7).margin(1e-10));
    REQUIRE(op.g == Approx(0.86452).margin(0.0001));
}
