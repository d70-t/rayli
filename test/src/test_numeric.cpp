#include <catch2/catch.hpp>

#include "numeric.hpp"

TEST_CASE("quadratic find_solution") {
    double r1, r2;
    REQUIRE(solve_quadratic(1., 0., -1., &r1, &r2));
    REQUIRE(r1 == -1.);
    REQUIRE(r2 == 1.);
}

TEST_CASE("quadratic detect_no_solution") {
    double r1, r2;
    REQUIRE(!solve_quadratic(1., 0., 1., &r1, &r2));
}
