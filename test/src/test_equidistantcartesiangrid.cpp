#include <catch2/catch.hpp>
#include <test_helpers.hpp>

#include <vgrid/EquidistantCartesian.hpp>

using namespace rayli::vgrid;

TEST_CASE("equidistant_cartesian_grid is_grid") {
    EquidistantCartesian grid{};
    assert_is_grid(grid);
}

TEST_CASE("equidistant_cartesian_grid iterate_horizontal") {
    EquidistantCartesian grid{};

    Ray ray{{-1, 0, 0.5}, {1, 0, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{1., 2., 0})));
}

TEST_CASE("equidistant_cartesian_grid iterate_horizontal_two_boxes") {
    EquidistantCartesian grid{{2, 1, 1}, {3, 1, 1}};

    Ray ray{{-1, 0, 0.5}, {1, 0, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{1., 4., 0}),
                   variant_with<VolumeSlice>(VolumeSlice{4., 7., 1})));
}

TEST_CASE("equidistant_cartesian_grid iterate_horizontal_reverse_y") {
    EquidistantCartesian grid{{2, 2, 2}};

    Ray ray{{0.5, 3, 0.5}, {0, -1, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 100000),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{1., 2., 2}),
                   variant_with<VolumeSlice>(VolumeSlice{2., 3., 0})));
}

TEST_CASE("equidistant_cartesian_grid iterate_horizontal_short") {
    EquidistantCartesian grid{};

    Ray ray{{-1, 0, 0.5}, {1, 0, 0}};
    REQUIRE_THAT(grid.walk_along(ray, 0, 1.5),
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(VolumeSlice{1., 1.5, 0})));
}
