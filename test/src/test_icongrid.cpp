#include "grid/ICONGrid.hpp"
#include <catch2/catch.hpp>

TEST_CASE("ICON grid") {
    std::vector<Eigen::Vector3d> vertices = {{1, 0, 0}, {1, 1, 0}, {1, 0, 1}};
    std::vector<std::array<size_t, 3>> triangles = {{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(), 1,
      2);

    SECTION("finds single cell") {
        REQUIRE(grid->find_cell_idx({1, 0.2, 0.2}) == 0u);
        REQUIRE(grid->find_cell_idx({-1, -0.2, -0.2}) == 1u);
    }

    SECTION("cell API") {
        REQUIRE(grid->get_cell(0).is_valid());
        REQUIRE(!grid->get_cell(1).is_valid());
    }

    SECTION("intersect edge") {
        auto edge = grid->get_edge(0);
        double t;
        Ray ray{{1.2, 0.2, 3}, {0, 0, -1}};
        REQUIRE(edge.intersect(ray, 0, 1000, t));
        REQUIRE(t == 3.0);
    }
}

TEST_CASE("ICON grid bounds") {
    std::vector<Eigen::Vector3d> vertices = {{0, 1, 1}, {-1, 0, 1}, {0, -1, 1}};
    std::vector<std::array<size_t, 3>> triangles = {{0, 1, 2}};
    auto grid = rayli::grid::create_icon_grid_from_vertices_and_triangles(
      vertices.begin(), vertices.end(), triangles.begin(), triangles.end(), 1,
      2);
    auto edge = grid->get_edge(2);
    auto bounds = edge.get_bounds();
    REQUIRE(bounds.upper == (Eigen::Vector3d{0, 2. / sqrt(2.), 2}));
    REQUIRE(bounds.lower ==
            (Eigen::Vector3d{0, -2. / sqrt(2.), 1. / sqrt(2.)}));
}
