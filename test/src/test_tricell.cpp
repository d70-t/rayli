#include <catch2/catch.hpp>
#include <vgrid/TriCell.hpp>

template <typename Slices>
void print_slices(const Slices& slices) {
    for (auto& slice : slices) {
        std::visit(overloaded{[](const SurfaceSlice& s) {
                                  std::cout << "surf " << s.idx << " " << s.t
                                            << "\n";
                              },
                              [](const VolumeSlice& s) {
                                  std::cout << "vol " << s.idx << " " << s.tnear
                                            << " -> " << s.tfar << "\n";
                              }},
                   slice);
    }
}

TEST_CASE("tricell") {
    SECTION("single cell") {
        auto mesh = std::make_shared<rayli::surface::TriangularMesh>();
        mesh->vertices = {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        mesh->faces = {{{0, 1, 2}}, {{1, 2, 3}}, {{0, 1, 3}}, {{0, 2, 3}}};
        mesh->repair();
        mesh->build_accelerators();

        auto cells_of_faces =
          std::make_shared<std::vector<std::array<size_t, 2>>>();
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});

        rayli::vgrid::TriCell vgrid(mesh, cells_of_faces);

        SECTION("interate vertical") {
            Ray ray{{0.25, 0.25, 10}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 100000);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 3);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 9.5);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 1);
            REQUIRE(std::get<VolumeSlice>(slices[1]) ==
                    (VolumeSlice{9.5, 10., 0}));
            REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 10.);
            REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 0);
        }

        SECTION("iterate leaving") {
            Ray ray{{0.25, 0.25, 0.25}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 100000);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 2);
            REQUIRE(std::get<VolumeSlice>(slices[0]) ==
                    (VolumeSlice{0, 0.25, 0}));
            REQUIRE(std::get<SurfaceSlice>(slices[1]).t == 0.25);
            REQUIRE(std::get<SurfaceSlice>(slices[1]).idx == 0);
        }

        SECTION("iterate inside") {
            Ray ray{{0.25, 0.25, 0.25}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 0.1);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 1);
            REQUIRE(std::get<VolumeSlice>(slices[0]) ==
                    (VolumeSlice{0, 0.1, 0}));
        }

        SECTION("iterate entering") {
            Ray ray{{0.25, 0.25, 10}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 9.75);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 2);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 9.5);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 1);
            REQUIRE(std::get<VolumeSlice>(slices[1]) ==
                    (VolumeSlice{9.5, 9.75, 0}));
        }
    }

    SECTION("two connected cells") {
        auto mesh = std::make_shared<rayli::surface::TriangularMesh>();
        mesh->vertices = {
          {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, -1}};
        mesh->faces = {{{0, 1, 2}}, {{1, 2, 3}}, {{0, 1, 3}}, {{0, 2, 3}},
                       {{1, 2, 4}}, {{0, 1, 4}}, {{0, 2, 4}}};
        mesh->repair();
        mesh->build_accelerators();

        auto cells_of_faces =
          std::make_shared<std::vector<std::array<size_t, 2>>>();
        cells_of_faces->push_back({0, 1});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});

        rayli::vgrid::TriCell vgrid(mesh, cells_of_faces);

        SECTION("iterate vertical") {
            Ray ray{{0.25, 0.25, 10}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 100000);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 5);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 9.5);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 1);
            REQUIRE(std::get<VolumeSlice>(slices[1]) ==
                    (VolumeSlice{9.5, 10., 0}));
            REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 10.);
            REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 0);
            REQUIRE(std::get<VolumeSlice>(slices[3]) ==
                    (VolumeSlice{10., 10.5, 1}));
            REQUIRE(std::get<SurfaceSlice>(slices[4]).t == 10.5);
            REQUIRE(std::get<SurfaceSlice>(slices[4]).idx == 4);
        }
    }

    SECTION("two independent cells") {
        auto mesh = std::make_shared<rayli::surface::TriangularMesh>();
        mesh->vertices = {{0, 0, 0},  {1, 0, 0},  {0, 1, 0},  {0, 0, 1},
                          {0, 0, -2}, {1, 0, -2}, {0, 1, -2}, {0, 0, -1}};
        mesh->faces = {{{0, 1, 2}}, {{1, 2, 3}}, {{0, 1, 3}}, {{0, 2, 3}},
                       {{4, 5, 6}}, {{5, 6, 7}}, {{4, 5, 7}}, {{4, 6, 7}}};
        mesh->repair();
        mesh->build_accelerators();

        auto cells_of_faces =
          std::make_shared<std::vector<std::array<size_t, 2>>>();
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({0, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});
        cells_of_faces->push_back({1, std::numeric_limits<size_t>::max()});

        rayli::vgrid::TriCell vgrid(mesh, cells_of_faces);

        SECTION("iterate") {
            Ray ray{{0.25, 0.25, 10}, {0, 0, -1}};
            auto path = vgrid.walk_along(ray, 0, 100000);
            auto slices = std::vector<LinearSlice>(path.begin(), path.end());
            REQUIRE(slices.size() == 6);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).t == 9.5);
            REQUIRE(std::get<SurfaceSlice>(slices[0]).idx == 1);
            REQUIRE(std::get<VolumeSlice>(slices[1]) ==
                    (VolumeSlice{9.5, 10., 0}));
            REQUIRE(std::get<SurfaceSlice>(slices[2]).t == 10.);
            REQUIRE(std::get<SurfaceSlice>(slices[2]).idx == 0);
            REQUIRE(std::get<SurfaceSlice>(slices[3]).t == 11.5);
            REQUIRE(std::get<SurfaceSlice>(slices[3]).idx == 5);
            REQUIRE(std::get<VolumeSlice>(slices[4]) ==
                    (VolumeSlice{11.5, 12., 1}));
            REQUIRE(std::get<SurfaceSlice>(slices[5]).t == 12);
            REQUIRE(std::get<SurfaceSlice>(slices[5]).idx == 4);
        }
    }
}
