#include <catch2/catch.hpp>
#include <texture/texture.hpp>

TEST_CASE("test_texture test_function_texture") {
    auto tex = function_texture<size_t>([](auto i) -> double { return i + 1; });

    CHECK(tex(1) == 2.);
}
