#include <catch2/catch.hpp>
#include <test_helpers.hpp>

#include "Eigen.hpp"
#include "test_helpers.hpp"
#include "vgrid/BallVGrid.hpp"

TEST_CASE("ballvgrid is_grid") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});
    assert_is_grid(grid);
}

TEST_CASE("ballvgrid iterate_through_one_center") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});

    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path, ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                         approx_vol_slice(9., 11., 0))));
}

TEST_CASE("ballvgrid iterate_through_one_center_with_offset") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});

    Ray ray{{110, 0, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 100, 100000);

    REQUIRE_THAT(path, ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                         approx_vol_slice(109., 111., 0))));
}

TEST_CASE("ballvgrid iterate_through_one_center_diagonal") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});

    Ray ray{{10, -10, -10}, Eigen::Vector3d{-1, 1, 1}.normalized()};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(
      path, ElementsAre<LinearSlice>(variant_with<VolumeSlice>(approx_vol_slice(
              10. * std::sqrt(3.) - 1., 10. * std::sqrt(3.) + 1., 0))));
}

TEST_CASE("ballvgrid center_and_out") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});

    Ray ray{{0, 0, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path, ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                         approx_vol_slice(0., 1., 0))));
}

TEST_CASE("ballvgrid off_center_and_out2") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{0, 0.5, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(
                     approx_vol_slice(0., sqrt(1. - 0.5 * 0.5), 0)),
                   variant_with<VolumeSlice>(approx_vol_slice(
                     sqrt(1. - 0.5 * 0.5), sqrt(4. * 4. - 0.5 * 0.5), 1))));
}

TEST_CASE("ballvgrid off_center_and_out3") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{1, 0.5, 0}, {1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                   approx_vol_slice(0., sqrt(4. * 4. - 0.5 * 0.5) - 1, 1))));
}

TEST_CASE("ballvgrid off_center_and_in_out") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{1, 0.5, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(
      path,
      ElementsAre<LinearSlice>(
        variant_with<VolumeSlice>(
          approx_vol_slice(0., 1. - sqrt(1. - 0.5 * 0.5), 1)),
        variant_with<VolumeSlice>(approx_vol_slice(
          1. - sqrt(1. - 0.5 * 0.5), 1. + sqrt(1. - 0.5 * 0.5), 0)),
        variant_with<VolumeSlice>(approx_vol_slice(
          1. + sqrt(1. - 0.5 * 0.5), 1. + sqrt(4. * 4. - 0.5 * 0.5), 1))));
}

TEST_CASE("ballvgrid iterate_through_two_center") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(
                   variant_with<VolumeSlice>(approx_vol_slice(6., 9., 1)),
                   variant_with<VolumeSlice>(approx_vol_slice(9., 11., 0)),
                   variant_with<VolumeSlice>(approx_vol_slice(11., 14., 1))));
}

TEST_CASE("ballvgrid iterate_through_one_of_two") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{10, 0, 2}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                   approx_vol_slice(10. - sqrt(12.), 10. + sqrt(12.), 1))));
}

TEST_CASE("ballvgrid iterate_through_one_of_two_shifted") {
    auto grid = rayli::vgrid::BallVGrid({3, 1, 2}, {1, 4});

    Ray ray{{13, 1, 4}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);

    REQUIRE_THAT(path,
                 ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                   approx_vol_slice(10. - sqrt(12.), 10. + sqrt(12.), 1))));
}

TEST_CASE("ballvgrid missing_grid") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1, 4});

    Ray ray{{10, 100, 0}, {-1, 0, 0}};
    auto path = grid.walk_along(ray, 0, 100000);
    REQUIRE(path.begin() == path.end());
    REQUIRE(!(path.begin() != path.end()));
}

TEST_CASE("ballvgrid inbound_from_inside") {
    auto grid = rayli::vgrid::BallVGrid({0, 0, 0}, {1});

    Ray ray{{1.5, 0, 0}, {-1, 0, 0}};
    auto path =
      grid.walk_along(ray, 1, std::numeric_limits<double>::infinity());

    REQUIRE_THAT(path, ElementsAre<LinearSlice>(variant_with<VolumeSlice>(
                         approx_vol_slice(1, 2.5, 0))));
}
