#include <catch2/catch.hpp>

#include <deduplicate.hpp>
#include <io/libradtran_files.hpp>
#include <microphysics.hpp>

#include <sstream>

using namespace rayli::io::libradtran;
using namespace rayli::microphysics;

TEST_CASE("libRadtran cloud file", "[io]") {
    std::stringstream infile(R"EOF(2 2 5 3
1.0 1.0 0 1 2 3 4 5
1 1 2 0.1 10
2 2 2 0.1 10
)EOF");

    auto cloud = read_cloud_3d(infile);

    SECTION("cloud properties are read correctly") {
        REQUIRE(cloud->Nx() == 2u);
        REQUIRE(cloud->Ny() == 2u);
        REQUIRE(cloud->Nz() == 5u);

        REQUIRE(cloud->dx() == 1000.0);
        REQUIRE(cloud->dy() == 1000.0);

        auto lwc_reff_cloud = dynamic_cast<Cloud3D<LWC_reff>*>(cloud.get());
        REQUIRE(lwc_reff_cloud != nullptr);
        REQUIRE(lwc_reff_cloud->data()(0, 0, 1).reff == 10);
        REQUIRE(lwc_reff_cloud->data()(1, 1, 1).reff == 10);
        REQUIRE(lwc_reff_cloud->data()(0, 1, 1).reff == 0);
        REQUIRE(lwc_reff_cloud->data()(0, 0, 0).reff == 0);
        REQUIRE(lwc_reff_cloud->data()(1, 1, 2).reff == 0);
    }

    SECTION("cloud can be deduplicated") {
        auto lwc_reff_cloud = dynamic_cast<Cloud3D<LWC_reff>*>(cloud.get());
        rayli::grid::RegularGrid<3, size_t> idx;
        idx.resize(lwc_reff_cloud->data().shape(0),
                   lwc_reff_cloud->data().shape(1),
                   lwc_reff_cloud->data().shape(2));
        std::vector<LWC_reff> dedup;

        deduplicate(lwc_reff_cloud->data().begin(),
                    lwc_reff_cloud->data().end(), idx.begin(),
                    std::back_inserter(dedup));

        REQUIRE_THAT(dedup,
                     Catch::Contains(std::vector<LWC_reff>{{0, 0}, {0.1, 10}}));

        REQUIRE(idx(0, 0, 1) == 1u);
        REQUIRE(idx(1, 1, 1) == 1u);
        REQUIRE(idx(0, 0, 0) == 0u);
    }
}
