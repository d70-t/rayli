#include <catch2/catch.hpp>

#include "GeometryUtils.hpp"

TEST_CASE("bounds bounds_of_multiple_points") {
    Bounds3d b(Eigen::Vector3d{0, 1, 3});
    b.extend_inplace(Eigen::Vector3d{2, -1, 4});
    b.extend_inplace(Eigen::Vector3d{-1, 0, 3.5});
    CHECK(b.lower == Eigen::Vector3d(-1, -1, 3));
    CHECK(b.upper == Eigen::Vector3d(2, 1, 4));
}

TEST_CASE("rotation rotation_from_cos") {
    for (double mu : {0., 0.5, 1., 0.7, 0.32}) {
        for (auto d : {Eigen::Vector3d{1., 0., 0.},
                       {0., 1., 0.},
                       {0., 0., 1.},
                       {3., 7., 11.},
                       {-1, 2., 0.}}) {
            d.normalize();
            Eigen::Matrix3d a = rotation_from_cos_axis(mu, d);
            Eigen::Matrix3d b =
              Eigen::AngleAxis(std::acos(mu), d).toRotationMatrix();

            /*
            std::cout << "mu: " << mu << " d: " << d.transpose() << "\n";
            std::cout << a << "\n";
            std::cout << b << "\n";
            std::cout << "\n";
            */
            REQUIRE(a.isApprox(b));
        }
    }
}
