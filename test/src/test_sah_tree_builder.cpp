#include <catch2/catch.hpp>

#include <vgrid/util/sahtreebuilder.hpp>

using namespace rayli::vgrid::util;

TEST_CASE("recursive_sah_tree_builder create_leaf_on_max_tree_depth") {
    // spdlog::set_level(spdlog::level::trace);
    RecursiveSAHTreeBuilder::Tree tree;
    std::vector<std::vector<size_t>> element_ids;
    std::vector<Bounds3d> element_bounds{{{0, 0, 0}, {1, 1, 1}},
                                         {{2, 2, 2}, {3, 3, 3}}};
    RecursiveSAHTreeBuilder builder(tree, element_bounds, element_ids);
    builder.build(0);
    CHECK(tree.nodes.size() == 1);
}

TEST_CASE(
  "recursive_sah_tree_builder create_leaf_target_element_count_reached") {
    // spdlog::set_level(spdlog::level::trace);
    RecursiveSAHTreeBuilder::Tree tree;
    std::vector<std::vector<size_t>> element_ids;
    std::vector<Bounds3d> element_bounds{{{0, 0, 0}, {1, 1, 1}},
                                         {{2, 2, 2}, {3, 3, 3}}};
    RecursiveSAHTreeBuilder builder(tree, element_bounds, element_ids);
    builder.build();
    CHECK(tree.nodes.size() == 1);
}

TEST_CASE("recursive_sah_tree_builder "
          "create_split_if_target_element_count_is_not_reached") {
    // spdlog::set_level(spdlog::level::trace);
    RecursiveSAHTreeBuilder::Tree tree;
    std::vector<std::vector<size_t>> element_ids;
    std::vector<Bounds3d> element_bounds{{{0, 0, 0}, {1, 1, 1}},
                                         {{2, 2, 2}, {3, 3, 3}}};
    RecursiveSAHTreeBuilder builder(tree, element_bounds, element_ids, 1);
    builder.build();
    CHECK(tree.nodes.size() == 3);
}
