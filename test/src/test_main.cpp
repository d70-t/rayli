#define CATCH_CONFIG_RUNNER
#include "logging.hpp"
#include <catch2/catch.hpp>

int main(int argc, char* argv[]) {
    spdlog::set_level(spdlog::level::off);
    int result = Catch::Session().run(argc, argv);
    return result;
}
