#include <catch2/catch.hpp>

#include "Ray.hpp"
#include "surface/TriangularMesh.hpp"

#include "logging.hpp"

using namespace rayli::surface;

TEST_CASE("triangular_mesh face_bounds") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    auto bounds = mesh.get_face(0).get_bounds();
    REQUIRE(bounds.lower == Eigen::Vector3d(-1, -1, 0));
    REQUIRE(bounds.upper == Eigen::Vector3d(1, 1, 0));
}

TEST_CASE("triangular_mesh face_area") {
    TriangularMesh mesh;
    mesh.vertices = {{0, 0, 0}, {0, 1, 0}, {1, 0, 0}};
    mesh.faces = {{{0, 1, 2}}};

    REQUIRE(mesh.get_face(0).area() == 0.5);
}

TEST_CASE("triangular_mesh face_intersection") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    Ray ray = {{0, 0, 1}, {0, 0, -1}};
    double t;
    REQUIRE(mesh.get_face(0).intersect(
      ray, 0, std::numeric_limits<double>::infinity(), t));
    REQUIRE(t == 1.0);
}

TEST_CASE("triangular_mesh face_no_intersection_if_going_away") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    Ray ray = {{0, 0, 1}, {0, 0, 1}};
    double t = std::numeric_limits<double>::infinity();
    REQUIRE(!mesh.get_face(0).intersect(
      ray, 0, std::numeric_limits<double>::infinity(), t));
    CHECK(std::isinf(t));
}

TEST_CASE("triangular_mesh face_no_intersection_if_starting_behind") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    Ray ray = {{0, 0, 1}, {0, 0, -1}};
    double t = std::numeric_limits<double>::infinity();
    CHECK(!mesh.get_face(0).intersect(
      ray, 2, std::numeric_limits<double>::infinity(), t));
    CHECK(std::isinf(t));
}

TEST_CASE("triangular_mesh face_intersection_is_inside_bounds") {
    TriangularMesh mesh;
    mesh.vertices = {{-151611.296875, 1065298.625, 89854.171875},
                     {-151611.296875, 1065255.125, 89838.734375},
                     {-151597.96875, 1065304., 89854.171875}};
    mesh.faces = {{{0, 1, 2}}};

    Ray ray = {{-153295.43309063, 1063638.912523308, 76945.36602832191},
               {0.1286833158620266, 0.1245258089911246, 0.983836331466703}};
    double t;
    REQUIRE(mesh.get_face(0).intersect(
      ray, 0, std::numeric_limits<double>::infinity(), t));
    CHECK(mesh.get_face(0).get_bounds().contains(ray.o + ray.d * t));
}

TEST_CASE("triangular_mesh face_intersection_below_tilted") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 2}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    Ray ray = {{0, 0, -1}, {0, 0, 1}};
    double t;
    REQUIRE(mesh.get_face(0).intersect(
      ray, 0, std::numeric_limits<double>::infinity(), t));
    REQUIRE(t == 2.0);
}

TEST_CASE("triangular_mesh find_intersecting_face") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {-2, 1, 0}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}};

    {
        Ray ray = {{0, 0, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 0u);
        REQUIRE(t == 1.0);
    }

    {
        Ray ray = {{-1, 0, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 1u);
        REQUIRE(t == 1.0);
    }
}

TEST_CASE("triangular_mesh find_no_intersecting_face_if_moving_away") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {-2, 1, 0}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}};

    {
        Ray ray = {{0, 0, 1}, {0, 0, 1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 2u);
        CHECK(std::isinf(t));
    }
}

TEST_CASE("triangular_mesh find_intersecting_face_with_accelerator") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {-2, 1, 0}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}};
    mesh.build_accelerators();

    {
        Ray ray = {{0, 0, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 0u);
        REQUIRE(t == 1.0);
    }

    {
        Ray ray = {{-1, 0, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 1u);
        REQUIRE(t == 1.0);
    }

    {
        Ray ray = {{1, 0, 2}, Eigen::Vector3d{-1, 0, -1}.normalized()};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 1u);
        REQUIRE(t == 2. * sqrt(2.));
    }

    {
        Ray ray = {{0.5, -0.5, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 0u);
        REQUIRE(t == 1.0);
    }

    {
        Ray ray = {{-0.5, -0.5, 2}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 0u);
        REQUIRE(t == 2.0);
    }

    {
        Ray ray = {{-1.5, 0.5, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 1u);
        REQUIRE(t == 1.0);
    }

    {
        Ray ray = {{-1.5, 1.5, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 2u);
    }
}

TEST_CASE("triangular_mesh find_face_in_flat_top_mesh") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    int side_count = 5;
    int top_count = 3;

    auto addxz = [&](double x, double z) {
        mesh.vertices.push_back({x, -10., z});
        mesh.vertices.push_back({x, 10., z});
    };
    for (int i = 0; i < side_count; ++i) {
        addxz(-top_count - side_count + i, -side_count + i);
    }
    for (int i = -top_count; i < top_count; ++i) {
        addxz(i, 0);
    }
    for (int i = 0; i <= side_count; ++i) {
        addxz(top_count + i, -i);
    }
    for (size_t i = 0; i < mesh.vertices.size() - 2; ++i) {
        mesh.faces.push_back({{i, i + 1, i + 2}});
    }

    // for(const auto& vertex: mesh.vertices) {
    //     std::cout << vertex.transpose() << std::endl;
    // }

    mesh.build_accelerators();

    {
        Ray ray = {{0, 0, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) <
                mesh.faces.size());
        REQUIRE(t == 1.0);
    }
}

TEST_CASE(
  "triangular_mesh find_no_intersecting_face_if_moving_away_with_accelerator") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {-2, 1, 0}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}};
    mesh.build_accelerators();

    {
        Ray ray = {{0, 0, 1}, {0, 0, 1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 2u);
        CHECK(std::isinf(t));
    }
}

TEST_CASE("triangular_mesh face_bounds_reversed_vertex_order") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 2, 1}}};

    auto bounds = mesh.get_face(0).get_bounds();
    REQUIRE(bounds.lower == Eigen::Vector3d(-1, -1, 0));
    REQUIRE(bounds.upper == Eigen::Vector3d(1, 1, 0));
}

TEST_CASE("triangular_mesh face_intersection_reversed_vertex_order") {
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 2, 1}}};

    Ray ray = {{0, 0, 1}, {0, 0, -1}};
    double t;
    REQUIRE(mesh.get_face(0).intersect(
      ray, 0, std::numeric_limits<double>::infinity(), t));
    REQUIRE(t == 1.0);
}

TEST_CASE("triangular_mesh find_intersecting_face_with_overlapping_triangle") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0},  {0, 1, 0}, {-1, -1, 0},
                     {-1, -1, 1}, {0, 1, 1}, {1, 1, 1.1}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}, {{3, 1, 4}}, {{3, 5, 4}}};
    mesh.build_accelerators();

    {
        Ray ray = {{0.5, -0.5, 1}, {0, 0, -1}};
        double t;
        REQUIRE(mesh.find_intersecting_face_idx(
                  ray, 0, std::numeric_limits<double>::infinity(), t) == 0u);
    }
}

TEST_CASE("triangular_mesh find_intersection_on_edge") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {-2, 1, 1}};
    mesh.faces = {{{0, 1, 2}}, {{2, 1, 3}}};

    {
        Ray ray = {{-0.5, 0, 1}, {0, 0, -1}};
        double t;
        size_t face_idx = mesh.find_intersecting_face_idx(
          ray, 0, std::numeric_limits<double>::infinity(), t);
        REQUIRE(((face_idx == 0) || (face_idx == 1)));
        REQUIRE(t == 1.0);
    }

    mesh.build_accelerators();

    {
        Ray ray = {{-0.5, 0, 1}, {0, 0, -1}};
        double t;
        size_t face_idx = mesh.find_intersecting_face_idx(
          ray, 0, std::numeric_limits<double>::infinity(), t);
        REQUIRE(((face_idx == 0) || (face_idx == 1)));
        REQUIRE(t == 1.0);
    }
}

TEST_CASE("triangular_mesh edges_of_face") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    mesh.repair();

    auto edge = mesh.get_face(0).edge(0);
    // vertices should be ordered by id
    REQUIRE(edge.v(0) == Eigen::Vector3d(1, -1, 0));
    REQUIRE(edge.v(1) == Eigen::Vector3d(0, 1, 0));
}

TEST_CASE("triangular_mesh edge_bounds") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}};
    mesh.faces = {{{0, 1, 2}}};

    mesh.repair();

    {
        auto bounds = mesh.get_face(0).edge(0).get_bounds();
        REQUIRE(bounds.lower == Eigen::Vector3d(0, -1, 0));
        REQUIRE(bounds.upper == Eigen::Vector3d(1, 1, 0));
    }

    {
        auto bounds = mesh.get_face(0).edge(1).get_bounds();
        REQUIRE(bounds.lower == Eigen::Vector3d(-1, -1, 0));
        REQUIRE(bounds.upper == Eigen::Vector3d(0, 1, 0));
    }

    {
        auto bounds = mesh.get_face(0).edge(2).get_bounds();
        REQUIRE(bounds.lower == Eigen::Vector3d(-1, -1, 0));
        REQUIRE(bounds.upper == Eigen::Vector3d(1, -1, 0));
    }
}

TEST_CASE("triangular_mesh get_neighbor_face") {
    // spdlog::set_level(spdlog::level::trace);
    TriangularMesh mesh;
    mesh.vertices = {{1, -1, 0}, {0, 1, 0}, {-1, -1, 0}, {1, 1, 0}};
    mesh.faces = {{{0, 1, 2}}, {{0, 3, 1}}};

    mesh.repair();

    REQUIRE(mesh.get_face(0).neighbor_face(0).id() == 1);
    REQUIRE(mesh.get_face(1).neighbor_face(2).id() == 0);
}
