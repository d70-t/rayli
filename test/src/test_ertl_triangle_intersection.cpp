#include <catch2/catch.hpp>

#include <Ray.hpp>
#include <intersection/ErtlTriangleIntersection.hpp>

#include "Eigen.hpp"
#include <array>

struct Triangle {
    inline const Eigen::Vector3d& v(int idx) const { return _v[idx]; }
    std::array<Eigen::Vector3d, 3> _v;
};

TEST_CASE("ertl_triangle_intersection hits_triangle") {
    Triangle tri{{Eigen::Vector3d{1, -1, 0}, Eigen::Vector3d{0, 1, 0},
                  Eigen::Vector3d{-1, -1, 0}}};
    Ray ray{{0, 0, 1}, {0, 0, -1}};
    double t = -1;
    REQUIRE(rayli::intersection::ertl_triangle(ray, tri, t));
    CHECK(t == 1.0);
}

TEST_CASE("ertl_triangle_intersection misses_triangle") {
    Triangle tri{{Eigen::Vector3d{1, -1, 0}, Eigen::Vector3d{0, 1, 0},
                  Eigen::Vector3d{-1, -1, 0}}};
    Ray ray{{10, 0, 1}, {0, 0, -1}};
    double t;
    CHECK(!rayli::intersection::ertl_triangle(ray, tri, t));
}

TEST_CASE("ertl_triangle_intersection no_intersection_when_parallel") {
    Triangle tri{{Eigen::Vector3d{1, -1, 0}, Eigen::Vector3d{0, 1, 0},
                  Eigen::Vector3d{-1, -1, 0}}};
    Ray ray{{10, 0, 0}, {-1, 0, 0}};
    double t;
    CHECK(!rayli::intersection::ertl_triangle(ray, tri, t));
}

TEST_CASE("ertl_triangle_intersection hits_triangle_on_edge") {
    Triangle tri{{Eigen::Vector3d{1, -1, 0}, Eigen::Vector3d{0, 1, 0},
                  Eigen::Vector3d{-1, -1, 0}}};
    Ray ray{{0.5, 0, 1}, {0, 0, -1}};
    double t = -1;
    REQUIRE(rayli::intersection::ertl_triangle(ray, tri, t));
    CHECK(t == 1.0);
}
